namespace App {
	export namespace Combat {
		export const enemyData: Record<string, Data.Combat.Enemy> = {};
		export const moves: Record<string, Data.Combat.Style> = {};
		export const encounterData: Record<string, Data.Combat.EncounterDesc> = {};
		export const clubData: Record<string, Data.Combat.ClubEncounter[]> = {};
		export const clubBetData: Record<string, string[]> = {};
	}

	export namespace Data {
		// namespace AvatarMaps {}
		export namespace Abamond {
			export const mobs: Record<string, Data.ModDesc[]> = {};
		}

		export const drugs: Record<string, DrugItemDesc> = {};
		export const food: Record<string, FoodItemDesc> = {};
		export const quests: Record<string, Tasks.Quest> = {};
		export const jobs: Record<string, Tasks.Job> = {};
		export const cosmetics: Record<string, CosmeticsItemDesc> = {};
		export const misc: Record<string, MiscConsumableItemDesc> = {};
		export const miscLoot: Record<string, MiscLootItemDesc> = {};
		export const clothes: Record<string, ClothingItemDesc | WigItemDesc> ={};
		export const stores: Record<string, StoreDesc> = {};
		export const npcs: Record<string, NPCDesc> = {};
		export const npcGroups: Record<string, string[]> = {};
		export const questItems: Record<string, QuestItemDesc> = {};
		export const lootBoxes: Record<string, LootBoxItemDesc> = {};
		export const slots: Record<string, ReelDesc> = {};

		export const effectLib: Record<string, EffectDesc> = {};
		export const effectLibClothingWear: Record<string, ClothingWearEffectDesc> = {};
		export const effectLibClothingActive: Record<string, ClothingActiveEffectDesc> = {};

		export const events: Record<string, EventDesc[]> = {};
		export const whoring: Record<string, Whoring.WhoringLocationDesc> = {};

		export const loot: Record<string, Data.LootItem[]> = {};
		export const lootTables: Record<string, LootTableItem[]> = {};

		export const tarot: Record<string, TarotCardDesc> = {};

		export type NPCEquipSlot = Exclude<ClothingSlot, ClothingSlot.Wig | ClothingSlot.Butt| ClothingSlot.Weapon> | "mascara";

		export interface PresetData extends Entity.QoSDaPlayerData {
			name: string;
		}

		export interface NPCEquipData extends Record<NPCEquipSlot, string|null> {
			glasses?: string;
		}

		export const dadNpc: Record<string, {
			data: PresetData,
			equip: NPCEquipData;
		}> = {};
		export namespace DAD {
			export const extraParts: Record<string, [da.BodyPartConstructor, Record<string, number>][]> = {};
			export let faceStruct: any;
			export const facePresets: Record<string, Entity.DaBodyPreset> = {};
		}

		export namespace Travel {
			export const destinations: Record<string, Destinations> = {};
			export const passageDisplayNames: Record<string, DynamicText> = {};
		}
	}

	export namespace Entity { }
	/**
	 * The basic game state objects.
	 * These will be serialized by the SugarCube and thus may not contain any functions
	 */
	export namespace GameState {}
	export namespace UI {
		export namespace Widgets { }
	}
}

// export the App object from the SugarCube closured eval()
/** @namespace */
globalThis.App = App;
