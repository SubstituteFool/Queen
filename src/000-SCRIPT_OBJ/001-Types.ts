namespace App {

	export enum CoreStat {
		Health = "health",
		Nutrition = "nutrition",
		Willpower = "willpower",
		Perversion = "perversion",
		Femininity = "femininity",
		Fitness = "fitness",
		Toxicity = "toxicity",
		Hormones = "hormones",
		Energy = "energy",
		Futa = "futa",
	}

	export type CoreStatStr = Uncapitalize<keyof typeof CoreStat>;

	export enum BodyPart {
		Face = "face",
		Lips = "lips",
		Bust = "bust",
		Ass = "ass",
		Waist = "waist",
		Hips = "hips",
		Penis = "penis",
		Hair = "hair",
		Balls = "balls",
	}

	export type BodyPartStr = Uncapitalize<keyof typeof BodyPart>;

	export enum BodyProperty {
		BustFirmness = "bustFirmness",
		Height = "height",
		Lactation = "lactation"
	}

	export type BodyPropertyStr = Uncapitalize<keyof typeof BodyProperty>;

	export type BodyStat = BodyPart | BodyProperty;
	export type BodyStatStr = BodyPartStr | BodyPropertyStr;

	export enum DerivedBodyStat {
		AssFirmness = "assFirmness",
	}

	export type DerivedBodyStatStr = Uncapitalize<keyof typeof DerivedBodyStat>;

	export namespace Skills {
		export enum Charisma {
			Seduction = "seduction",
			Singing = "singing",
			Dancing = "dancing",
			Styling = "styling",
			Courtesan = "courtesan",
		}

		export type CharismaStr = Uncapitalize<keyof typeof Charisma>;

		export enum Domestic {
			Cleaning = "cleaning",
			Cooking = "cooking",
			Serving = "serving",
		}

		export type DomesticStr = Uncapitalize<keyof typeof Domestic>;

		export enum Piracy {
			Swashbuckling = "swashbuckling",
			Sailing = "sailing",
			Navigating = "navigating",
		}

		export type PiracyStr = Uncapitalize<keyof typeof Piracy>;

		export enum Sexual {
			HandJobs = "handJobs",
			TitFucking = "titFucking",
			BlowJobs = "blowJobs",
			AssFucking = "assFucking",
			BoobJitsu = "boobJitsu",
			AssFu = "assFu"
		}

		export type SexualStr = Uncapitalize<keyof typeof Sexual>;

		export type Any = Charisma | Domestic | Piracy | Sexual;
		export type AnyStr = CharismaStr | DomesticStr | PiracyStr | SexualStr;
	}

	export enum Stat {
		Core = "core",
        Skill = "skill",
        Body = "body"
	}

	export type StatTypeStr = Uncapitalize<keyof typeof Stat>;

	export interface StatTypeMap {
		[Stat.Core]: CoreStat;
		[Stat.Skill]: Skills.Any;
		[Stat.Body]: BodyStat | DerivedBodyStat;
	}

	export interface StatTypeStrMap {
		"core": CoreStatStr;
		"skill": Skills.AnyStr;
		"body": BodyStatStr | DerivedBodyStatStr;
	}

	export enum NpcStat {
		Mood = "mood",
		Lust = "lust"
	}

	export type NPCStats = {[key in NpcStat]: number};

	export const enum QuestStage {
		Intro = "intro",
		Middle = "middle",
		Finish = "finish",
		JournalEntry = "journalEntry",
		JournalComplete = "journalComplete"
	}

	export const enum QuestState {
		CanComplete = "cancomplete",
		Available = "available",
		Unavailable = "unavailable",
		Completed = "completed",
		Active = "active"
	}

	export type DayPhase = 0 | 1 | 2 | 3 | 4; // 0 morning, 1 afternoon, 2 evening, 3 night, 4 late night

	export enum Phase {
		Morning = 0,
		Afternoon = 1,
		Evening = 2,
		Night = 3,
		LateNight = 4
	}

	export enum ClothingSlot {
		Neck = "neck",
		Bra = "bra",
		Nipples = "nipples",
		Corset = "corset",
		Panty = "panty",
		Wig = "wig",
		Hat = "hat",
		Stockings = "stockings",
		Shirt = "shirt",
		Pants = "pants",
		Penis = "penis",
		Dress = "dress",
		Costume = "costume",
		Shoes = "shoes",
		Butt = "butt",
		Weapon = "weapon",
		Mascara = "mascara"
	}

	export namespace Items {
		export const enum Rarity {
			Common = "common",
			Uncommon = "uncommon",
			Rare = "rare",
			Legendary = "legendary"
		}

		export enum Category {
			Drugs = "drugs",
			Food = "food",
			Cosmetics = "cosmetics",
			MiscConsumable = "miscConsumable",
			MiscLoot = "miscLoot",
			Clothes = "clothes",
			Weapon = "weapon",
			Quest = "quest",
			LootBox = "lootBox",
			Reel = "reel"
		}
		export interface ItemTypeInventoryTypeMap {
			[Category.Drugs]: Entity.InventoryManager,
			[Category.Food]: Entity.InventoryManager,
			[Category.Cosmetics]: Entity.InventoryManager,
			[Category.MiscConsumable]: Entity.InventoryManager,
			[Category.MiscLoot]: Entity.InventoryManager,
			[Category.Clothes]: Entity.ClothingManager,
			[Category.Weapon]: Entity.ClothingManager,
			[Category.Quest]: Entity.InventoryManager,
			[Category.LootBox]: Entity.InventoryManager,
			[Category.Reel]: Entity.InventoryManager,
		}
	}

	export namespace Combat {
		export type Effect = 'STUNNED' | 'BLINDED' | 'GUARDED' | 'DODGING' | 'BLOODTHIRST' | 'SEEKING' | 'LIFE LEECH' | 'PARRY';
	}
}
