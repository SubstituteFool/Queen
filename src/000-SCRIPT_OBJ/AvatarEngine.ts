namespace App.Entity {

	interface DrawEventData{
		e: string;
		h: number;
		w: number;
		c?: HTMLElement;
	}

	export interface DaBodyPreset {
		age?: number;
		basedim: Record<string, number>;
		// eslint-disable-next-line @typescript-eslint/naming-convention
		Mods: Record<string, number>;
		decorativeParts?: any[];
		hairFill?: string;
		browFill?: string;
		lushFill?: string;
	}

	export interface QoSDaPlayerData extends DaBodyPreset{
		fem: number;
		/**
		 *  0: female, 1: male, 2: shemale
		 */
		gender: Gender;
		sub: number;
		clothes: string[];
	}

	export class QoSDaPlayer extends da.Player {
		gender!: Gender;
		constructor(data: any) {
			super(data);
		}
	}

	// NOTES:
	// This is the first hack at integrating the dynamic avatar engine with our little game.
	// Pros: Was fast to do. Cons: A lot of customization ahead + lots of overhead now on adding clothing.
	// TODO:
	// - Cleanup code (ongoing)
	// - Fork a proper branch of the source someplace for our custom parts.
	// - Add some custom hair styles (looks painful).
	// - Add clothing to the renderer.
	// - Map all clothes to some models.
	// - Make any parts that are missing.
	export class AvatarEngine {
		#pcData: QoSDaPlayerData;
		#lastDrawn: da.DrawingExports | null;
		#width: number | undefined;
		#height: number | undefined;
		#element!: string;
		#npcCache: Record<string, {canvasData: string | null, portraitData: string | null}> = {};

		constructor() {
			console.debug("Loading DA system…");
			const t0 = performance.now();
			console.debug("Creating patterns");

			// Loading patterns. Embedded patterns are in the story passage named the same as the
			// file name. Get the text attribute for the encoded string.
			da.addPattern("white hearts", Story.get("txt_heart_1").text);
			da.addPattern("pink checks", Story.get("txt_pink_checks").text);
			da.addPattern("pink tartan", Story.get("txt_pink_tartan").text)
			da.addPattern("pink gingham", Story.get("txt_pink_gingham").text);
			da.addPattern("red tartan", Story.get("txt_red_tartan").text);
			da.addPattern("black lace", Story.get("txt_black_lace").text);
			da.addPattern("black purple stripe", Story.get("txt_black_purple_stripe").text);
			da.addPattern("red plastic", Story.get("txt_red_plastic").text);
			da.addPattern("cow print", Story.get("txt_cow_print").text);
			da.addPattern("black fur", Story.get("txt_black_fur").text);
			da.addPattern("jolly roger big", Story.get("txt_jolly_roger_big").text);
			da.addPattern("black sequins", Story.get("txt_black_sequin").text);
			da.addPattern("horizontal pink stripe", Story.get("txt_horizontal_pink_white_stripe").text);
			da.addPattern("pink polka dot", Story.get("txt_pink_polka_dot").text);
			da.addPattern("vertical pink stripe", Story.get("txt_vertical_pink_white_stripe").text);
			da.addPattern("pink chevron", Story.get("txt_pink_chevron").text);
			da.addPattern("pink flowers", Story.get("txt_pink_flowers").text);
			da.addPattern("leather", Story.get("txt_leather").text);
			da.addPattern('blue cotton', Story.get("txt_blue_cotton").text);
			da.addPattern('pink leather', Story.get("txt_pink_leather").text);
			da.addPattern('hot pink', Story.get("txt_hot_pink").text);
			da.addPattern("purple lace", Story.get("txt_purple_lace").text);
			da.addPattern("red cotton", Story.get("txt_red_cotton").text);
			da.addPattern("red sequin", Story.get("txt_red_sequin").text);
			da.addPattern("red velvet", Story.get("txt_red_velvet").text);
			da.addPattern("pink fishnet", Story.get("txt_pink_fishnet").text);
			// Gradients loaded as textures.
			da.addPattern("pink athletic socks", AvatarEngine._pinkAthleticSocks);

			da.load().then(() => {
				// Functions needed here to overwrite some default behavior of
				// the DA library.
				da.extendDimensionCalc("human.penisSize", function () {
					return this.getDim('penisSize');
				});

				da.extendDimensionCalc("human.testicleSize", function () {
					return this.getDim('testicleSize');
				});

				da.extendDimensionCalc('human.waistWidth', function () {
					return this.getDim('waistWidth');
				});

				da.extendDimensionCalc('human.breastSize', function () {
					return this.getDim('breastSize');
				});

				const t2 = performance.now();
				console.debug(`Loaded DA in ${t2 - t0} ms.`);
			}, () => {
				console.error("Failed to load DA");
			});

			this.#pcData = {
				// provide specific values here to override the default ones set
				age: 26,
				fem: 0,
				sub: 0,
				gender: 0,
				// base physical dimensions
				basedim: {
					armThickness: 58,
					armLength: 45,
					handSize: 120,
					legFullness: 4,
					legLength: 97,
					neckLength: 80,
					neckWidth: 35,
					skin: 3,
				},
				Mods: {breastPerkiness: 4},
				decorativeParts: [],
				// overriding clothing (default to simple red underwear)
				clothes: [],
			};

			this.#lastDrawn = null;
		}

		npcSettingHandler(): void {
			// NO OP for now
		}

		settingHandler(): void {
			const container = $('#avatarContainer');
			if (EventHandlers.hasPlayerState() == true && container && settings.displayAvatar == true) {
				container.css("display", "block");
				this.drawPortrait();
			} else if (EventHandlers.hasPlayerState() == true && container && settings.displayAvatar == false) {
				container.css("display", "none");
			}
			if ($('#mirrorContainer').length) {
				Engine.play(passage(), true); // Tell engine to reload current passage
			}
		}

		get defaultData(): QoSDaPlayerData {return this.#pcData;}
		get npcPortraits() {return this.#npcCache;}
		get lastDrawn(): da.DrawingExports | null {return this.#lastDrawn;}

		// Bound event handlers and methods for drawing canvas and portraits

		/**
		 * Draws a full body picture of the player character.
		 * @param element Where to attach the canvas
		 * @param  height height of the canvas
		 * @param width width of the canvas
		 */
		drawCanvas(element: string, height: number, width: number): void {
			this.#height = height;
			this.#width = width;
			this.#element = element;
			$(document).one(":passageend", this._drawCanvas.bind(this));
		}

		/**
		 * Called on :passageend by DrawCanvas()
		 */
		private _drawCanvas() {
			let canvasGroup = null;
			if (typeof canvasGroup === 'undefined' || canvasGroup == null) {
				canvasGroup = da.getCanvasGroup(this.#element,
					{
						border: "1px solid black",
						width: this.#width !== undefined ? this.#width : 360,
						height: this.#height !== undefined ? this.#height : 800,
					});
			}

			let pc = new QoSDaPlayer(this.getPCData());
			pc = AvatarEngine._attachParts(pc);
			da.draw(canvasGroup, pc, {
				printHeight: false,
				printAdditionalInfo: false,
				renderShoeSideView: false,
				printVitals: false,
				offsetX: 10,
				offsetY: 0
			}).then((exports) => {
				this.#lastDrawn = exports;
			}, undefined);
		}

		/**
		 * Draw a small portrait of the player character.
		 */
		queuePortraitDraw(canvas?: HTMLElement): void {
			if (settings.displayAvatar == true)
				$(document).one(":passageend", {e: 'avatarFace', h: 240, w: 240, c: canvas}, this.drawPortrait.bind(this));
		}

		/**
		 * Called on ":passageend" by DrawPortrait();
		 * Requires a hidden canvas 'hiddenCanvas' for drawing a large picture of the player.
		 * Draws specifically to an element with the id of 'avatarFace'. See main.twee for implementation.
		 */
		drawPortrait(event?: JQuery.PassageRenderingEvent<unknown, DrawEventData>): void {
			if (!settings.displayAvatar) return;
			/* eslint-disable-next-line prefer-const */
			let {e, w, h, c: canvasGroup} = (event ? event.data : {e: 'avatarFace', h: 240, w: 240, c: undefined});
			if (canvasGroup === undefined) {
				canvasGroup = da.getCanvasGroup("hiddenCanvas", {
					border: "none",
					width: 1400,
					height: 2400
				});
			}

			let pc = new QoSDaPlayer(this.getPCData());
			pc = AvatarEngine._attachParts(pc);

			da.draw(canvasGroup, pc, {printHeight: false, printAdditionalInfo: false, renderShoeSideView: false, printVitals: false})
				.then((exports) => {
					const center = {
						x: exports[da.Part.RIGHT].skull.x,
						y: (exports[da.Part.RIGHT].skull.y) - (h * 0.18)
					};

					const portraitCanvas = da.getCanvas("portrait",
						{
							width: w,
							height: h,
							border: "solid 1px goldenrod",
							position: "relative",
							parent: document.getElementById(e),
						});

					da.drawFocusedWindow(portraitCanvas,
						exports,
						{
							center: center,
							width: (w * 0.40), // scaling on the point, smaller is closer, larger is zoomed out.
							height: (h * 0.40)
						});
				}, undefined);
		}

		/**
		 *
		 * @param npc The ID of the NPC to render.
		 * @param element The element to attach the drawing to
		 * @param height height of the canvas / image created.
		 * @param width width of the canvas / image created.
		 */
		drawCanvasNPC(npc: string, element: string | HTMLElement, height: number, width: number): void {
			$(document).one(":passageend", {n: npc, e: element, h: height, w: width}, this._drawCanvasNPC.bind(this));
		}

		drawPortraitNPC(npc: string, element: string | HTMLElement, height: number, width: number): void {
			$(document).one(":passageend", {n: npc, e: element, h: height, w: width}, this._drawPortraitNPC.bind(this));
		}

		/**
		 * Called from DrawNPC() on :passageend
		 * @param {object} e { n: npc id, e: html element, h: height, w: width }
		 */
		private _drawCanvasNPC(e: JQuery.PassageRenderingEvent<unknown, {n: string, e: JQuery.Selector | HTMLElement, h: number, w: number}>) {
			const data = e.data;

			// Supply default NPC.
			if (Data.dadNpc.hasOwnProperty(data.n) == false) {
				if (Data.npcs.hasOwnProperty(data.n)) {
					data.n = Data.npcs[data.n].gender == 1 ? 'DefaultMale' : 'DefaultFemale';
				} else {
					data.n = 'DefaultMale';
				}
			}

			// NPC data is cached, so just write it out
			if (this.npcPortraits.hasOwnProperty(data.n) && this.npcPortraits[data.n].canvasData != null) {
				const cached = $("<img>").addClass('npcCanvas');
				cached.attr('id', 'npcCanvasImage');
				cached.attr('src', this.npcPortraits[data.n].canvasData);
				if (typeof data.e === "string") {
					$(data.e).append(cached);
				} else {
					$(data.e).append(cached);
				}
				return;
			}

			const npcData = Data.dadNpc[data.n].data;
			const npcEquip = Data.dadNpc[data.n].equip;
			const npcParts = Data.DAD.extraParts[data.n];

			const canvasGroup = da.getCanvasGroup(data.e,
				{
					border: "1px solid goldenrod",
					width: data.w,
					height: data.h
				});

			let pc = new QoSDaPlayer(npcData);
			pc = AvatarEngine._attachPartsNPC(pc, npcEquip, npcParts);

			da.draw(canvasGroup, pc, {
				printHeight: false,
				printAdditionalInfo: false,
				renderShoeSideView: false,
				printVitals: false,
				offsetX: 10,
				offsetY: 0
			}).then(() => {
				// if the number of layers change, this needs to change too
				const canvas = (typeof data.e === "string" ? (document.getElementById(`${data.e}16`)) : data.e) as HTMLCanvasElement;
				const canvasData = canvas.toDataURL();
				this._cacheNPCCanvas(data.n, canvasData);
			}, undefined);
		}

		private _drawPortraitNPC(e: JQuery.PassageRenderingEvent<unknown, {n: string, e: string | HTMLElement, h: number, w: number}>) {
			const data = e.data;

			// Supply default NPC.
			if (Data.dadNpc.hasOwnProperty(data.n) == false) {
				if (Data.npcs.hasOwnProperty(data.n)) {
					data.n = Data.npcs[data.n].gender == 1 ? 'DefaultMale' : 'DefaultFemale';
				} else {
					data.n = 'DefaultMale';
				}
			}

			// NPC data is cached, so just write it out
			if (this.npcPortraits.hasOwnProperty(data.n) && this.npcPortraits[data.n].portraitData != null) {
				const cached = $("<img>").addClass('npcPortrait');
				cached.attr('id', 'npcPortraitImage');
				cached.attr('src', this.npcPortraits[data.n].portraitData);
				$(`#${data.e}`).append(cached);
				return;
			}

			const npcData = Data.dadNpc[data.n].data;
			const npcEquip = Data.dadNpc[data.n].equip;
			const npcParts = Data.DAD.extraParts[data.n];

			const canvasGroup = da.getCanvasGroup("hiddenNPCPortraitCanvas", {
				border: "none",
				width: 1400,
				height: 2400
			});

			let pc = new QoSDaPlayer(npcData);
			pc = AvatarEngine._attachPartsNPC(pc, npcEquip, npcParts);

			da.draw(canvasGroup, pc, {printHeight: false, printAdditionalInfo: false, renderShoeSideView: false, printVitals: false})
				.then((exports) => {
					const portraitCanvas = da.getCanvas("npcPortrait",
						{
							width: data.w,
							height: data.h,
							border: "solid 1px goldenrod",
							position: "relative",
							parent: typeof data.e === "string" ? document.getElementById(data.e) : data.e,
						});

					da.drawFocusedWindow(portraitCanvas,
						exports,
						{
							center: exports[da.Part.RIGHT].neck.nape,
							width: 75,
							height: 75
						});

					const canvas = (document.getElementById('npcPortrait')) as HTMLCanvasElement;
					const portraitData = canvas.toDataURL();
					this._cacheNPCPortrait(data.n, portraitData);
				}, undefined);
		}

		private _cacheNPCCanvas(key: string, data: string) {
			if (this.npcPortraits.hasOwnProperty(key)) {
				this.npcPortraits[key].canvasData = data;
			} else {
				this.npcPortraits[key] = {canvasData: data, portraitData: null};
			}
		}

		private _cacheNPCPortrait(key: string, data: string) {
			if (this.npcPortraits.hasOwnProperty(key)) {
				this.npcPortraits[key].portraitData = data;
			} else {
				this.npcPortraits[key] = {canvasData: null, portraitData: data};
			}
		}

		private static _attachPartsNPC(pc: QoSDaPlayer, npcEquip: Record<Data.NPCEquipSlot, string | null>,
			parts?: [da.BodyPartConstructor, Record<string, number>][]) {
			// Default gender parts.
			if (pc.gender == 1 || pc.gender == 2) {
				const penis = da.Part.create(da.PenisHuman, {side: "right"});
				pc.attachPart(penis);
				const balls = da.Part.create(da.TesticlesHuman, {side: "right"});
				pc.attachPart(balls);
			}

			if (pc.gender == 1) {
				pc.removeSpecificPart(da.OversizedChest);
				pc.removeSpecificPart(da.ChestHuman);
			}

			if (pc.gender == 0 || pc.gender == 2) {
				const bust = da.Part.create(da.OversizedChest, {side: null});
				pc.attachPart(bust);
				pc.removeSpecificPart(da.NipplesHuman);
				const nips = da.Part.create(da.OversizedChestNipples, {side: null});
				pc.attachPart(nips);
			}

			if (pc.gender == 0) {
				pc.removeSpecificPart(da.PenisHuman);
				pc.removeSpecificPart(da.TesticlesHuman);
			}

			// Additional Parts
			if (typeof parts !== 'undefined') {
				for (const part of parts) {
					const toAdd = da.Part.create(part[0], part[1]);
					pc.attachPart(toAdd, pc.decorativeParts);
				}
			}
			return AvatarEngine._clothesHandlerNPC(pc, npcEquip);
		}

		private static _clothesHandlerNPC(pc: QoSDaPlayer, npcEquip: Record<Data.NPCEquipSlot, string | null>) {
			for (const slot of Object.keys(npcEquip)) {
				const id = npcEquip[slot];
				if (id == null) continue;
				const items = Data.AvatarMapping.clothes[id];
				if (items) {
					AvatarEngine.handleClothingItems(pc, items);
				} else {
					console.log("Unable to map clothes to npc: " + id);
				}
			}

			return pc;
		}

		getPCData(): QoSDaPlayerData {
			let data = this.#pcData;
			data = AvatarEngine._mapHeight(data);
			data = AvatarEngine._mapHormones(data);
			data = AvatarEngine._mapFace(data);
			data = AvatarEngine._mapHair(data);
			data = AvatarEngine._mapGenitals(data);
			data = AvatarEngine._mapUpperBody(data);
			data = AvatarEngine._mapLowerBody(data);
			return data;
		}

		private static _attachParts(pc: QoSDaPlayer): QoSDaPlayer {
			const penis = da.Part.create(da.PenisHuman, {side: null});
			pc.attachPart(penis);
			const balls = da.Part.create(da.TesticlesHuman, {side: null});
			pc.attachPart(balls);
			const bust = da.Part.create(da.OversizedChest, {side: null});
			pc.attachPart(bust);
			pc.removeSpecificPart(da.NipplesHuman);
			const nips = da.Part.create(da.OversizedChestNipples, {side: null});
			pc.attachPart(nips);

			return AvatarEngine._clothesHandler(pc);
		}

		private static _mapFace(data: QoSDaPlayerData) {
			// Copy player face data into data.
			let face = setup.player.faceData;
			if (face == null || typeof face === 'undefined') { // kludge just in case
				setup.player.setFace('Default 1');
				face = setup.player.faceData;
			}

			for (const p in face.basedim) data.basedim[p] = face.basedim[p];
			for (const p in face.Mods) data.Mods[p] = face.Mods[p];

			const hormoneMod = (AvatarEngine._c(CoreStat.Hormones) / 200);
			const lipMod = (setup.player.getStat(Stat.Body, BodyPart.Lips) / 100);
			const faceMod = (setup.player.getStat(Stat.Body, BodyPart.Face) / 100);

			// Set the face 'femininity'. Clamp to the lowest set on the face data.
			data.basedim.faceFem = _.clamp(
				(20 * faceMod) + (20 * hormoneMod),
				face.basedim.faceFem, 40);

			// Slightly shorten the face, but no lower than 200 unless the player has it
			// set to be shorter.
			const faceLength = face.basedim.faceLength < 195 ? face.basedim.faceLength :
				face.basedim.faceLength * (1.0 - ((.065 * faceMod) + (.065 * hormoneMod)));

			data.basedim.faceLength = _.clamp(faceLength, 195, 240);

			// Lip Size
			data.basedim.lipSize = _.clamp(
				(10 + (15 * lipMod)), face.basedim.lipSize, 35);

			// Lip top
			data.Mods.lipTopSize = _.clamp((-20 + (55 * lipMod)), face.Mods.lipTopSize, 35);

			// Lip bottom
			data.Mods.lipBotSize = _.clamp((-80 + (120 * lipMod)), face.Mods.lipBotSize, 120);

			// Lip Part
			data.Mods.lipParting = _.clamp(
				(36 * lipMod), face.Mods.lipParting, 36);

			// Lip width
			data.Mods.lipWidth = _.clamp(
				(-10 - (70 * lipMod)), -80, face.Mods.lipWidth);

			// Eyelashes
			data.basedim.eyelashLength = _.clamp(
				(6 * hormoneMod), face.basedim.eyelashLength, 6);

			return data;
		}

		private static _mapUpperBody(data: QoSDaPlayerData) {
			const hormoneMod = (AvatarEngine._c(CoreStat.Hormones) / 200);
			const breast = setup.player.getStat(Stat.Body, BodyPart.Bust) / 2;
			const areola = (50 * hormoneMod);
			const upperMuscle = (30 * (AvatarEngine._c(CoreStat.Fitness) / 100));
			data.basedim.breastSize = breast;
			data.basedim.areolaSize = areola;
			data.basedim.upperMuscle = upperMuscle;
			// Data.basedim.shoulderWidth = 64;
			return data;
		}

		private static _mapLowerBody(data: QoSDaPlayerData) {
			const femMod = (AvatarEngine._c(CoreStat.Femininity) / 100);
			const lowerMuscle = (35 * (AvatarEngine._c(CoreStat.Fitness) / 100));
			const ass = 5 + (35 * setup.player.getStat(Stat.Body, BodyPart.Ass) / 100);
			const hips = 100 + (75 * (setup.player.getStat(Stat.Body, BodyPart.Hips) / 100));
			const waist = 70 + (100 * (setup.player.getStat(Stat.Body, BodyPart.Waist) / 100));
			data.basedim.lowerMuscle = lowerMuscle;
			data.basedim.buttFullness = ass;
			data.basedim.hipWidth = hips;
			data.basedim.waistWidth = waist;
			data.basedim.legFem = (10 + (30 * femMod));
			return data;
		}

		private static _mapHeight(data: QoSDaPlayerData) {
			data.basedim.height = AvatarEngine._b(BodyProperty.Height);
			return data;
		}

		// TODO: Make new penis body part as the existing one doesn't scale large enough for the game.
		private static _mapGenitals(data: QoSDaPlayerData) {
			const penis = 30 + (170 * setup.player.getStat(Stat.Body, BodyPart.Penis) / 100);
			const balls = 25 + (75 * setup.player.getStat(Stat.Body, BodyPart.Balls) / 100);
			data.basedim.penisSize = penis;
			data.basedim.testicleSize = balls;
			return data;
		}

		private static _mapHormones(data: QoSDaPlayerData) {
			const hormoneMod = (AvatarEngine._c(CoreStat.Hormones) / 200);
			data.fem = 11 * hormoneMod;

			return data;
		}

		/**
		 * Map the players hair attributes
		 */
		private static _mapHair(data: QoSDaPlayerData) {
			const wig = setup.player.getEquipmentInSlot(ClothingSlot.Wig);
			let length = wig?.hairLength ?? AvatarEngine._b(BodyPart.Hair);
			const color = wig != null ?
				App.Data.Lists.hairColors[wig.hairColor] : Data.Lists.hairColors[setup.player.hairColor];
			// const browFill = Data.Lists.HairColors[setup.player.hairColor];
			// const lashFill = Data.Lists.HairColors[setup.player.hairColor];
			const style = wig != null ? wig.hairStyle : setup.player.hairStyle;

			// TODO: We are missing some hair styles, so either we create them later or get creative.

			let dadHairStyle = 2;
			switch (style) {
				case 'a spunky boy cut"':
					// avatar: (min 30, max 110) game: (max 10)
					length = Math.min(110, ((80 * (length / 20)) + 30));
					dadHairStyle = 7;
					break;
				case 'a loose and flippy style':
					// avatar: (min 45, max 60), game: (max 20)
					length = Math.min(60, ((15 * (length / 20)) + 30));
					dadHairStyle = 2;
					break;
				case 'a short bob style':
				case 'a short bob':
					// avatar: (min 35, max 50)  game (max 30)
					length = Math.min(50, ((15 * (length / 30)) + 35));
					dadHairStyle = 3;
					break;
				case 'a breezy style':
					// avatar (min 60, max 110) game (max 50)
					length = Math.min(110, ((50 * (length / 50)) + 60));
					dadHairStyle = 6;
					break;
				case 'a loose cut with flowing curls':
					dadHairStyle = 4;
					break;
				case 'cute pig tails':
					// avatar (min 90, max 110) game (50)
					length = Math.min(110, ((20 * (length / 50)) + 90));
					dadHairStyle = 5;
					break;
				case 'a long pony tail':
					// avatar (min 90, max 110) game (100)
					length = Math.min(110, ((20 * (length / 100)) + 90));
					dadHairStyle = 5;
					break;
				case 'a slick up-do style':
					// avatar (min 50, max 110) game (20)
					length = Math.min(110, ((60 * (length / 20)) + 50));
					dadHairStyle = 1;
					break;
				case 'extravagantly long braids':
					dadHairStyle = 4;
					break;
				case 'extremely long twin tails':
					// avatar (min 90, max 110) game (80)
					length = Math.min(110, ((20 * (length / 80)) + 90));
					dadHairStyle = 5;
					break;
				default:
					dadHairStyle = 2;
			}

			data.basedim.hairLength = length;
			data.basedim.hairStyle = dadHairStyle;
			data.basedim.hairHue = color.h;
			data.basedim.hairSaturation = color.s;
			data.basedim.hairLightness = color.l;
			// data.browFill = browFill; // TODO: brow and lush colors
			// data.lashFill = lashFill;

			return data;
		}

		private static _clothesHandler(pc: QoSDaPlayer) {
			for (const o of Object.values(setup.player.equipment)) {
				if (!o || o.slot == ClothingSlot.Wig) continue;

				const items = Data.AvatarMapping.clothes.hasOwnProperty(o.tag) ?
					Data.AvatarMapping.clothes[o.tag] :
					Data.AvatarMapping.placeholders[o.slot];
				if (items) {
					AvatarEngine.handleClothingItems(pc, items);
				}
			}

			return pc;
		}

		static handleClothingItems(pc: QoSDaPlayer, items: Data.AvatarMapping.ItemData[]): void {
			for (const item of items) {
				// const str = "da.Clothes.create(" + items[i].c + "," + JSON.stringify(items[i].a) + ")";
				// This nonsense here is just to be safe due to script 'compiling' order of Tweego.
				const pattern = item.a?.pattern;
				if (item.a && typeof pattern === 'function') {
					let patternOb: da.CachedPattern | null = null;
					try {
						patternOb = pattern();
					} catch (err) {
						console.log(err);
					}
					if (patternOb) item.a.fill = patternOb;
				}
				const part = item.a == null ?
					da.Clothes.create(item.c) : da.Clothes.create(item.c, item.a);
				pc.wearClothing(part);
			}
		}

		private static _b(s: BodyStatStr) {return PR.statToCm(setup.player, s);}
		private static _bp(s: BodyProperty) {return setup.player.getStat(Stat.Body, s);}

		private static _c(s: CoreStatStr) {return setup.player.getStat(Stat.Core, s);}

		// Gradients
		// I want to be able to dynamically scale these, waiting on a response from the DA developer about this.

		private static _pinkAthleticSocks(ctx: CanvasRenderingContext2D, _ex: da.DrawingExports) {
			const grd = ctx.createLinearGradient(150.000, 0.000, 150.000, 180.000);

			// Add colors
			grd.addColorStop(0.000, 'lightpink');
			grd.addColorStop(0.514, 'lightpink');
			grd.addColorStop(0.521, 'white');
			grd.addColorStop(0.536, 'white');
			grd.addColorStop(0.541, 'lightpink');
			grd.addColorStop(0.551, 'lightpink');
			grd.addColorStop(0.560, 'white');
			grd.addColorStop(0.571, 'white');
			grd.addColorStop(0.580, 'lightpink');
			grd.addColorStop(1.000, 'lightpink');

			return grd;
		}
	}
}
