namespace App.Combat {
	/**
	 * Make sure to instantiate this object to a namespace that will not be cleared during
	 * normal play. Prior to drawing the UI with the <<combatUI>> widget, make sure that you
	 * call this.InitializeScene() to clear variables and then this.loadEncounter({}) from the
	 * PREVIOUS passage inside a <<link>> macro, so that the state of this object will not be
	 * overwritten if the player goes into another menu screen while playing.
	 */
	export class CombatEngine {
		#target: Combatant | null = null;
		#encounterData: Data.Combat.EncounterDesc | null = null;
		#enemies: Combatant[] = [];
		#player: Player | null = null;
		#timeline: Combatant[] = [];
		#UI_MaxInitiative = 13;
		#hpBars: Record<string, JQuery<HTMLElement>> = {};
		#staminaBars: Record<string, JQuery<HTMLElement>> = {};
		#chatLog: string[] = [];
		#lastSelectedStyle = "UNARMED"; // Don't overwrite
		#flee = 0;
		#noWeapons = false;
		#fleePassage = 'Cabin';
		#lootBuffer: string[] = [];
		#introChat: string | null = null;

		get lastSelectedStyle(): string {
			return this.#lastSelectedStyle;
		}

		set lastSelectedStyle(s: string) {
			this.#lastSelectedStyle = s;
			sessionStorage.setItem('QOS_ENCOUNTER_COMBAT_STYLE', this.#lastSelectedStyle);
		}

		get enemies(): Combatant[] {return this.#enemies;}

		get duelMode(): boolean {
			assertIsDefined(this.#encounterData);
			return this.#encounterData.fatal;
		}

		get winPassage(): string {
			assertIsDefined(this.#encounterData);
			return this.#encounterData.winPassage;
		}

		get losePassage(): string {
			assertIsDefined(this.#encounterData);
			return this.#encounterData.losePassage;
		}

		/**
		 * Call before load encounter.
		 * @param opts option object
		 */
		initializeScene(opts: {flee?: number, fleePassage?: string, noWeapons?: boolean, intro?: string}): void {
			this.#encounterData = null;
			this.#enemies = [];
			this.#player = null;
			this.#timeline = [];
			this.#hpBars = {};
			this.#staminaBars = {};
			this.#chatLog = [];
			this.#lootBuffer = [];
			this.#noWeapons = false;

			if (typeof opts !== 'undefined') {
				if (!_.isUndefined(opts.flee)) this.#flee = opts.flee;
				if (!_.isUndefined(opts.fleePassage)) this.#fleePassage = opts.fleePassage;
				if (!_.isUndefined(opts.noWeapons)) this.#noWeapons = opts.noWeapons;
				if (!_.isUndefined(opts.intro)) {
					this.#introChat = opts.intro;
					sessionStorage.setItem('QOS_ENCOUNTER_INTRO', this.#introChat);
				}
			}

			sessionStorage.setItem('QOS_ENCOUNTER_FLEE', this.#flee.toString());
			sessionStorage.setItem('QOS_ENCOUNTER_FLEE_PASSAGE', this.#fleePassage);
			sessionStorage.setItem('QOS_ENCOUNTER_NO_WEAPONS', this.#noWeapons ? "true" : "false");
		}

		/**
		 * Load an encounter to the engine.
		 * Come back later and serialize the enemy stats and player stats to save to sessionStorage
		 * and then reconstitute them on reload of page.
		 * @param encounter encounter data record
		 */
		loadEncounter(encounter: string): void {
			sessionStorage.setItem('QOS_ENCOUNTER_KEY', encounter);
			const eqData = clone(Combat.encounterData[encounter]);
			if (!eqData) {
				throw "Encounter name invalid";
			}
			this.#encounterData = eqData;
			for (const e of eqData.enemies) this._addEnemy(e);
			this._addPlayer(setup.player);
			if (this.#introChat && this.#introChat != null && this.#introChat != 'null') {
				this._addChat(this.#introChat);
			} else if (typeof eqData.intro !== "undefined") {
				this._addChat(eqData.intro);
			}
		}

		drawUI(): void {
			// Reconstitute some data from the session storage if the player reloaded the page to be a cheat.
			if (this.#encounterData == null) {
				this.#lastSelectedStyle = sessionStorage.getItem('QOS_ENCOUNTER_COMBAT_STYLE') as string;
				this.#flee = parseInt(sessionStorage.getItem('QOS_ENCOUNTER_FLEE') as string);
				this.#fleePassage = sessionStorage.getItem('QOS_ENCOUNTER_FLEE_PASSAGE') as string;
				this.#noWeapons = (sessionStorage.getItem('QOS_ENCOUNTER_NO_WEAPONS') === "true");
				this.loadEncounter(sessionStorage.getItem('QOS_ENCOUNTER_KEY') as string);
				this.#introChat = sessionStorage.getItem('QOS_ENCOUNTER_INTRO');
			}

			$(document).one(":passageend", this._drawUI.bind(this));
		}

		drawResults(): void {
			$(document).one(":passageend", this._drawWinLog.bind(this));
		}

		startCombat(): void {
			this._startRound();
		}

		// UI COMPONENTS

		private _drawUI(): void {
			// Main UI Components.
			this._drawInitiativeBar();
			this._drawEnemyContainers();
			this._drawChatLog();
			this._drawStyles();
			this._drawCombatButtons();

			// Default Control buttons
			this._addDefaultCommands();

			// Status for players.
			this._updatePlayerComboBar();
			this._updatePlayerStaminaBar();
			// Start the fight.
			this._startRound();
		}

		private _drawCombatButtons(): void {
			const selectedStyle = $('#combatStyles').children("option:selected").val() as string;
			const root = $('#CombatCommands');
			root.empty();

			const d = Combat.moves[selectedStyle].moves;

			for (const prop in d) {
				const m = d[prop];
				const container = $('<div>').addClass("CombatButtonContainer");
				const span = $('<span>').addClass("CombatToolTip").html(
					"<span style='color:yellow'>" + m.name + "</span><br>" + m.description);
				const button = $('<div>').attr('id', 'combatButton' + prop.replace(/ /g, '_')).addClass("CombatButton");
				button.addClass(m.icon);
				if ((this.#player as Player).engine.checkCommand(m)) {
					button.on("click", {cmd: prop}, this._combatCommandHandler.bind(this));
					button.addClass("CombatButtonEnabled");
				} else {
					button.addClass("CombatButtonDisabled");
				}
				container.append(span);
				container.append(button);
				root.append(container);
			}
		}

		private _updateCombatButtons() {
			const selectedStyle = $('#combatStyles').children("option:selected").val() as string;
			const d = Combat.moves[selectedStyle].moves;

			for (const prop in d) {
				const button = $("#combatButton" + prop.replace(/ /g, '_'));
				button.off('click');
				assertIsDefined(this.#player);
				if (this.#player.engine.checkCommand(d[prop])) {
					button.on("click", {cmd: prop}, this._combatCommandHandler.bind(this));
					button.removeClass("CombatButtonDisabled");
					button.addClass("CombatButtonEnabled");
				} else {
					button.removeClass("CombatButtonEnabled");
					button.addClass("CombatButtonDisabled");
				}
			}
		}

		private _addDefaultCommands() {
			const defendBtn = $('#cmdDefend');
			defendBtn.on("click", {cmd: 'defend'}, this._combatCommandHandler.bind(this));
			const fleeBtn = $('#cmdFlee');
			fleeBtn.on("click", {cmd: 'flee'}, this._combatCommandHandler.bind(this));
			const restoreBtn = $('#cmdRestoreStamina');
			restoreBtn.on("click", {cmd: 'restoreStamina'}, this._combatCommandHandler.bind(this));
		}

		private _drawStyles() {
			const root = $('#combatStyles');
			assertIsDefined(this.#player);
			const styles = this.#player.availableMoveset;
			if (Object.keys(styles).includes(this.lastSelectedStyle) == false) {
				this.lastSelectedStyle = 'UNARMED';
			}

			for (const prop in styles) {
				if (this.#noWeapons == true && prop == 'SWASHBUCKLING') continue; // Don't allow weapon combat.
				const opt = $('<option>').attr('value', prop).text(styles[prop]);
				if (prop == this.lastSelectedStyle) opt.attr('selected', 'selected');
				root.append(opt);
			}

			root.on("change", this._combatStyleChangeHandler.bind(this));
		}

		private _drawChatLog() {
			const root = $('#EnemyGUI');
			const log = $('<div>').attr('id', 'CombatChatLog');
			root.append(log);
			for (const clm of this.#chatLog) {
				log.append("<P class='ChatLog'>" + clm + "</P>");
			}

			log.scrollTop(log.prop("scrollHeight"));
		}

		private _drawWinLog() {
			const root = $('#WinDiv');
			const log = $('<div>').attr('id', 'WinChatLog');
			root.append(log);
			for (const clm of this.#chatLog) {
				log.append("<P class='ChatLog'>" + clm + "</P>");
			}

			log.scrollTop(log.prop("scrollHeight"));
		}

		private _drawEnemyContainers() {
			const root = $('#EnemyGUI');
			root.empty();

			for (let i = 0; i < this.#enemies.length; i++) {
				const container = $('<div>').attr('id', `EnemyContainer${i}`).addClass('EnemyContainer');
				container.append('<div>').addClass('EnemyTitle').text(this.#enemies[i].title);
				const frame = $('<div>').attr('id', `EnemyPortrait${i}`).addClass('EnemyPortrait');
				frame.addClass("EnemySelectEnabled");
				frame.addClass(this.#enemies[i].portrait);
				frame.on("click", {index: i}, this._selectEnemyHandler.bind(this));
				container.append(frame);
				root.append(container);
				this._drawStatus(frame, i);
				this._updateHPBar(this.#enemies[i]);
				this._updateStaminaBar(this.#enemies[i]);
				this._updateEnemyPortraits();
			}
		}

		private _drawStatus(container: JQuery<HTMLElement>, enemyIndex: number) {
			const frame = $('<div>').attr('id', `StatusFrame${enemyIndex}`).addClass('EnemyStatusFrame');

			const hp = $('<div>').attr('id', `HpHolder${enemyIndex}`).addClass('EnemyHPHolder');
			const hpBar = $('<div>').attr('id', `EnemyHpBar${enemyIndex}`).addClass('EnemyHpBar');
			const hpCover = $('<div>').attr('id', `EnemyHpBarCover${enemyIndex}`).addClass('EnemyHpBarCover');
			hp.append(hpBar);
			hpBar.append(hpCover);
			frame.append(hp);
			this.#hpBars[this.#enemies[enemyIndex].id] = hpCover;

			const stamina = $('<div>').attr('id', `StaminaHolder${enemyIndex}`).addClass('EnemyStaminaHolder');
			const staminaBar = $('<div>').attr('id', `EnemyStaminaBar${enemyIndex}`).addClass('EnemyStaminaBar');
			const staminaCover = $('<div>').attr('id', `EnemyStaminaBarCover${enemyIndex}`).addClass('EnemyStaminaBarCover');

			stamina.append(staminaBar);
			staminaBar.append(staminaCover);
			frame.append(stamina);
			this.#staminaBars[this.#enemies[enemyIndex].id] = staminaCover;

			container.append(frame);
		}

		private _updateEnemyPortraits() {
			for (let i = 0; i < this.#enemies.length; i++) {
				const frame = $(`#EnemyPortrait${i}`);
				frame.removeClass("ActivePortraitFrame");
				frame.removeClass("InactivePortraitFrame");

				if (this.#enemies[i].isDead) {
					frame.css("opacity", 0.5);
					frame.addClass("InactivePortraitFrame");
					frame.removeClass("EnemySelectEnabled");
					if (this.#enemies[i] == this.#target) this.#target = null;
					frame.off("click");
				} else {
					if (this.#enemies[i] == this.#target) {
						frame.addClass("ActivePortraitFrame");
					} else {
						frame.addClass("InactivePortraitFrame");
					}
				}
			}
		}

		private _updateHPBar(obj: Combatant) {
			const element = this.#hpBars[obj.id];
			const x = Math.ceil(190 * (obj.health / obj.mMaxHealth));
			element.css('width', `${x}px`);
		}

		private _updateStaminaBar(obj: Combatant) {
			const element = this.#staminaBars[obj.id];
			const x = Math.ceil(190 * (obj.stamina / obj.maxStamina));
			element.css('width', `${x}px`);
		}

		private _updatePlayerStaminaBar() {
			assertIsDefined(this.#player);
			const element = $('#PlayerStaminaBar');
			const x = Math.ceil(120 * (this.#player.stamina / this.#player.maxStamina));
			element.css('width', `${x}px`);
		}

		private _updatePlayerComboBar() {
			assertIsDefined(this.#player);
			const element = $('#PlayerComboBar');
			const x = Math.ceil(120 * (this.#player.combo / this.#player.maxCombo));
			element.css('width', `${x}px`);
		}

		private _drawInitiativeBar() {
			this._generateTimeLine(); // Generate data structure

			const root = $('#InitiativeBar');
			root.empty(); // Clear all elements that already exist.

			// Title
			root.append($('<div>').addClass('InitiativePortrait').text('TURN'));

			const max = this.#timeline.length >= this.#UI_MaxInitiative ? this.#UI_MaxInitiative : this.#timeline.length;
			// Create portrait frames.
			for (let i = 0; i < max; i++) {
				const frame = $('<div>').attr('id', `InitiativePortrait${i}`).addClass('InitiativePortrait');

				if (i == 0) {
					frame.addClass('ActiveInitiativeFrame');
				} else {
					frame.addClass('InactiveInitiativeFrame');
				}
				frame.text(this.#timeline[i].name);
				root.append(frame);
			}
		}
		// OTHER INTERNAL METHODS

		private _addChat(m: string) {
			m = this._formatMessage(m);
			this.#chatLog.push(m);
		}

		private _formatMessage(m: string, o?: Combatant) {
			const enemy = o ?? this.#enemies[0];

			const npc = setup.world.npc("Dummy"); // Fake NPC
			npc.data.name = enemy.name; // Swapsie name
			// Custom replacer(s)
			m = m.replace(/(ENEMY_([0-9]+))/g, (_m, _f, n: string) => `<span class='npc'>${this.#enemies[parseInt(n)].name}</span>`);
			m = m.replace(/NPC_PRONOUN/g,  () => enemy.gender == 1 ? "his" : enemy.gender == -1 ? "its" : "her");

			m = PR.tokenizeString(setup.player, npc, m);
			return m;
		}

		private _writeMessage(m: string, o?: Combatant) {
			m = this._formatMessage(m, o);
			this.#chatLog.push(m);
			const log = $('#CombatChatLog');
			log.append("<P class='ChatLog'>" + m + "</P>");
			// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
			log.animate({scrollTop: log.prop("scrollHeight")}, 500);
		}

		/**
		 * Add an enemy object to the encounter.
		 * @param e enemy object data
		 */
		private _addEnemy(e: string) {
			const d = Combat.enemyData[e];
			const enemy = new Combat.Combatant(d,
				this._updateNPCStatusCB.bind(this),
				this._updatePlayerStatusCB.bind(this),
				this._chatLogCB.bind(this));
			enemy.id = `ENEMY_${this.#enemies.length}`;
			this.#enemies.push(enemy);
		}

		/**
		 * Add the player to the encounter.
		 */
		private _addPlayer(p: Entity.Player) {
			const data: Data.Combat.Enemy = {
				name: p.slaveName,
				title: 'NAME',
				health: p.getStat(Stat.Core, CoreStat.Health),
				maxHealth: 100,
				energy: p.getStat(Stat.Core, CoreStat.Energy),
				maxStamina: 100,
				stamina: 100,
				speed: 50,
				moves: 'UNARMED',
				// FIXME Player should not inherit base enemy
				attack: -1,
				defense: -1,
				gender: NaN,
			};

			this.#player = new Combat.Player(p, data,
				this._updatePlayerStatusCB.bind(this),
				this._updateNPCStatusCB.bind(this),
				this._chatLogCB.bind(this),
				this._loseFight.bind(this),
				this._winFight.bind(this));
			this.#player.id = "PLAYER";

			if (this.#noWeapons == true && this.lastSelectedStyle == 'SWASHBUCKLING') {
				this._switchMoveSet('UNARMED');
			} else if (this.lastSelectedStyle == 'SWASHBUCKLING' && !this.#player.isEquipped(ClothingSlot.Weapon)) {
				this._switchMoveSet('UNARMED');
			} else {
				this._switchMoveSet(this.lastSelectedStyle);
			}
		}

		private _switchMoveSet(name: string) {
			assertIsDefined(this.#player);
			const moveSet = Combat.moves[name];
			this.#player.changeMoveSet(moveSet.engine, this._updatePlayerStatusCB.bind(this),
				this._updateNPCStatusCB.bind(this),
				this._chatLogCB.bind(this));
			this.lastSelectedStyle = name;
		}

		/**
		 * Creates an array of the predicted order of turns.
		 */
		private _generateTimeLine() {
			assertIsDefined(this.#player);

			this.#timeline = [];
			const timeline: Record<number, Combatant[]> = {};
			CombatEngine._populateTimeline(this.#player, timeline);
			for (let i = 0; i < this.#enemies.length; i++) {
				if (this.#enemies[i].isDead == true) continue;
				CombatEngine._populateTimeline(this.#enemies[i], timeline);
			}

			// Navigate timeline. Rely on the expected behavior of first set keys being retrieved first.
			// If this doesn't work then get keys as array, sort, navigate that. Dumb but whatever.
			let position = 0;
			for (const prop in timeline) {
				if (position >= this.#UI_MaxInitiative) break; // Abort out of range of the portraits.

				const arr = timeline[prop];
				for (let x = 0; x < arr.length; x++) {
					if (position >= this.#UI_MaxInitiative) break; // Abort nested loop
					this.#timeline.push(timeline[prop][x]);
					position++;
				}
			}
		}

		private static _populateTimeline(o: Combatant, timeline: Record<number, Combatant[]>) {
			const time = o.getTimeline(5); // Get 5 turns.
			for (let i = 0; i < time.length; i++) {
				const t = time[i];
				if (timeline.hasOwnProperty(t)) {
					timeline[t].push(o);
				} else {
					timeline[t] = [o];
				}
			}
		}

		private _startRound() {
			// Who's turn is it?
			if (this._getCombatant().isNPC) {
				this._enemyTurn();
			} else {
				this._playerTurn();
			}
		}

		private _nextRound() {
			this._drawInitiativeBar();
			this._startRound();
		}

		private _getCombatant() {
			return this.#timeline[0];
		}

		private _enemyTurn() {
			assertIsDefined(this.#player);

			this._getCombatant().startTurn();
			this._getCombatant().doAI(this.#player, this._chatLogCB.bind(this));
			this._getCombatant().endTurn();

			if (!this.#player.isDead)
				setTimeout(this._nextRound.bind(this), 500); // Don't attack if PC is defeated.
		}

		private _playerTurn() {
			assertIsDefined(this.#player);

			if (this.#player.isDead) return; // Don't do anything if defeated.

			if (this.#player.hasEffect('STUNNED')) {
				this._writeMessage("<span style='color:red'>You are stunned!</span>", this.#player);
				this._getCombatant().startTurn();
				this._getCombatant().endTurn();
				this._nextRound();
			}

			if (this.#enemies.filter(o => o.isDead == false).length == 0) {
				this._writeMessage("You have defeated all your foes!", this.#player);
				setTimeout(this._winFight.bind(this), 500);
			}
			// this._WriteMessage("It is your turn!", this.#player);
		}

		// CALLBACKS

		private _combatCommandHandler(e: JQuery.ClickEvent<unknown, {cmd: string}>) {
			if (this._getCombatant() !== this.#player) return;

			if (e.data.cmd == 'flee') {
				if (this.#flee == 0) {
					this._writeMessage("<span style='color:red'>You cannot flee!</span>", this.#player);
					return;
				}
				if (Math.floor(Math.random() * 100) > this.#flee) {
					this._writeMessage("<span style='color:red'>You attempt to flee, but fail!</span>", this.#player);
					this._getCombatant().startTurn();
					this._getCombatant().addWeaponDelay(10);
					this._getCombatant().endTurn();
					this._nextRound();
					return;
				} else {
					assertIsDefined(this.#encounterData);
					if (!_.isUndefined(this.#encounterData.fleeHandler))
						this.#encounterData.fleeHandler(this.#player.player, this.#encounterData);

					Engine.play(this.#fleePassage);
				}
				return;
			}

			if (e.data.cmd == 'defend') {
				this._getCombatant().startTurn();
				this._getCombatant().engine.defend();
				this._getCombatant().endTurn();
				this._nextRound();
				return;
			}

			if (e.data.cmd == 'restoreStamina') {
				if (SugarCube.setup.player.coreStats.energy <= 0) {
					this._writeMessage("<span style='color:red'>You do not have enough energy!</span>", this.#player);
					return;
				} else {
					this._getCombatant().startTurn();
					this._getCombatant().engine.recover();
					this._getCombatant().endTurn();
					this._nextRound();
					return;
				}
			}

			if (this.#target == null) {
				this._writeMessage("<span style='color:red'>Select a target first!</span>");
				return;
			}

			const command = this._getCombatant().moves[e.data.cmd];
			this._getCombatant().startTurn();
			this._getCombatant().engine.attackTarget(this.#target, command);
			this._getCombatant().endTurn();
			this._drawInitiativeBar();
			this._startRound();
		}

		private _combatStyleChangeHandler() {
			const list = $('#combatStyles');
			const style = list.val() as string;
			this._switchMoveSet(style);
			this._drawCombatButtons();
		}

		private _selectEnemyHandler(e: JQuery.ClickEvent<any, {index: number}>) {
			const obj = this.#enemies[e.data.index];
			if (obj.isDead != true) {
				this.#target = obj;
			}

			this._updateEnemyPortraits();
		}

		/**
		 * call back for writing to the chat log window
		 */
		private _chatLogCB(m: string, o: Combatant) {
			this._writeMessage(m, o);
		}

		private _updatePlayerStatusCB() {
			this._updatePlayerComboBar();
			this._updatePlayerStaminaBar();
			this._updateCombatButtons();
		}

		private _updateNPCStatusCB(npc: Combatant) {
			this._updateHPBar(npc);
			this._updateStaminaBar(npc);
			this._updateEnemyPortraits();
		}

		private _loseFight(player: Entity.Player) {
			assertIsDefined(this.#encounterData);
			const lh = this.#encounterData.loseHandler;
			if (lh)
				lh(player, this.#encounterData);

			if (this.duelMode == true) {
				player.coreStats.health = 10; // Don't kill them…
				Engine.play(this.losePassage);
			} else {
				// you ded.
				setup.eventEngine.passageOverride = this.losePassage;
				Engine.play(this.losePassage);
			}
		}

		private _winFight() {
			const loot = this.#encounterData?.loot;
			if (loot !== undefined) {
				// Generate loot.
				for (const l of loot) {
					this._grantLoot(l);
				}

				if (this.#lootBuffer.length > 0) {
					const lootMessage = this.#encounterData?.lootMessage;
					if (lootMessage !== undefined) {
						this._addChat("<span style='color:yellow'>" + lootMessage + "</span>");
					} else {
						this._addChat("<span style='color:yellow'>You find some loot…</span>")
					}

					for (const l of this.#lootBuffer)
						this._addChat("<span style='color:yellow'>&check; </span>" + l);
				}
			}

			if (this.#encounterData?.winHandler !== undefined)
				this.#encounterData.winHandler(Entity.Player.instance, this.#encounterData);

			Engine.play(this.winPassage);
		}

		private _grantLoot(data: Data.Combat.EncounterLoots.Any) {
			if (data.chance == 100 || data.chance <= Math.floor(Math.random() * 100)) {
				const amt = random(data.min, data.max);
				if (data.type == 'Coins') {
					this.#player?.player.earnMoney(amt, GameState.IncomeSource.Loot);
					this.#lootBuffer.push(`${amt} coins`);
				} else if (data.type == 'Random') {
					const item = Items.pickItem([Items.Category.Food, Items.Category.Drugs, Items.Category.Cosmetics], {price: amt});
					if (item != null) {
						this.#player?.player.addItem(item.cat, item.tag, 0);
						this.#lootBuffer.push(item.desc);
					}
				} else {
					// Specific loot.
					const item = this.#player?.player.addItem(data.type, data.tag, amt);
					if (item) {
						if (amt < 2) {
							this.#lootBuffer.push(item.description);
						} else {
							this.#lootBuffer.push(`${item.description} x${amt}`);
						}
					}
				}
			}
		}

		// Somewhat hackish stuff to support fight club
	}
}
