namespace App.Combat.Engines {
	// Generic engine with default behavior
	export abstract class Generic {
		#owner: Combatant;
		#myStatusCB: MyStatusCallback;
		#theirStatusCB: TheirStatusCallback;
		#chatLogCB: ChatLogCallback;
		#attackHistory: string[] = []; // TODO refactor the class to generic and take this as a parameter?

		constructor(owner: Combatant, myStatusCB: MyStatusCallback, theirStatusCB: TheirStatusCallback, chatLogCB: ChatLogCallback) {
			this.#owner = owner; // The object that owns this engine.
			this.#myStatusCB = myStatusCB; // Callback for updating a html component
			this.#theirStatusCB = theirStatusCB; // Callback for updating a html component
			this.#chatLogCB = chatLogCB; // Callback for updating an html component

			if (typeof this.#owner === 'undefined') throw new Error("You must associate this engine with an owner.");
		}

		get combatClass(): string {return "GENERIC";} // TODO refactor to take constructor parameter

		get owner(): Combatant {return this.#owner}

		get lastMove(): string | null {
			return this.#attackHistory.length >= 1 ? this.#attackHistory[this.#attackHistory.length - 1] : null;
		}

		/**
		 * Attack the enemy
		 */
		attackTarget(target: Combatant, command: Data.Combat.Move): boolean {
			const roll = this.calculateHit(target);

			// Try to hit target
			if (roll > 0) {
				this.owner.recoverCombo(this.generateCombo(target, command, roll));
				this.#attackHistory.push(command.name);
				this.consumeResources(command);
				this.applyEffects(target, command, roll);
				this.doDamage(target, command, roll);
				return true;
			} else {
				this.#attackHistory.push("Miss");
				this.consumeResources(command);
				const message = this.getMissMessage(command.miss);
				this.printMessage(message, target);
				return false;
			}
		}

		recover(): void {
			// use energy for stamina
			this.owner.useEnergy(1);
			this.owner.recoverStamina(100);
			this.owner.addWeaponDelay(10);
			if (this.owner.isNPC == true) {
				this.printMessage("<span style='color:lime'>NPC_NAME catches a second wind!</span>", this.owner);
			} else {
				this.printMessage("<span style='color:lime'>You pull deep from your reserves and catch a second wind!</span>", this.owner);
			}
		}

		defend(): void {
			this.owner.recoverStamina(10); // Regain some stamina
			this.owner.addEffect('GUARDED', 2);
			this.owner.addWeaponDelay(10);
			if (this.owner.isNPC == true) {
				this.printMessage("<span style='color:lime'>NPC_NAME assumes a defensive position.</span>", this.owner);
			} else {
				this.printMessage("<span style='color:lime'>You assume a defensive position and catch your breath.</span>", this.owner);
			}
		}

		abstract doAI(target: Combatant): void;

		consumeResources(command: Data.Combat.Move): void {
			this.owner.useStamina(command.stamina);
			this.owner.useCombo(command.combo);
			this.owner.addWeaponDelay(command.speed);
		}

		checkCommand(command: Data.Combat.Move): boolean {
			if (command.unlock(this.owner) == false) return false;
			if (command.stamina > this.owner.stamina) return false;
			if (command.combo > this.owner.combo) return false;

			return true;
		}

		/**
		 * Calculate if an attack hits.
		 * @param target  Entity that you are attacking
		 */
		calculateHit(target: Combatant): number {
			const myRoll = this.owner.attackRoll(); // Includes getting attack buffs
			const theirRoll = target.defenseRoll(); // Includes getting defense buffs
			return (myRoll - theirRoll);
		}

		/**
		* @param message Miss array from attack definition.
		* @param target  Object we are attacking.
		*/
		printMessage(message: string, target: Combatant): void {
			if (typeof this.#chatLogCB === 'function') {
				if (this.owner.isNPC == true) {
					this.#chatLogCB(message, this.owner);
				} else {
					this.#chatLogCB(message, target);
				}
			}
		}

		doDamage(target: Combatant, command: Data.Combat.Move, roll: number): void {
			let dmg = this.calculateDamage(target, command, roll);
			// Apply effect bonuses
			if (this.owner.hasEffect('BLOODTHIRST')) dmg = Math.ceil(dmg * 1.5);
			if (target.hasEffect('GUARDED')) dmg = Math.floor(dmg * 0.7);

			if (target.hasEffect('PARRY')) {
				dmg = 0; // block all damage.
				target.reduceEffect('PARRY', 1); // Reduce parry counter.
				this.#attackHistory.push("Miss"); // We missed. Sadface.
				if (this.owner.isNPC == true) {
					this.printMessage("You parry NPC_NAME's attack!", target);
				} else {
					this.printMessage("NPC_NAME parries your attack!", target);
				}
			} else {
				if (target instanceof Combat.Player) {
					const dmgMod = Math.min(target.player.getWornSkillBonus("damageResistance"), 90) / 100; // capped
					dmg = (dmg - Math.ceil(dmg * dmgMod));
				}
				this.printHit(command.hit, target, roll, dmg);
				target.takeDamage(dmg);
			}

			if (this.owner.hasEffect('LIFE LEECH')) {
				const heal = Math.ceil(dmg * 0.5);
				this.owner.recoverHealth(heal);
				if (this.owner.isNPC == true) {
					this.printMessage(`NPC_NAME heals <span style='color:lime'>${heal}</span> damage.`, target);
				} else {
					this.printMessage(`You heal <span style='color:lime'>${heal}</span> damage.`, target);
				}
			}
		}

		calculateDamage(_target: Combatant, _command: Data.Combat.Move, _roll: number): number {
			return 1;
		}

		applyEffects(_target: Combatant, _command: Data.Combat.Move, _roll: number): void {
			// no-op
		}

		generateCombo(_target: Combatant, _command: Data.Combat.Move, _roll: number): number {
			return 0;
		}

		printHit(attacks: string[][], target: Combatant, roll: number, damage: number): void {
			const len = Math.floor(Math.max(0, Math.min((attacks.length * roll), (attacks.length - 1))));
			let msg = (typeof this.owner.isNPC !== 'undefined' && this.owner.isNPC == true) ? attacks[len][1] : attacks[len][0];
			msg += ` <span style='color:red'>(${damage})</span>`;
			this.printMessage(msg, target);
		}

		/**
		 *
		 * @param arr Message to show to chat log for misses.
		 */
		getMissMessage(arr: string[][]): string {
			const missMessage = arr.randomElement();
			return (typeof this.owner.isNPC !== 'undefined' && this.owner.isNPC == true) ? missMessage[1] : missMessage[0];
		}
	}

	/**
	 * Unarmed combat class
	 */
	export class Unarmed extends Generic {
		override get combatClass(): string {return "UNARMED";}

		/**
		 * Calculate the damage of an unarmed attack
		 * @param target
		 * @param command
		 * @param Roll
		 * @returns Damage
		 */
		override calculateDamage(target: Combatant, command: Data.Combat.Move, _roll: number): number {
			let base = 1;

			const owner = this.owner;
			if (owner instanceof Combat.Player) {
				base = 1 + (Math.random() * 3) + Math.max(1, Math.min((owner.player.getStat(Stat.Core, CoreStat.Fitness) / 25), 4));
				base = Math.floor(base);
			} else {
				base = base + Math.floor(owner.attack / 20);
			}

			if (command.name != "Knee") {
				base = Math.floor(base * command.damage); // Add damage mod
			} else {
				const mod = target.gender == 1 ? 4.0 : 2.0; // Knee attack does more damage on male enemies
				base = Math.floor(base * mod);
			}

			return base;
		}

		/**
		 * Generate any combo points
		 * @returns number of combo points to grant
		 */
		override generateCombo(_target: Combatant, command: Data.Combat.Move, _roll: number): number {
			if ((command.name == "Punch" && this.lastMove == "Kick") ||
				(command.name == "Kick" && this.lastMove == "Punch")) {
				return 1;
			}

			return 0;
		}

		/**
		 * Apply effects to enemy
		 * @param target
		 * @param command
		 * @param Roll
		 */
		override applyEffects(target: Combatant, command: Data.Combat.Move, roll: number): void {
			if (command.name == 'Haymaker') {
				const chance = Math.max(10, Math.min((100 * roll), 100));
				if (chance >= Math.floor(Math.random() * 100)) {
					target.addEffect('STUNNED', 2);
					if (this.owner.isNPC) {
						this.printMessage("<span style='color:yellow'>NPC_NAME stuns you!</span>", target);
					} else {
						this.printMessage("<span style='color:yellow'>You stun NPC_NAME!</span>", target);
					}
				}
			}
		}

		override doAI(target: Combatant): void {
			if (this.owner.combo >= this.owner.moves["Knee"].combo) {
				this.attackTarget(target, this.owner.moves["Knee"]);
			} else if (this.owner.combo >= this.owner.moves["Haymaker"].combo && Math.floor(Math.random() * 100) >= 60) {
				this.attackTarget(target, this.owner.moves["Haymaker"]);
			} else if (this.lastMove == "Kick") {
				this.attackTarget(target, this.owner.moves["Punch"]);
			} else {
				this.attackTarget(target, this.owner.moves["Kick"]);
			}
		}
	}

	/**
	 * Swashbuckling Class
	 */
	export class Swashbuckling extends Generic {
		override get combatClass(): string {return "SWASHBUCKLING";}

		/**
		 * Calculate the damage of an swashbuckling attack
		 * @returns Damage
		 */
		 override calculateDamage(target: Combatant, command: Data.Combat.Move, _roll: number): number {
			let base = 1;
			const owner = this.owner;
			if (owner instanceof Combat.Player) { // TODO replace with visitors?
				const weaponQuality = owner.getWeaponQuality();
				const bonus = owner.getWeaponBonus();
				const skill = owner.attack;
				const fitness = owner.player.getStat(Stat.Core, CoreStat.Fitness);
				const mod = 1 + (skill / 100) + (fitness / 100);
				base = Math.ceil(weaponQuality * mod) + bonus;
			} else {
				base += Math.floor(owner.attack / 10);
			}

			if (command.name == 'Riposte') { // Converts combo points into extra damage.
				// Drain all combo points.
				const combo = this.owner.combo;
				this.owner.useCombo(combo);

				base = base + (combo * 2); // bonus base damage from combo points.
			}

			if (command.name == 'Behead') { // Chance to do massive damage against enemies at low health
				if (target.health / target.mMaxHealth < 0.5) {
					const chance = (65 - Math.floor((100 * (target.health / target.mMaxHealth))));
					if (chance >= Math.floor(Math.random() * 100)) {
						base = target.health;
					}
				}
			}

			base = Math.floor(base * command.damage); // Add damage mod

			return base;
		}

		/**
		 * Generate any combo points
		 * @returns number of combo points to grant
		 */
		 override generateCombo(_target: Combatant, command: Data.Combat.Move, _roll: number): number {
			if (
				(command.name == "Slash" && this.lastMove == "Stab") ||
				(command.name == "Stab" && this.lastMove == "Slash") ||
				(command.name == 'Slash' && this.lastMove == 'Riposte') ||
				(command.name == 'Parry')
			) {
				return 1;
			}

			return 0;
		}

		/**
		 * Apply effects to enemy
		 */
		 override applyEffects(_target: Combatant, command: Data.Combat.Move, _roll: number): void {
			if (command.name == 'Parry') {
				this.owner.addEffect('GUARDED', 2);
				this.owner.addEffect('PARRY', 2);
			} else if (command.name == 'Stab' && this.lastMove == 'Riposte') {
				this.owner.addEffect('SEEKING', 3);
			}
		}

		override doAI(target: Combatant): void {
			if (this.owner.combo >= this.owner.moves["Behead"].combo) {
				this.attackTarget(target, this.owner.moves["Behead"]);
			} else if (this.owner.combo >= this.owner.moves["Cleave"].combo && Math.floor(Math.random() * 100) >= 60) {
				this.attackTarget(target, this.owner.moves["Cleave"]);
			} else if (this.owner.combo >= this.owner.moves["Riposte"].combo && this.lastMove == 'Parry') {
				this.attackTarget(target, this.owner.moves["Riposte"]);
			} else if ((this.lastMove != 'Riposte' && this.lastMove != 'Parry') && Math.floor(Math.random() * 100) >= 70) {
				this.attackTarget(target, this.owner.moves['Parry']);
			} else if (this.lastMove == "Stab") {
				this.attackTarget(target, this.owner.moves["Slash"]);
			} else {
				this.attackTarget(target, this.owner.moves["Stab"]);
			}
		}
	}

	/**
	 * Boob-jitsu Class
	 * */
	export class Boobjitsu extends Generic {
		override get combatClass(): string {return "BOOBJITSU";}

		override calculateDamage(_target: Combatant, command: Data.Combat.Move, _roll: number): number {
			const base = Math.floor(this.owner.bust / 10) + Math.floor(this.owner.attack / 10);
			return Math.floor(base * command.damage); // Add damage mod
		}

		/**
		 * Generate any combo points
		 * @returns number of combo points to grant
		 */
		 override generateCombo(_target: Combatant, command: Data.Combat.Move, _roll: number): number {
			if ((command.name == "Jiggle" && this.lastMove == "Wobble") ||
				(command.name == "Wobble" && this.lastMove == "Jiggle")) {
				return 1;
			}

			if (command.name == 'Bust Out' &&
				(this.lastMove == 'Twirl' || this.lastMove == 'Boob Quake' || this.lastMove == 'Titty Twister')) {
				return 2;
			}

			return 0;
		}

		/**
		 * Apply effects to enemy
		 */
		 override applyEffects(target: Combatant, command: Data.Combat.Move, _roll: number): void {
			if (command.name == 'Jiggle') {
				target.addEffect('BLINDED', 3);
			}

			if (command.name == 'Wobble') {
				this.owner.addEffect('BLOODTHIRST', 3);
			}

			if (command.name == 'Bust Out') {
				this.owner.addEffect('SEEKING', 1);
			}

			if (command.name == 'Twirl') {
				this.owner.addEffect('LIFE LEECH', 1);
			}

			if (command.name == 'Boob Quake') {
				if (Math.floor(100 * Math.random()) <= (this.owner.attack / 2)) {
					target.addEffect('STUNNED', 3);
					if (this.owner.isNPC) {
						this.printMessage("<span style='color:yellow'>NPC_NAME stuns you!</span>", target);
					} else {
						this.printMessage("<span style='color:yellow'>You stun NPC_NAME!</span>", target);
					}
				}
			}

			if (command.name == 'Titty Twister') {
				if (Math.floor(100 * Math.random()) <= this.owner.attack) {
					target.addEffect('STUNNED', 4);
					if (this.owner.isNPC) {
						this.printMessage("<span style='color:yellow'>NPC_NAME stuns you!</span>", target);
					} else {
						this.printMessage("<span style='color:yellow'>You stun NPC_NAME!</span>", target);
					}
				}
			}
		}

		override doAI(target: Combatant): void {
			if (this.owner.combo >= this.owner.moves["Titty Twister"].combo) {
				this.attackTarget(target, this.owner.moves["Titty Twister"]);
			} else if (this.owner.combo >= this.owner.moves["Boob Quake"].combo && Math.floor(Math.random() * 100) >= 60) {
				this.attackTarget(target, this.owner.moves["Boob Quake"]);
			} else if (this.owner.combo >= this.owner.moves["Twirl"].combo && Math.floor(Math.random() * 100) >= 80) {
				this.attackTarget(target, this.owner.moves["Twirl"]);
			} else if (this.lastMove == 'Titty Twister' || this.lastMove == 'Boob Quake' || this.lastMove == 'Twirl') {
				this.attackTarget(target, this.owner.moves['Bust Out']);
			} else if (this.lastMove == "Wobble") {
				this.attackTarget(target, this.owner.moves["Jiggle"]);
			} else {
				this.attackTarget(target, this.owner.moves["Wobble"]);
			}
		}
	}

	/**
	 * Ass-fu Class
	 */
	export class Assfu extends Generic {
		override get combatClass(): string {return "ASSFU";}

		override calculateDamage(_target: Combatant, command: Data.Combat.Move, _roll: number): number {
			const base = Math.floor(this.owner.ass / 10) + Math.floor(this.owner.attack / 10);
			return Math.floor(base * command.damage); // Add damage mod
		}

		/**
		 * Generate any combo points
		 * @returns number of combo points to grant
		 */
		 override generateCombo(_target: Combatant, command: Data.Combat.Move, _roll: number): number {
			if ((command.name == "Shake It" && this.lastMove == "Booty Slam") ||
				(command.name == "Booty Slam" && this.lastMove == "Shake It")) {
				return 1;
			}

			if (command.name == 'Twerk' &&
				(this.lastMove == 'Ass Quake' || this.lastMove == 'Thunder Buns' || this.lastMove == 'Buns of Steel')) {
				return 2;
			}

			return 0;
		}

		/**
		 * Apply effects to enemy
		 */
		 override applyEffects(target: Combatant, command: Data.Combat.Move, _roll: number): void {
			if (command.name == 'Shake It') {
				target.addEffect('BLINDED', 3);
			}

			if (command.name == 'Twerk') {
				this.owner.addEffect('DODGING', 3);
			}

			if (command.name == 'Ass Quake') {
				if (Math.floor(100 * Math.random()) <= (this.owner.attack / 3)) {
					target.addEffect('STUNNED', 3);
					if (this.owner.isNPC) {
						this.printMessage("<span style='color:yellow'>NPC_NAME stuns you!</span>", target);
					} else {
						this.printMessage("<span style='color:yellow'>You stun NPC_NAME!</span>", target);
					}
				}
			}

			if (command.name == 'Thunder Buns') {
				if (Math.floor(100 * Math.random()) <= this.owner.attack / 2) {
					target.addEffect('STUNNED', 4);
					if (this.owner.isNPC) {
						this.printMessage("<span style='color:yellow'>NPC_NAME stuns you!</span>", target);
					} else {
						this.printMessage("<span style='color:yellow'>You stun NPC_NAME!</span>", target);
					}
				}
			}

			if (command.name == 'Buns of Steel') {
				if (Math.floor(100 * Math.random()) <= this.owner.attack) {
					target.addEffect('STUNNED', 4);
					if (this.owner.isNPC) {
						this.printMessage("<span style='color:yellow'>NPC_NAME stuns you!</span>", target);
					} else {
						this.printMessage("<span style='color:yellow'>You stun NPC_NAME!</span>", target);
					}
				}

				this.owner.addEffect('GUARDED', 4);
			}
		}

		override doAI(target: Combatant): void {
			if (this.owner.combo >= this.owner.moves["Buns of Steel"].combo) {
				this.attackTarget(target, this.owner.moves["Buns of Steel"]);
			} else if (this.owner.combo >= this.owner.moves["Thunder Buns"].combo && Math.floor(Math.random() * 100) >= 60) {
				this.attackTarget(target, this.owner.moves["Thunder Buns"]);
			} else if (this.owner.combo >= this.owner.moves["Ass Quake"].combo && Math.floor(Math.random() * 100) >= 80) {
				this.attackTarget(target, this.owner.moves["Ass Quake"]);
			} else if (this.lastMove == 'Buns of Steel' || this.lastMove == 'Thunder Buns' || this.lastMove == 'Ass Quake') {
				this.attackTarget(target, this.owner.moves['Twerk']);
			} else if (this.lastMove == "Booty Slam") {
				this.attackTarget(target, this.owner.moves["Shake It"]);
			} else {
				this.attackTarget(target, this.owner.moves["Booty Slam"]);
			}
		}
	}

	// MONSTER CLASSES
	// ====================================================

	// Kraken
	export class Kraken extends Generic {
		override get combatClass(): string {return "KRAKEN";}

		/**
		 * Calculate the damage of an unarmed attack
		 * @returns Damage
		 */
		override calculateDamage(_target: Combatant, command: Data.Combat.Move, _roll: number): number {
			if (command.name == 'Ejaculate1' || command.name == 'Ejaculate2') return 0;

			return Math.ceil(Math.max(2, (5 * Math.random())) * command.damage);
		}

		/**
		 * Generate any combo points
		 * @returns number of combo points to grant
		 */
		 override generateCombo(_target: Combatant, _command: Data.Combat.Move, _roll: number): number {
			return 0;
		}

		/**
		 * Apply effects to enemy
		 */
		 override applyEffects(target: Combatant, command: Data.Combat.Move, _roll: number): void {
			if (target instanceof Combat.Player) { // No effects on non-player characters.
				const player = target.player;
				if (command.name == 'Grab') {
					this.owner.addEffect('SEEKING', 4);
				} else if (command.name == 'Ejaculate1') {
					player.adjustStatXP(CoreStat.Perversion, 20);
					player.adjustStatXP(CoreStat.Willpower, -20);
					player.adjustBodyXP(BodyPart.Lips, 100);
					player.adjustStatXP(CoreStat.Hormones, 100);
					player.adjustBodyXP(BodyPart.Bust, 100);
				} else if (command.name == 'Ejaculate2') {
					player.adjustStatXP(CoreStat.Perversion, 20);
					player.adjustStatXP(CoreStat.Willpower, -20);
					player.adjustBodyXP(BodyPart.Hips, 100);
					player.adjustStatXP(CoreStat.Hormones, 100);
					player.adjustBodyXP(BodyPart.Ass, 100);
				}
			}
		}

		override doAI(target: Combatant): void {
			if (this.owner.stamina < 30 && (50 >= (100 * Math.random()))) {
				this.defend();
				return;
			}

			switch (this.lastMove) {
				case 'Ejaculate2':
				case 'Ejaculate1':
					this.attackTarget(target, this.owner.moves["Strangle"]);
					break;
				case 'Ass':
					this.attackTarget(target, this.owner.moves['Ejaculate2']);
					break;
				case 'Mouth':
					this.attackTarget(target, this.owner.moves["Ejaculate1"]);
					break;
				case 'Strangle':
					if (100 * Math.random() >= 50) {
						this.attackTarget(target, this.owner.moves['Ass']);
					} else {
						this.attackTarget(target, this.owner.moves['Mouth']);
					}
					break;
				case 'Grab':
					this.attackTarget(target, this.owner.moves['Strangle']);
					break;
				default:
					this.attackTarget(target, this.owner.moves['Grab']);
			}
		}
	}

	/**
	 * Siren
	 */
	export class Siren extends Generic {
		override get combatClass(): string {return "SIREN";}

		/**
		 * Calculate the damage of an unarmed attack
		 * @returns Damage
		 */
		 override calculateDamage(_target: Combatant, command: Data.Combat.Move, _roll: number): number {
			return Math.ceil(Math.max(1, ((this.owner.attack / 10) * Math.random())) * command.damage);
		}

		/**
		 * Generate any combo points
		 * @returns number of combo points to grant
		 */
		 override generateCombo(_target: Combatant, command: Data.Combat.Move, _roll: number): number {
			if ((command.name == 'Touch' && this.lastMove == 'Toss') ||
				(command.name == 'Toss' && this.lastMove == 'Touch') ||
				(this.lastMove == 'Miss')) {
				return 1;
			}
			return 0;
		}

		/**
		 * Apply effects to enemy
		 */
		 override applyEffects(target: Combatant, command: Data.Combat.Move, _roll: number): void {
			if (target.isNPC == true) return; // No effects on non-player characters.

			if (command.name == 'Scream') {
				target.addEffect('STUNNED', 2);
				this.owner.addEffect('BLOODTHIRST', 2);
				if (this.owner.isNPC) {
					this.printMessage("<span style='color:yellow'>NPC_NAME stuns you!</span>", target);
				} else {
					this.printMessage("<span style='color:yellow'>You stun NPC_NAME!</span>", target);
				}
			} else if (command.name == 'Drown') {
				target.addEffect('BLINDED', 3);
			}
		}

		override doAI(target: Combatant): void {
			if (this.owner.combo >= this.owner.moves["Drown"].combo) {
				this.attackTarget(target, this.owner.moves["Drown"]);
			} else if (this.owner.combo >= this.owner.moves["Scream"].combo && Math.floor(Math.random() * 100) >= 75) {
				this.attackTarget(target, this.owner.moves["Scream"]);
			} else if (this.lastMove == "Toss") {
				this.attackTarget(target, this.owner.moves["Touch"]);
			} else {
				this.attackTarget(target, this.owner.moves["Toss"]);
			}
		}
	}

	/**
	 * Boobpire
	 */
	export class Boobpire extends Generic {
		override get combatClass(): string {return "BOOBPIRE";}

		/**
		 * Calculate the damage of an unarmed attack
		 * @returns Damage
		 */
		 override calculateDamage(_target: Combatant, command: Data.Combat.Move, _roll: number): number {
			return Math.ceil(Math.max(1, ((this.owner.attack / 10) * Math.random())) * command.damage);
		}

		/**
		 * Generate any combo points
		 * @returns number of combo points to grant
		 */
		 override generateCombo(_target: Combatant, command: Data.Combat.Move, _roll: number): number {
			if ((command.name == 'Touch' && this.lastMove == 'Toss') ||
				(command.name == 'Toss' && this.lastMove == 'Touch')) {
				return 1;
			}

			return 0;
		}

		/**
		 * Apply effects to enemy
		 */
		 override applyEffects(target: Combatant, command: Data.Combat.Move, _roll: number): void {
			if (target instanceof Combat.Player) { // No effects on non-player characters.
				if (command.name == 'Bite') {
					target.addEffect('STUNNED', 2);
					this.owner.addEffect('BLOODTHIRST', 2);
					if (this.owner.isNPC) {
						this.printMessage("<span style='color:yellow'>NPC_NAME stuns you!</span>", target);
					} else {
						this.printMessage("<span style='color:yellow'>You stun NPC_NAME!</span>", target);
						this.printMessage("<span style='color:red'>Your chest feels hot!</span>", target);
						// Drain breast xp
						target.player.adjustBodyXP(BodyPart.Bust, Math.ceil((50 * Math.random()) * -1.0));
					}
				}
			}
		}

		override doAI(target: Combatant): void {
			if (this.owner.combo >= this.owner.moves["Claw"].combo) {
				this.attackTarget(target, this.owner.moves["Claw"]);
			} else if (this.owner.combo >= this.owner.moves["Bite"].combo && Math.floor(Math.random() * 100) >= 50) {
				this.attackTarget(target, this.owner.moves["Bite"]);
			} else if (this.lastMove == "Toss") {
				this.attackTarget(target, this.owner.moves["Touch"]);
			} else {
				this.attackTarget(target, this.owner.moves["Toss"]);
			}
		}
	}

	export interface EngineConstructor {
		new(combatant: Combatant, myStatusCB: MyStatusCallback, theirStatusCB: TheirStatusCallback, chatLogCB: ChatLogCallback): Generic;
	}
}
