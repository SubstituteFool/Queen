namespace App.Combat {

	export type MyStatusCallback = (combatant: Combatant) => void;
	export type TheirStatusCallback = (combatant: Combatant) => void;
	export type ChatLogCallback = (message: string, combatabt: Combatant) => void;

	/**
	 * Base combatant class, used by NPC's and inherited by the player class.
	 */
	export class Combatant {
		#data: Data.Combat.Enemy;
		#name: string;
		#title: string;
		#portrait: string;

		#engine!: Engines.Generic;
		// Number of turns taken
		#turn = 1;
		// Current delay due to last attack speed
		#weaponDelay = 0;

		#id!: string;
		// Combo meter - hidden on enemies
		#combo = 0;

		// Is this object alive or dead?
		#dead = false;

		// FIXME replace with static type App.Combat.Effect
		#allowedEffects: Effect[] = ['STUNNED', 'BLINDED', 'GUARDED', 'DODGING', 'BLOODTHIRST', 'SEEKING', 'LIFE LEECH', 'PARRY']

		#effects: Partial<Record<Effect, number>> = {};

		#myStatus!: MyStatusCallback;
		#chatLog!: ChatLogCallback;

		constructor(data: Data.Combat.Enemy, myStatusCB: MyStatusCallback, theirStatusCB: TheirStatusCallback, chatLogCB: ChatLogCallback) {
			this.#data = clone(data); // don't mangle our dictionary
			// Naming
			if (this.#data.name.includes('RANDOM_MALE_NAME')) {
				this.#name = this.#data.name.replace(/RANDOM_MALE_NAME/g, Data.names.Male.randomElement());
			} else if (this.#data.name.includes('RANDOM_FEMALE_NAME')) {
				this.#name = this.#data.name.replace(/RANDOM_FEMALE_NAME/g, Data.names.Female.randomElement());
			} else {
				this.#name = this.#data.name;
			}
			this.#title = this.#data.title.replace(/NAME/g, this.#name);

			this.#portrait = (this.#data.portraits ?? ['pugilist_a']).randomElement();

			this.changeMoveSet(Combat.moves[this.#data.moves].engine ?? Combat.Engines.Generic, myStatusCB, theirStatusCB, chatLogCB);
		}

		get name(): string {return this.#name;}
		get portrait(): string {return this.#portrait;}
		get id(): string {return this.#id;}
		set id(n: string) {this.#id = n;}
		get title(): string {return this.#title;}
		get health(): number {return this.#data.health;}
		get mMaxHealth(): number {return this.#data.maxHealth;}
		get maxEnergy(): number {return 10;} // Hardcoded - same as player max energy.
		get energy(): number {return this.#data.energy;}
		get maxCombo(): number {return 10;} // Hardcoded
		get combo(): number {return this.#combo;}
		// get Skill() {return this.#data.Skill;}

		get bust(): number {
			return this.#data.bust ?? this.attack;
		}

		get ass(): number {
			return this.#data.ass ?? this.attack;
		}

		get engine(): Engines.Generic {return this.#engine;}
		get moves(): Record<string, Data.Combat.Move> {return Combat.moves[this.engine.combatClass].moves;}
		/** @deprecated by definition, type fields may not exist */
		get isNPC(): boolean {return true;}
		get isDead(): boolean {return this.#dead;}
		get maxStamina(): number {return this.#data.maxStamina;}
		get stamina(): number {return this.#data.stamina;}
		get speed(): number {return this.#data.speed;}
		get weaponDelay(): number {return this.#weaponDelay;}
		get turn(): number {return this.#turn;}
		get gender(): Gender {return this.#data.gender;}
		get attack(): number {return this.#data.attack;}
		get defense() : number{return this.#data.defense;}
		get allowedEffects(): Effect[] {return this.#allowedEffects;}
		get effects(): Partial<Record<Effect, number>> {return this.#effects;}

		hasEffect(e: Combat.Effect): boolean {return this.#effects.hasOwnProperty(e);}

		/**
		 * Add a valid effect
		 */
		addEffect(e: Effect, d: number): void {
			if (this.allowedEffects.includes(e) == true) {
				this.#effects[e] = Math.abs(this.#effects[e] ?? 0) + d;
			}
		}

		/**
		 * Reduce an effect
		 */
		reduceEffect(e: Effect, d: number): void {
			if (this.#effects.hasOwnProperty(e)) {
				this.#effects[e] = (this.#effects[e] ?? 0) - d;
				if (this.#effects[e] as number <= 0) {
					delete this.#effects[e];
				}
			}
		}

		takeDamage(n: number): void {
			this.#data.health -= n;
			if (this.#data.health <= 0) {
				this.#dead = true;
				this.#chatLog("NPC_NAME is defeated!", this);
			}
			this.#myStatus(this);
		}

		recoverHealth(n: number): void {
			this.#data.health += n;
			if (this.#data.health >= this.mMaxHealth) this.#data.health = this.mMaxHealth;
			this.#myStatus(this);
		}

		useEnergy(n: number): void {
			this.#data.energy -= n;
			this.#myStatus(this);
		}

		recoverEnergy(n: number): void {
			this.#data.energy += n;
			if (this.#data.energy >= this.maxEnergy) this.#data.energy = this.maxEnergy;
			this.#myStatus(this);
		}

		useStamina(n: number): void {
			this.#data.stamina -= n;
			this.#myStatus(this);
		}

		recoverStamina(n: number): void {
			this.#data.stamina += n;
			if (this.#data.stamina >= this.maxStamina) this.#data.stamina = this.maxStamina;
			this.#myStatus(this);
		}

		useCombo(n: number): void {
			this.#combo -= n;
			this.#myStatus(this);
		}

		recoverCombo(n: number): void {
			this.#combo += n;
			if (this.#combo >= this.maxCombo) this.#combo = this.maxCombo;
			this.#myStatus(this);
		}

		/**
		 * Call this after using attack, but before ending turn.
		 */
		addWeaponDelay(n: number): void {
			this.#weaponDelay += n;
		}

		startTurn(): void {
			this.#weaponDelay = 0;
		}

		endTurn(): void {
			this.#turn += 1;
			for (const k of Object.keys(this.effects)) {
				this.reduceEffect(k, 1);
			}
		}

		/**
		 * Get the speed for the entity at the current turn, or optional turn in the future
		 * @param n The number of turns to predict.
		 */
		getTimeline(n: number): number[] {
			const arr: number[] = [];
			for (let i = 1; i <= n; i++) {
				const time = (this.speed * (this.turn + i)) + this.weaponDelay;
				arr.push(time);
			}

			return arr;
		}

		/**
		 *
		 * @param mine My skill value
		 * @param difficulty to check against
		 * @param mod bonus / penalty to roll
		 * @return Floating point between 0.1 and 2.0 indicating value of roll.
		 */
		skillRoll(mine: number, difficulty: number, mod: number): number {
			const targetRoll = (100 - Math.max(5, Math.min((50 + (mine - difficulty)), 95)));
			const diceRoll = Math.floor(Math.random() * 100) + mod;

			return Math.max(0.1, Math.min((diceRoll / targetRoll), 2.0)); // Default same as player object
		}

		/**
		 * Simulate an attack roll.
		 * @returns roll - floating point between 0.1 and 2.0
		 */
		attackRoll(): number {
			const attack = this.attack;
			let mod = 0;

			if (this.hasEffect("BLINDED")) mod -= 30;
			if (this.hasEffect("SEEKING")) mod += 30;

			// TODO: Put some checks here to get values from effects
			return this.skillRoll(attack, 100, mod);
		}

		/**
		 * Simulate a defensive roll.
		 * @returns roll - floating point between 0.1 and 2.0
		 */
		defenseRoll(): number {
			const def = this.defense;
			let mod = 0;

			if (this.hasEffect("STUNNED")) mod -= 30;
			if (this.hasEffect("DODGING")) mod += 30;

			// TODO: Put some checks here to get values from effects
			return this.skillRoll(def, 100, mod);
		}

		/**
		 * Generally only used by players to switch their attack style on the fly.
		 * Called from CombatEngine
		 * @param engine reference to personal engine class
		 * @param myStatusCB callback to update my status elements in ui
		 * @param theirStatusCB callback to update enemy status elements in ui
		 * @param chatLogCB callback to update chat log in ui
		 */
		changeMoveSet(engine: Engines.EngineConstructor, myStatusCB: MyStatusCallback, theirStatusCB: TheirStatusCallback, chatLogCB: ChatLogCallback): void {
			// Callbacks
			this.#myStatus = myStatusCB;
			this.#chatLog = chatLogCB;
			// Moveset and personal combat engine
			this.#engine = new engine(this, myStatusCB, theirStatusCB, chatLogCB);
		}

		doAI(target: Combatant, chatLogCB: ChatLogCallback): void {
			if (this.hasEffect("STUNNED")) {
				chatLogCB("NPC_NAME looks dazed and fumbles around.", this);
				return;
			}

			if (this.stamina < 10) {
				if (this.energy > 0) {
					this.engine.recover();
				} else {
					this.engine.defend();
				}
				return;
			}

			// Call Engine for Specific routine.
			this.engine.doAI(target);
		}

		protected die(): void {
			this.#dead = true;
		}
	}

	type LooseHandler = (player: Entity.Player) => void;
	type WinHandler = (player: Entity.Player) => void;

	/**
	 * Player combatant class. Just a few tweaks from the base.
	 */
	export class Player extends Combatant {
		#player: Entity.Player;
		#loseHandler: LooseHandler;
		#winHandler: WinHandler;

		constructor(player: Entity.Player, data: Data.Combat.Enemy, myStatusCB: MyStatusCallback, theirStatusCB: TheirStatusCallback, chatLogCB: ChatLogCallback, loseHandler: LooseHandler, winHandler: WinHandler) {
			super(data, myStatusCB, theirStatusCB, chatLogCB);
			this.#player = player;
			this.#loseHandler = loseHandler;
			this.#winHandler = winHandler;
		}

		override get name(): string {return this.#player.slaveName;}
		get player(): Entity.Player {return this.#player;}
		override get isNPC(): boolean {return false;}

		get availableMoveset() {
			const a: Record<string, string> = {UNARMED: "Unarmed"};

			if (this.#player.isEquipped(ClothingSlot.Weapon, true)) a["SWASHBUCKLING"] = "Swashbuckling";
			if (this.#player.getStat(Stat.Skill, Skills.Sexual.BoobJitsu) > 0) a["BOOBJITSU"] = "Boob-Jitsu";
			if (this.#player.getStat(Stat.Skill, Skills.Sexual.AssFu) > 0) a["ASSFU"] = "Ass-Fu";

			return a;
		}

		// Player attack depends on what skill they are using.
		override get attack(): number {
			let val = 0;

			switch (this.engine.combatClass) {
				case 'SWASHBUCKLING':
					val = this.player.getStat(Stat.Skill, Skills.Piracy.Swashbuckling);
					val += this.player.rollBonus(Stat.Skill, Skills.Piracy.Swashbuckling);
					break;
				case 'BOOBJITSU':
					val = this.player.getStat(Stat.Skill, Skills.Sexual.BoobJitsu);
					val += this.player.rollBonus(Stat.Skill, Skills.Sexual.BoobJitsu);
					break;
				case 'ASSFU':
					val = this.player.getStat(Stat.Skill, Skills.Sexual.AssFu);
					val += this.player.rollBonus(Stat.Skill, Skills.Sexual.AssFu);
					break;
				default:
					val = 20;
					val += Math.floor(this.player.getStat(Stat.Core, CoreStat.Fitness) * 0.8); // Cap it at 80% of fitness.
					// no roll bonus for unarmed attacks
					break;
			}

			return val;
		}

		override get defense(): number {
			let val = 0;

			switch (this.engine.combatClass) {
				case 'SWASHBUCKLING':
					val = this.player.getStat(Stat.Skill, Skills.Piracy.Swashbuckling);
					// Technicaly a private method… but whatever javascript
					val += this.player.rollBonus(Stat.Skill, Skills.Piracy.Swashbuckling);
					break;
				case 'BOOBJITSU':
					val = this.player.getStat(Stat.Skill, Skills.Sexual.BoobJitsu);
					val += this.player.rollBonus(Stat.Skill, Skills.Sexual.BoobJitsu);
					break;
				case 'ASSFU':
					val = this.player.getStat(Stat.Skill, Skills.Sexual.AssFu);
					val += this.player.rollBonus(Stat.Skill, Skills.Sexual.AssFu);
					break;
				default:
					val = 20;
					val += Math.floor(this.player.getStat(Stat.Core, CoreStat.Fitness) * 0.4); // Cap it at 40% of fitness.
					val += Math.floor(this.player.getStat(Stat.Skill, Skills.Charisma.Dancing) * 0.4); // Cap it at 40% of dancing skill.
					// no roll bonus for unarmed attacks
					break;
			}

			return val;
		}

		// Grant xp to players
		override attackRoll(): number {
			const mod = super.attackRoll();
			this.grantXP(Math.floor(10 * mod));
			return mod;
		}

		override defenseRoll(): number {
			const mod = super.defenseRoll();
			this.grantXP(Math.floor(10 * mod));
			return mod;
		}

		grantXP(xp: number): void {
			switch (this.engine.combatClass) {
				case 'SWASHBUCKLING':
					this.player.adjustSkillXP(Skills.Piracy.Swashbuckling, xp);
					break;
				case 'BOOBJITSU':
					this.player.adjustSkillXP(Skills.Sexual.BoobJitsu, xp);
					break;
				case 'ASSFU':
					this.player.adjustSkillXP(Skills.Sexual.AssFu, xp);
					break;
				default:
					this.player.adjustStatXP(CoreStat.Fitness, Math.floor(xp / 2));
					this.player.adjustSkillXP(Skills.Charisma.Dancing, Math.floor(xp / 2));
					break;
			}
		}

		override useEnergy(n : number): void {
			const x = Math.abs(n) * -1; // Always reduce
			this.player.adjustStat(CoreStat.Energy, x);
			App.PR.refreshTwineMeter(CoreStat.Energy);
		}

		override recoverEnergy(n: number): void {
			this.player.adjustStat(CoreStat.Energy, n);
			App.PR.refreshTwineMeter(CoreStat.Energy);
		}

		override takeDamage(n: number): void {
			const x = Math.abs(n) * -1; // Always reduce
			this.player.adjustStat(CoreStat.Health, x);
			App.PR.refreshTwineMeter(CoreStat.Health);
			// Test for loss -
			if (!this.player.isAlive) {
				this.#loseHandler(this.player);
				this.die();
			}
		}

		override recoverHealth(n: number): void {
			this.player.adjustStat(CoreStat.Health, n);
			App.PR.refreshTwineMeter(CoreStat.Health);
		}

		isEquipped(slot: ClothingSlot): boolean {
			return this.player.isEquipped(slot, true);
		}

		/**
		 * Attempt to figure out the quality of an equipped weapon.
		 * @returns value 1 through 5.
		 */
		getWeaponQuality(): number {
			if (this.isEquipped(ClothingSlot.Weapon,) == false) return 1;
			const o = this.player.getEquipmentInSlot(ClothingSlot.Weapon);
			if (!o || typeof o === 'undefined' || o == null) return 1;

			switch (o.data.rarity) {
				case Items.Rarity.Common:
					return 3;
				case Items.Rarity.Uncommon:
					return 4;
				case Items.Rarity.Rare:
					return 5;
				case Items.Rarity.Legendary:
					return 6;
			}
		}

		getWeaponBonus(): number {
			return this.player.getWornSkillBonus("sharpness");
		}

		override get bust(): number {return this.player.getStat(Stat.Body, BodyPart.Bust);}

		override get ass(): number {return this.player.getStat(Stat.Body, BodyPart.Ass);}
	}
}
