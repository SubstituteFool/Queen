namespace App.Entity {

	interface SliderData {
		TYPE: string;
		ATTRIB: string;
		LABEL: string;
		MIN: number;
		MAX: number;
		STEP: number;
	}

	interface ListElement {
		TYPE: string;
		ATTRIB: string;
		LABEL: string;
		ITEMS: any[];
	}

	interface GetItemsArgsTypeMap {
		'OBJ': "NPCLIST" | keyof typeof Data.Lists;
		'ARR': keyof typeof Data.Lists;
		'CLOTHES': ClothingSlot;
		'WEAPON': ClothingSlot;
	}

	export class NPCAvatar {
		private _canvasHeight = 800;
		private _canvasWidth = 360;
		private _canvasElement = "npcRender";
		private _portraitHeight = 240;
		private _portraitWidth = 180;
		private _portraitElement = "npcRenderPortrait";

		private _equip!: Data.NPCEquipData;

		private _npcData: Data.PresetData | null = null;

		private _sliders: Record<string, SliderData> = {};
		private _faceData: any = {};

		private _lists: Record<string, ListElement> = {};
		private _pc: QoSDaPlayer | null = null;
		private _loadID: string | null = null;
		private _init = false;
		private _root!: JQuery<HTMLElement>;

		constructor() {
			// mo-op
		}

		get npc() {return this._npcData;}
		get face() {return this._faceData;}
		get equip() {return this._equip;}
		get pc(): QoSDaPlayer| null {return this._pc;}
		get sliders() {return this._sliders;}
		get lists() {return this._lists;}
		get loadID() {return this._loadID;}

		init(id: string) {
			if (this._init == true) return;
			this._loadID = id;
			this._npcData = $.extend(true, {}, Data.dadNpc[id].data);
			this._equip = $.extend(true, {}, Data.dadNpc[id].equip);
			this._init = true;

			// Copy face data structure and populate it from loaded npc data.
			this._faceData = $.extend(true, {}, Data.DAD.faceStruct);

			for (const p in this._faceData.basedim) {
				if (this._npcData.basedim.hasOwnProperty(p)) this._faceData.basedim[p] = this._npcData.basedim[p];
			}

			for (const p in this._faceData.Mods) {
				if (this._npcData.Mods.hasOwnProperty(p)) this._faceData.Mods[p] = this._npcData.Mods[p];
			}
		}

		reLoad(id: string) {
			this._loadID = id;
			this._npcData = $.extend(true, {}, Data.dadNpc[id].data);
			this._equip = $.extend(true, {}, Data.dadNpc[id].equip);
			this._faceData = $.extend(true, {}, Data.DAD.faceStruct);

			for (const p in this._faceData.basedim) {
				if (this._npcData.basedim.hasOwnProperty(p)) this._faceData.basedim[p] = this._npcData.basedim[p];
			}

			for (const p in this._faceData.Mods) {
				if (this._npcData.Mods.hasOwnProperty(p)) this._faceData.Mods[p] = this._npcData.Mods[p];
			}

			Engine.play("NPCTester");
		}

		addList<T extends "OBJ" | "CLOTHES">(type: T, attrib: GetItemsArgsTypeMap[T], label: string): string {
			const id = type + "_" + attrib;
			if (this._lists.hasOwnProperty(id) == false) {
				this._lists[id] = {
					TYPE: type,
					ATTRIB: attrib,
					LABEL: typeof label === 'undefined' ? attrib : label,
					ITEMS: NPCAvatar._getItems(type, attrib)!
				};
			}

			return id;
		}

		addSlider(id: string, name: string, min: number, max: number, step: number) {
			const parts = id.split("_");

			this._sliders[id] = {
				TYPE: parts[0],
				ATTRIB: parts[1],
				LABEL: name,
				MIN: min,
				MAX: max,
				STEP: step
			}

			return id;
		}

		drawUI(rootID: string) {
			this._root = $("#" + rootID);
			$(document).one(":passageend", this._drawUI.bind(this));
		}

		private _drawUI() {
			for (const sliderId in this.sliders) {
				this._drawLabel(sliderId);
				this._drawSlider(sliderId);
				this._drawValue(sliderId);
			}

			for (const listId in this.lists) {
				this._drawLabel(listId);
				this._drawDropDown(listId);
			}
		}

		drawCanvas(element: string, height: number, width: number) {
			this._canvasHeight = height;
			this._canvasWidth = width;
			this._canvasElement = element;
			$(document).one(":passageend", this._drawCanvas.bind(this));
		}

		private _drawCanvas() {
			let canvasGroup = null;
			if (typeof canvasGroup === 'undefined' || canvasGroup == null) {
				canvasGroup = da.getCanvasGroup(this._canvasElement,
					{
						border: "1px solid goldenrod",
						width: this._canvasWidth,
						height: this._canvasHeight
					});
			}

			let pc = new QoSDaPlayer(this.npc);
			pc = this._attachParts(pc);
			da.draw(canvasGroup, pc, {
				printHeight: false,
				printAdditionalInfo: false,
				renderShoeSideView: false,
				offsetX: 10,
				offsetY: 0
			});
			this._pc = pc;
		}

		drawPortrait(element: string, height: number, width: number) {
			this._portraitElement = element;
			this._portraitHeight = height;
			this._portraitWidth = width;

			$(document).one(":passageend", this._drawPortrait.bind(this));
		}

		private _drawPortrait() {
			const canvasGroup = da.getCanvasGroup("hiddenPortraitCanvas", {
				border: "none",
				width: 2100,
				height: 3600
			});

			let pc = new QoSDaPlayer(this.npc!);
			pc = this._attachParts(pc);

			da.draw(canvasGroup, pc, {printHeight: false, printAdditionalInfo: false, renderShoeSideView: false})
				.then((exports) => {
				// draw just the head in a separate canvas
				// first retrieve/create the canvas if it's the first time we're getting it
					const portraitCanvas = da.getCanvas("portraitFace",
						{
							width: this._portraitWidth,
							height: this._portraitHeight,
							// can add any CSS style here like border
							border: "solid 1px goldenrod",
							// you can also position it absolutely
							// position: "absolute",
							// top     : "10px",
							// left    : "10px",
							// or relative to a parent
							position: "relative",
							parent: document.getElementById(this._portraitElement),
						});

					const eyeCanvas = da.getCanvas("portraitEye",
						{
							width: this._portraitWidth,
							height: this._portraitHeight,
							border: "solid 1px goldenrod",
							position: "relative",
							parent: document.getElementById(this._portraitElement + "Eye"),
						});

					// you can call this multiple times to draw different parts (with different canvases)
					da.drawFocusedWindow(portraitCanvas,
						exports,
						{
							center: exports[da.Part.RIGHT].neck.nape,
							width: 50,
							height: 50
						});

					da.drawFocusedWindow(eyeCanvas,
						exports,
						{
							center: exports[da.Part.RIGHT].eyes.center,
							width: 15,
							height: 15
						});
				});

			// da.hideCanvasGroup("hiddenCanvas");
		}

		private _drawLabel(id: string) {
			$(this._getAttrib(id)).append(
				$("<div>").addClass("npcWorkshopLabel").text(this._getObj(id).LABEL));
		}

		private _drawDropDown(id: string) {
			const root = $("<select>").attr("id", id + "_DropDown").addClass("NPCDropDown");
			$(this._getAttrib(id)).append(root);

			const items = (this._getObj(id) as ListElement).ITEMS;
			const selected = this._getSelectedItem(id);
			for (let i = 0; i < items.length; i++) {
				const opt = $('<option>').attr('value', items[i].v).text(items[i].t);
				if (items[i].v == selected) opt.attr('selected', 'selected');
				root.append(opt);
			}

			root.on("change", {ID: id}, this._dropDownChanged.bind(this));
		}

		private _drawSlider(id: string) {
			const root = $(this._getAttrib(id));
			const element = $("<div>").addClass("npcSliderClass").attr("id", id + "_Slider");
			root.append(element);
			let statStart = this._getStat(id);

			if (statStart === undefined) {
				console.log("Unable to map property:" + id);
				statStart = this._getMin(id);
			}
			const slider = noUiSlider.create(element.get(0), {
				start: statStart,
				step: this._getStep(id),
				range: {
					min: [this._getMin(id)],
					max: [this._getMax(id)]
				}
			});
			const type = this.sliders[id].TYPE;
			const attr = this.sliders[id].ATTRIB;
			slider.on("change." + id, (v) => {this.doSliderUpdate(type as keyof Data.PresetData, attr, v /* , ID */);});
			slider.on("update." + id, (v) => {this.doSliderValUpdate(v, id);});
		}

		private _drawValue(id: string) {
			$(this._getAttrib(id)).append(
				$("<div>").addClass("npcWorkshopValue").attr("id", id + "_Value").html(this.doSliderValUpdate(this._getStat(id), id)));
		}

		private _getAttrib(id: string) {
			return "#" + id;
		}

		private _getObj(id: string) {
			const parts = id.split("_");
			const type = parts[0];
			if (type == 'OBJ' || type == 'ARR' || type == 'CLOTHES' || type == 'WEAPON') {
				return this.lists[id];
			} else {
				return this.sliders[id];
			}
		}

		private _getStep(id: string) {
			return (this._getObj(id) as SliderData).STEP;
		}

		private _getMin(id: string) {
			return (this._getObj(id) as SliderData).MIN;
		}

		private _getMax(id: string) {
			return (this._getObj(id) as SliderData).MAX;
		}

		/**
		 *
		 * @param id base_prop | basedim_prop | Mods_prop
		 */
		private _getStat(id: string) {
			let val = null;
			try {
				const parts = id.split("_");
				// @ts-expect-error FIXME
				val = parts[0] == 'base' ? this.npc![parts[1] as keyof Data.PresetData] : this.npc![parts[0] as keyof Data.PresetData][parts[1]];
			} catch (error) {
				console.log("Error:_getStat(" + id + ")");
			}

			return val;
		}

		private _attachParts(pc: QoSDaPlayer) {
			if (pc.gender == 1 || pc.gender == 2) {
				const penis = da.Part.create(da.PenisHuman, {side: "right"});
				pc.attachPart(penis);
				const balls = da.Part.create(da.TesticlesHuman, {side: "right"});
				pc.attachPart(balls);
			}

			if (pc.gender === 1) {
				pc.removeSpecificPart(da.OversizedChest);
				pc.removeSpecificPart(da.ChestHuman);
			}

			if (pc.gender == 0 || pc.gender == 2) {
				const bust = da.Part.create(da.OversizedChest, {side: null});
				pc.attachPart(bust);
				pc.removeSpecificPart(da.NipplesHuman);
				const nips = da.Part.create(da.OversizedChestNipples, {side: null});
				pc.attachPart(nips);
			}

			if (pc.gender === 0) {
				pc.removeSpecificPart(da.PenisHuman);
				pc.removeSpecificPart(da.TesticlesHuman);
			}
			return this._clothesHandler(pc);
		}

		private _clothesHandler(pc: QoSDaPlayer) {
			for (const e of Object.entries(this.equip)) {
				const id = e[1];
				if (id === null) {continue;}
				const items = Data.AvatarMapping.clothes[id];
				if (items) {
					AvatarEngine.handleClothingItems(pc, items);
				}
			}

			return pc;
		}

		private static _getItems<T extends keyof GetItemsArgsTypeMap>(type: T, attrib: GetItemsArgsTypeMap[T]) {
			let items;

			if (type == 'OBJ') {
				if (attrib == "NPCLIST") {
					items = Object.entries(Data.dadNpc).map((v, _i, _a) => new Object({t: v[1].data.name, v: v[0]}));
				} else {
					items = Object.keys((Data.Lists as any)[attrib]);
				}
			} else if (type == 'ARR') {
				items = Data.Lists[attrib as GetItemsArgsTypeMap['ARR']] as any[];
				if (items.length > 0 && typeof items[0] === 'object') {
					switch (attrib) {
						case 'HairStyles':
						case 'MakeupStyles':
							items = items.map(o => o.SHORT);
							break;
						default:
							items = items.map(o => o.NAME);
					}
				}
			} else if (type == 'WEAPON' || type == 'CLOTHES') {
				items = [{t: "nothing", v: "nothing"}];
				Array.prototype.push.apply(items, Object.entries(App.Data.clothes)
					.filter(o => o[1].slot == attrib)
					.map((v, _i, _a) => new Object({t: v[0], v: v[0]})));
			}
			return items;
		}

		private _getSelectedItem(id: string) {
			const o = this._getObj(id);

			switch (o.TYPE) {
				case 'CLOTHES':
				case 'WEAPON':
					const e = this.equip[o.LABEL as keyof Data.NPCEquipData];
					if (e != null) return e;
					break;
			}

			switch (o.ATTRIB) {
				case 'NPCLIST':
					return this.loadID;
				case 'HairStyles':
					const key = this.npc;
					// @ts-ignore
					const res = Data.Lists[o.ATTRIB].filter(o => o.SHORT == key);
					if (res.length > 0) return res[0].SHORT;
					break;
				case 'HairColors':
					return this.npc!.hairFill;
				case 'MakeupStyles':
					return setup.player.makeupStyle;
			}

			return undefined;
		}

		doSliderUpdate(type: keyof Data.PresetData | 'base', attr: string, valAr: string[]) {
			const val = parseFloat(valAr[0]);

			switch (attr) {
				case "fem":
					this.npc!.basedim.legFem = (50 * (val / 11));
					break;
				case "upperMuscle":
					this.npc!.basedim.lowerMuscle = val;
					break;
			}

			if (type == 'base') {
				(this.npc! as any)[attr] = val;
			} else {
				(this.npc! as any)[type][attr] = val;
				if (this.face[type].hasOwnProperty(attr)) this.face[type][attr] = val;
			}

			this._drawCanvas();
			this._drawPortrait();
			this.doSliderValUpdate(val, type + "_" + attr);
			this.exportData();
		}

		doSliderValUpdate(val: any, id: string) {
			$("#" + id + "_Value").html(val);
			return val;
		}

		private _dropDownChanged(e: JQuery.ChangeEvent<any, {ID: string}>) {
			const o = this._getObj(e.data.ID);
			const list = $("#" + e.data.ID + "_DropDown");
			const val = list.val() as string;

			switch (o.TYPE) {
				case 'CLOTHES':
				case 'WEAPON':
					if (val == 'nothing') {
						this.equip[o.LABEL as Data.NPCEquipSlot] = null;
					} else {
						this.equip[o.LABEL as Data.NPCEquipSlot] = val;
					}
					this._drawCanvas();
					this._drawPortrait();
					break;
			}

			switch (o.ATTRIB) {
				case 'NPCLIST':
					this.reLoad(val);
					break;
				case 'HairStyles':
					// setup.player.HairStyle = val;
					this._drawCanvas();
					// Avatar._DrawPortrait();
					break;
				case 'HairColors':
					// setup.player.HairColor = val;
					this._drawCanvas();
					// Avatar._DrawPortrait();
					break;
			}

			this.exportData();
		}

		exportData() {
			const data = {
				data: this.npc,
				equip: this.equip
			};

			$('#dataOutput').val(JSON.stringify(data));
			$('#faceDataOutput').val(JSON.stringify(this.face));
		}
	}
}
