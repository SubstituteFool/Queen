namespace App {
	export class EventEngine {
		#passageOverride: string | null = null;
		#fromPassage: string | null = null;
		#toPassage: string | null = null;

		constructor() {
			// no-op
		}

		get passageOverride(): string | null {return this.#passageOverride;}
		set passageOverride(n: string | null) {this.#passageOverride = n;}

		get fromPassage(): string | null {
			if (this.#fromPassage == null) this._loadState();
			return this.#fromPassage;
		}

		get toPassage(): string | null {
			if (this.#toPassage == null) this._loadState();
			return this.#toPassage;
		}

		nextPassage(): void {
			if (this.#toPassage == null) this._loadState();
			Engine.play(this.#toPassage as string);
		}

		backPassage(): void {
			if (this.#fromPassage == null) this._loadState();
			Engine.play(this.#fromPassage as string);
		}

		/**
		 * Checks a dictionary of destinations (including 'any') for conditions to process.
		 * On a successful evaluation return a string of a twine passage to the initiating handler
		 * that overrides the characters navigation to that passage. Also, save both the calling
		 * passage and the original destination passage for later use by the passage we are
		 * redirecting to.
		 * @param player
		 * @param fromPassage
		 * @param toPassage
		 * @returns OverridePassage
		 */
		checkEvents(player: Entity.Player, fromPassage: string, toPassage: string): string | null {
			// Init the audio engine on passage click to get around browsers blocking auto play
			setup.audio.init();
			setup.audio.transition(toPassage);

			EventEngine._d("From: " + fromPassage + ",To:" + toPassage);

			// One time override
			if (this.passageOverride != null) {
				const tmp = this.passageOverride;
				this.passageOverride = null;
				return tmp;
			}

			// Check gameover conditions first. Hardcoded just because they are rare.
			if (Entity.Player.instance.getStat(Stat.Core, CoreStat.Health) <= 0) {
				EventEngine._d("Player died event.");
				// State.temporary.followup = passageName;
				return "DeathEnd";
			}

			if (Entity.Player.instance.getStat(Stat.Core, CoreStat.Willpower) <= 15) {
				EventEngine._d("Player lost too much willpower event.");
				// State.temporary.followup = passageName;
				return "WillPowerEnd";
			}

			// Look for forced events. We need to fire them off regardless.
			const event = EventEngine._findForcedEvent(fromPassage, toPassage);
			if (event != null) {
				EventEngine._setFlags(player, event.id);
				this._saveState(fromPassage, toPassage);
				return event.passage;
			}

			// For now, let's throttle events to 1 per day.
			if (player.questFlags.hasOwnProperty("LAST_EVENT_DAY") && player.questFlags["LAST_EVENT_DAY"] == setup.world.day) return null;

			// Location specific events get checked first.
			let validEvents = EventEngine._filterEvents(player, fromPassage, toPassage);

			// todo: have to add quest flags to character when event fires off.
			if (validEvents.length > 0) {
				const ev = EventEngine._selectEvent(validEvents);
				if (ev.check(player) == true) {
					EventEngine._setFlags(player, ev.id);
					this._saveState(fromPassage, toPassage);
					return ev.passage;
				}
			}

			validEvents = EventEngine._filterEvents(player, fromPassage, "Any");

			if (validEvents.length > 0) {
				const ev = EventEngine._selectEvent(validEvents);
				if (ev.check(player) == true) {
					EventEngine._setFlags(player, ev.id);
					this._saveState(fromPassage, toPassage);
					return ev.passage;
				}
			}

			return null;
		}

		private _saveState(fromPassage: string, toPassage: string) {
			this.#fromPassage = fromPassage;
			this.#toPassage = toPassage;
			sessionStorage.setItem('QOS_EVENT_FROM_PASSAGE', this.#fromPassage);
			sessionStorage.setItem('QOS_EVENT_TO_PASSAGE', this.#toPassage);
		}

		private _loadState() {
			this.#fromPassage = sessionStorage.getItem('QOS_EVENT_FROM_PASSAGE');
			this.#toPassage = sessionStorage.getItem('QOS_EVENT_TO_PASSAGE');
		}

		/**
		 * Filter events down to only potentially valid ones.
		 * @param player
		 * @param fromPassage
		 * @param poPassage
		 */
		private static _filterEvents(player: Entity.Player, fromPassage: string, poPassage: string) {
			if (Data.events.hasOwnProperty(poPassage) == false || Data.events[poPassage].length < 1) return [];
			return Data.events[poPassage].filter((o) => {
				return ((o.from == 'Any' || o.from == fromPassage) && (setup.world.day >= o.minDay)
					&& (o.phase.includes(setup.world.phase))
					&& (o.maxDay == 0 ? true : setup.world.day <= o.maxDay)
					&& (o.maxRepeat == 0 ? true :
						(player.questFlags.hasOwnProperty("EE_" + o.id + "_COUNT") ? player.questFlags["EE_" + o.id + "_COUNT"] as number < o.maxRepeat : true))
					&& (player.questFlags.hasOwnProperty("EE_" + o.id + "_LAST") ? player.questFlags["EE_" + o.id + "_LAST"] as number + o.cool < setup.world.day : true)
				);
			});
		}

		/**
		 * Select an event from the available ones.
		 */
		private static _selectEvent(eventArray: Data.EventDesc[]): Data.EventDesc {
			EventEngine._d("_SelectEvent:");
			EventEngine._d(eventArray);
			const events = eventArray.filter(o => o.hasOwnProperty('FORCE') && o.force && o.check(setup.player));

			if (events.length > 0) return events[0];

			return eventArray[Math.floor(Math.random() * eventArray.length)];
		}

		/**
		 * Check to see if we need to force an event for quests.
		 * They should all be single fire events otherwise things might get weird. Use with caution.
		 * @param fromPassage
		 * @param toPassage
		 * @returns Event data
		 */
		private static _findForcedEvent(fromPassage: string, toPassage: string) {
			if (Data.events.hasOwnProperty(toPassage) == false || Data.events[toPassage].length == 0) {
				return null;
			}
			const events = Data.events[toPassage].filter((o) => {
				return (o.from == 'Any' || o.from == fromPassage) &&
					o.hasOwnProperty('FORCE') &&
					o.force == true &&
					(o.maxRepeat == 0 ? true :
						(setup.player.questFlags.hasOwnProperty("EE_" + o.id + "_COUNT") ?
							setup.player.questFlags["EE_" + o.id + "_COUNT"] as number < o.maxRepeat : true)) &&
					o.check(setup.player) == true
			});

			return events.length > 0 ? events[0] : null;
		}

		/**
		 * Helper Method to set quest flags in player state for event tracking.
		 * @param player
		 * @param key
		 */
		private static _setFlags(player: Entity.Player, key: string) {
			const countKey = "EE_" + key + "_COUNT";
			const lastKey = "EE_" + key + "_LAST";

			player.questFlags["LAST_EVENT_DAY"] = setup.world.day;
			player.questFlags[lastKey] = setup.world.day;

			player.increaseFlagValue(Entity.FlagType.Quest, countKey, 1);
		}

		/**
		 * @param player
		 * @param id Event Id
		 * @returns Last Day event was fired. 0 if never fired
		 */
		static eventFired(player: Entity.Player, id: string): TaskFlag {
			const lastKey = "EE_" + id + "_LAST";
			if (player.questFlags.hasOwnProperty(lastKey) == false) return 0;
			return player.questFlags[lastKey];
		}

		/**
		 * Manually trigger an event and ignore checks, coolsdowns, etc.
		 * @param passage
		 * @param id
		 */
		fireEvent(passage: string, id: string): void {
			const event = Data.events[passage].filter(o => o.id == id)[0];
			console.log(event);
			EventEngine._setFlags(setup.player, event.id);
			this._saveState(event.from, passage);
			Engine.play(event.passage);
		}

		private static _d(m: unknown) {
			if (setup.world.debugMode) console.log(m);
		}
	}
}
