namespace App {
	/**
	 * This class manages the "job" system. Basically short term repeatable quests.
	 * They are not tracked by the journal and focus mostly on earning cheap rewards and flavor text.
	 */
	export class JobEngine {
		#jobs: Job[] = [];
		#danceInfo = { // Hack for dancing jobs.
			dance: Data.Fashion.Style.SexyDancer,
			day: 1,
			phase: 0
		};

		constructor() {
			// no-op
		}

		/**
		 * The Job engine singleton
		 */
		static get instance(): JobEngine {
			return setup.jobEngine;
		}

		/**
		 * Hack for reporting the current 'desired dance' for dance halls.
		 */
		getDance(): Data.Fashion.Style {
			if (this.#danceInfo.day < setup.world.day || this.#danceInfo.phase < setup.world.phase) {
				this.#danceInfo.dance = Object.values(Data.Fashion.Style).randomElement();
				this.#danceInfo.day = setup.world.day;
				this.#danceInfo.phase = setup.world.phase;
			}
			return this.#danceInfo.dance;
		}

		/**
		 * Loads the job data into an array of Job objects. Can be called again if necessary.
		 */
		loadJobs(): void {
			if (this.#jobs.length < 1) {
				this.#jobs = Object.values(Data.jobs).map(jd => new Job(jd));
			}
		}

		/**
		 * Lists all jobs at person/location
		 */
		getJobs(_player: Entity.Player, giver: string): Job[] {
			this.loadJobs();
			return this.#jobs.filter(j => j.giver == giver).sort();
		}

		/**
		 * Return a Job by it's id property.
		 */
		getJob(jobID: string): Job | undefined {
			this.loadJobs();
			return this.#jobs.find(j => j.id() == jobID);
		}

		jobAvailable(player: Entity.Player, npc: Entity.NPC, jobID: string): Job | undefined {
			this.loadJobs();
			return this.#jobs.find(j => j.id() == jobID && j.available(player, npc));
		}

		/**
		 * Lists all AVAILABLE jobs at a person
		 */
		getAvailableJobs(player: Entity.Player, giver: string): Job[] {
			const npc = setup.world.npc(giver);
			return this.getJobs(player, giver).filter(j => j.available(player, npc));
		}

		/**
		 * Lists available jobs at a location
		 */
		getAvailableLocationJobs(player: Entity.Player, giver: string): Job[] {
			return this.getJobs(player, giver).filter(j => j.requirements(player, giver));
		}

		/**
		 * Lists all UNAVAILABLE jobs on person
		 */
		getUnavailableJobs(player: Entity.Player, giver: string): Job[] {
			return this.getJobs(player, giver)
				.filter(j => !j.available(player, setup.world.npc(giver)) && !j.hidden())
				.sort();
		}

		/**
		 * Lists unavailable jobs at a location
		 */
		getUnavailableLocationJobs(player: Entity.Player, giver: string): Job[] {
			return this.getJobs(player, giver).filter(j => !j.requirements(player, giver));
		}

		/**
		 * True if there are AVAILABLE jobs at this person/location
		 */
		jobsAvailable(player: Entity.Player, giver: string): boolean {
			return this.getAvailableJobs(player, giver).length != 0;
		}

		/**
		 * True if there are ANY jobs at this person/location
		 */
		hasJobs(player: Entity.Player, giver: string): boolean {
			return (this.getAvailableJobs(player, giver).length != 0) ||
				(this.getUnavailableJobs(player, giver).length != 0);
		}

		static hasJobFlag(player: Entity.Player, flag: string): boolean {
			return player.jobFlags.hasOwnProperty(flag);
		}

		static getJobFlag<T extends TaskFlag>(player: Entity.Player, flag: string): T | undefined {
			return player.jobFlags[flag] as T;
		}

		static setJobFlag(player: Entity.Player, flag: string, value: TaskFlag): void {
			if (value === undefined) {
				delete player.jobFlags[flag];
			} else {
				player.jobFlags[flag] = value;
			}
		}

		// this.Init(Data.JobData);
	}
}
