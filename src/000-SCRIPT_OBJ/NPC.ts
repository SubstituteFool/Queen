namespace App.Entity {

	/**
	 * NPC Entity
	 */
	export class NPC {
		private readonly _data: Data.NPCDesc;
		private _state: GameState.NPC;
		private _nameOverride?: string;

		constructor(data: Data.NPCDesc, state: GameState.NPC) {
			this._data = data;
			this._state = state;
		}

		get name(): string {return this._nameOverride ?? this._data.name;}
		set name(v: string) {this._nameOverride = v;}

		get pName(): string {return "@@.npc;" + this.name + "@@";}
		get mood(): number {return this._state.mood;}
		get pMood(): string {return PR.getRating(NpcStat.Mood, this.mood, true);}
		get lust(): number {return this._state.lust;}
		get pLust(): string {return PR.getRating(NpcStat.Lust, this.lust, true);}
		get title(): string {
			return this._data.title.replace("{NAME}", this.name);
		}

		get briefDesc(): string {
			if (this._data.briefDesc) {
				return this._data.briefDesc;
			}
			return `${this.title} at @@.location-name;${App.UI.resolveDynamicText(setup.world.pc, this._data.location)}@@`;
		}

		get shortDesc(): string {return this.title + " (" + this.pMood + ", " + this.pLust + ")";}
		get longDesc(): string {return this._data.longDesc.replace("{NAME}", this.name);}
		get hasStore(): boolean {return (typeof this._data.store !== 'undefined');}
		get storeName(): string | undefined {return this._data.store;}
		get storeTitle(): string | null {
			return this._data.store ? Data.stores[this._data.store].name : null;
		}

		getFlag(flag: string): GameState.NPCQuestFlagValue {
			if (typeof this._state.questFlags[flag] === 'undefined') return 0;
			return this._state.questFlags[flag].value;
		}

		setFlag(flag: string, value: GameState.NPCQuestFlagValue, tmp: number): void {
			if (typeof tmp === 'undefined') tmp = 0;
			this._state.questFlags[flag] = {value,  tmp};
		}

		clearFlag(flag: string): void {
			if (typeof this._state.questFlags[flag] === 'undefined') return;
			delete this._state.questFlags[flag];
		}

		resetFlags(): void{
			for (const prop in this._state.questFlags) {
				if (!this._state.questFlags.hasOwnProperty(prop)) continue;
				if (this._state.questFlags[prop].tmp != 0) delete this._state.questFlags[prop];
			}
		}

		/**
		 * Adjust an NPC statistic (dictionary value)
		 */
		adjustStat(stat: NpcStat, shift: number): void {
			this._state[stat] = Math.ceil(Math.max(1, Math.min((this._state[stat] + shift), 100)));
		}

		/**
		 * Get the value of an NPC stat.
		 */
		getStat(stat: NpcStat): number {
			return this._state[stat];
		}

		adjustFeelings(): void {
			this.adjustStat(NpcStat.Mood, this._data.dailyMood);
			this.adjustStat(NpcStat.Lust, this._data.dailyLust);
		}

		get data(): Data.NPCDesc {
			return this._data;
		}
	}
}
