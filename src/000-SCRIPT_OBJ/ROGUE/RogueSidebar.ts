namespace App.Rogue {
	interface SideBarOptions {
		display: ROT.Display;
		position: XY;
		size: XY;
	}
	export class Sidebar {
		#data: string[] = [];
		#options: SideBarOptions;

		constructor(options: SideBarOptions) {
			this.#options = Object.assign({}, options);
		}

		clear(): void {
			this.#data = [];
		}

		write(text: string): void {
			this.#data.push(text);
		}

		title(text: string): void {
			const o = this.#options;
			const d = o.display;
			const pos = o.position;
			const size = o.size;

			d.drawText(pos.x, pos.y, text, size.x);
		}

		level(text: string): void {
			const o = this.#options;
			const d = o.display;
			const pos = o.position;
			const size = o.size;

			d.drawText(pos.x, pos.y + 1, text, size.x);
		}

		torches(text: string): void {
			const o = this.#options;
			const d = o.display;
			const pos = o.position;
			const size = o.size;

			d.drawText(pos.x, pos.y + 3, text, size.x);
		}

		shovels(text: string): void {
			const o = this.#options;
			const d = o.display;
			const pos = o.position;
			const size = o.size;

			d.drawText(pos.x, pos.y + 4, text, size.x);
		}

		help(): void {
			const o = this.#options;
			const d = o.display;
			const pos = o.position;
			const size = o.size;

			d.drawText(pos.x, pos.y + 6, "COMMANDS", size.x);

			if (settings.alternateControlForRogue == true) {
				d.drawText(pos.x, pos.y + 7, "Move: WASD", size.x);
				d.drawText(pos.x, pos.y + 8, "(diagonal): QEZX", size.x);
				d.drawText(pos.x, pos.y + 9, "Torch = T", size.x);
				d.drawText(pos.x, pos.y + 10, "Dig = R", size.x);
				d.drawText(pos.x, pos.y + 11, "Up/Down = R", size.x);
			} else {
				d.drawText(pos.x, pos.y + 7, "Move = NUMPAD", size.x);
				d.drawText(pos.x, pos.y + 8, "Torch = /", size.x);
				d.drawText(pos.x, pos.y + 9, "Dig = NUMPAD5", size.x);
				d.drawText(pos.x, pos.y + 10, "Up/Down = NUMPAD5", size.x);
			}
		}

		flush(): void {
			const o = this.#options;
			const d = o.display;
			const pos = o.position;
			const size = o.size;

			/* clear */
			for (let i = 0; i < size.x; i++) {
				for (let j = 0; j < size.y; j++) {
					d.draw(pos.x + i, pos.y + j, null, null, null);
				}
			}

			const text = this.#data.join(" ");
			d.drawText(pos.x, pos.y, text, size.x);
		}
	}
}
