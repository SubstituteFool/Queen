namespace App.GameState {

	export interface CharacterName {
		given: string;
		original: string;
		slave: string;
		nick: string;
	}

	export class Character {
		name: CharacterName = {
			given: "Joseph",
			original: "Smithe",
			slave: "Josie",
			nick: ""
		};
	}

	export type SkillUseStat = {
		success: number;
		failure: number;
	}

	export type SkillUseStats = Partial<Record<Skills.Any, SkillUseStat>>;

	export interface CoffinDiceStats {
		played: number;
		won: number;
		lost: number;
		coinsWon: number;
		coinsLost: number;
		itemsWon: number;
		itemsWonValue: number;
		sexPaid: number;
	}

	export const enum CommercialActivity {
		Betting = 'betting',
		Gambling = 'gambling',
		Quests = 'quests',
		Jobs = 'jobs'
	}

	export const enum IncomeSource {
		SexualJobs = 'sexualJobs',
		Loot = 'loot',
		Whoring = 'whoring',
		Unknown = 'unknown'
	}

	export const enum SpendingTarget {
		Shopping = 'shopping'
	}

	export type AnyIncome = CommercialActivity | IncomeSource;
	export type AnySpending = CommercialActivity | SpendingTarget;

	interface StatAndXp<T extends PropertyKey> {
		value: Record<T, number>;
		xp: Record<T, number>;
	}

	export interface EquipmentSet {
		name: string;
		items: Partial<Record<ClothingSlot, Data.ItemNameTemplateEquipment>>;
	}

	export class Human extends Character {
		age = 18;
		gender = Gender.Female;
		core: StatAndXp<CoreStat> = {
			value: {
				energy: 0,
				femininity: 0,
				fitness: 0,
				futa: 0,
				health: 0,
				hormones: 0,
				nutrition: 0,
				perversion: 0,
				toxicity: 0,
				willpower: 0
			},
			xp: {
				energy: 0,
				femininity: 0,
				fitness: 0,
				futa: 0,
				health: 0,
				hormones: 0,
				nutrition: 0,
				perversion: 0,
				toxicity: 0,
				willpower: 0
			}
		};

		skill: StatAndXp<Skills.Any> = {
			value: {
				// charisma
				dancing: 0, singing: 0, seduction: 0, styling: 0, courtesan: 0,
				// domestic
				cleaning: 0, cooking: 0, serving: 0,
				// piracy
				swashbuckling: 0, sailing: 0, navigating: 0, boobJitsu: 0, assFu: 0,
				// sexual
				assFucking: 0, blowJobs: 0, handJobs: 0, titFucking: 0
			},
			xp: {
				// charisma
				dancing: 0, singing: 0, seduction: 0, styling: 0, courtesan: 0,
				// domestic
				cleaning: 0, cooking: 0, serving: 0,
				// piracy
				swashbuckling: 0, sailing: 0, navigating: 0, boobJitsu: 0, assFu: 0,
				// sexual
				assFucking: 0, blowJobs: 0, handJobs: 0, titFucking: 0
			}
		};

		body: StatAndXp<BodyStat> = {
			value: {
				ass: 0,
				balls: 0,
				bust: 0,
				bustFirmness: 0,
				face: 0,
				hair: 0,
				height: 0,
				hips: 0,
				lactation: 0,
				lips: 0,
				penis: 0,
				waist: 0,
			},
			xp: {
				ass: 0,
				balls: 0,
				bust: 0,
				bustFirmness: 0,
				face: 0,
				hair: 0,
				height: 0,
				hips: 0,
				lactation: 0,
				lips: 0,
				penis: 0,
				waist: 0,
			}
		};

		skinColor = {
			hue: 26,
			saturation: 91,
			value: 245
		};

		eyeColor = "brown";
		equipment: Partial<Record<ClothingSlot, {id: Data.ItemNameTemplateEquipment; locked: boolean} | null>> = {};
	}

	interface Makeup {
		style: string;
		bonus: number;
	}

	interface Hair extends Makeup {
		color: Data.HairColor;
	}

	interface SpendingTargetMap {
		[CommercialActivity.Betting]: number;
		[CommercialActivity.Gambling]: number;
		[CommercialActivity.Jobs]: number;
		[CommercialActivity.Quests]: number;
		[SpendingTarget.Shopping]: Record<Items.Category, number>;
	}

	export interface GameStats {
		moneyEarned: Record<CommercialActivity | IncomeSource, number>;
		moneySpendings: {[P in CommercialActivity | SpendingTarget]: SpendingTargetMap[P]};
		tokensEarned: number;
		skills: SkillUseStats;
		coffin: CoffinDiceStats;
	}

	export class Player extends Human {
		money = 0;
		tokens = 0;
		makeup: Makeup = {
			style: "plain faced",
			bonus: 0
		};

		hair: Hair = {
			style: "",
			bonus: 0,
			color: "black"
		};

		sailDays = 1;
		lastUsedMakeup = "minimal blush and lipstick";
		lastUsedHair = "a spunky boy cut";
		lastQuickWardrobe = "best clothes";

		jobFlags: Record<string, TaskFlag> = {};
		questFlags: Record<string, TaskFlag> = {};

		voodooEffects: Record<string, string | number> = {};
		history: Record<string, Record<string, number>> = {
			items: {},
			clothingKnowledge: {}, // Flag to gain knowledge
			clothingEffectsKnown: {},
			daysWorn: {},
			sex: {},
			customers: {},
			money: {}
		};

		faceData!: Entity.DaBodyPreset;

		/**
		 * Clothing items the Player owns.
		 */
		wardrobe: Data.ItemNameTemplateEquipment[] = [];
		inventory: Partial<Record<keyof Items.InventoryItemTypeMap, {[x: string]: number}>> = {};
		inventoryFavorites: Set<string> = new Set();
		equipmentSets: EquipmentSet[] = [];

		slots: {[x: number]: string | null} = {0: null, 1: null, 2: null, 3: null, 4: null, 5: null, 6: null, 7: null, 8: null};

		currentSlots = 3; // Starting allocation of whoring

		bodyEffects: string[] = []; // lists effect names

		gameStats: GameStats = {
			moneyEarned: {
				betting: 0,
				gambling: 0,
				jobs: 0,
				loot: 0,
				quests: 0,
				sexualJobs: 0,
				whoring: 0,
				unknown: 0
			},
			moneySpendings: {
				betting: 0,
				gambling: 0,
				quests: 0,
				jobs: 0,
				shopping: {
					clothes: 0,
					cosmetics: 0,
					drugs: 0,
					food: 0,
					lootBox: 0,
					miscConsumable: 0,
					miscLoot: 0,
					quest: 0,
					reel: 0,
					weapon: 0
				}
			},
			tokensEarned: 0,
			skills: {},
			coffin: {
				played: 0,
				won: 0,
				lost: 0,
				coinsWon: 0,
				coinsLost: 0,
				itemsWon: 0,
				itemsWonValue: 0,
				sexPaid: 0
			}
		};
	}
}
