namespace App {

	export type TaskFlag = boolean | number | string | undefined;

	/**
	 * Represents a basic task for the player. Could be either a job or quest.
	 */
	export abstract class Task {
		private readonly _taskData: Data.Tasks.Task;
		private _sceneBuffer: Scene[];
		private _missingRequirements: string[]

		constructor(data: Data.Tasks.Task) {
			console.assert(data !== undefined);
			this._taskData = data;
			this._sceneBuffer = [];
			this._missingRequirements = [];
		}

		get taskData(): Data.Tasks.Task {
			return this._taskData;
		}

		get scenes(): Scene[] {
			return this._sceneBuffer;
		}

		private static _gte(x: number, y: number) {return (x >= y);}
		private static _lte(x: number, y: number) {return (x <= y);}
		private static _gt(x: number, y: number) {return (x > y);}
		private static _lt(x: number, y: number) {return (x < y);}
		private static _eq(x: number, y: number) {return (x == y);}
		private static _ne(x: number, y: number) {return (x != y);}

		id(): string {return this.taskData.id;}
		title(): string {return this.taskData.title;}
		get giver(): string {return this.taskData.giver;}
		hidden(): boolean {return this.taskData.hidden ?? false;}

		/**
		 * Check to see if the player meets the other requirements to take the task.
		 * Usually skill, stat, body related or quest flags.
		 * @param player
		 * @param npc
		 * @param requirements
		 * @param missingRequirements Will be filled with textual description of missing requirements
		 */
		static checkRequirements(player: Entity.Player, npc: Entity.NPC | string,
			requirements: Data.Tasks.Requirements.Any[], missingRequirements?: string[]): boolean {
			let statusFlag = true;
			let result = true;
			if (missingRequirements === undefined) missingRequirements = [];

			for (const r of requirements) {
				// const {TYPE: Type, name: Name, VALUE: Value, CONDITION: Condition, OPT: Option} = r;
				let reqString = "";

				switch (r.type) {
					case Stat.Core:
						if (cmp(player.getStat(r.type, r.name), r.value, r.condition) == false) {
							statusFlag = false;
							if (r.condition == "lt" || r.condition == "lte") {
								reqString = PR.colorizeString(r.value, r.name + " stat is too high");
							} else {
								reqString = PR.colorizeString(r.value, r.name + " stat is too low");
							}
						}
						break;
					case Stat.Skill:
						if (cmp(player.getStat(r.type, r.name), r.value, r.condition) == false) {
							statusFlag = false;
							if (r.condition == "lt" || r.condition == "lte") {
								reqString = PR.colorizeString(r.value, r.name + " skill is too high");
							} else {
								reqString = PR.colorizeString(r.value, r.name + " skill is too low");
							}
						}
						break;
					case Stat.Body:
						if (cmp(player.getStat(r.type, r.name), r.value, r.condition) == false) {
							statusFlag = false;
							if (r.condition == "lt" || r.condition == "lte") {
								reqString = PR.colorizeString(r.value, "Too much " + r.name);
							} else {
								reqString = PR.colorizeString(r.value, "Not enough " + r.name);
							}
						}
						break;
					case "meta":
						let pStat = 0;
						if (r.name == 'beauty') pStat = player.beauty;
						if (cmp(pStat, r.value, r.condition) == false) {
							statusFlag = false;
							if (r.condition == "lt" || r.condition == "lte") {
								reqString = PR.colorizeString(r.value, "Too much " + r.name.toLowerCase());
							} else {
								reqString = PR.colorizeString(r.value, "Not enough " + r.name.toLowerCase());
							}
						}
						break;
					case "money":
						if (cmp(player.money, r.value, r.condition) == false) {
							statusFlag = false;
							reqString = `@@.attention;Need Money (${r.value - player.money})`;
						}
						break;
					case "tokens":
						if (cmp(player.tokens, r.value, r.condition) == false) {
							statusFlag = false;
							reqString = `@@.attention;Need Tokens (${r.value - player.tokens})`;
						}
						break;
					case "npcStat":
						if (typeof npc === "string") npc = setup.world.npc(npc);
						if (typeof r.option !== 'undefined') npc = setup.world.npc(r.option);
						if (typeof npc !== 'undefined') {
							if (cmp(npc.getStat(r.name), r.value, r.condition) == false) {
								statusFlag = false;
								if (r.condition == "lt" || r.condition == "lte") {
									reqString = PR.colorizeString(r.value, npc.name + "'s " + r.name + " is too high");
								} else {
									reqString = PR.colorizeString(r.value, npc.name + "'s " + r.name + " is too low");
								}
							}
						}
						break;
					case "daysPassed":
						const offset = r.name ? Quest.getFlag(player, r.name) as number : 0;
						statusFlag = cmp(setup.world.day, r.value + offset, r.condition);
						if (!statusFlag) {
							const diffDays = setup.world.day - r.value - offset;
							const waitConditions: Data.Tasks.Condition[] = ["eq", "gt", "gte"];
							if (waitConditions.includes(r.condition ?? "eq")) {
								reqString = String.format("wait {0} days", diffDays);
							} else {
								reqString = diffDays > 0 ? `${diffDays} days left` : `expired ${0 - diffDays} days ago`;
							}
						}
						break;
					case "quest":
						if (!cmpAny(Quest.byTag(r.name).state(player), r.value, r.condition)) {
							// TODO "cancomplete" is not grammatically correct
							reqString = `@@.attention;Quest '${Data.quests[r.name].title}' not ${r.value}`;
							statusFlag = false;
						}
						break
					case "questFlag":
						if (cmpAny(player.questFlags[r.name], r.value, r.condition ?? "eq") === false) {
							if (Data.quests[r.name] !== undefined) {
								reqString = `@@.attention;Quest '${Data.quests[r.name].title}' not complete@@`;
							}
							statusFlag = false;
						}
						break;
					case "jobFlag":
						if (cmpAny(player.jobFlags[r.name], r.value, r.condition ?? "eq") == false) {
							statusFlag = false;
						}
						break;
					case "item":
						{
							const itm = player.getItemById(r.name);
							if (itm === undefined) {
								statusFlag = false;
								reqString = `@@.state-negative;Missing item '${r.name}' &times;${r.value}@@`;
							} else {
								if (!(itm instanceof Items.Clothing)) {
									if (cmp(itm.charges, r.value, r.condition) == false) {
										statusFlag = false;
										reqString = `@@.state-negative;Missing item '${r.name}' &times;${r.value}@@`;
									}
								} else {
									// this is clothing
									// TODO sort out quantity and name printout:
									// What if CLOTHING/ specify VALUE greater than 1?
									statusFlag = (r.value) === 1;
									if (!statusFlag) {
										reqString = `@@.state-negative;Missing clothing ${r.name}@@`;
									}
								}
							}
						}
						break;
					case "equipped":
						if (!player.isEquipped(r.name)) {
							statusFlag = false;
							reqString = `@@.state-negataive;Must have '${r.name}' equipped.@@`;
						}
						break;
					/* TODO: Refactor this to check also for wearing specific items. **/
					case "isWearing": {
						const eqItem = r.slot !== undefined ? player.getEquipmentInSlot(r.slot) : player.wardrobeItem(r.name);
						statusFlag = (eqItem !== null) === r.value;

						if (r.isLocked !== undefined && eqItem && (eqItem.isLocked == r.isLocked) !== r.value) {
							statusFlag = false;
						}
					}
						break;
					case "styleCategory":
						if (cmp(player.getStyleSpecRating(Data.Fashion.Style[r.name]), r.value, r.condition) == false) {
							statusFlag = false;
							if (r.condition == "lt" || r.condition == "lte") {
								reqString = PR.colorizeString(r.value, `Too much style '${r.name}'`);
							} else {
								reqString = PR.colorizeString(r.value, `Not enough style '${r.name}'`);
							}
						}
						break;
					case "style":
						if (cmp(player.style, r.value, r.condition) == false) {
							statusFlag = false;
							if (r.condition == "lt" || r.condition == "lte") {
								reqString = PR.colorizeString(r.value, "Style and Grooming too high");
							} else {
								reqString = PR.colorizeString(r.value, "Style and Grooming too low");
							}
						}
						break;
					case "hairStyle":
						if ((player.getHairStyle() == r.name) != r.value) {
							statusFlag = false;
							reqString = `@@.state-negative;Need '${r.name}' hair style@@`;
						}
						break;
					case "hairColor":
						if ((player.getHairColor() == r.name) != r.value) {
							statusFlag = false;
							reqString = `@@.state-negative;Need '${r.name}' hair color@@`;
						}
						break;
					case "portName":
						if ((player.getShipLocation().title == r.name) != r.value) {
							statusFlag = false;
							reqString = `@@.state-negative;Need Port'${r.name}'@@`;
						}
						break;
					case "inPort":
						if (player.isInPort(r.value) != r.condition) {
							statusFlag = false;
						}
						break;
					case "trackCustomers":
						const flag = player.questFlags[`track_${r.name}`] as number;
						const count = player.getHistory("customers", r.name);
						if (count - flag < r.value) statusFlag = false;
						break;
					case "trackProgress":
						statusFlag = cmp(Quest.getProgressValue(player, r.name), r.value, r.condition);
						break;
				}

				if (reqString != "") missingRequirements.push(reqString);
				if (!statusFlag && result) {result = false;}
			}

			return result;
		}

		protected abstract availableScenes(player: Entity.Player, npc: Entity.NPC): Scene[];

		protected get missingRequirements(): string[] {
			return this._missingRequirements;
		}

		protected clearMissingRequirements(): void {
			this._missingRequirements = [];
		}

		/**
		 * Play the selected task scenes.
		 * @virtual
		 */
		playScenes(player:Entity.Player, npc: Entity.NPC): Scene[] {
			const results: Data.Tasks.CheckResults = {};

			Task.debug("PlayScenes", "Started");
			this._sceneBuffer = [];

			const scenes = this.availableScenes(player, npc);
			for (const s of scenes) {
				s.calculateScene(results);
				this._sceneBuffer.push(s);
			}

			Task.debug("PlayScenes", "Ended");
			return this._sceneBuffer.filter(s => s.triggered);
		}

		completeScenes(_player: Entity.Player): void {
			this._sceneBuffer.filter(s => s.triggered).forEach(s => s.completeScene())
		}

		/**
		 * Tokenize a string and return result.
		 */
		protected tokenize(player: Entity.Player, npc: Entity.NPC, str?: string): string {
			if (typeof str == 'undefined') return "";
			return PR.tokenizeString(player, npc, str);
		}

		abstract get pay(): number;
		abstract get tokens(): number;

		abstract get flagType(): Entity.FlagType;

		static get debugPrintEnabled(): boolean {
			return Task._debug;
		}

		static set debugPrintEnabled(v: boolean) {
			Task._debug = v;
		}

		static debug(fun: string, str: string): void {
			if (Task.debugPrintEnabled == true)
				console.debug(fun + ":" + str + "\n");
		}

		private static _debug: boolean;
	}

	interface RewardItem {
		name: Data.ItemNameTemplateAny;
		value: number;
		opt?: "random" | "wear";
	}

	interface ItemChoiceElement {
		name: Data.ItemNameTemplateAny;
		value: number;
	}
	export class SceneRewards {
		pay: number;
		tokens: number;
		// ScaledValues: number[];
		items: (RewardItem|number)[];
		itemChoices: ItemChoiceElement[];
		chosenItem: number;
		slotUnlockCount: number;
		constructor() {
			this.pay = 0;
			this.tokens = 0;
			// this.ScaledValues = [];
			this.items = [];
			this.itemChoices = [];
			this.chosenItem = -1;
			this.slotUnlockCount = 0;
		}
	}

	/**
	 * Stores and plays a "scene" from a job.
	*/
	export class Scene {
		private static _debug = false;
		private readonly _task: Task;
		protected player: Entity.Player;
		private readonly _npc: Entity.NPC;
		private readonly _sceneData: Data.Tasks.JobScene;
		private _triggered: boolean;
		protected strBuffer: string;
		private _results: Data.Tasks.CheckResults;
		private _rewardItems: SceneRewards;

		constructor(task: Task, player: Entity.Player, npc: Entity.NPC, sceneData: Data.Tasks.JobScene) {
			this._task = task;
			this.player = player;
			this._npc = npc;
			this._sceneData = sceneData;
			this._triggered = false;
			this.strBuffer = "";
			this._results = {};
			this._rewardItems = new SceneRewards();
		}

		get id(): string {return this._sceneData.id;}
		get triggered(): boolean {return this._triggered;}
		print(): string {return (this.strBuffer !== "") ? this.strBuffer + "\n" : "";}
		get results(): Data.Tasks.CheckResults {return this._results;}
		get rewardItems(): SceneRewards {return this._rewardItems;}

		/**
		 * Tokenize a string
		 */
		protected tokenize(str: string): string {
			return PR.tokenizeString(this.player, this._npc, str);
		}

		/**
		 * Process a Trigger rule and return true/false state on if it fires.
		 */
		private _processTrigger(t: Data.Tasks.Triggers.Any, checks: Data.Tasks.CheckResults): boolean {
			Scene.debug('_ProcessTrigger', Utils.dirObject(t));

			switch (t.type) {
				case "npcStat":
					return cmp(this._npc.getStat(t.name), t.value, t.condition);
				case "random100":
					return cmp(Math.ceil((100 * Math.random()) + 1), t.value, t.condition);
				case "counter":
					const counterValue = this.player.flagValue(this._task.flagType, t.name, 0);
					if (t.opt === "random")
						return cmp(counterValue as number, Math.ceil(t.value * Math.random()), t.condition);
					return cmp(counterValue as number, t.value, t.condition);
				case Stat.Core:
				case Stat.Body:
				case Stat.Skill:
					if (t.opt === "random")
						return cmp(this.player.getStatPercent(t.type, t.name), Math.ceil((100 * Math.random()) + 1), t.condition);
					return cmp(this.player.getStatPercent(t.type, t.name), t.value, t.condition);
				case "flagSet":
					return t.value ? this.player.jobFlags.hasOwnProperty(t.name) : !this.player.jobFlags.hasOwnProperty(t.name)
				case "tag":
					if (checks.hasOwnProperty(t.name) == false) return true;
					const percent = Math.ceil((checks[t.name].result / checks[t.name].value) * 100);
					return cmp(percent, t.value, t.condition);
				case 'hasItem':
					// Format like CATEGORY/NAME
					return (typeof this.player.getItemById(t.name) !== 'undefined');
				case "trackProgress":
					return cmp(t.value, Quest.getProgressValue(this.player, t.name), t.condition);
			}
		}

		/**
		 * Check the status of each trigger in a group and return true/false on if the scene fires
		 */
		private _processTriggers(triggers: Data.Tasks.Triggers.Any[], checks: Data.Tasks.CheckResults) {
			const anyFailed = triggers.some((t) => {
				const r = this._processTrigger(t, checks);
				Scene.debug("_ProcessTriggers", `Trigger result: ${r}`);
				return !r;
			});
			return !anyFailed;
		}

		/**
		 * Check the status of each trigger in a group and return true/false on if the scene fires
		 */
		private _processTriggersAny(triggers: Data.Tasks.Triggers.Any[], checks: Data.Tasks.CheckResults) {
			return triggers.some((t) => {
				const r = this._processTrigger(t, checks);
				Scene.debug("_ProcessTriggersAny", `Trigger result: ${r}`);
				return r;
			});
		}

		/**
		 * Process a reward rule from a scene and fill _RewardItems and _RewardObjects
		 */
		private _calculateReward(post: Data.Tasks.Posts.Any, checks: Data.Tasks.CheckResults) {
			Scene.debug("_CalculateReward", `${Utils.dirObject(post)}, ${Utils.dirObject(checks)}`);

			let val: number | undefined = undefined;

			switch (post.type) {
				case "money":
					val = Tasks.postNumericValue(post, checks);
					this._rewardItems.pay += Math.floor(val);
					Scene.debug("_CalculateReward", "Pay +=" + this._rewardItems.pay.toString());
					break;
				case "tokens":
					val = Tasks.postNumericValue(post, checks);
					this._rewardItems.tokens += Math.floor(val);
					Scene.debug("_CalculateReward", "Tokens +=" + this._rewardItems.tokens.toString());
					break;
				case "item": // any item by ID
					val = Tasks.postNumericValue(post, checks);
					Scene.debug("_CalculateReward", "Items.push(" + {name: post.name, value: val, ppt: post.opt}.toString() + ")");
					this._rewardItems.items.push({name: post.name, value: val, opt: undefined});
					break;
				case Items.Category.Food:
				case Items.Category.Drugs:
				case Items.Category.Cosmetics:
				case Items.Category.LootBox:
					val = Tasks.postNumericValue(post, checks);
					Scene.debug("_CalculateReward", "Items.push(" + {name: Items.makeId(post.type, post.name), value: val}.toString() + ")");
					this._rewardItems.items.push({name: Items.makeId(post.type, post.name), value: val});
					break;
				case Items.Category.Clothes:
				case Items.Category.Weapon:
					val = Tasks.postNumericValue(post, checks);
					if (this.player.ownsWardrobeItem(post.name)) {
						const clothCost = Math.floor(Items.calculateBasePrice(post.type, post.name) * 0.3);
						this._rewardItems.pay += clothCost;
						this._rewardItems.items.push(clothCost);
						break;
					} else {
						this._rewardItems.items.push({name: Items.makeId(post.type, post.name), value: val, opt: post.wear});
					}
					break;
				case "pickItem":
					const item = Items.pickItem(post.name, post.value);
					if (item != null) {
						if (item.cat == 'clothes' && this.player.ownsWardrobeItem(item.tag)) {
							const itemCost = Math.floor(Items.calculateBasePrice(item.cat, item.tag) * 0.3);
							this._rewardItems.pay += itemCost;
							this._rewardItems.items.push(itemCost);
						} else {
							this._rewardItems.items.push({name: Items.makeId(item.cat, item.tag), value: 1});
						}
					}
					break;
				case "itemChoice":
					this._rewardItems.itemChoices.push({name: post.name, value: Tasks.postNumericValue(post, checks)});
					break;
				case "slot":
					this._rewardItems.slotUnlockCount += 1;
					break;
			}
		}

		/**
		 * Process a reward rule from a scene.
		 */
		private _applyReward(r: Data.Tasks.Posts.Any, checks: Data.Tasks.CheckResults) {
			Scene.debug("_ApplyReward", Utils.dirObject(r));

			switch (r.type) {
				case "money":
				case "tokens":
					// The total scene pay is handled upstream
					break;
				case "item": // any item by ID
				case Items.Category.Food:
				case Items.Category.Drugs:
				case Items.Category.Cosmetics:
				case Items.Category.LootBox:
				case Items.Category.Clothes:
				case "pickItem":
					const itemRec = this._rewardItems.items.shift();
					if (_.isUndefined(itemRec)) {
						throw `Exhausted reward items array while applying rewards for scene ${this.id}`;
					}
					if (typeof (itemRec) !== 'number') { // if item was converted to money, its value was added to the _RewardItems.Pay
						const n = Items.splitId(itemRec.name);
						if (itemRec.value > 0) {
							const wear =  r.type === Items.Category.Clothes ? r.wear : undefined;
							if (Items.isEquipmentCategory(n.category)) {
								this.player.addItem(n.category, n.tag, undefined, wear);
							} else {
								this.player.addItem(n.category, n.tag, itemRec.value);
							}
						} else {
							this.player.takeItem(itemRec.name, 0 - itemRec.value);
						}
					}
					break;
				case "itemChoice":
					if (this._rewardItems.chosenItem >= 0 && this._rewardItems.itemChoices.length > 0) {
						const val = Tasks.postNumericValue(r, checks);
						const chosenItemRec = this._rewardItems.itemChoices[this._rewardItems.chosenItem];
						const nameSplit = Items.splitId(chosenItemRec.name);
						if (val >= 0) {
							this.player.addItem(nameSplit.category, nameSplit.tag, val);
						} else {
							this.player.takeItem(chosenItemRec.name, 0 - val);
						}
						this._rewardItems.itemChoices = [];
					}
					break;
				case "statXp":
				case "bodyXp":
				case "skillXp":
					if (r.name == CoreStat.Willpower) { // Override and apply corruption effect.
						this.player.corruptWillPower(Tasks.postNumericValue(r, checks), 75); // default scaling for jobs.
					} else {
						const nameTypeMap = {
							statXp: Stat.Core,
							bodyXp: Stat.Body,
							skillXp: Stat.Skill
						};
						this.player.adjustXP(nameTypeMap[r.type], r.name, Tasks.postNumericValue(r, checks));
					}
					break;
				case "corruptWillpower":
					let difficulty = 20;
					if (r.name == 'medium') {
						difficulty = 50;
					} else if (r.name == 'high') {
						difficulty = 75;
					}
					this.player.corruptWillPower(r.value, difficulty);
					break;
				case Stat.Core:
					this.player.adjustStat(r.name, Tasks.postNumericValue(r, checks));
					break;
				case Stat.Body:
					this.player.adjustBody(r.name, Tasks.postNumericValue(r, checks));
					break;
				case Stat.Skill:
					this.player.adjustSkill(r.name, Tasks.postNumericValue(r, checks));
					break;
				case "quest":
					const q = new Quest(Data.quests[r.name]);
					if (r.value === "start") q.accept(this.player);
					if (r.value === "complete") q.complete(this.player);
					break;
				case "jobFlag":
				case "questFlag": {
					const flags = r.type === "jobFlag" ? this.player.jobFlags : this.player.questFlags;
					if (r.value == undefined) {
						delete flags[r.name];
					} else if (r.op === "add") {
						this.player.increaseFlagValue(r.type === "jobFlag" ? Entity.FlagType.Job : Entity.FlagType.Quest, r.name, r.value as number);
					} else {
						flags[r.name] = r.value;
					}
				}
					break;
				case "counter":
					if (r.value !== undefined) {
						this.player.increaseFlagValue(this._task.flagType, r.name, r.value);
					} else {
						this.player.deleteFlag(this._task.flagType, r.name);
					}
					break;
				case "store":
					if (setup.world.storeInventory[r.name].inventory.length < 1) {
						StoreEngine.openStore(this.player, this._npc);
					}

					if (r.opt == "lock") StoreEngine.toggleStoreItem(r.name, r.value, 1);
					if (r.opt == "unlock") StoreEngine.toggleStoreItem(r.name, r.value, 0);
					break;
				case "resetShop":
					if (setup.world.storeInventory[r.name].inventory.length > 1) {
						setup.world.storeInventory[r.name].lastStocked = 0;
					}
					break;
				case "npcStat":
					(r.npc ? npcByTag(r.npc) : [this._npc]).forEach(npc => npc.adjustStat(r.name, Tasks.postNumericValue(r, checks)));
					break;
				case "sailDays":
					this.player.advanceSailDays(Tasks.postNumericValue(r, checks));
					break;
				case "setClothingLock":
					this.player.clothing.setLock(r.slot, r.value);
					break;
				case "slot":
					this.player.unlockSlot();
					break;
				case "trackCustomers":
					// Let's set a tag in the player to start tracking their history
					Quest.setFlag(this.player, `track_${r.name}`, this.player.getHistory("customers", r.name));
					break;
				case "trackProgress": {
					if (r.value === undefined) {
						Quest.setProgressValue(this.player, r.name, r.value);
					} else {
						const curVal = Quest.getProgressValue(this.player, r.name);
						const act = r.op ?? "add";
						let newVal = curVal;
						let value = r.value;
						if (r.factor) {
							value = r.value * checks[r.factor].mod;
						}
						switch (act) {
							case "add":
								newVal += value
								break;
							case "multiply":
								newVal *= value;
								break;
							case "set":
								newVal = value;
								break;
						}
						Quest.setProgressValue(this.player, r.name, newVal);
					}
				}
					break;
				case "bodyEffect":
					if (r.value) {
						this.player.addBodyEffect(r.name);
					} else {
						this.player.removeBodyEffect(r.name);
					}
					break;
				case "effect":
					Data.effectLib[r.name].fun(this.player);
					break;
				case "gameState":
					switch (r.name) {
						case "dayPhase":
							setup.world.advancePhaseTo(Tasks.postNumericValue(r, checks));
							break;
					}
					break;
				case "saveDate":
					this.player.setFlagValue(this._task.flagType, r.name, setup.world.day);
					break;
			}

			function npcByTag(tag: string | string[], npcs?: Entity.NPC[]): Entity.NPC[] {
				const res: Entity.NPC[] = npcs ?? [];
				if (Array.isArray(tag)) {
					tag.forEach(t => npcByTag(t, res));
				} else {
					if (tag.startsWith('@')) { // npc group
						Data.npcGroups[tag.slice(1)].forEach((t) => {res.push(setup.world.npc(t));});
					} else {
						res.push(setup.world.npc(tag));
					}
				}
				return res;
			}
		}

		/**
		 * Process a check rule on a a scene, store results in this._Checks[Tag]
		 */
		private _processCheck(check: Data.Tasks.Checks.Any, results: Data.Tasks.CheckResults) {
			const scaling = (check.opt != "noScaling");
			const returnRawValue = check.opt === "raw";
			let value = check.value ?? 100;
			let result = 0;

			if (returnRawValue) {
				switch (check.type) {
					case Stat.Body:
					case Stat.Core:
					case Stat.Skill:
						result = this.player.getStat(check.type, check.name);
						break;
					case "func":
						result = check.func(this.player, this, results);
						value = 1;
						break;
					case "meta":
						switch (check.name) {
							case "beauty":
								result = this.player.beauty;
								break;
							case "danceStyle":
								result = (this.player.getStyleSpecRating(Data.Fashion.Style.SexyDancer) + this.player.getStyleSpecRating(JobEngine.instance.getDance())) / 2;
								break;
						}
				}
			} else {
				switch (check.type) {
					case Stat.Core:
					case Stat.Body:
						result = this.player.statRoll(check.type, check.name, check.difficulty, value, scaling);
						break;
					case Stat.Skill:
						// This skill is inoperable until you get some points in it.
						if (check.name == Skills.Charisma.Courtesan && this.player.getStat(Stat.Skill, check.name) == 0) {
							result = 0;
							break;
						}
						result = this.player.skillRoll(check.name, check.difficulty, value, scaling);
						break;
					case "meta":
						switch (check.name) {
							case "beauty":
								result = Entity.Player.genericRoll(this.player.beauty, check.difficulty, value, scaling);
								break;
							case "danceStyle":
								const defaultStyleResult = Entity.Player.genericRoll(
									this.player.getStyleSpecRating(Data.Fashion.Style.SexyDancer), check.difficulty, value, scaling);
								const specStyleResult = Entity.Player.genericRoll(
									this.player.getStyleSpecRating(JobEngine.instance.getDance()), check.difficulty, value, scaling);
								result = (defaultStyleResult + specStyleResult) / 2;
								break;
						}
						break;
					case "func":
						result = check.func(this.player, this, results);
						break;
				}
			}
			const r: Data.Tasks.CheckResult = {
				result: result,
				value: value,
				mod: result / value
			};
			Scene.debug("_ProcessCheck", `${check.tag}: ${JSON.stringify(r)}`);
			results[check.tag] = r;
			this._results[check.tag] = r;
		}

		/**
		 * Process all the checks in a scene.
		 */
		private _processChecks(checks: Data.Tasks.Checks.Any[], results: Data.Tasks.CheckResults) {
			for (const check of checks)
				this._processCheck(check, results);
		}

		/**
		 * Process all the "POST" flags in a scene. Generally they give rewards/set status flags.
		 * @param Posts
		 * @param {object} Checks
		 */
		private _processPost(posts: Data.Tasks.Posts.Any[], checks: Data.Tasks.CheckResults) {
			Scene.debug("_ProcessPost", JSON.stringify(checks));
			posts.forEach((p) => {this._calculateReward(p, checks)});
		}

		/**
		 * Iterate through all of the text fragments, filter them basing on check results for a scene,
		 * and then combine them into a string.
		 */
		private _sceneText(checks: Data.Tasks.CheckResults): string {
			const text = this._sceneData.text ?? "";
			if (typeof text === "string") {
				return text;
			}

			let results: string[] = [];
			const usedTags: Set<string> = new Set();
			let percent = 0;
			const calcCheckValue = (check: Data.Tasks.CheckResult) => check.value > 1 ? Math.floor(check.mod * 100) : check.result;
			const colorize =  (a: string) => (typeof a !== 'undefined') ? PR.colorizeString(percent, a.slice(2, -2), 100) : "";

			for (const textFragment of text) {
				if (typeof textFragment === "string") {
					results.push(textFragment);
					usedTags.clear(); // allows to process the next set of choices
				} else {
					const tags = Object.keys(textFragment).filter(k => k !== "text");
					const combinedTag = tags.join('&');
					if (!usedTags.has(combinedTag)) {
						let pass = true;
						for (const t of tags) {
							console.assert(checks.hasOwnProperty(t) && typeof (textFragment[t]) === "number",
								`Wrong tag reference in the 'text' property of scene ${this._sceneData.id} for tag ${t}`);
							const checkValue = calcCheckValue(checks[t]);
							if (checkValue > textFragment[t]) {
								pass = false;
								break;
							} else {
								if (checks[t].value > 1) {
									percent = checkValue;
								}
							}
						}
						if (pass) {
							usedTags.add(combinedTag);
							 // use the last computed percent for the colorizing
							results.push(textFragment.text.replace(/@@((?!color:).)+?@@/g, colorize));
						}
					}
				}
			}

			results = results.filter(r => r !== "");
			return results.length > 0 ? " " + results.join(" ") : "";
		}

		/**
		 * Process all the rules in a scene and store the results in the object.
		 */
		calculateScene(checks: Data.Tasks.CheckResults): void {
			Scene.debug(this.id + ":CalculateScene", "Started");

			this._results = checks;
			const triggers = this._sceneData.triggers ?? [];
			if ((triggers.length > 0) && !this._processTriggers(triggers, checks)) return;
			const triggersAny = this._sceneData.triggersAny ?? [];
			if ((triggersAny.length > 0) && !this._processTriggersAny(triggersAny, checks)) return;

			this._triggered = true;
			Scene.debug(this.id + ":CalculateScene", 'Scene triggered');

			this._processChecks(this._sceneData.checks ?? [], checks);
			const posts = this._sceneData.post ?? [];
			if (posts.length > 0) this._processPost(posts, checks);
			this.strBuffer = this.tokenize(this._sceneText(checks));

			Scene.debug(this.id + ":CalculateScene", "Ended");
		}

		completeScene(): void {
			Scene.debug(this.id + ":CompleteScene", "Started");
			this.player.earnMoney(this._rewardItems.pay, GameState.CommercialActivity.Jobs);
			this.player.adjustTokens(this._rewardItems.tokens);
			const posts = this._sceneData.post ?? [];
			for (const p of posts) {
				this._applyReward(p, this._results);
			}
			Scene.debug(this.id + ":CompleteScene", "Ended");
		}

		static get debugPrintEnabled(): boolean {
			return Scene._debug;
		}

		static set debugPrintEnabled(v: boolean) {
			Scene._debug = v;
		}

		static debug(fun: string, str: string): void {
			if (Scene.debugPrintEnabled)
				console.debug(fun + ":" + str + "\n");
		}
	}

	// --------------------- Jobs ----------------------------
	/**
	 * Represents a job.
	 */
	export class Job extends Task {
		constructor(data: Data.Tasks.Job) {
			super(data);
		}

		override get taskData(): Data.Tasks.Job {
			return super.taskData as Data.Tasks.Job;
		}

		// eslint-disable-next-line class-methods-use-this
		override get flagType(): Entity.FlagType {
			return Entity.FlagType.Job;
		}

		get pay(): number {return this.taskData.pay ?? 0;}
		get tokens(): number {return this.taskData.tokens ?? 0;}
		get phases(): NonEmptyArray<DayPhase> {return this.taskData.phases;}

		/**
		 * Check to see if the player meets the other requirements to take the job.
		 *  Usually skill, stat, body related or quest flags.
		 */
		requirements(player: Entity.Player, npc: Entity.NPC | string): boolean {
			this.clearMissingRequirements();
			return Task.checkRequirements(player, npc, this._req(), this.missingRequirements);
		}

		ratingAndCosts(): string {
			let res = "";
			for (let i = 0; i < this.taskData.rating; i++) res += " &#9733;";
			res = PR.colorizeString(this.taskData.rating, res, 5);

			const time = this._getCost("time");
			const energy = this._getCost(Stat.Core, CoreStat.Energy);
			const money = this._getCost("money");
			const tokens = this._getCost("tokens");
			const strings: string[] = [];

			if (time != 0) strings.push(`@@.item-time;Time ${time}@@`);
			if (energy != 0) strings.push(`@@.item-energy;Energy ${energy}@@`);
			if (money != 0) strings.push(`@@.item-money;Money ${money}@@`);
			if (tokens != 0) strings.push(`@@.item-courtesan-token;Tokens ${tokens}@@`);

			if (strings.length != 0) res += ` [${strings.join("&nbsp;")}]`;
			return res;
		}

		/**
		 * Print the title of a job.
		 * @param brief print brief description
		 */
		override title(brief = false): string {
			const output = this.taskData.title;
			if (brief) return output;

			return `${output} ${this.ratingAndCosts()}`;
		}

		/**
		 * Calculate cost of a job
		 */
		private _getCost(type: Data.Tasks.Costs.CostType, name?: string) {
			let check: Data.Tasks.Costs.Any[] = [];
			if (typeof name !== 'undefined') {
				check = $.grep(this._cost(), (c) => {
					return c.type == type && ((c.type == Stat.Core || c.type == Stat.Body || c.type == Stat.Skill || c.type == "item") && c.name == name);
				});
			} else {
				check = $.grep(this._cost(), c => c.type == type);
			}
			if (typeof check !== 'undefined' && check.length > 0) return check[0].value;
			return 0;
		}

		get energyCost(): number {
			return this._getCost(Stat.Core, CoreStat.Energy);
		}

		get timeCost(): number {
			return this._getCost("time");
		}

		/**
		 * Check to see if the player meets the "Cost" requirement for a job. Usually energy as this
		 * is subtracted from their status upon taking the job.
		 */
		cost(player: Entity.Player): boolean {
			if (typeof this._cost() === 'undefined') return true;

			const checkFail = (c: Data.Tasks.Costs.Any) => {
				switch (c.type) {
					case Stat.Core:
					case Stat.Body:
					case Stat.Skill:
						return player.getStat(c.type, c.name) < c.value;
					case "money":
						return player.money < c.value;
					case "tokens":
						return player.tokens < c.value;
					default:
						return false;
				}
			};
			return this._cost().some(checkFail) ? false : true;
		}

		/**
		 * Deduct the costs of the Job from the player (usually energy, but could be other stats)
		 */
		private _deductCosts(player: Entity.Player) {
			if (typeof this._cost() === 'undefined') return;
			for (const c of this._cost()) {
				Job.debug("_DeductCosts", Utils.dirObject(c));
				switch (c.type) {
					case Stat.Core:
						player.adjustStat(c.name, Math.floor(c.value * -1.0));
						break;
					case Stat.Skill:
						player.adjustSkill(c.name, Math.floor(c.value * -1.0));
						break;
					case Stat.Body:
						player.adjustBody(c.name, Math.floor(c.value * -1.0));
						break;
					case "money":
						player.spendMoney(Math.floor(c.value), GameState.CommercialActivity.Jobs);
						break;
					case "tokens":
						player.adjustTokens(Math.floor(c.value * -1.0));
						break;
					case "item":
						if (Items.isEquipment(c.name)) {
							// TODO
							// player.clothing.removeItem(c.name);
						} else {
							player.inventory.removeItem(c.name);
						}
						break;
					case "time":
						setup.world.nextPhase(c.value);
						break;
				}
			}
		}

		/**
		 * Check to see if the time cost falls within the open/allowed phases of the activity and that there
		 * is enough time in the day to complete it.
		 */
		private _checkTime() {
			return (($.inArray(setup.world.phase, this.phases) != -1) && (this._getCost("time") + setup.world.phase <= 4));
		}

		/**
		 * Check to see if the time cost falls within the open/allowed phases of the activity
		 */
		private _checkReady(player: Entity.Player): boolean;
		private _checkReady(player: Entity.Player, opt: false): boolean;
		private _checkReady(player: Entity.Player, opt: true): number;
		private _checkReady(player: Entity.Player, opt = false): boolean | number {
			const key = this.id() + "_LastCompleted";
			const flagValue = player.jobFlags[key] ? player.jobFlags[key] as number : 0;
			if (opt) return (flagValue + this.taskData.days - setup.world.day);
			return ((flagValue == 0) || (flagValue + this.taskData.days <= setup.world.day));
		}

		/**
		 * Set's the last completed flag for the job.
		 */
		private _setLastCompleted(player: Entity.Player) {
			const key = this.id() + "_LastCompleted";
			player.jobFlags[key] = setup.world.day;
		}

		/**
		 * Check to see if this job is available to be used.
		 */
		available(player: Entity.Player, npc: Entity.NPC): boolean {
			Job.debug("Job.Available:", this.id());
			Job.debug("Cost:", `${this.cost(player)}`);
			Job.debug("Requirements:", `${this.requirements(player, npc)}`);
			Job.debug("_CheckTime:", `${this._checkTime()}`);
			Job.debug("_CheckReady:", `${this._checkReady(player)}`);

			return this.cost(player) && this.requirements(player, npc) &&
				this._checkTime() && this._checkReady(player);
		}

		/**
		 * Print out the requirements (missing) string for a job.
		 */
		reqString(player: Entity.Player, npc: Entity.NPC): string {
			const output: string[] = [];

			const coolDown = this._checkReady(player, true);
			if (this.onCoolDown(player))
				output.push(`@@.state-neutral;Available in ${coolDown} day${coolDown > 1 ? "s@@" : "@@"}`);

			if (this.onCoolDown(player) == false && this._checkTime() == false) {
				output.push("@@.state-neutral;Only Available@@");
				for (let i = 0; i < this.phases.length; i++)
					output.push(PR.phaseIcon(this.phases[i]));
			}
			if (!this.requirements(player, npc)) {
				output.push(UI.pWithTooltip("(Requirements not met)", this.missingRequirements, ["state-negative"]));
			}
			return output.join(' ');
		}

		/**
		 * Check to see if a job is ready (if it's known about).
		 */
		ready(player: Entity.Player): boolean {
			return this.cost(player) && this._checkTime() && this._checkReady(player);
		}

		/**
		 * Return if the job is on cool down.
		 */
		onCoolDown(player: Entity.Player): boolean {
			Job.debug("OnCoolDown", JSON.stringify(player.jobFlags));
			return (this._checkReady(player) != true);
		}

		override completeScenes(player: Entity.Player): void {
			player.earnMoney(this.pay, GameState.CommercialActivity.Jobs);
			player.adjustTokens(this.tokens);
			this._deductCosts(player);
			this._setLastCompleted(player);
			super.completeScenes(player);
		}

		/**
		 * Print the intro to a job.
		 */
		intro(player: Entity.Player, npc: Entity.NPC): string {
			return PR.tokenizeString(player, npc, this.taskData.intro);
		}

		/**
		 * Print the "start" scene of a job.
		 */
		printStart(player: Entity.Player, npc: Entity.NPC): string {
			const text = this.taskData.start;
			return text ? this.tokenize(player, npc, text) + "\n" : "";
		}

		/**
		 * Print the "end" scene of a job.
		 */
		printEnd(player: Entity.Player, npc: Entity.NPC): string {
			const text = this.taskData.end;
			const jobEnd = text ? this.tokenize(player, npc, text) + "\n" : "";
			// if (this._SummarizeTask() != "") JobEnd += this._SummarizeTask();
			return jobEnd;
		}

		protected override availableScenes(player: Entity.Player, npc: Entity.NPC): Scene[] {
			return this.taskData.scenes.map(x => new JobScene(this, player, npc, x));
		}

		/**
		 * Tokenize a string and return result.
		 */
		protected override tokenize(player: Entity.Player, npc: Entity.NPC, str: string): string {
			if (typeof str == 'undefined') return "";
			str = str.replace(/JOB_PAY/g, this.printPay());

			if (str.indexOf("JOB_RESULTS") != -1) { // Kludge because replace evaluates function even if no pattern match. Dumb.
				str = str.replace(/JOB_RESULTS/g, this.printJobResults());
			}
			return super.tokenize(player, npc, str);
		}

		private _cost() {return this.taskData.cost ?? [];}
		private _req() {return this.taskData.requirements ?? [];}

		/**
		 * Match a result to a string and return the colorized version.
		 */
		 private _matchResult(tag: string, result: number, value: number): string {
			const output = "";
			const percent = Math.floor((result / value) * 100);

			const colorize = (a:string ): string => typeof a !== 'undefined' ? PR.colorizeString(percent, a.slice(2, -2), 100) : "";

			for (const jr of this.taskData.jobResults ?? [])
				if (percent <= jr[tag])
					return jr.text.replace(/@@((?!color:).)+?@@/g, colorize);

			return output;
		}

		/**
		 * Print the "job results" at the end of a job.
		 */
		protected printJobResults(): string {
			const checks = this.scenes[this.scenes.length - 1].results;
			const cKeys = Object.keys(checks);
			let results: string[] = [];

			for (let i = 0; i < cKeys.length; i++)
				results.push(this._matchResult(cKeys[i], checks[cKeys[i]].result, checks[cKeys[i]].value));
			results = results.filter(r => r != "");
			return results.length > 0 ? " " + results.join(" ") : results.join("");
		}

		/**
		 * Calculate the total pay from all scenes for the task.
		 */
		protected printPay(): string {
			let pay = this.taskData.pay;
			if (pay === undefined) pay = 0;
			for (const sb of this.scenes) {
				pay += sb.rewardItems.pay;
			}
			return (pay > 0) ? `<span class='item-money'>${pay}</span>` : "";
		}

		private _printTokens(): string {
			let tokens = this.taskData.tokens;
			if (tokens === undefined) tokens = 0;
			for (const sb of this.scenes) {
				tokens += sb.rewardItems.tokens;
			}
			return (tokens > 0) ? `<span class='item-courtesan-token'>${tokens}</span>` : "";
		}

		/**
		 * Print all of the items earned in this task
		 */
		/*
		private _SummarizeTask(): string {
			var items: string[] = [];
			var Pay = this.TaskData.PAY;
			var Tokens = this.TaskData.TOKENS;

			if (Pay === undefined) Pay = 0;
			if (Tokens === undefined) Tokens = 0;

			for (const sb of this.Scenes) {
				Pay += sb.RewardItems().Pay;
				Tokens += sb.RewardItems().Tokens;

				for (const ri of sb.RewardItems().Items) {
					var n = Items.SplitId(ri["Name"]);
					var oItem = Items.Factory(n.Category, n.Tag);
					items.push(oItem.Description + " x " + ri["Value"]);
				}
			}

			if (Tokens > 0) {
				items.unshift(`<span class='item-courtesan-token'>${Tokens} courtesan tokens</span>\n`);
			}

			if (Pay > 0) {
				items.unshift(`<span class='item-money'>${Pay} coins</span>\n`);
			}

			if (items.length > 0 || Pay > 0 || Tokens > 0) {
				items.unshift("<span style='color:cyan'>Your receive some items:</span>");
			}

			if (items.length > 0) return items.join("\n") + "\n";
			return "";
		}
*/
	}
	/**
	 * Stores and plays a "scene" from a job.
	*/
	class JobScene extends Scene {
		constructor(job: Job, player: Entity.Player, npc: Entity.NPC, sceneData: Data.Tasks.JobScene) {
			super(job, player, npc, sceneData);
		}

		/**
		 * Tokenize a string
		 */
		protected override tokenize(str: string) {
			// str = str.replace(/JOB_PAY/g, this.Pay());
			return super.tokenize(str);
		}
	}

	// ---------------------  Quests -------------------------
	export class Quest extends Task {
		/**
		 * Returns a list of quest entries that fit the criteria on the "flag" option.
		 * * cancomplete - Player can possibly complete these now.
		 * * available - Player can accept these quests.
		 * * completed - Quests the player has completed.
		 * * active - Quests that are currently active.
		 * * any - return all quests that match the above criteria.
		 * @param Flag
		 * @param Player
		 * @param npc - The NPC ID of the quest giver. Optional.
		 */
		static list(state: QuestState| QuestState[] | "any", player: Entity.Player, npc?: string): Quest[] {
			const allQuests = Object.values(Data.quests)
				.filter(qd => npc === undefined || qd.giver === npc)
				.map(qd => new Quest(qd));
			const npcObj = npc !== undefined ? setup.world.npc(npc) : undefined;

			if (state === "any") { // TODO clarify "any" and "unavailable" relation
				return allQuests.filter(q => q.state(player, npcObj) !== "unavailable");
			}

			const states = Array.isArray(state) ? state : [state];

			return allQuests.filter(q => states.includes(q.state(player, npcObj)));
		}

		/**
		 * Create quest by tag
		 */
		static byTag(tag: string): Quest {
			return new Quest(Data.quests[tag]);
		}

		/**
		 * Creates a quest object that operates a virtual quest
		 *
		 * The virtual quest does not have a definition (in Data.Quests)
		 * and its only property is the quest ID. Thus it can be used only for
		 * testing or setting quest flags.
		 */
		static virtualById(id: string): Quest {
			return new Quest({id: id} as Data.Tasks.Quest);
		}

		/**
		 * Retrieve a quest related flag.
		 */
		static getFlag(player: Entity.Player, flag: string): TaskFlag {
			if ((typeof player.questFlags[flag] === 'undefined')) return undefined;
			return player.questFlags[flag];
		}

		/**
		 * Set a quest flag value.
		 */
		static setFlag(player: Entity.Player, flag: string, value: TaskFlag): void {
			if (typeof value === 'undefined') {
				delete player.questFlags[flag];
			} else {
				player.questFlags[flag] = value;
			}
		}

		/**
		 * Helper function to read progress value
		 * @param player - player being checked.
		 * @param key - key from check entry.
		 */
		static getProgressValue(player: Entity.Player, key: string): number {
			const res = Quest.getFlag(player, "progress_" + key);
			return res === undefined ? 0. : res as number;
		}

		/**
		 * Helper function to read progress value
		 * @param player - player being checked.
		 * @param key - key from check entry.
		 * @param value value or undefined to remove the flag
		 */
		static setProgressValue(player: Entity.Player, key: string, value?: number): void {
			Quest.setFlag(player, "progress_" + key, value);
		}

		/**
		 * Handles time-dependent changes for active quest
		 */
		static nextDay(player: Entity.Player): void {
			Quest.list(QuestState.Active, player).forEach(q => q.nextDay(player));
		}

		/**
		 * @param player
		 * @param id Quest id
		 */
		static isActive(player: Entity.Player, id: string): boolean {
			return player.questFlags[id] === 'ACTIVE';
		}

		/**
		 * @param player
		 * @param id Quest id
		 */
		static isCompleted(player: Entity.Player, id: string): boolean {
			return Quest.getFlag(player, id) === 'COMPLETED';
		}

		static allMissingItems(player: Entity.Player): Record<Data.ItemNameTemplateAny, number> {
			return Quest.list(QuestState.Active, player)
				.map(q => q.missingItems(player))
				.reduce((acc: Record<Data.ItemNameTemplateAny, number>, qr) => {
					for (const e of Object.entries(qr)) {
						const ex = acc[e[0]];
						if (!ex) {
							acc[e[0]] = e[1]
						} else {
							acc[e[0]] = ex + e[1];
						}
					}
					return acc;
				}, {});
		}

		constructor(data: Data.Tasks.Quest) {
			super(data);
		}

		override get taskData(): Data.Tasks.Quest {
			return super.taskData as Data.Tasks.Quest;
		}

		// eslint-disable-next-line class-methods-use-this
		override get flagType(): Entity.FlagType {
			return Entity.FlagType.Quest;
		}

		get journalEntry(): string {return this.taskData.journalEntry;}
		get journalCompleteEntry(): string {return this.taskData.journalComplete;}
		get checks(): NonEmptyArray<Data.Tasks.Requirements.Any> | undefined {return this.taskData.checks;}
		get pay(): number { return 0;}
		get tokens(): number { return 0;}

		/**
		 * Maps scene
		 */
		private _makeSceneData(sceneId: QuestStage): Data.Tasks.JobScene {
			const res: Data.Tasks.JobScene = {
				id: sceneId,
				text: this.taskData[sceneId],
			};
			const addToPost = (item: Data.Tasks.Posts.Any) => {
				if (!res.post) {
					res.post = [clone(item)];
				} else {
					res.post.push(clone(item))
				}
			};

			switch (sceneId) {
				case "intro":
					this.taskData.onAccept?.forEach(addToPost);
					break;
				case "middle":
					break;
				case "finish":
					if (this.taskData.post) {
						res.post = clone(this.taskData.post);
					}
					if (this.taskData.reward) {
						this.taskData.reward.forEach(addToPost);
					}

					const isConsumableItem = (r: Data.Tasks.Requirements.Any): r is Data.Tasks.Requirements.Item => {
						return r.type === 'item' &&
						!r.name.startsWith(`${Items.Category.Clothes}/`);
					};

					if (this.taskData.checks) {
						const itemsToConsume = this.taskData.checks.filter(isConsumableItem);
						for (const itemRule of itemsToConsume) {
							addToPost({type: "item", value: 0 - itemRule.value, name: itemRule.name});
						}
					}
					break;
			}
			return res;
		}

		protected override availableScenes(player: Entity.Player, npc: Entity.NPC): Scene[] {
			const scenes: Scene[] = [];
			if (this.isAvailable(player, npc)) {
				scenes.push(new QuestIntroScene(player, npc, this._makeSceneData(QuestStage.Intro), this));
			} else if (this.canComplete(player, npc)) {
				scenes.push(new QuestFinishingScene(player, npc, this._makeSceneData(QuestStage.Finish), this));
			} else if (this.isActive(player)) {
				scenes.push(new QuestMiddleScene(player, npc, this._makeSceneData(QuestStage.Middle), this));
			}
			return scenes;
		}

		isAvailable(player: Entity.Player, npc?: Entity.NPC): boolean {
			if (this.isCompleted(player) || this.isActive(player)) return false;
			if (npc === undefined) npc = setup.world.npc(this.giver);
			return Task.checkRequirements(player, npc, this.taskData.pre ?? []);
		}

		isActive(player: Entity.Player): boolean {
			return Quest.isActive(player, this.id());
		}

		canComplete(player: Entity.Player, npc?: Entity.NPC): boolean {
			if ((typeof player.questFlags[this.id()] === 'undefined')) return false;
			if (player.questFlags[this.id()] == 'COMPLETED') return false; // Should never happen eh??
			if (npc === undefined) npc = setup.world.npc(this.giver);
			return Task.checkRequirements(player, npc, this.taskData.checks ?? []);
		}

		isCompleted(player: Entity.Player): boolean {
			return Quest.isCompleted(player, this.id());
		}

		state(player: Entity.Player, npc?: Entity.NPC): QuestState {
			if (Quest.isCompleted(player, this.id())) {
				return QuestState.Completed;
			}
			// npc is required for all further tests
			const lNPC = npc ? npc : setup.world.npc(this.giver);
			const questFlag = player.questFlags[this.id()];
			if (questFlag === undefined) { // available or unavailabel
				return (Task.checkRequirements(player, lNPC, this.taskData.pre ?? [])) ? QuestState.Available : QuestState.Unavailable;
			}
			return Task.checkRequirements(player, lNPC, this.taskData.checks ?? []) ? QuestState.CanComplete : QuestState.Active;
		}

		missingItems(player: Entity.Player): Record<Data.ItemNameTemplateAny, number> {
			const res: Record<Data.ItemNameTemplateAny, number> = {};
			for (const c of this.checks ?? []) {
				switch (c.type) {
					case "item":
						if (c.hidden) {
							continue;
						}
						const invItem = player.getItemById(c.name);
						if (invItem === undefined) {
							res[c.name] = c.value;
						} else if (invItem instanceof Items.Consumable && invItem.charges < c.value) {
							res[c.name] = c.value - invItem.charges;
						}
						break;
				}
			}
			return res;
		}

		/**
		 * Accepts the quest
		 *
		 * This method is not meant to be used for "Accept" button in the quest dialog because
		 * it creates its own copy of the INTRO scene.
		 */
		accept(player: Entity.Player): void {
			const scene = new QuestIntroScene(player, setup.world.npc(this.giver), this._makeSceneData(QuestStage.Intro), this);
			const checks = {};
			scene.calculateScene(checks);
			scene.completeScene();
		}

		/**
		 * Completes the quest
		 */
		complete(player: Entity.Player): void {
			const scene = new QuestFinishingScene(player, setup.world.npc(this.giver), this._makeSceneData(QuestStage.Finish), this);
			const checks = {};
			scene.calculateScene(checks);
			scene.completeScene();
		}

		/**
		 * Marks quest as completed without processing the finishing scene
		 */
		markAsCompleted(player: Entity.Player): void {
			Quest.setFlag(player, this.id(), "COMPLETED");
			Quest.setFlag(player, this.id() + "_CompletedOn", setup.world.day);
		}

		/**
		 * Handles time-dependent changes for active quest
		 */
		nextDay(player: Entity.Player): void {
			this.taskData.onDayPassed?.call(this, player);
		}

		/**
		 * Day when the quest was accepted by player
		 */
		acceptedOn(player: Entity.Player): number {
			const r = Quest.getFlag(player, this.id() + "_AcceptedOn");
			return typeof r === 'number' ? r : 1;
		}

		/**
		 * Day when the quest was completed by player
		 */
		completedOn(player: Entity.Player): number {
			const r = Quest.getFlag(player, this.id() + "_CompletedOn");
			return typeof r === 'number' ? r : 1;
		}
	}

	class QuestScene extends Scene {
		protected readonly quest: Quest;
		private readonly _sceneName: QuestStage;

		constructor(player: Entity.Player, npc: Entity.NPC, sceneData: Data.Tasks.JobScene,
			quest: Quest, sceneName: QuestStage) {
			super(quest, player, npc, sceneData);

			this.quest = quest;
			this._sceneName = sceneName;
		}

		override get id() {
			return this._sceneName;
		}
	}

	class QuestIntroScene extends QuestScene {
		constructor(player: Entity.Player, npc: Entity.NPC, sceneData: Data.Tasks.JobScene, quest: Quest) {
			super(player, npc, sceneData, quest, QuestStage.Intro);
		}

		/**
		 * Accept the quest and process triggers.
		 */
		override completeScene() {
			super.completeScene();
			Quest.setFlag(this.player, this.quest.id(), "ACTIVE");
			Quest.setFlag(this.player, this.quest.id() + "_AcceptedOn", setup.world.day);
		}
	}

	class QuestMiddleScene extends QuestScene {
		constructor(player: Entity.Player, npc: Entity.NPC, sceneData: Data.Tasks.JobScene, quest: Quest) {
			super(player, npc, sceneData, quest, QuestStage.Middle);
		}

		/**
		 * Process all the rules in a scene and store the results in the object.
		 */
		override calculateScene(checks: Data.Tasks.CheckResults) {
			super.calculateScene(checks);
			this.strBuffer += "<br/><br/>" + PR.pQuestRequirements(this.quest.id(), this.player);
		}
	}

	class QuestFinishingScene extends QuestScene {
		constructor(player: Entity.Player, npc: Entity.NPC, sceneData: Data.Tasks.JobScene, quest: Quest) {
			super(player, npc, sceneData, quest, QuestStage.Finish);
		}

		/**
		 * Completes the quest.
		 */
		override completeScene() {
			super.completeScene();
			this.quest.markAsCompleted(this.player);
		}
	}

	/**
	 * Helper function - runs comparisons.
	 */
	function cmp(a: number, b: number, c?: Data.Tasks.Condition): boolean;
	function cmp(a: string, b: string, c?: "eq" | "ne" | "==" | "!=" | boolean): boolean;
	function cmp(a: TaskFlag, b: TaskFlag, c?: Data.Tasks.Condition): boolean;
	function cmp(a: TaskFlag, b: TaskFlag, c: Data.Tasks.Condition = true) {
		if (a === undefined || b === undefined) {
			switch (c) {
				case "eq":
				case "==":
				case true:
					return a === b;
				case "ne":
				case "!=":
				case false:
					return a !== b;
				default:
					throw "Only equality tests are allowed with undefined values";
			}
		}
		switch (c) {
			case "gte":
			case ">=":
				return a >= b;
			case "lte":
			case "<=":
				return a <= b;
			case "gt":
			case ">":
				return a > b;
			case "lt":
			case "<":
				return a < b;
			case "eq":
			case "==":
				return a == b;
			case "ne":
			case "!=":
				return a != b;
			default:
				return (a == b) === c;
		}
	}

	export function cmpAny(test: TaskFlag, val: TaskFlag | TaskFlag[], c?: Data.Tasks.Condition): boolean {
		if (Array.isArray(val)) {
			return val.some(v => cmp(test, v, c));
		}
		return cmp(test, val, c);
	}

	namespace Tasks {
		export function postNumericValue(p: Data.Tasks.Posts.NumericValue, checks: Data.Tasks.CheckResults): number {
			let res = p.value;
			if (p.opt === "random") {
				res = Math.floor((res * State.random()) + 1);
			}
			// value refers to a result of a previous check
			if (p.factor) {
				// Retrieve the result of another check and modify the value of this reward by that scaling percentage.
				const check = checks[p.factor];
				if (check === undefined) {
					throw new Error(`Could not find check named '${p.factor}'`);
				}
				res *= checks[p.factor].mod;
			}
			return res;
		}
	}
}
