namespace App.UI {
	export const link = (function () {
		let counter = 0;

		// reset all handlers for each passage
		$(document).on(':passageinit', () => {
			State.temporary.linkHandlers = {};
			counter = 0;
		});

		return makeLink;

		type AnyHandler = (...args: any) => void;
		/**
		 * Creates a markup for a SugarCube link which executes given function with given arguments
		 *
		 * @param linkText link text
		 * @param handler callable object
		 * @param args arguments
		 * @param passage the passage name to link to
		 * @returns  link in SC markup
		 */
		function makeLink<THandler extends AnyHandler>(linkText: string, handler: THandler, args: Parameters<THandler>, passage = '', tooltip = '') {
			// pack handler and data
			State.temporary.linkHandlers[counter] = {
				f: handler,
				args: args
			};

			// can't say _linkHandlers here because SC does not recognize its own notation in "..._varName"
			const scHandlerText =
				`State.temporary.linkHandlers[${counter}].f(...State.temporary.linkHandlers[${counter}].args);`;
			++counter;

			if (passage) {
				return UI.pPassageLink(linkText, passage, scHandlerText, tooltip);
			} else {
				if (tooltip) {
					throw "Tooltips are not supported by the <<link>> markup.";
				}
				// data-passage scheme does not work with empty passage name
				return `<<link "${linkText}">><<run ${scHandlerText}>><</link>>`;
			}
		}
	})();

	/**
	 * Creates a HTML element with custom SugarCube attributes which works as a passage link
	 *
	 * The result works in the same way as the wiki markup in the SugarCube
	 * @see https://www.motoslave.net/sugarcube/2/docs/#markup-html-attribute
	 * @param linkText link text
	 * @param passage the passage name to link to
	 * @param setter setter text
	 * @param tooltip tooltip text
	 * @param elementType element type, default is 'a'. Could be any of 'a', 'audio', img', 'source', 'video'
	 * @returns element text
	 *
	 * @example
	 * // equal to [[Go to town|Town]]
	 * App.UI.passageLink("Go to town", "Town")
	 */
	export function pPassageLink(linkText: string, passage: string, setter = '', tooltip = '', elementType = 'a'): string {
		let res = `<${elementType} data-passage="${passage}"`;
		if (setter) {
			res += ` data-setter="${Utils.escapeHtml(setter)}"`;
		}
		if (tooltip) {
			res += ` title="${tooltip}"`;
		}
		res += `>${linkText}</${elementType}>`;
		return res;
	}

	/**
	 * Replaces contents of the element, identified by the given selector, with wiki'ed new content
	 *
	 * The function is an analogue to the SugarCube `<<replace>>` macro (and is a simplified version of it)
	 * @param selector
	 * @param newContent
	 */
	export function replace(element: string | HTMLElement, ...newContent: (string | Node)[]): void {
		const target = typeof element === "string" ? document.querySelector(element) as HTMLElement : element;
		target.innerText = '';
		for (const nc of newContent) {
			if (typeof nc === "string") {
				const ins = jQuery(document.createDocumentFragment());
				ins.wiki(nc);
				ins.appendTo(target);
			} else {
				target.append(nc);
			}
		}
	}

	export function pWithTooltip(text: string, tooltipText: string | string[], classNames?: string[], styles?: string[]): string {
		const makeTooltip = (tooltipContent: string | string[]): string => {
			if (Array.isArray(tooltipContent)) {
				if (tooltipContent.length === 1) {
					return makeTooltip(tooltipContent[0]);
				}
				return `<div class="tooltip"><ul>${tooltipContent.map(e => `<li>${e}</li>`).join('')}</ul></div>`;
			} else {
				return `<span class="tooltip">${tooltipContent}</span>`;
			}
		};
		const tooltip = makeTooltip(tooltipText);
		let classes = ["textWithTooltip"];
		if (classNames) {
			classes = classes.concat(classNames);
		}

		let styleStr = "";
		if (styles && styles.length > 0) {
			styleStr = ` style="${styles.join(';')}"`;
		}
		return `<span class="${classes.join(' ')}"${styleStr}>${text}${tooltip}</span>`;
	}

	export function makeElement<T extends keyof HTMLElementTagNameMap>(tag: T, content?: string, classNames?: string[]): HTMLElementTagNameMap[T] {
		const element = document.createElement(tag);
		if (classNames !== undefined) {
			if (Array.isArray(classNames)) {
				element.classList.add(...classNames);
			} else {
				element.classList.add(classNames);
			}
		}
		if (content) {
			element.append(content);
		}
		return element;
	}

	export function appendNewElement<T extends keyof HTMLElementTagNameMap>(
		tag: T, parent: ParentNode, content?: string, classNames?: string[]): HTMLElementTagNameMap[T] {
		const element = makeElement(tag, content, classNames);
		parent.append(element);
		return element;
	}

	export function appendTextNode(parent: ParentNode, text: string): void {
		parent.append(document.createTextNode(text));
	}

	export interface FormattedFragment {
		text: string;
		style: string | string[];
	}

	export function appendFormattedFragment(parent: ParentNode, text: FormattedFragment): HTMLSpanElement {
		return appendNewElement('span', parent, text.text, Array.isArray(text.style) ? text.style : [text.style]);
	}

	export function appendFormattedText(parent: ParentNode, ...text: (string | FormattedFragment)[]): void {
		for (const t of text) {
			if (typeof t === "string") {
				$(parent).wiki(t);
			} else {
				appendNewElement('span', parent, t.text, Array.isArray(t.style) ? t.style : [t.style]);
			}
		}
	}

	type PassageLinkMap = Pick<HTMLElementTagNameMap, 'a' | 'audio' | 'img' | 'source' | 'video'>;
	type PassageLinkHandler = (passageName: string) => void;
	/**
	 * Creates a HTML element with custom SugarCube attributes which works as a passage link
	 *
	 * The result works in the same way as the wiki markup in the SugarCube
	 * @see https://www.motoslave.net/sugarcube/2/docs/#markup-html-attribute
	 * @param linkText link text
	 * @param passage the passage name to link to
	 * @param handler setter text (optional)
	 * @param tooltip text (optional)
	 * @param element type (optional) default is 'a'.
	 * Could be any of 'a', 'audio', img', 'source', 'video'
	 * @returns element text
	 *
	 * @example
	 * // equal to [[Go to town|Town]]
	 * passageLink("Go to town", "Town")
	 */
	export function passageLink(linkText: string, passage: string, handler?: PassageLinkHandler, tooltip?: string): HTMLAnchorElement;
	export function passageLink<L extends keyof PassageLinkMap>(linkText: string, passage: string, handler: PassageLinkHandler,
		tooltip: string, elementType: L): PassageLinkMap[L];
	export function passageLink<L extends keyof PassageLinkMap>(linkText: string, passage: string, handler?: PassageLinkHandler,
		tooltip = '', elementType?: L): PassageLinkMap[L] {
		const res = document.createElement(elementType ?? 'a');
		res.setAttribute("data-passage", passage);
		res.onclick = (ev) => {
			ev.preventDefault();
			if (handler) {
				handler(passage);
			}
			Engine.play(passage);
		};

		if (tooltip) {
			res.title = tooltip;
		}
		if (linkText.includes('@@') || linkText.includes('<')) {
			$(res).wiki(linkText);
		} else {
			res.textContent = linkText;
		}
		return res as PassageLinkMap[L];
	}

	/**
	 * Show a list of links (or disabled links) as a delimited strip
	 * @param {Array<Node|string>} links
	 * @returns {HTMLUListElement}
	 */
	export function linksStrip(...links: (Node | string)[]): HTMLUListElement {
		const strip = document.createElement('ul');
		strip.className = "choices-strip";

		links.reduce((list: HTMLUListElement, lnk) => {
			const li = document.createElement("li");
			if (typeof lnk === "string") {
				$(li).wiki(lnk); // must be SC markup
			} else {
				li.append(lnk);
			}
			list.appendChild(li);
			return list;
		}, strip);
		return strip;
	}

	const colorScaleBaseColors = ['red', 'yellow', 'lime', 'cyan', 'blue', '#bf00ff'];
	const meterColors = ["red", "brown", "yellow", "cyan", "lime"];

	let colorScaler = chroma.scale(colorScaleBaseColors).mode('lab');
	let meterColorScaler = chroma.scale(meterColors).mode('lab');

	function resetColorScale() {
		const isLightTheme = chroma(getComputedStyle(document.body).backgroundColor).luminance() > 0.5;
		let baseColors: chroma.Color[] = [];
		let baseMeterColors: chroma.Color[] = [];
		if (isLightTheme) {
			baseColors = colorScaleBaseColors.map(c => chroma(c).darken(2));
			baseMeterColors = meterColors.map(c => chroma(c).darken(1.2));
		} else {
			baseColors = colorScaleBaseColors.map(c => chroma(c));
			baseMeterColors = meterColors.map(c => chroma(c));
		}
		colorScaler = chroma.scale(baseColors).mode('lab'); // uniform scale over the given colors
		meterColorScaler = chroma.scale(baseMeterColors).mode('lab');
	}

	/**
     * Returns a color that defines the percentage type of the "value" passed.
     * @param Value
     * @param Max - defaults to 100, Percent = Value / Opt
     */
	export function colorScale(value: number, max = 100): string {
		return colorScaler(Math.clamp(value / max, 0, 1)).hex();
	}

	/**
     * Returns a color that defines the percentage type of the "value" passed.
     * @param Value
     * @param Max - defaults to 100, Percent = Value / Opt
     */
	export function meterColor(value: number, max = 100): string {
		return meterColorScaler(Math.clamp(value / max, 0, 1)).hex();
	}

	export function scaleColorStrip(width: string, height: string): string {
		const res: string[] = [];
		res.push(`<div style="width:${width}">`);
		const stripElementStype = `display:inline-block;height:${height};width:1%;`
		const colors = colorScaler.colors(100);
		for (let i = 0; i < 100; ++i) {
			res.push(`<span style="${stripElementStype}background-color:${colors[i]}"></span>`);
		}
		res.push('</div>');
		return res.join('');
	}

	/**
	 * Renders out a 10 star meter surrounded with brackets.
	 * @param score - Current stat/score
	 * @param maxScore - Maximum stat/score
	 * @param invertMeter - reverse direction of stars relative to score so that high scores are less stars.
	 * @param htmlSafe
	*/
	export function rMeter(score: number, maxScore: number, invertMeter = false): HTMLSpanElement {
		const clampedScore = Math.clamp(score, 0, maxScore);
		const units = (maxScore / 10);
		const stars = Math.floor((clampedScore / units));
		let sMeter = "";
		const nMeter = invertMeter ? (maxScore - score) : score;
		let i = 0;

		const res = document.createElement("span");
		res.append(document.createTextNode("["));
		const colorStars = document.createElement("span");
		for (i = 0; i < stars; i++) {
			sMeter += "★";
		}
		colorStars.textContent = sMeter;
		colorStars.style.color = meterColor(nMeter, maxScore);
		res.append(colorStars);

		if ((10 - stars) != 0) {
			sMeter = "";
			for (i = 0; i < (10 - stars); i++) {
				sMeter += "★";
			}
			const greyStars = document.createElement("span");
			greyStars.textContent = sMeter;
			greyStars.style.color = "grey";
			res.append(greyStars);
		}

		res.append(document.createTextNode(settings.displayMeterNumber ? `] ${score}` : "]"));
		return res;
	}

	export function rCoreStatMeter(statName: CoreStat, player: Entity.Player, invert = false): HTMLSpanElement {
		const statValue = player.getStat(Stat.Core, statName);

		if (statName == CoreStat.Hormones) {
			if (statValue > 100) // Return "Female" version of this meter.
				return rMeter((player.getStat(Stat.Core, statName) - 100), 100, invert);
			if (statValue <= 100)
				return rMeter((100 - statValue), 100, invert);
		}

		return rMeter(player.getStat(Stat.Core, statName), player.getMaxStat(Stat.Core, statName), invert);
	}

	 /**
	 * Print out a 10 star colorized stat meter for a body part.
	 * @param invert - reverse direction of stars relative to score so that high scores are less stars.
	 */
	export function rBodyMeter(statName: BodyStat, player: Entity.Player, invert?: boolean): HTMLSpanElement {
		return rMeter(player.getStat(Stat.Body, statName), 100, invert);
	}

	export function rPostProcess(passageName: string, content: HTMLElement): HTMLElement | null {
		// find all npcs
		const npcElements = content.querySelectorAll("[data-npc-name]");
		for (const npcElem of npcElements) {
			const npcName = npcElem.getAttribute("data-npc-name");
			const customArgs = npcElem.getAttribute("data-npc-args");
			const npcArg: PassageRef[] = [];
			if (customArgs?.length) {
				for (const arg of customArgs.split(';')) {
					if (arg.includes(',')) {
						const tmp = arg.split(',');
						npcArg.push([tmp[0], tmp[1]]);
					} else {
						npcArg.push(arg);
					}
				}
			}
			if (npcName?.length) {
				UI.replace(npcElem as HTMLElement, wNPC(setup.player, npcName, npcArg)); // TODO custom params
			}
		}

		// find all joblink
		const locationJobs = content.querySelectorAll("[data-location-job]");
		for (const lj of locationJobs) {
			const jobLocation = lj.getAttribute("data-location-job");
			if (jobLocation?.length) {
				const newContent = wLocationJobs(setup.player, jobLocation);
				if (newContent) {
					UI.replace(lj as HTMLElement, newContent);
				}
			}
		}

		const destinations = Data.Travel.destinations[passageName];
		if (destinations) {
			const travelDiv = content.querySelector("#travel");
			const travels = wTravels(setup.player, destinations);
			if (travelDiv && travels) {
				UI.replace(travelDiv as HTMLElement, travels);
				return null;
			}
			if (travels) {
				travels.style.marginTop = "2ex";
			}
			return travels;
		}
		return null;
	}

	export function loadTheme(name: string): void {
		const fn = `./themes/theme-${name}.css`;
		importStyles(fn)
			.then(() => {
				resetColorScale();
			},
			() => {
				console.error(`Could not load theme '${name} CSS file '${fn}!`)
			});
	}
}
