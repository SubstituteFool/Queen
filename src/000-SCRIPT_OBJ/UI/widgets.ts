namespace App.UI {
	export abstract class SelfUpdatingTable {
		#table: HTMLTableElement;
		#header: HTMLTableSectionElement;
		#tbody: HTMLTableSectionElement;

		constructor() {
			this.#table = UI.makeElement('table');
			this.#header = UI.appendNewElement('thead', this.#table);
			this.#tbody = UI.appendNewElement('tbody', this.#table);
		}

		get table(): HTMLTableElement {return this.#table;}
		get header(): HTMLTableSectionElement {return this.#header;}
		get body(): HTMLTableSectionElement {return this.#tbody;}

		render(): Node {
			this.update();
			return this.#table;
		}

		protected abstract update(): void;
	}

	export type PassageRef = string | [string, string | null];

	function pPassageLinkStrip(passages: PassageRef[], prefix: string, className: string): string {
		if (passages.length === 0) {
			return '';
		}

		return `@@.${className};${prefix}@@: ` + passages.map((p) => {
			if (typeof p === 'string') {
				return p.startsWith('[[') || p.startsWith('<') ? p : `[[${p}]]`; // works for "caption|passage_name" too
			}
			// disabled links are passed in as ["text", null]
			return p[1] === null ? `@@.state-disabled;[${p[0]}]@@` : `[[${p[0]}|${p[1]}]]`;
		}).join('&thinsp;|&thinsp;');
	}

	export function pActionLinkStrip(passages: PassageRef[]): string {
		return pPassageLinkStrip(passages, 'Action', 'action-general');
	}

	export function pInteractLinkStrip(passages?: PassageRef[], exitCaption: string | null = 'Exit'): string {
		let links: PassageRef[] = [];
		if (exitCaption !== null) {
			const gbm = State.variables.gameBookmark;
			if (!_.isEmpty(gbm) && gbm !== passage()) {
				links.push(`[[${exitCaption}|${gbm}]]`);
			}
		}
		if (passages && passages.length) {
			links = links.concat(passages);
		}
		return pPassageLinkStrip(links, 'Interact', 'action-interact');
	}

	export function pTravelLinkStrip(passages: PassageRef[], caption = "Travel"): string {
		return pPassageLinkStrip(passages, caption, 'action-travel');
	}

	export function isLockedDestinationWithNote(d: Data.Travel.Destination): d is Data.Travel.LockedDestinationWithNote {
		return d.hasOwnProperty('note');
	}

	export function resolveDynamicText(player: Entity.Player): undefined;
	export function resolveDynamicText(player: Entity.Player, text: DynamicText): string;
	export function resolveDynamicText(player: Entity.Player, text?: DynamicText): string | undefined {
		if (text === undefined) {
			return undefined;
		}
		return typeof text === "string" ? text : PR.tokenizeString(player, undefined, text(player));
	}

	export function wCustomMenuLink(text: string, action?: string | Entity.NPC | Job, passageName?: string): HTMLAnchorElement {
		const res = document.createElement("a");
		res.classList.add("link-internal");
		const targetPassage = passageName ?? text;
		res.append(text);
		res.onclick = () => {
			if (action) {
				State.variables.menuAction = action;
			}
			if (!tags().includes("custom-menu")) {
				State.variables.gameBookmark = passage();
			}
			Engine.play(targetPassage);
		}
		return res;
	}

	export function wTravels(player: Entity.Player, destinations: Data.Travel.Destinations): HTMLDivElement {
		let noteDiv: HTMLDivElement | null = null;
		const res = document.createElement("div");
		const travels = appendNewElement("div", res, undefined, ['action-row']);
		const divLabel = appendNewElement("div", travels, undefined, ['action-caption']);
		appendNewElement("span", divLabel, "Travel", ['action-travel']);
		const listDiv = appendNewElement("div", travels, undefined, ['action-links']);
		const links = appendNewElement("ul", listDiv, undefined, ['choices-strip']);
		for (const d of destinations) {
			const link = document.createElement("li");
			if (typeof d === "string") {
				link.append(passageLink(resolveDynamicText(player, Data.Travel.passageDisplayNames[d]) ?? d, d));
				links.append(link);
			} else {
				const available = d.available ? d.available(player) : true;
				if (!available) {
					continue;
				}

				if (isLockedDestinationWithNote(d)) {
					if (!noteDiv) {
						noteDiv = appendNewElement("div", res);
						noteDiv.id = "travel-locked-notification";
					}
					const anchor = appendNewElement("a", link, resolveDynamicText(player, d.text));
					anchor.onclick = () => {
						UI.replace(noteDiv!, resolveDynamicText(player, d.note));
					};
				} else {
					const destPasage = typeof d.destination === "string" ? d.destination : d.destination(player);
					const enabled = d.enabled ? d.enabled(player) : true;
					const dt = d.text;
					const text = dt ? resolveDynamicText(player, dt) :
						resolveDynamicText(player, Data.Travel.passageDisplayNames[destPasage]) ?? destPasage;
					if (enabled) {
						const action = d.action;
						link.append(passageLink(text, destPasage,
							action ? passageName => action(player, passageName) : undefined));
					} else {
						appendNewElement("span", link, text, ['state-disabled']);
					}
				}
				links.append(link);
			}
		}

		// late night teleport
		if (setup.world.phase >= Phase.LateNight && !tags().includes("no-teleport")) {
			const lnt = appendNewElement("div", res, " It's getting kind of late… ");
			lnt.append(passageLink("head back to the Mermaid", "Cabin"));
			lnt.style.marginLeft = "0.5em";
		}

		return res;
	}

	export function wNPC(player: Entity.Player, npcTag: string, custom?: PassageRef[]): HTMLDivElement {
		const npc = setup.world.npc(npcTag);
		const res = makeElement("div", undefined, ["npc-interact"]);
		$(res).wiki(PR.tokenizeString(player, npc, npc.shortDesc));
		const npcMenu = appendNewElement("div", res,)
		appendNewElement("span", npcMenu, "NPC Menu", ["npc-menu"]);
		npcMenu.append(": ");

		const npcLinks = appendNewElement("ul", npcMenu, undefined, ['choices-strip'])
		appendNewElement("li", npcLinks).append(wCustomMenuLink("Examine", npcTag, "ExamineNPC"));

		if (setup.jobEngine.hasJobs(player, npcTag)) {
			const liJobs = appendNewElement("li", npcLinks);
			liJobs.append(wCustomMenuLink("Jobs", npcTag));
			if (setup.jobEngine.jobsAvailable(player, npcTag)) {
				appendNewElement("span", liJobs, " (!)", ["task-available"]);
			}
		}
		if (Quest.list("any", player, npcTag).length > 0) {
			const liQuests = appendNewElement("li", npcLinks);
			liQuests.append(wCustomMenuLink("Quests", npcTag));
			if (Quest.list(QuestState.Available, player, npcTag).length > 0) {
				appendNewElement("span", liQuests, " (!)", ["task-available"]);
			}
			if (Quest.list(QuestState.CanComplete, player, npcTag).length > 0) {
				appendNewElement("span", liQuests, " (!)", ["task-cancomplete"]);
			}
		}

		if (StoreEngine.hasStore(npc)) {
			const liShop = appendNewElement("li", npcLinks);
			if (StoreEngine.isOpen(npc)) {
				liShop.append(wCustomMenuLink("Shop", npc));
			} else {
				appendNewElement("span", liShop, "Shop", ['state-disabled']);
			}
		}

		if (custom) {
			for (const pr of custom) {
				if (typeof pr === "string") {
					appendNewElement("li", npcLinks).append(wCustomMenuLink(pr, npcTag, pr));
				} else {
					appendNewElement("li", npcLinks).append(wCustomMenuLink(pr[0], npcTag, pr[1] ?? undefined));
				}
			}
		}
		return res;
	}

	export function wLocationJobs(player: Entity.Player, location: string, brief = false): HTMLDivElement | null {
		const jobList = setup.jobEngine.getAvailableLocationJobs(player, location);
		if (!jobList.length) {
			return null;
		}
		const res = document.createElement("div");
		if (!brief) {
			appendNewElement("span", res, "Action", ["action-general"]);
			res.append(": ");
		}
		const links = appendNewElement("ul", res, undefined, ['choices-strip'])
		for (const j of jobList) {
			const li = appendNewElement("li", links);
			if (j.ready(player)) {
				li.append(wCustomMenuLink(j.title(true), j, "SelfJobs"));
			} else {
				appendNewElement("span", li, j.title(true), ["state-disabled"]);
			}
			$(li).wiki(j.ratingAndCosts());
		}
		return res;
	}

	export function pJobResults(player: Entity.Player, npc: Entity.NPC, job: Job): string {
		const start = job.printStart(player, npc);
		const scenes = job.playScenes(player, npc);
		const end = job.printEnd(player, npc);

		let res: string[] = [];
		if (start.length) {
			res.push(start);
		}
		for (const s of scenes) {
			res.push(s.print());
		}
		if (end.length) {
			res.push(end);
		}

		const results = PR.pTaskRewards(job);
		if (results.length) {
			res.push("@@color:cyan;Your receive some items:@@");
			res = res.concat(results);
		}

		job.completeScenes(setup.player);
		return res.join('<br/>');
	}

	export function rBodyScore(player: Entity.Player): HTMLTableElement {
		const res = makeElement("table", undefined, ["body-stats"]);
		res.style.width = "100%";
		res.style.textAlign = "left";

		const tbody = appendNewElement("tbody", res);

		const statsOrder: BodyStat[] = [
			BodyProperty.Height,
			BodyPart.Hair, BodyPart.Face, BodyPart.Lips,
			BodyPart.Bust, BodyProperty.Lactation,
			BodyPart.Waist, BodyPart.Hips, BodyPart.Ass, BodyPart.Penis, BodyPart.Balls
		];
		const shownIfPresent: Set<BodyStatStr> = new Set([BodyPart.Bust, BodyProperty.Lactation, BodyPart.Penis, BodyPart.Balls]);
		const invertedStats: BodyStatStr[] = [BodyPart.Waist]

		for (const st of statsOrder) {
			if (player.bodyStats[st] > 0 || !shownIfPresent.has(st)) {
				const tr = appendNewElement("tr", tbody);
				appendNewElement("td", tr, `${_.pascalCase(st)}:`);
				const meter = appendNewElement("td", tr);
				meter.id = st;
				meter.classList.add('score-meter');
				meter.append(rBodyMeter(st, player, invertedStats.includes(st)));
				const pStr = PR.tokenizeString(player, undefined, 'p' + st.toUpperCase());
				if (pStr) {
					tippy(meter, {content: Wikifier.wikifyEval(pStr)})
				}
			}
		}
		return res;
	}

	export function rGameScore(): DocumentFragment {
		const res = new DocumentFragment();

		const player = setup.player;

		appendNewElement("div", res, player.slaveName, ['state-feminity']);

		const dayDiv = appendNewElement("div", res, `Day ${setup.world.day}, `);
		dayDiv.append(Wikifier.wikifyEval(`${setup.world.phaseName(false)} ${PR.phaseIconStrip()}`));

		const coins = appendNewElement("div", res, "Coins: ", ['item-money']);
		appendNewElement("span", coins, `${player.money}`).id = "Money";

		if (player.skills.courtesan) {
			const tokens = appendNewElement("div", res, "Tokens: ", ['state-sexiness']);
			appendNewElement("span", tokens, `${player.tokens}`).id = "Tokens";
		}

		const linkHandler = () => {
			const passageObj = Story.get(passage());
			if (!passageObj.tags.contains("custom-menu")) {
				State.variables.gameBookmark = passage();
			}
		};

		const linksDiv = appendNewElement("div", res);
		linksDiv.id = "game-info-links";
		linksDiv.style.textAlign = "center";
		linksDiv.append(linksStrip(
			passageLink("Journal", "Journal", linkHandler),
			passageLink("Skills", "Skills", linkHandler),
			passageLink("Inventory", "Inventory", linkHandler),
		));

		const scoreTable = appendNewElement("table", res);
		scoreTable.style.width = "100%";
		scoreTable.style.marginTop = "0.5ex";
		const scoreTableBody = appendNewElement("tbody", scoreTable);

		const statsOrder: CoreStat[] = [
			CoreStat.Health, CoreStat.Energy, CoreStat.Willpower,
			CoreStat.Perversion, CoreStat.Nutrition, CoreStat.Femininity
		];

		for (const st of statsOrder) {
			const tr = appendNewElement("tr", scoreTableBody);
			appendNewElement("td", tr, `${_.pascalCase(st)}:`);
			const meterTd = appendNewElement("td", tr);
			meterTd.id = st;
			meterTd.append(rCoreStatMeter(st, setup.player));
			meterTd.classList.add('score-meter');
		}
		// Toxicity is inverted
		let tr = appendNewElement("tr", scoreTableBody);
		appendNewElement("td", tr, "Toxicity:");
		let meterTd = appendNewElement("td", tr);
		meterTd.id = "toxicity";
		meterTd.append(rCoreStatMeter(CoreStat.Toxicity, setup.player, true));
		meterTd.classList.add('score-meter');

		// hormones meter needs a special symbol
		tr = appendNewElement("tr", scoreTableBody);
		appendNewElement("td", tr, "Hormones:");
		meterTd = appendNewElement("td", tr);
		meterTd.id = "hormones";
		meterTd.append(rCoreStatMeter(CoreStat.Hormones, setup.player));
		meterTd.innerHTML += PR.pHormoneSymbol();
		meterTd.classList.add('score-meter');

		// update avatar if empty
		if (settings.displayAvatar && document.getElementById("avatarFace")?.children.length === 0) {
			setup.avatar.drawPortrait();
		}

		// update body score
		if (settings.displayBodyScore) {
			const container = $("#bodyScoreContainer");
			container.empty();
			container.append(rBodyScore(setup.player));
		}

		return res;
	}
}
namespace App.UI.Widgets {

	function jobButton(job: Job, isAvailabe: boolean, player: Entity.Player, npc: Entity.NPC) {
		const res = document.createDocumentFragment();
		const btn = appendNewElement("button", res, undefined, ["job-button"]);
		if (isAvailabe) {
			btn.textContent = "Accept";
			btn.onclick = () => {
				UI.replace("#JobUI", UI.pJobResults(player, npc, job));
			};
		} else {
			btn.classList.add("job-button-disabled");
			if (job.onCoolDown(player)) {
				btn.textContent = "COOLDOWN";
			} else {
				btn.textContent = "LOCKED";
			}
			$(res).wiki(' ' + job.reqString(player, npc));
		}
		return res;
	}

	export function rJobList(player: Entity.Player, npc: Entity.NPC, availableJobs: Job[], unavailableJobs: Job[]): HTMLSpanElement {
		const jobUI = document.createElement('span');
		jobUI.id = "JobUI";

		for (const j of availableJobs) {
			const jobDiv = appendNewElement("div", jobUI, undefined, ["job-intro"]);
			$(jobDiv).wiki(j.title() + "<br/>" + j.intro(player, npc));
			appendNewElement("br", jobDiv);
			jobDiv.append(jobButton(j, true, player, npc));
			appendNewElement("br", jobDiv);
		}

		for (const j of unavailableJobs) {
			const jobDiv = appendNewElement("div", jobUI, undefined, ["job-intro"]);
			$(jobDiv).wiki(j.title() + "<br/>" + j.intro(player, npc));
			appendNewElement("br", jobDiv);
			jobDiv.append(jobButton(j, false, player, npc));
			appendNewElement("br", jobDiv);
		}
		return jobUI;
	}
}
