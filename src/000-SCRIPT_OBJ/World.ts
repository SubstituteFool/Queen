namespace App.Entity {
	export class World {
		static get instance(): World {
			return World._instance;
		}

		get state(): GameState.IGame { /* eslint-disable-line class-methods-use-this */
			return State.variables;
		}

		/**
		 * Previously we stored a dictionary of NPC objects.
		 *  Now we just store some vital information and make new ones as needed.
		 */
		npc(npcTag: string): Entity.NPC {
			const data = Data.npcs[npcTag];
			if (!this.state.npc.hasOwnProperty(npcTag)) {
				this.state.npc[npcTag] = {questFlags: {}, mood: data.mood, lust: data.lust};
			}
			return new Entity.NPC(data, this.state.npc[npcTag]);
		}

		npcGroup(name: string): Entity.NPC[] {
			const gr = Data.npcGroups[name];
			if (!gr) {
				throw Error(`Could not find NPC group '${name}'`);
			}
			return gr.map(n => this.npc(n));
		}

		/**
		 * With Icon = true then return string time with Icon.
		 * With Icon = false return numerical time of day.
		 */
		phaseName(addIcon = false): string {
			const phase = this.state.phase;
			return addIcon ? `${PR.phaseName(phase)} ${PR.phaseIcon(phase)}` : PR.phaseName(phase);
		}

		// Game/Environment Variables
		get day(): number {return this.state.day;}
		set day(n: number) {this.state.day = n;}
		get phase(): Phase {return this.state.phase;} // 0 morning, 1 afternoon, 2 evening, 3 night, 4 late night
		set phase(n: Phase) {this.state.phase = n;}

		get pc(): Entity.Player {return setup.player; }
		get storeInventory(): Record<string, GameState.StoreInventory> {return this.state.storeInventory;}
		get npcs(): Record<string, GameState.NPC> {return this.state.npc;}

		get debugMode(): boolean {return this.state.debugMode == true;}
		set debugMode(v: boolean) {this.state.debugMode = v;}
		get difficultySetting(): number {return this.state.difficultySetting;}
		get whoreTutorial(): boolean {return this.state.tutorial.whoring == true;}
		set whoreTutorial(v: boolean) {this.state.tutorial.whoring = v;}

		npcNextDay(): void {
			for (const prop in this.npcs) { // NPC mood/desire/quest flags.
				const npcState = this.npcs[prop];
				if (!npcState) continue;
				const npc = this.npc(prop);
				npc.adjustFeelings();
				npc.resetFlags();
			}
		}

		nextDay(): void {
			this.pc.nextDay();
			this.npcNextDay();

			this.state.day++;
			this.state.phase = Phase.Morning;
			Quest.nextDay(this.pc);
			setup.avatar.queuePortraitDraw();
		}

		/**
		 * Move time counter to next phase of day.
		 * @param phases - Number of phases to increment.
		 */
		nextPhase(phases = 1): void {
			// Can't advance to next day, only do that when sleeping.
			for (let i = 0; i < phases && this.state.phase <= Phase.LateNight; ++i) {
				setup.world.state.phase++;
				this.pc.nextPhase();
			}
		}

		advancePhaseTo(phase: Phase): number {
			if (this.phase > phase) {
				return 0;
			}
			const diff = phase - this.phase;
			this.nextPhase(diff);
			return diff;
		}

		saveLoaded(): void {
			this.pc.saveLoaded();
		}

		initNewGame(): void {
			GameState.setNewGameVariables();
			Quest.byTag("Q001").accept(this.pc);
		}

		private static readonly _instance: World = new World();
	}
}
