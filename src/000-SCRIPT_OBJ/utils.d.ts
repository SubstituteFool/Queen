// implementations are in Utils.ts

declare global {
	interface Math {
		/** Returns average value of the arguments */
		mean(v0: number, ...vn: number[]): number;
	}

	/*
	interface Object {
		hasOwnProperty<X extends Record<string, unknown>, Y extends PropertyKey>(this: X, prop: Y): this is X & Record<Y, unknown>;
	}
	*/

	type ActualPropertyKey<T extends PropertyKey> = T extends number ? string : T;

	interface ObjectConstructor {
		keys<K extends PropertyKey, V>(o: Partial<Record<K, V>>): ActualPropertyKey<K>[];
		entries<K extends PropertyKey, V>(o: {[x in K]?: V}): [ActualPropertyKey<K>, V][];
		/**
		 * Like @ref Object.assign(), but throws if any property in @see props attemts to
		 * overwrite a property with the same name in @see target
		 * @param target
		 * @param props
		 */
		append<T>(target: Record<PropertyKey, T>, props: Record<PropertyKey, T>): Record<PropertyKey, T>;
	}

	interface Array<T> {
		randomElement(): T;
	}
}

declare module "lodash" {
	interface LoDashStatic {
		/**
		 * Converts string to pascal case.
		 *
		 * @param string The string to convert.
		 * @return Returns the pascal cased string.
		 */
		pascalCase(string?: string): string;
	}
}
export {}
