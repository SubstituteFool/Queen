namespace App.Data {
	Object.append(App.Data.clothes, {
		npcPirate: { // +40
			name: "npcPirate",
			shortDesc: "pirate costume",
			longDesc: "You shouldn't be looking at this.",
			slot: ClothingSlot.Costume,
			restrict: [ClothingSlot.Shirt, ClothingSlot.Pants, ClothingSlot.Dress, ClothingSlot.Costume],
			color: "black",
			rarity: Items.Rarity.Legendary,
			type: Items.ClothingType.OnePiece,
			wearEffect: [],
			activeEffect: [],
			style: [],
			inMarket: false
		},

		npcBertie: { // +40
			name: "npcBertie",
			shortDesc: "pirate costume",
			longDesc: "You shouldn't be looking at this.",
			slot: ClothingSlot.Costume,
			restrict: [ClothingSlot.Shirt, ClothingSlot.Pants, ClothingSlot.Dress, ClothingSlot.Costume],
			color: "black",
			rarity: Items.Rarity.Legendary,
			type: Items.ClothingType.OnePiece,
			wearEffect: [],
			activeEffect: [],
			style: [],
			inMarket: false
		},
	});
}
