// STYLE TABLE
// TYPE         COMMON  UNCOMMON    RARE    LEGENDARY
// ACCESSORY    3       6           9       12
// CLOTHING     5       10          15      20
// ONE PIECE    10      20          30      40
namespace App.Data {
	Object.append(App.Data.clothes, {
		// WIG SLOT

		// HAT SLOT
		"pirate hat": { // +12
			name: "pirate hat",
			shortDesc: "a cute little pirate hat",
			longDesc: "\
    This cute little pirate hat has an adorable version of the Jolly Rodger on it, complete with bows. A large ribbon \
    is affixed to the side and flows down your back.",
			slot: ClothingSlot.Hat,
			color: "black",
			rarity: Items.Rarity.Legendary,
			type: Items.ClothingType.Accessory,
			activeEffect: ["MINOR_PIRATES_GRACE", "DAMAGE_RESIST_MINOR"],
			style: [Fashion.Style.PirateSlut],
			inMarket: false
		},

		// NECK SLOT

		// NIPPLES SLOT
		"skull piercings": { // +12
			name: "skull piercings",
			shortDesc: "skull and anchor nipple piercings",
			longDesc: "\
    These dangling piercings are made out of silver and consists of tiny anchors festooned with skulls that \
    hang from a delicate silver chain. They can be either clipped through the nipples or pierced through.\
    ",
			slot: ClothingSlot.Nipples,
			color: "silver",
			rarity: Items.Rarity.Legendary,
			type: Items.ClothingType.Accessory,
			wearEffect: ["KINKY_CLOTHING"],
			activeEffect: [],
			style: [Fashion.Style.PirateSlut, Fashion.Style.SexyDancer],
			inMarket: false
		},

		// BRA SLOT
		"pirate bra": { // +15
			name: "pirate bra",
			shortDesc: "a {COLOR} push-up bra with the Jolly Roger",
			longDesc: "\
    This sexy silk bra is fringed with gold and has a skull and bones, the traditional symbol of piracy emblazoned \
    on the right breast cup.",
			slot: ClothingSlot.Bra,
			color: "black",
			rarity: Items.Rarity.Rare,
			type: Items.ClothingType.Clothing,
			wearEffect: ["FEMININE_CLOTHING"],
			style: [Fashion.Style.PirateSlut]
		},

		// CORSET SLOT
		"pistol brace": { // +9
			name: "pistol brace",
			shortDesc: "a {COLOR} leather pistol brace",
			longDesc: "\
    More corset than bandoleer, it cinches tightly around your waist and there are two small red bows at the tops \
    of the pistol holsters.",
			slot: ClothingSlot.Corset,
			color: "black",
			rarity: Items.Rarity.Rare,
			type: Items.ClothingType.Accessory,
			wearEffect: ["FEMININE_CLOTHING", "WAIST_TRAINING"],
			activeEffect: ["CUT_THROAT", "DAMAGE_RESIST_MINOR"],
			style: [Fashion.Style.PirateSlut, Fashion.Style.Bdsm]
		},

		// PANTY SLOT
		"pirate thong": { // +15
			name: "pirate thong",
			shortDesc: "a {COLOR} thong with the Jolly Roger",
			longDesc: "\
    This t-back thong gently separates your arse cheeks, making them more prominent and luscious looking. It \
    has the phrase 'Plunder my Booty' written across the rear waistband and a small skull and bones embroidered \
    on the crotch.",
			slot: ClothingSlot.Panty,
			color: "black",
			rarity: Items.Rarity.Rare,
			type: Items.ClothingType.Clothing,
			wearEffect: ["FEMININE_CLOTHING"],
			style: [Fashion.Style.PirateSlut]
		},

		// STOCKINGS SLOT
		"fishnet tights": { // +10
			name: "fishnet tights",
			shortDesc: "a pair of {COLOR} fishnet tights",
			longDesc: "These fishnet tights wrap themselves perfectly to your legs. The feeling is quite exquisite.",
			slot: ClothingSlot.Stockings,
			color: "black",
			rarity: Items.Rarity.Uncommon,
			type: Items.ClothingType.Clothing,
			wearEffect: ["FEMININE_CLOTHING"],
			style: [
				Fashion.Style.PirateSlut, Fashion.Style.Bdsm, Fashion.Style.NaughtyNun,
				Fashion.Style.Bimbo, Fashion.Style.HighClassWhore, Fashion.Style.SluttyLady
			]
		},

		"fishnet stockings": { // +10
			name: "fishnet stockings",
			shortDesc: "a pair of {COLOR} fishnet stockings",
			longDesc: "These fishnet stockings are topped with black lace and held up by garter straps.",
			slot: ClothingSlot.Stockings,
			color: "black",
			rarity: Items.Rarity.Uncommon,
			type: Items.ClothingType.Clothing,
			wearEffect: ["FEMININE_CLOTHING"],
			style: [
				Fashion.Style.PirateSlut, Fashion.Style.Bdsm, Fashion.Style.NaughtyNun,
				Fashion.Style.Bimbo, Fashion.Style.HighClassWhore, Fashion.Style.SluttyLady, Fashion.Style.SexyDancer
			]
		},

		// SHIRT SLOT

		// PANTS SLOT

		// DRESS SLOT
		"pirate waistcoat": { // +40
			name: "pirate waistcoat",
			shortDesc: "a short {COLOR} velvet waistcoat",
			longDesc: "\
    This tiny waistcoat only has one button, leaving your cleavage exposed. It's also quite short, \
    with the coat tails only barely covering your ass and leaving your front entirely exposed.",
			slot: ClothingSlot.Dress,
			restrict: [ClothingSlot.Shirt, ClothingSlot.Pants, ClothingSlot.Dress, ClothingSlot.Costume],
			color: "black",
			rarity: Items.Rarity.Legendary,
			type: Items.ClothingType.OnePiece,
			wearEffect: ["FEMININE_CLOTHING"],
			activeEffect: ["MAJOR_PIRATES_GRACE", "FLIRTY", "DAMAGE_RESIST_MAJOR"],
			style: [Fashion.Style.PirateSlut]
		},

		"pirate queen regalia": { // +40
			name: "pirate queen regalia",
			shortDesc: "pirate queen regalia",
			longDesc: "\
    This outfit consists of a low cut black top, tiny black micro skirt and a short waisted coat with \
    large golden epaulets. The buttons on the coat are polished white ivory and the embroidery is made of \
    expensive gold thread. \
    ",
			slot: ClothingSlot.Dress,
			restrict: [ClothingSlot.Shirt, ClothingSlot.Pants, ClothingSlot.Dress, ClothingSlot.Costume],
			color: "black",
			rarity: Items.Rarity.Legendary,
			type: Items.ClothingType.OnePiece,
			wearEffect: ["FEMININE_CLOTHING", "KINKY_CLOTHING"],
			activeEffect: ["MAJOR_PIRATES_GRACE", "DAMAGE_RESIST_MAJOR"],
			style: [Fashion.Style.PirateSlut],
			inMarket: false
		},

		// COSTUME SLOT
		"pirate stripper costume": { // +40
			name: "pirate stripper costume",
			shortDesc: "flimsy sequinned pirate costume",
			longDesc: "\
    This tiny costume consists of a tiny pleated black skirt and white top with a black vest embroidered \
    with sequins. The skirt has tiny Jolly Rodgers (skull and bones) stitched to it and is designed to \
    be flipped and spun about. It's obviously designed more for play than for actual daily use. \
    ",
			slot: ClothingSlot.Costume,
			restrict: [ClothingSlot.Shirt, ClothingSlot.Pants, ClothingSlot.Dress, ClothingSlot.Costume],
			color: "black",
			rarity: Items.Rarity.Legendary,
			type: Items.ClothingType.OnePiece,
			wearEffect: ["FEMININE_CLOTHING", "KINKY_CLOTHING"],
			activeEffect: ["MAJOR_STRIPPERS_ALLURE"],
			style: [Fashion.Style.PirateSlut, Fashion.Style.SexyDancer],
			inMarket: false
		},

		// SHOES SLOT
		"pirate boots": { // +10
			name: "pirate boots",
			shortDesc: "very tall thigh-high {COLOR} leather boots",
			longDesc: "\
    The heels on these boots are outrageously high and pointed. The supple and shiny leather comes up \
    to your mid thigh and folds over on itself.",
			slot: ClothingSlot.Shoes,
			color: "black",
			rarity: Items.Rarity.Uncommon,
			type: Items.ClothingType.Clothing,
			wearEffect: ["FEMININE_CLOTHING"],
			activeEffect: ["DAMAGE_RESIST_MINOR"],
			style: [Fashion.Style.PirateSlut, Fashion.Style.SexyDancer, Fashion.Style.Bdsm]
		},

		"pirate ankle boots": { // +20
			name: "pirate ankle boots",
			shortDesc: "{COLOR} ankle boots with giant silver buckles",
			longDesc: "\
    These stylish high heeled boots come up well past your ankle with the tops folded down. Each one \
    sports a large silver buckle. They are surprisingly light and durable.\
    ",
			slot: ClothingSlot.Shoes,
			color: "black",
			rarity: Items.Rarity.Legendary,
			type: Items.ClothingType.Clothing,
			wearEffect: ["FEMININE_CLOTHING"],
			activeEffect: ["FANCY_MOVES", "DAMAGE_RESIST_MAJOR"],
			style: [Fashion.Style.PirateSlut],
			inMarket: false
		},

		// BUTT SLOT

		// PENIS SLOT

		// WEAPON SLOT (huh?)
		"rusty cutlass": {
			name: "rusty cutlass", shortDesc: "a rusty {COLOR} cutlass",
			longDesc: "\
    This rusty sword has seen better days. Obviously the pirate who used to own this blade never took care \
    of it and unfortunately it's deteriorated too much to be repaired in any way. \
    ",
			slot: ClothingSlot.Weapon,
			color: "steel",
			rarity: Items.Rarity.Common,
			type: Items.ClothingType.Weapon,
			wearEffect: [],
			activeEffect: [],
			style: [Fashion.Style.PirateSlut],
			inMarket: false
		},

		"iron cutlass": {
			name: "iron cutlass",
			shortDesc: "a {COLOR} iron cutlass",
			longDesc: "\
    This black sword is made out of heavy iron. It's brittle compared to proper steel, but unusually sharp.",
			slot: ClothingSlot.Weapon,
			color: "black",
			rarity: Items.Rarity.Uncommon,
			type: Items.ClothingType.Weapon,
			wearEffect: [], activeEffect: ["SHARP_BLADE_COMMON", "CUT_THROAT", "DAMAGE_RESIST_MINOR"],
			style: [Fashion.Style.PirateSlut],
			inMarket: true
		},

		"steel cutlass": {
			name: "steel cutlass",
			shortDesc: "a nasty looking {COLOR} cutlass",
			longDesc: "The edge on this curved blade is wicked sharp and glints in the sun. Just holding it you feel like a real pirate.",
			slot: ClothingSlot.Weapon,
			color: "steel",
			rarity: Items.Rarity.Uncommon,
			type: Items.ClothingType.Weapon,
			wearEffect: [], activeEffect: ["SHARP_BLADE_UNCOMMON", "CUT_THROAT", "DAMAGE_RESIST_MINOR"],
			style: [Fashion.Style.PirateSlut],
			inMarket: false
		},

		"kiplers kutter": {
			name: "kiplers kutter",
			shortDesc: "a glittering shiny cutlass",
			longDesc: "\
    You won this sword in a sparring match with @@.npc;First Mate Kipler@@, a feat that the crew \
    are still talking about. The sword itself is old, far older than the man who once wielded it and it \
    has various letters etched into the blade in a language that's completely undecipherable.\
    ",
			slot: ClothingSlot.Weapon,
			color: "silver",
			rarity: Items.Rarity.Rare,
			type: Items.ClothingType.Weapon,
			wearEffect: [],
			activeEffect: ["SHARP_BLADE_RARE", "MAJOR_PIRATES_GRACE", "DAMAGE_RESIST_MAJOR"],
			style: [Fashion.Style.PirateSlut],
			inMarket: false
		},
	});
}
