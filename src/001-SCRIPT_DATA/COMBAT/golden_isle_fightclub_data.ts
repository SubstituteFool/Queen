App.Combat.clubData['Golden Isle'] = [
	{
		title: 'Brawler - One vs. One.',
		winsRequired: 0,
		maxWins: 4,
		encounter: 'Golden Isle Brawler',
	},
	{
		title: 'Boxer - One vs. One.',
		winsRequired: 2,
		maxWins: 6,
		encounter: 'Golden Isle Boxer',
	},
	{
		title: 'Pugilist - One vs. One.',
		winsRequired: 5,
		maxWins: 10,
		encounter: 'Golden Isle Pugilist',
	},
	{
		title: 'Champion - One vs. One.',
		winsRequired: 10,
		maxWins: 0,
		encounter: 'Golden Isle Champion',
	}
];

// Enemies that you can bet on.
App.Combat.clubBetData['Golden Isle'] = ['Golden Isle Brawler', 'Golden Isle Boxer', 'Golden Isle Pugilist', 'Golden Isle Champion'];

// Enemies
App.Combat.enemyData['Golden Isle Brawler'] = {
	name: 'RANDOM_MALE_NAME',
	title: 'Brawler NAME',
	health: 60,
	maxHealth: 60,
	energy: 3,
	attack: 60,
	defense: 30,
	maxStamina: 100,
	stamina: 100,
	speed: 50,
	moves: 'UNARMED',
	gender: 1,
	portraits: ['pugilist_a', 'pugilist_b', 'pugilist_c']
};

App.Combat.enemyData['Golden Isle Boxer'] = {
	name: 'RANDOM_MALE_NAME',
	title: 'Boxer NAME',
	health: 80,
	maxHealth: 80,
	energy: 3,
	attack: 80,
	defense: 40,
	maxStamina: 100,
	stamina: 100,
	speed: 50,
	moves: 'UNARMED',
	gender: 1,
	portraits: ['pugilist_a', 'pugilist_b', 'pugilist_c']
};

App.Combat.enemyData['Golden Isle Pugilist'] = {
	name: 'RANDOM_MALE_NAME',
	title: 'Pugilist NAME',
	health: 100,
	maxHealth: 100,
	energy: 3,
	attack: 100,
	defense: 50,
	maxStamina: 100,
	stamina: 100,
	speed: 50,
	moves: 'UNARMED',
	gender: 1,
	portraits: ['pugilist_a', 'pugilist_b', 'pugilist_c']
};

App.Combat.enemyData['Golden Isle Champion'] = {
	name: 'RANDOM_MALE_NAME',
	title: 'Champ NAME',
	health: 120,
	maxHealth: 120,
	energy: 3,
	attack: 120,
	defense: 60,
	maxStamina: 100,
	stamina: 100,
	speed: 50,
	moves: 'UNARMED',
	gender: 1,
	portraits: ['pugilist_a', 'pugilist_b', 'pugilist_c']
};

// Encounter Descriptions
App.Combat.encounterData['Golden Isle Brawler'] = {
	enemies: ["Golden Isle Brawler"],
	fatal: false,
	winPassage: "CombatWinFightClubGoldenIsle",
	losePassage: "CombatLoseFightClubGoldenIsle",
	intro: "You step up to the ring, ready to fight. ENEMY_0 scoffs at you.",
	lootMessage: "You claim your victory prize…",
	loot: [
		{
			chance: 100,
			type: 'Coins',
			min: 45,
			max: 120
		},
		{
			chance: 100,
			type: 'Random',
			min: 100,
			max: 200
		},
	],
	winHandler: (p) => {
		setup.world.nextPhase(1);
		App.PR.addFightClubResult(p, 'Golden Isle', true);
	},
	loseHandler: (p) => {
		setup.world.nextPhase(1);
		App.PR.addFightClubResult(p, 'Golden Isle', false);
	},
	fleeHandler: () => {
		setup.world.nextPhase(1);
	}
};

App.Combat.encounterData['Golden Isle Boxer'] = {
	enemies: ["Golden Isle Boxer"],
	fatal: false,
	winPassage: "CombatWinFightClubGoldenIsle",
	losePassage: "CombatLoseFightClubGoldenIsle",
	intro: "You step up to the ring, ready to fight. ENEMY_0 scoffs at you.",
	lootMessage: "You claim your victory prize…",
	loot: [
		{
			chance: 100,
			type: 'Coins',
			min: 60,
			max: 140
		},
		{
			chance: 100,
			type: 'Random',
			min: 125,
			max: 250
		},
	],
	winHandler: (p) => {
		setup.world.nextPhase(1);
		App.PR.addFightClubResult(p, 'Golden Isle', true);
	},
	loseHandler: (p) => {
		setup.world.nextPhase(1);
		App.PR.addFightClubResult(p, 'Golden Isle', false);
	},
	fleeHandler: () => {
		setup.world.nextPhase(1);
	}
};

// Pugilist
App.Combat.encounterData['Golden Isle Pugilist'] = {
	enemies: ["Golden Isle Pugilist"],
	fatal: false,
	winPassage: "CombatWinFightClubGoldenIsle",
	losePassage: "CombatLoseFightClubGoldenIsle",
	intro: "You step up to the ring, ready to fight. ENEMY_0 scoffs at you.",
	lootMessage: "You claim your victory prize…",
	loot: [
		{
			chance: 100,
			type: 'Coins',
			min: 80,
			max: 160
		},
		{
			chance: 100,
			type: 'Random',
			min: 150,
			max: 300
		},
	],
	winHandler: (p) => {
		setup.world.nextPhase(1);
		App.PR.addFightClubResult(p, 'Golden Isle', true);
	},
	loseHandler: (p) => {
		setup.world.nextPhase(1);
		App.PR.addFightClubResult(p, 'Golden Isle', false);
	},
	fleeHandler: () => {
		setup.world.nextPhase(1);
	}
};

// Champ
App.Combat.encounterData['Golden Isle Champion'] = {
	enemies: ["Golden Isle Champion"],
	fatal: false,
	winPassage: "CombatWinFightClubGoldenIsle",
	losePassage: "CombatLoseFightClubGoldenIsle",
	intro: "You step up to the ring, ready to fight. ENEMY_0 scoffs at you.",
	lootMessage: "You claim your victory prize…",
	loot: [
		{
			chance: 100,
			type: 'Coins',
			min: 100,
			max: 200
		},
		{
			chance: 100,
			type: 'Random',
			min: 200,
			max: 500
		},
	],
	winHandler: (p) => {
		setup.world.nextPhase(1);
		App.PR.addFightClubResult(p, 'Golden Isle', true);
	},
	loseHandler: (p) => {
		setup.world.nextPhase(1);
		App.PR.addFightClubResult(p, 'Golden Isle', false);
	},
	fleeHandler: () => {
		setup.world.nextPhase(1);
	}
};
