App.Combat.moves["BOOBPIRE"] = {
	engine: App.Combat.Engines.Siren,
	moves: {
		Touch: {
			name: "Touch",
			description: "\
        Monster Attack - No Info<br>\
        ",
			icon: "slash_icon",
			stamina: 5,
			combo: 0, // Costs no combo points
			speed: 10,
			damage: 1.0,
			unlock: () => true,
			miss: [
				[
					"BUG - ATTACK NOT USABLE BY PLAYER",
					"NPC_NAME reaches for you, but misses!"
				]
			],
			hit: [
				[
					"BUG - ATTACK NOT USABLE BY PLAYER!",
					"NPC_NAME lands an icy touch on you, so cold it burns!"
				]
			]
		},
		Toss: {
			name: "Toss",
			description: "\
        Monster Attack - No Info<br>\
        ",
			icon: "slash_icon",
			stamina: 10,
			combo: 0, // Costs no combo points
			speed: 10,
			damage: 1.2,
			unlock: () => true,
			miss: [
				[
					"BUG - ATTACK NOT USABLE BY PLAYER",
					"NPC_NAME tries to grab you, but you break free!"
				]
			],
			hit: [
				[
					"BUG - ATTACK NOT USABLE BY PLAYER!",
					"NPC_NAME grabs you and tosses you to the ground!"
				]
			]
		},
		Bite: {
			name: "Bite",
			description: "\
        Monster Attack - No Info<br>\
        ",
			icon: "slash_icon",
			stamina: 10,
			combo: 2,
			speed: 20,
			damage: 0.5,
			unlock: () => true,
			miss: [
				[
					"BUG - ATTACK NOT USABLE BY PLAYER",
					"NPC_NAME tries to bite you, but misses!"
				]
			],
			hit: [
				[
					"BUG - ATTACK NOT USABLE BY PLAYER!",
					"NPC_NAME sinks NPC_PRONOUN fangs into your chest!"
				]
			]
		},
		Claw: {
			name: "Claw",
			description: "\
        Monster Attack - No Info<br>\
        ",
			icon: "slash_icon",
			stamina: 20,
			combo: 3,
			speed: 20,
			damage: 2.5,
			unlock: () => true,
			miss: [
				[
					"BUG - ATTACK NOT USABLE BY PLAYER",
					"NPC_NAME flails at you with NPC_PRONOUN claws, but misses!"
				]
			],
			hit: [
				[
					"BUG - ATTACK NOT USABLE BY PLAYER!",
					"NPC_NAME rakes you with NPC_PRONOUN deadly claws!"
				]
			]
		}
	}
}
