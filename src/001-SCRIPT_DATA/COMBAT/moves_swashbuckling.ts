App.Combat.moves["SWASHBUCKLING"] = {
	engine: App.Combat.Engines.Swashbuckling,
	moves: {
		Slash: {
			name: "Slash",
			description: "\
        A fast, quick, attack.<br>\
        If used after a riposte, recover some combo points.<br>\
        <span style='color:darkred'>DMG LOW</span> \
        <span style='color:darkgoldenrod'>STA LOW</span> \
        <span style='color:cyan'>SPD FAST</span><br>\
        <span style='color:deeppink'>Combo Builder</span>\
        ",
			icon: "slash_icon",
			stamina: 5,
			combo: 0, // Costs no combo points
			speed: 5,
			damage: 1.0,
			unlock: function (player) {
				return player instanceof App.Combat.Player && player.isEquipped(App.ClothingSlot.Weapon);
			},
			miss: [
				[
					"You swing at NPC_NAME with your pEQUIP(Weapon|$weapon), but miss!",
					"NPC_NAME swings at you, but misses!"
				],
				[
					"You slash at NPC_NAME with your pEQUIP(Weapon|$weapon), but the attack is deflected!",
					"NPC_NAME slashes at you with NPC_PRONOUN weapon, but you deflect the attack!"
				],
				[
					"NPC_NAME dodges your stab!",
					"You dodge NPC_NAME's stab!"
				]
			],
			hit: [
				[
					"You hit NPC_NAME with a light slash!",
					"NPC_NAME hits you with a light slash!"
				],
				[
					"You hit NPC_NAME with a solid slash!",
					"NPC_NAME slashes you with a solid slash!"
				],
				[
					"You attack NPC_NAME with a brutal slash!",
					"NPC_NAME attacks you with a brutal slash!"
				],
				[
					"You assault NPC_NAME with a vicious slash!",
					"NPC_NAME assaults you with a vicious slash!"
				]
			]
		},
		Stab: {
			name: "Stab",
			description: "\
        A powerful weapon stab.<br>\
        If used after a riposte, grant an accuracy buff.<br>\
        <span style='color:darkred'>DMG MED</span> \
        <span style='color:darkgoldenrod'>STA MED</span> \
        <span style='color:cyan'>SPD SLOW</span><br>\
        <span style='color:deeppink'>Combo Builder</span>\
        ",
			icon: "stab_icon",
			stamina: 10,
			combo: 0, // Costs no combo points
			speed: 10,
			damage: 1.5,
			unlock: function (player) {
				return player instanceof App.Combat.Player && player.isEquipped(App.ClothingSlot.Weapon);
			},
			miss: [
				[
					"You stab at NPC_NAME with your pEQUIP(Weapon|$weapon), but miss!",
					"NPC_NAME stabs at you, but misses!"
				],
				[
					"You stab at NPC_NAME with your pEQUIP(Weapon|$weapon), but the attack is deflected!",
					"NPC_NAME stabs at you with NPC_PRONOUN weapon, but you deflect the attack!"
				],
				[
					"NPC_NAME dodges your stab!",
					"You dodge NPC_NAME's stab!"
				]
			],
			hit: [
				[
					"You hit NPC_NAME with a shallow stab!",
					"NPC_NAME hits you with a shallow stab!"
				],
				[
					"You hit NPC_NAME with a solid stab!",
					"NPC_NAME slashes you with a solid stab!"
				],
				[
					"You attack NPC_NAME with a deep stab!",
					"NPC_NAME attacks you with a deep stab!"
				],
				[
					"You assault NPC_NAME with a brutal stab!",
					"NPC_NAME assaults you with a brutal stab!"
				]
			]
		},
		Parry: {
			name: "Parry",
			description: "\
        A defensive move, does no damage. Sets up for riposte.<br> \
        You cannot parry twice in a row or after a riposte.<br>\
        <span style='color:darkred'>DMG NONE</span> \
        <span style='color:darkgoldenrod'>STA LOW</span> \
        <span style='color:cyan'>SPD VERY SLOW</span><br>\
        <span style='color:deeppink'>Combo Builder</span>\
        ",
			icon: "parry_icon",
			stamina: 5,
			combo: 0, // Costs no combo points
			speed: 20,
			damage: 0,
			unlock: function (player) {
				return player instanceof App.Combat.Player && player.isEquipped(App.ClothingSlot.Weapon) && player.engine.lastMove != 'Parry' && player.engine.lastMove != 'Riposte';
			},
			miss: [
				[
					"You try to parry with your pEQUIP(Weapon|$weapon), but fail!",
					"NPC_NAME tries to parry with NPC_PRONOUN weapon, but fails!"
				]
			],
			hit: [
				[
					"You parry with your pEQUIP(Weapon|$weapon)!",
					"NPC_NAME parries with NPC_PRONOUN weapon!"
				]
			]
		},
		Riposte: {
			name: "Riposte",
			description: "\
        Use after a parry success. Converts combo points into extra damage.<br>\
        <span style='color:darkred'>DMG HIGH</span> \
        <span style='color:darkgoldenrod'>STA MED</span> \
        <span style='color:cyan'>SPD SLOW</span><br>\
        <span style='color:deeppink'>Combo Consumer (Variable)</span>\
        ",
			icon: "riposte_icon",
			stamina: 10,
			combo: 1,
			speed: 15,
			damage: 2,
			unlock: function (player) {
				return player instanceof App.Combat.Player && player.isEquipped(App.ClothingSlot.Weapon) && player.engine.lastMove == 'Parry';
			},
			miss: [
				[
					"You try to riposte NPC_NAME with your pEQUIP(Weapon|$weapon), but miss!",
					"NPC_NAME tries to riposte with NPC_PRONOUN weapon, but misses!"
				]
			],
			hit: [
				[
					"You riposte NPC_NAME with your pEQUIP(Weapon|$weapon)!",
					"NPC_NAME ripostes with NPC_PRONOUN weapon!"
				]
			]
		},
		Cleave: {
			name: "Cleave",
			description: "\
        Usable only after a slash.<br>\
        <span style='color:darkred'>DMG HIGH</span> \
        <span style='color:darkgoldenrod'>STA LOW</span> \
        <span style='color:cyan'>SPD FAST</span><br>\
        <span style='color:deeppink'>Combo Consumer</span>\
        ",
			icon: "cleave_icon",
			stamina: 5,
			combo: 3, // Costs no combo points
			speed: 5,
			damage: 2.5,
			unlock: function (player) {
				return player instanceof App.Combat.Player && player.isEquipped(App.ClothingSlot.Weapon) && player.engine.lastMove == 'Slash';
			},
			miss: [
				[
					"You try to cleave NPC_NAME with your pEQUIP(Weapon|$weapon), but miss!",
					"NPC_NAME tries to cleave you with NPC_PRONOUN weapon, but misses!"
				]
			],
			hit: [
				[
					"You cleave NPC_NAME with your pEQUIP(Weapon|$weapon)!",
					"NPC_NAME cleaves you with NPC_PRONOUN weapon!"
				]
			]
		},
		Behead: {
			name: "Behead",
			description: "\
        Small chance to insta-kill enemies below 50% health.<br>\
        <span style='color:darkred'>DMG HIGH</span> \
        <span style='color:darkgoldenrod'>STA HIGH</span> \
        <span style='color:cyan'>SPD SLOW</span><br>\
        <span style='color:deeppink'>Combo Consumer</span>\
        ",
			icon: "behead_icon",
			stamina: 15,
			combo: 5,
			speed: 20,
			damage: 3.0,
			unlock: function (player) {
				return player instanceof App.Combat.Player && player.isEquipped(App.ClothingSlot.Weapon);
			},
			miss: [
				[
					"You try to behead NPC_NAME with your pEQUIP(Weapon|$weapon), but miss!",
					"NPC_NAME tries to behead you with NPC_PRONOUN weapon, but misses!"
				]
			],
			hit: [
				[
					"You chop at NPC_NAME's vitals with your pEQUIP(Weapon|$weapon)!",
					"NPC_NAME chops at your vitals with NPC_PRONOUN weapon!"
				]
			]
		},
	}
};
