// Fightclub menu
App.Combat.clubData['Salty Mermaid'] = [
	{
		title: 'Landlubber -  One vs. One.',
		winsRequired: 0,
		maxWins: 2,
		encounter: 'Salty Mermaid Landlubber',
	},
	{
		title: 'Swabbie - One vs. One.',
		winsRequired: 2,
		maxWins: 6,
		encounter: 'Salty Mermaid Swabbie',
	},
	{
		title: 'Middy - One vs. One.',
		winsRequired: 5,
		maxWins: 10,
		encounter: 'Salty Mermaid Middy',
	},
	{
		title: 'Seadog - One vs. One.',
		winsRequired: 8,
		maxWins: 14,
		encounter: 'Salty Mermaid Seadog',
	},
	{
		title: 'Marine - One vs. Two.',
		winsRequired: 12,
		maxWins: 17,
		encounter: 'Salty Mermaid Marine',
	},
	{
		title: 'Privateer - One vs. Two.',
		winsRequired: 16,
		maxWins: 21,
		encounter: 'Salty Mermaid Privateer',
	},
	{
		title: 'Buccaneer - One vs. Two.',
		winsRequired: 20,
		maxWins: 100,
		encounter: 'Salty Mermaid Buccaneer',
	},
	{
		title: '??? Mystery ???',
		winsRequired: 100,
		maxWins: 101,
		encounter: 'Salty Mermaid Mystery'
	}
];

// Enemies that you can bet on.
App.Combat.clubBetData['Salty Mermaid'] = [
	'Salty Mermaid Landlubber', 'Salty Mermaid Swabbie', 'Salty Mermaid Middy', 'Salty Mermaid Seadog',
	'Salty Mermaid Landlubber', 'Salty Mermaid Swabbie', 'Salty Mermaid Middy', 'Salty Mermaid Seadog',
	'Salty Mermaid Landlubber', 'Salty Mermaid Swabbie', 'Salty Mermaid Middy', 'Salty Mermaid Seadog',
	'Kipler Unarmed'
];

// Enemies
App.Combat.enemyData['Salty Mermaid Landlubber'] = {
	name: 'RANDOM_MALE_NAME',
	title: 'Landlubber NAME',
	health: 40,
	maxHealth: 40,
	energy: 3,
	attack: 40,
	defense: 20,
	maxStamina: 100,
	stamina: 100,
	speed: 50,
	moves: 'UNARMED',
	gender: 1,
	portraits: ['pirate_a', 'pirate_b', 'pirate_c', 'pirate_d', 'pugilist_a', 'pugilist_b', 'pugilist_c']
};

App.Combat.enemyData['Salty Mermaid Swabbie'] = {
	name: 'RANDOM_MALE_NAME',
	title: 'Swabbie NAME',
	health: 60,
	maxHealth: 60,
	energy: 3,
	attack: 50,
	defense: 25,
	maxStamina: 100,
	stamina: 100,
	speed: 50,
	moves: 'UNARMED',
	gender: 1,
	portraits: ['pirate_a', 'pirate_b', 'pirate_c', 'pirate_d', 'pugilist_a', 'pugilist_b', 'pugilist_c']
};

App.Combat.enemyData['Salty Mermaid Middy'] = {
	name: 'RANDOM_MALE_NAME',
	title: 'Middy NAME',
	health: 80,
	maxHealth: 80,
	energy: 3,
	attack: 60,
	defense: 30,
	maxStamina: 100,
	stamina: 100,
	speed: 50,
	moves: 'UNARMED',
	gender: 1,
	portraits: ['pirate_a', 'pirate_b', 'pirate_c', 'pirate_d', 'pugilist_a', 'pugilist_b', 'pugilist_c']
};

App.Combat.enemyData['Salty Mermaid Seadog'] = {
	name: 'RANDOM_MALE_NAME',
	title: 'Seadog NAME',
	health: 100,
	maxHealth: 100,
	energy: 3,
	attack: 70,
	defense: 35,
	maxStamina: 100,
	stamina: 100,
	speed: 50,
	moves: 'UNARMED',
	gender: 1,
	portraits: ['pirate_a', 'pirate_b', 'pirate_c', 'pirate_d', 'pugilist_a', 'pugilist_b', 'pugilist_c']
};

// Encounters
App.Combat.encounterData['Salty Mermaid Landlubber'] = {
	enemies: ["Salty Mermaid Landlubber"],
	fatal: false,
	winPassage: "CombatWinFightClubMermaid",
	losePassage: "CombatLoseFightClubMermaid",
	intro: "You step up to the ring, ready to fight. ENEMY_0 scoffs at you.",
	lootMessage: "You claim your victory prize…",
	loot: [
		{
			chance: 100,
			type: 'Coins',
			min: 10,
			max: 50
		},
		{
			chance: 80,
			type: 'Random',
			min: 50,
			max: 100
		},
	],
	winHandler: (p) => {
		setup.world.nextPhase(1);
		App.PR.addFightClubResult(p, 'Salty Mermaid', true);
	},
	loseHandler: (p) => {
		setup.world.nextPhase(1);
		App.PR.addFightClubResult(p, 'Salty Mermaid', false);
	},
	fleeHandler: () => {
		setup.world.nextPhase(1);
	}
};

App.Combat.encounterData['Salty Mermaid Swabbie'] = {
	enemies: ["Salty Mermaid Swabbie"],
	fatal: false,
	winPassage: "CombatWinFightClubMermaid",
	losePassage: "CombatLoseFightClubMermaid",
	intro: "You step up to the ring, ready to fight. ENEMY_0 scoffs at you.",
	lootMessage: "You claim your victory prize…",
	loot: [
		{
			chance: 100,
			type: 'Coins',
			min: 20,
			max: 75
		},
		{
			chance: 80,
			type: 'Random',
			min: 75,
			max: 150
		},
	],
	winHandler: (p) => {
		setup.world.nextPhase(1);
		App.PR.addFightClubResult(p, 'Salty Mermaid', true);
	},
	loseHandler: (p) => {
		setup.world.nextPhase(1);
		App.PR.addFightClubResult(p, 'Salty Mermaid', false);
	},
	fleeHandler: () => {
		setup.world.nextPhase(1);
	}
};

App.Combat.encounterData['Salty Mermaid Middy'] = {
	enemies: ["Salty Mermaid Middy"],
	fatal: false,
	winPassage: "CombatWinFightClubMermaid",
	losePassage: "CombatLoseFightClubMermaid",
	intro: "You step up to the ring, ready to fight. ENEMY_0 scoffs at you.",
	lootMessage: "You claim your victory prize…",
	loot: [
		{
			chance: 100,
			type: 'Coins',
			min: 30,
			max: 100
		},
		{
			chance: 80,
			type: 'Random',
			min: 100,
			max: 200
		},
	],
	winHandler: (p) => {
		setup.world.nextPhase(1);
		App.PR.addFightClubResult(p, 'Salty Mermaid', true);
	},
	loseHandler: (p) => {
		setup.world.nextPhase(1);
		App.PR.addFightClubResult(p, 'Salty Mermaid', false);
	},
	fleeHandler: () => {
		setup.world.nextPhase(1);
	}
};

App.Combat.encounterData['Salty Mermaid Seadog'] = {
	enemies: ["Salty Mermaid Seadog"],
	fatal: false,
	winPassage: "CombatWinFightClubMermaid",
	losePassage: "CombatLoseFightClubMermaid",
	intro: "You step up to the ring, ready to fight. ENEMY_0 scoffs at you.",
	lootMessage: "You claim your victory prize…",
	loot: [
		{
			chance: 100,
			type: 'Coins',
			min: 40,
			max: 130
		},
		{
			chance: 80,
			type: 'Random',
			min: 150,
			max: 250
		},
	],
	winHandler: (p) => {
		setup.world.nextPhase(1);
		App.PR.addFightClubResult(p, 'Salty Mermaid', true);
	},
	loseHandler: (p) => {
		setup.world.nextPhase(1);
		App.PR.addFightClubResult(p, 'Salty Mermaid', false);
	},
	fleeHandler: () => {
		setup.world.nextPhase(1);
	}
};

App.Combat.encounterData['Salty Mermaid Marine'] = {
	enemies: ["Salty Mermaid Swabbie", "Salty Mermaid Swabbie"],
	fatal: false,
	winPassage: "CombatWinFightClubMermaid",
	losePassage: "CombatLoseFightClubMermaid",
	intro: "You step up to the ring, ready to fight. ENEMY_0 scoffs at you.",
	lootMessage: "You claim your victory prize…",
	loot: [
		{
			chance: 100,
			type: 'Coins',
			min: 60,
			max: 150
		},
		{
			chance: 80,
			type: 'Random',
			min: 150,
			max: 250
		},
		{
			chance: 80,
			type: 'Random',
			min: 150,
			max: 250
		},
	],
	winHandler: (p) => {
		setup.world.nextPhase(1);
		App.PR.addFightClubResult(p, 'Salty Mermaid', true);
	},
	loseHandler: (p) => {
		setup.world.nextPhase(1);
		App.PR.addFightClubResult(p, 'Salty Mermaid', false);
	},
	fleeHandler: () => {
		setup.world.nextPhase(1);
	}
};

App.Combat.encounterData['Salty Mermaid Privateer'] = {
	enemies: ["Salty Mermaid Middy", "Salty Mermaid Middy"],
	fatal: false,
	winPassage: "CombatWinFightClubMermaid",
	losePassage: "CombatLoseFightClubMermaid",
	intro: "You step up to the ring, ready to fight. ENEMY_0 scoffs at you.",
	lootMessage: "You claim your victory prize…",
	loot: [
		{
			chance: 100,
			type: 'Coins',
			min: 80,
			max: 200
		},
		{
			chance: 80,
			type: 'Random',
			min: 150,
			max: 250
		},
		{
			chance: 80,
			type: 'Random',
			min: 150,
			max: 250
		},
	],
	winHandler: (p) => {
		setup.world.nextPhase(1);
		App.PR.addFightClubResult(p, 'Salty Mermaid', true);
	},
	loseHandler: (p) => {
		setup.world.nextPhase(1);
		App.PR.addFightClubResult(p, 'Salty Mermaid', false);
	},
	fleeHandler: () => {
		setup.world.nextPhase(1);
	}
};

App.Combat.encounterData['Salty Mermaid Buccaneer'] = {
	enemies: ["Salty Mermaid Seadog", "Salty Mermaid Seadog"],
	fatal: false,
	winPassage: "CombatWinFightClubMermaid",
	losePassage: "CombatLoseFightClubMermaid",
	intro: "You step up to the ring, ready to fight. ENEMY_0 scoffs at you.",
	lootMessage: "You claim your victory prize…",
	loot: [
		{
			chance: 100,
			type: 'Coins',
			min: 100,
			max: 250
		},
		{
			chance: 80,
			type: 'Random',
			min: 150,
			max: 250
		},
		{
			chance: 80,
			type: 'Random',
			min: 150,
			max: 250
		},
	],
	winHandler: (p) => {
		setup.world.nextPhase(1);
		App.PR.addFightClubResult(p, 'Salty Mermaid', true);
	},
	loseHandler: (p) => {
		setup.world.nextPhase(1);
		App.PR.addFightClubResult(p, 'Salty Mermaid', false);
	},
	fleeHandler: () => {
		setup.world.nextPhase(1);
	}
};

App.Combat.encounterData['Salty Mermaid Mystery'] = {
	enemies: ["Kipler Unarmed"],
	fatal: false,
	winPassage: "CombatWinFightClubMermaid",
	losePassage: "CombatLoseFightClubMermaid",
	intro: "You step up to the ring, ready to fight. ENEMY_0 scoffs at you.",
	lootMessage: "You claim your victory prize…",
	loot: [
		{
			chance: 100,
			type: 'Coins',
			min: 250,
			max: 500
		},
		{
			chance: 100,
			type: 'Random',
			min: 250,
			max: 500
		},
		{
			chance: 100,
			type: 'Random',
			min: 250,
			max: 500
		},
	],
	winHandler: (p) => {
		setup.world.nextPhase(1);
		App.PR.addFightClubResult(p, 'Salty Mermaid', true);
	},
	loseHandler: (p) => {
		setup.world.nextPhase(1);
		App.PR.addFightClubResult(p, 'Salty Mermaid', false);
	},
	fleeHandler: () => {
		setup.world.nextPhase(1);
	}
};
