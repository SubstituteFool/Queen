// Not sure if I want to use this or not.
namespace App.Data {
	Object.append(loot, {
		DUNGEON_COMMON: [
			{category: Items.Category.MiscConsumable, tag: "torch", min: 1, max: 3},
			{category: Items.Category.MiscConsumable, tag: "shovel", min: 1, max: 1},
			{category: Items.Category.MiscConsumable, tag: "junk", min: 1, max: 1},
			{category: Items.Category.Drugs, tag: "hair tonic", min: 1, max: 1},
			{category: Items.Category.Drugs, tag: "fairy dust", min: 1, max: 1},
			{category: Items.Category.Drugs, tag: "lip balm", min: 1, max: 1},
			{category: Items.Category.Food, tag: "beer", min: 1, max: 3},
			{category: Items.Category.Food, tag: "cheap wine", min: 1, max: 3},
			{category: Items.Category.Food, tag: "hardtack", min: 1, max: 1},
			{category: Items.Category.MiscLoot, tag: "broken rune", min: 1, max: 1}
		],

		DUNGEON_UNCOMMON: [
			{category: Items.Category.Drugs, tag: "pixie dust", min: 1, max: 1},
			{category: Items.Category.Drugs, tag: "khafkir", min: 1, max: 1},
			{category: Items.Category.Drugs, tag: "apColdsfoot", min: 1, max: 1},
			{category: Items.Category.Drugs, tag: "apDamiana", min: 1, max: 1},
			{category: Items.Category.Drugs, tag: "apRose", min: 1, max: 1},
			{category: Items.Category.Drugs, tag: "apBaneberry", min: 1, max: 1},
			{category: Items.Category.Food, tag: "hardtack", min: 1, max: 3},
			{category: Items.Category.Food, tag: "milkdew melon", min: 1, max: 2},
			{category: Items.Category.Food, tag: "butter gourd", min: 1, max: 2},
			{category: Items.Category.Food, tag: "orgeat", min: 1, max: 3},
			{category: Items.Category.MiscLoot, tag: "old arrowhead", min: 1, max: 1}
		],

		DUNGEON_RARE: [
			{category: Items.Category.LootBox, tag: "rare lolita loot box", min: 1, max: 1},
			{category: Items.Category.LootBox, tag: "rare pirate loot box", min: 1, max: 1},
			{category: Items.Category.LootBox, tag: "rare dancer loot box", min: 1, max: 1},
			{category: Items.Category.LootBox, tag: "rare sissy loot box", min: 1, max: 1},
			{category: Items.Category.Drugs, tag: "siren elixir", min: 1, max: 1},
			{category: Items.Category.Drugs, tag: "apSarsaparilla", min: 1, max: 1},
			{category: Items.Category.Drugs, tag: "apWidowsWeb", min: 1, max: 1},
			{category: Items.Category.Food, tag: "pirates plunder", min: 1, max: 1},
			{category: Items.Category.Food, tag: "purple mushrooms", min: 1, max: 3},
			{category: Items.Category.MiscLoot, tag: "glowing crystal", min: 1, max: 1}
		],

		DUNGEON_LEGENDARY: [
			{category: Items.Category.Drugs, tag: "siren elixir", min: 1, max: 1},
			{category: Items.Category.Drugs, tag: "nereid philtre", min: 1, max: 1},
			{category: Items.Category.Drugs, tag: "succubus philtre", min: 1, max: 1},
			{category: Items.Category.Food, tag: "pirates plunder", min: 1, max: 2},
			{category: Items.Category.Food, tag: "purple mushrooms", min: 1, max: 3},
			{category: Items.Category.Food, tag: "ambrosia", min: 1, max: 1},
			{category: Items.Category.MiscLoot, tag: "stone tablet", min: 1, max: 1}
		]
	});
}
