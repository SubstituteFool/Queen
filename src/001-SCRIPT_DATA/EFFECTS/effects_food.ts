namespace App.Data {
	Object.append(effectLib, {
		SLAVE_GRUEL: {
			fun: (p) => {
				p.adjustStat(CoreStat.Nutrition, 15);
				p.adjustStatXP(CoreStat.Nutrition, 75);
				p.adjustStatXP(CoreStat.Willpower, -25);
				p.adjustStatXP(CoreStat.Perversion, 50);
				p.adjustStatXP(CoreStat.Hormones, 50);
			},
			value: 30,
			knowledge: [
				"Nutrition Up++", "Satiation+++", "WillPower Down-",
				"Perversion Up+", "Female Hormones++"
			]
		},
		SNACK: {
			fun: (p) => {p.adjustStat(CoreStat.Nutrition, 5); p.adjustStatXP(CoreStat.Nutrition, 10);},
			value: 10,
			knowledge: ["Nutrition Up", "Satiation Up+"]
		},

		UNWHOLESOME_MEAL: {
			fun: (p) => {
				p.adjustStat(CoreStat.Toxicity, 10);
			},
			value: 0,
			knowledge: ["Unwholesome Meal-"]
		},

		LIGHT_WHOLESOME_MEAL: {
			fun: (p) => {
				p.adjustStat(CoreStat.Nutrition, 10);
				p.adjustStatXP(CoreStat.Nutrition, 25);
				p.adjustStat(CoreStat.Toxicity, -5);
			},
			value: 30,
			knowledge: ["Wholesome Meal+"]
		},
		WHOLESOME_MEAL: {
			fun: (p) => {
				p.adjustStat(CoreStat.Nutrition, 20);
				p.adjustStatXP(CoreStat.Nutrition, 50);
				p.adjustStat(CoreStat.Toxicity, -10);
			},
			value: 60,
			knowledge: ["Wholesome Meal++"]
		},
		LIGHT_ALCOHOL: {
			fun: (p) => {
				p.adjustStat(CoreStat.Nutrition, 5);
				p.adjustStatXP(CoreStat.Nutrition, 15);
				p.adjustStat(CoreStat.Toxicity, 5);
			},
			value: 20,
			knowledge: ["Nutrition Up", "Satiation Up", "Toxicity Up"]
		},
		HARD_ALCOHOL: {
			fun: (p) => {
				p.adjustStat(CoreStat.Nutrition, 5);
				p.adjustStatXP(CoreStat.Nutrition, 10);
				p.adjustStat(CoreStat.Toxicity, 30);
				p.adjustStatXP(CoreStat.Willpower, 50);
			},
			value: 100,
			knowledge: ["Nutrition Up", "Satiation Up", "Toxicity Up--", "WillPower Up+"]
		},
	});
}
