Object.append(App.Data.effectLib, {
	/** FOOD BOX */
	FOOD_LOOT_BOX_COMMON: {
		fun: () => {return App.Loot.instance.generateLootBox(App.Data.lootTables["FOOD"], 100, 300);},
		value: 0,
		knowledge: ["Loot Box+"]
	},
	FOOD_LOOT_BOX_UNCOMMON: {
		fun: () => {return App.Loot.instance.generateLootBox(App.Data.lootTables["FOOD"], 200, 500);},
		value: 0,
		knowledge: ["Loot Box++"]
	},
	FOOD_LOOT_BOX_RARE: {
		fun: () => {return App.Loot.instance.generateLootBox(App.Data.lootTables["FOOD"], 300, 700);},
		value: 0,
		knowledge: ["Loot Box+++"]
	},
	FOOD_LOOT_BOX_LEGENDARY: {
		fun: () => {return App.Loot.instance.generateLootBox(App.Data.lootTables["FOOD"], 400, 1000);},
		value: 0,
		knowledge: ["Loot Box++++"]
	},
	/** LANDLUBBER BOX */
	LANDLUBBER_LOOT_BOX_COMMON: {
		fun: () => {return App.Loot.instance.generateLootBox(App.Data.lootTables["LANDLUBBER"], 500, 1000);},
		value: 0,
		knowledge: ["Loot Box+"]
	},

	/** LOLITA_SISSY BOX */
	LOLITA_SISSY_LOOT_BOX_COMMON: {
		fun: () => {return App.Loot.instance.generateLootBox(App.Data.lootTables["LOLITA_SISSY"], 500, 1000);},
		value: 0,
		knowledge: ["Loot Box+"]
	},
	LOLITA_SISSY_LOOT_BOX_UNCOMMON: {
		fun: () => {return App.Loot.instance.generateLootBox(App.Data.lootTables["LOLITA_SISSY"], 1000, 1500);},
		value: 0,
		knowledge: ["Loot Box++"]
	},
	LOLITA_SISSY_LOOT_BOX_RARE: {
		fun: () => {return App.Loot.instance.generateLootBox(App.Data.lootTables["LOLITA_SISSY"], 1500, 2000);},
		value: 0,
		knowledge: ["Loot Box+++"]
	},
	LOLITA_SISSY_LOOT_BOX_LEGENDARY: {
		fun: () => {return App.Loot.instance.generateLootBox(App.Data.lootTables["LOLITA_SISSY"], 2000, 2500);},
		value: 0,
		knowledge: ["Loot Box++++"]
	},
	/** PATHETIC_LOSER BOX */
	PATHETIC_LOSER_LOOT_BOX_COMMON: {
		fun: () => {return App.Loot.instance.generateLootBox(App.Data.lootTables["PATHETIC_LOSER"], 1000, 2000);},
		value: 0,
		knowledge: ["Loot Box+"]
	},

	/** PIRATE_BDSM BOX */
	PIRATE_BDSM_LOOT_BOX_COMMON: {
		fun: () => {return App.Loot.instance.generateLootBox(App.Data.lootTables["PIRATE_BDSM"], 500, 1000);},
		value: 0,
		knowledge: ["Loot Box+"]
	},
	PIRATE_BDSM_LOOT_BOX_UNCOMMON: {
		fun: () => {return App.Loot.instance.generateLootBox(App.Data.lootTables["PIRATE_BDSM"], 1000, 1500);},
		value: 0,
		knowledge: ["Loot Box++"]
	},
	PIRATE_BDSM_LOOT_BOX_RARE: {
		fun: () => {return App.Loot.instance.generateLootBox(App.Data.lootTables["PIRATE_BDSM"], 1500, 2000);},
		value: 0,
		knowledge: ["Loot Box+++"]
	},
	PIRATE_BDSM_LOOT_BOX_LEGENDARY: {
		fun: () => {return App.Loot.instance.generateLootBox(App.Data.lootTables["PIRATE_BDSM"], 2000, 2500);},
		value: 0,
		knowledge: ["Loot Box++++"]
	},
	/** SISSY_BIMBO BOX */
	SISSY_BIMBO_LOOT_BOX_COMMON: {
		fun: () => {return App.Loot.instance.generateLootBox(App.Data.lootTables["SISSY_BIMBO"], 500, 1000);},
		value: 0,
		knowledge: ["Loot Box+"]
	},
	SISSY_BIMBO_LOOT_BOX_UNCOMMON: {
		fun: () => {return App.Loot.instance.generateLootBox(App.Data.lootTables["SISSY_BIMBO"], 1000, 1500);},
		value: 0,
		knowledge: ["Loot Box++"]
	},
	SISSY_BIMBO_LOOT_BOX_RARE: {
		fun: () => {return App.Loot.instance.generateLootBox(App.Data.lootTables["SISSY_BIMBO"], 1500, 2000);},
		value: 0,
		knowledge: ["Loot Box+++"]
	},
	SISSY_BIMBO_LOOT_BOX_LEGENDARY: {
		fun: () => {return App.Loot.instance.generateLootBox(App.Data.lootTables["SISSY_BIMBO"], 2000, 2500);},
		value: 0,
		knowledge: ["Loot Box++++"]
	},
	/** DANCER_BIMBO BOX */
	DANCER_BIMBO_LOOT_BOX_COMMON: {
		fun: () => {return App.Loot.instance.generateLootBox(App.Data.lootTables["DANCER_BIMBO"], 500, 1000);},
		value: 0,
		knowledge: ["Loot Box+"]
	},
	DANCER_BIMBO_LOOT_BOX_UNCOMMON: {
		fun: () => {return App.Loot.instance.generateLootBox(App.Data.lootTables["DANCER_BIMBO"], 1000, 1500);},
		value: 0,
		knowledge: ["Loot Box++"]
	},
	DANCER_BIMBO_LOOT_BOX_RARE: {
		fun: () => {return App.Loot.instance.generateLootBox(App.Data.lootTables["DANCER_BIMBO"], 1500, 2000);},
		value: 0,
		knowledge: ["Loot Box+++"]
	},
	DANCER_BIMBO_LOOT_BOX_LEGENDARY: {
		fun: () => {return App.Loot.instance.generateLootBox(App.Data.lootTables["DANCER_BIMBO"], 2000, 2500);},
		value: 0,
		knowledge: ["Loot Box++++"]
	},
	/** COURTESAN BOX */
	COURTESAN_LOOT_BOX_COMMON: {
		fun: () => {return App.Loot.instance.generateLootBox(App.Data.lootTables["COURTESAN"], 500, 1000);},
		value: 0,
		knowledge: ["Loot Box+"]
	},
	COURTESAN_LOOT_BOX_UNCOMMON: {
		fun: () => {return App.Loot.instance.generateLootBox(App.Data.lootTables["COURTESAN"], 1000, 1500);},
		value: 0,
		knowledge: ["Loot Box++"]
	},
	COURTESAN_LOOT_BOX_RARE: {
		fun: () => {return App.Loot.instance.generateLootBox(App.Data.lootTables["COURTESAN"], 1500, 2000);},
		value: 0,
		knowledge: ["Loot Box+++"]
	},
	COURTESAN_LOOT_BOX_LEGENDARY: {
		fun: () => {return App.Loot.instance.generateLootBox(App.Data.lootTables["COURTESAN"], 2000, 2500);},
		value: 0,
		knowledge: ["Loot Box++++"]
	},
});
