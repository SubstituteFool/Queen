namespace App.Data {
	Object.append(events, {
		Portside: [
			{
				id: 'BoobpireAttack',
				from: 'Any',
				maxRepeat: 0,
				minDay: 0,
				maxDay: 0,
				cool: 5,
				phase: [2, 3, 4],
				passage: 'BoobpireEvent',
				check: p => p.getStat(Stat.Body, BodyPart.Bust) >= 50 && Math.random() < 0.33
			},
		],

		Bazaar: [
			{
				id: 'BoobpireAttack',
				from: 'Any',
				maxRepeat: 0,
				minDay: 0,
				maxDay: 0,
				cool: 5,
				phase: [2, 3, 4],
				passage: 'BoobpireEvent',
				check: p => p.getStat(Stat.Body, BodyPart.Bust) >= 50 && Math.random() < 0.33
			},
		],

		GI_GovernorsMansionInside: [
			{
				id: 'BertieMapStolen',
				from: 'GI_GovernorsMansion',
				force: true,
				maxRepeat: 1,
				minDay: 0,
				maxDay: 0,
				cool: 5,
				phase: [2, 3, 4],
				passage: 'BertieMapStolenEvent',
				check: p => (p.questFlags['BERTIE_SISSY_CompletedOn'] as number + 2 ?? Number.POSITIVE_INFINITY) <= setup.world.day
			},
		]
	});
}
