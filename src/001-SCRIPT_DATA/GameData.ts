namespace App.Data {
	export const names = {/* spell-checker: disable */
		Male: [
			"Ailen", "Aldritch", "Allan", "Arnold", "Austyn", "Ayden",
			"Bobby", "Brennan", "Brewster", "Brod", "Barthon",
			"Cal", "Clifford", "Colby", "Conner", "Creighton", "Cruz",
			"Dale", "Devyn", "Dexter", "Dylan", "Dover", "Dwight",
			"Elfred", "Erick",
			"Fredrich", "Felix", "Faustino",
			"Garfield", "Gerard", "Granger", "Gimedon", "Grumdonore", "Gulgurn",
			"Harman", "Hawthorne", "Hector", "Howard", "Hulfahgrum", "Hjoluadren",
			"Johan", "Jon", "Josef", "Junior",
			"Kameron", "Keanu", "Keegan", "Kramahnam",
			"Leroy", "Larry", "Leonard",
			"Marc", "Manton", "Milton", "Magtharn", "Muirrus",
			"Neddy", "Newman", "Normand", "Norris",
			"Oswald", "Oxton",
			"Ransford", "Regnauld", "Ridley", "Rinaldo", "Rochester", "Ryman",
			"Scot", "Seabright", "Shawn", "Stephen",
			"Taylor", "Townes", "Theramyr",
			"William", "Welsh", "Wescott", "Wheeler"
		],
		Female: [
			"Abbie", "Anne", "Aubree", "Arianna", "Alissa", "Astrid", "Alena", "Alice",
			"Bobbi", "Brianne", "Brittney", "Bailey", "Blossom", "Bianca",
			"Clovis", "Carly", "Catie", "Chloe",
			"Delilah", "Dellie", "Dinah", "Demi", "Daisy",
			"Edlyn", "Emi", "Evie", "Emma", "Elizabeth",
			"Fanny", "Felicia", "Fossie", "Fina",
			"Gretchen", "Gabbi",
			"Heidi", "Hayley",
			"Isabelle", "Isolde",
			"Josie", "Joelle", "Jenni", "Jacki", "Jynri",
			"Kelly", "Kylie", "Kyla", "Kailee",
			"Luella", "Lisbeth", "Lisa", "Lizzie", "Lenora",
			"Missy", "Miranda", "Molly", "Mitsi", "Maya", "Macie", "Mina", "Millicent",
			"Nikki", "Nadia",
			"Ophelia",
			"Penny", "Paula", "Priss",
			"Rosie", "Robin",
			"Sylvie", "Shelli", "Suzy", "Svetlana", "Sybil", "Susanna",
			"Tessa", "Tanis", "Taffy", "Telia", "Tabbi",
			"Uma", "Ursula", "Uta",
			"Vannesa", "Vicki", "Victoria",
			"Wanda", "Winnie",
			"Yvette", "Yolanda", "Yessica",
			"Zoe"
		],
		Sissy: [
			"Abigail", "Angelica", "Annabelle", "Annie",
			"Bambi", "Bambina", "Bebelle", "Bliss", "Blossom", "Bonnie", "Boo", "Bootie", "Brandi", "Brie", "Bubbles", "Bunny", "Buttercup", "Butterscotch",
			"Camilla", "Candi", "Capri", "Caramella", "Cassie", "Cecily", "Celia", "Chastity", "Chéri", "Cherry", "Cici", "Cinnamon", "Claribel", "Clarissa", "Cosi", "Crystal",
			"Daisy", "Delia", "Delilah", "Dixie", "Dolly", "Dottie",
			"Elisabetta", "Elise", "Eliza", "Esposa", "Evie",
			"Felicity", "Fiona", "Flora", "Foxy", "Freckles",
			"Gabbi", "Ginger", "Giselle", "Goldie",
			"Isadora", "Izzy",
			"Jade", "Jasmine", "Jewel", "Jezie", "Jilly", "Josie", "Juliette", "Juniper",
			"Kandi", "Karma", "Kelli", "Kimmy", "Katty", "Kitty", "Krissy",
			"Larissa", "Layla", "Liliana", "Lily", "Lizzy", "Lola", "Lolita", "Lolly", "Lottie", "Luanna", "Luella", "Lulu", "Lyla",
			"Maddie", "Maisie", "Mariella", "Marigold", "Merci", "Minnie", "Minx", "Missy", "Muffin", "Muffy",
			"Nellie", "Nicolette", "Nissa",
			"Peaches", "Pippa", "Pippy", "Pixie", "Polly", "Prim", "Princess", "Priss",
			"Queenie", "Quinnie",
			"Richelle", "Rosie", "Roxie",
			"Sadie", "Sassy", "Scarlett", "Seraphina", "Shari", "Shayla", "Sheila", "Silvie", "Sirena", "Susie", "Süsse",
			"Tammi", "Tatiana", "Tessa", "Tiana", "Tiffy", "Tillie", "Tizzy", "Tottie", "Tulip",
			"Viola", "Violeta", "Vixen", "Yazmin", "Ysela",
			"Zelda", "Zoey"
		]
	};/* spell-checker: enable */

	export namespace Lists {
		export const skillSynergy: Data.SkillSynergyData = {
			handJobs: [
				{type: "body", name: "face", bonus: 0.20},
				{type: "core", name: "femininity", bonus: 0.10},
				{type: "skill", name: "courtesan", bonus: 0.10}
			],
			blowJobs: [
				{type: "body", name: "lips", bonus: 0.20},
				{type: "core", name: "femininity", bonus: 0.10},
				{type: "skill", name: "courtesan", bonus: 0.10}
			],
			titFucking: [
				{type: "body", name: "bust", bonus: 0.20},
				{type: "core", name: "femininity", bonus: 0.10},
				{type: "skill", name: "courtesan", bonus: 0.10}
			],
			assFucking: [
				{type: "body", name: "ass", bonus: 0.20},
				{type: "core", name: "femininity", bonus: 0.10},
				{type: "skill", name: "courtesan", bonus: 0.10}
			],
			dancing: [
				{type: "core", name: "femininity", bonus: 0.10},
				{type: "core", name: "fitness", bonus: 0.10}
			],
			swashbuckling: [{type: "core", name: "fitness", bonus: 0.20}],
			seduction: [
				{type: "skill", name: "courtesan", bonus: 0.5},
				{type: "core", name: "femininity", bonus: 0.20}
			],
			serving: [{type: "skill", name: "courtesan", bonus: 0.5}],
			femininity: [{type: "skill", name: "courtesan", bonus: 0.5}],
		};
		export const bodyConfig: Record<BodyStatStr | DerivedBodyStat, Data.BodyStatConfig> = {
			face: {
				min: 0, max: 100, cmMin: 0, cmMax: 0,
				leveling: {
					0: {cost: 100, step: 1, adjective: "hideous"},
					4: {cost: 100, step: 1, adjective: "offensive"},
					8: {cost: 140, step: 1, adjective: "rough"},
					13: {cost: 150, step: 1, adjective: "coarse"},
					18: {cost: 175, step: 1, adjective: "homely"},
					24: {cost: 160, step: 1, adjective: "ordinary"},
					29: {cost: 210, step: 1, adjective: "unrefined"},
					34: {cost: 220, step: 1, adjective: "fair"},
					39: {cost: 240, step: 1, adjective: "nice"},
					44: {cost: 260, step: 1, adjective: "pleasing"},
					49: {cost: 280, step: 1, adjective: "appealing"},
					54: {cost: 300, step: 1, adjective: "delightful"},
					59: {cost: 320, step: 1, adjective: "pretty"},
					63: {cost: 420, step: 1, adjective: "lovely"},
					67: {cost: 440, step: 1, adjective: "alluring"},
					71: {cost: 460, step: 1, adjective: "stunning"},
					75: {cost: 480, step: 1, adjective: "dazzling"},
					79: {cost: 500, step: 1, adjective: "exquisite"},
					83: {cost: 520, step: 1, adjective: "gorgeous"},
					87: {cost: 540, step: 1, adjective: "beautiful"},
					91: {cost: 560, step: 1, adjective: "magnificent"},
					95: {cost: 600, step: 1, adjective: "radiant"},
					99: {cost: 650, step: 1, adjective: "bewitching"},
					100: {cost: 2800, step: 1, adjective: "angelic"}
				}
			},
			lips: {
				min: 0, max: 100, cmMin: 0, cmMax: 0,
				leveling: {
					0: {cost: 100, step: 1, adjective: "withered"},
					5: {cost: 100, step: 1, adjective: "withered"},
					10: {cost: 100, step: 1, adjective: "wilted"},
					15: {cost: 125, step: 1, adjective: "wrinkled"},
					20: {cost: 125, step: 1, adjective: "thin"},
					26: {cost: 200, step: 1, adjective: "narrow"},
					32: {cost: 225, step: 1, adjective: "puckered"},
					38: {cost: 250, step: 1, adjective: "rounded"},
					46: {cost: 300, step: 1, adjective: "plump"},
					54: {cost: 350, step: 1, adjective: "full"},
					62: {cost: 400, step: 1, adjective: "thick"},
					71: {cost: 450, step: 1, adjective: "prominent"},
					80: {cost: 500, step: 1, adjective: "tremulous"},
					89: {cost: 550, step: 1, adjective: "luscious"},
					98: {cost: 600, step: 1, adjective: "cartoonishly huge"},
					100: {cost: 1500, step: 1, adjective: "dick sucking"}
				}
			},
			bust: {
				min: 0, max: 100, cmMin: 80, cmMax: 130,
				levelingCost: (level) => {
					if (level <= 0) return 0;
					const cup = level / 3;
					return Math.floor((9.51 * cup * cup + 39.7 * cup + 190) * 3.23);
				},
				leveling: {
					0: {adjective: "flat"},
					5: {adjective: "swollen"},
					8: {adjective: "perky"},
					11: {adjective: "shapely"},
					15: {adjective: "sizable"},
					19: {adjective: "prominent"},
					24: {adjective: "luscious"},
					29: {adjective: "plentiful"},
					34: {adjective: "generous"},
					39: {adjective: "pendulous"},
					44: {adjective: "substantial"},
					49: {adjective: "voluptuous"},
					54: {adjective: "extensive"},
					59: {adjective: "voluminous"},
					64: {adjective: "gigantic"},
					70: {adjective: "stupendous"},
					76: {adjective: "massive"},
					82: {adjective: "colossal"},
					88: {adjective: "immense"},
					94: {adjective: "enormous"},
					100: {adjective: "mountainous"}
				}
			},
			bustFirmness: {
				min: 0, max: 100, cmMin: -1, cmMax: -1,
				leveling: {
					0: {cost: 500, step: 1, adjective: "saggy"},
					5: {cost: 500, step: 1, adjective: "saggy"},
					10: {cost: 400, step: 1, adjective: "slightly saggy"},
					15: {cost: 125, step: 1, adjective: "mushy and a bit saggy"},
					20: {cost: 125, step: 1, adjective: "squashy"},
					26: {cost: 100, step: 1, adjective: "soft"},
					32: {cost: 125, step: 1, adjective: "mellow"},
					38: {cost: 150, step: 1, adjective: "delicate"},
					46: {cost: 250, step: 1, adjective: "shapely"},
					54: {cost: 300, step: 1, adjective: "firm"},
					62: {cost: 400, step: 1, adjective: "very firm"},
					71: {cost: 500, step: 1, adjective: "exceptionally firm"},
					80: {cost: 1000, step: 1, adjective: "stiff"},
					89: {cost: 2000, step: 1, adjective: "spherical"},
					98: {cost: 4000, step: 1, adjective: "hard"},
					100: {cost: 8000, step: 1, adjective: "rigid"}
				}
			},
			lactation: {
				min: 0, max: 100, cmMin: 0, cmMax: 0,
				leveling: {
					0: {cost: 100, step: 1, adjective: ""},
					5: {cost: 100, step: 1, adjective: ""},
					10: {cost: 100, step: 1, adjective: ""},
					15: {cost: 125, step: 1, adjective: ""},
					20: {cost: 125, step: 1, adjective: ""},
					26: {cost: 200, step: 1, adjective: ""},
					32: {cost: 225, step: 1, adjective: ""},
					38: {cost: 250, step: 1, adjective: ""},
					46: {cost: 300, step: 1, adjective: ""},
					54: {cost: 350, step: 1, adjective: ""},
					62: {cost: 400, step: 1, adjective: ""},
					71: {cost: 450, step: 1, adjective: ""},
					80: {cost: 500, step: 1, adjective: ""},
					89: {cost: 550, step: 1, adjective: ""},
					98: {cost: 600, step: 1, adjective: ""},
					100: {cost: 1500, step: 1, adjective: ""}
				}
			},
			ass: {
				min: 0, max: 100, cmMin: 80, cmMax: 130,
				leveling: {
					0: {cost: 100, step: 1, adjective: "bony"},
					1: {cost: 100, step: 1, adjective: "flat"},
					2: {cost: 100, step: 1, adjective: "skinny"},
					3: {cost: 150, step: 1, adjective: "tiny"},
					5: {cost: 150, step: 1, adjective: "thin"},
					8: {cost: 150, step: 1, adjective: "average"},
					11: {cost: 175, step: 1, adjective: "rounded"},
					15: {cost: 175, step: 1, adjective: "curved"},
					19: {cost: 200, step: 1, adjective: "ample"},
					24: {cost: 200, step: 1, adjective: "thick"},
					29: {cost: 220, step: 1, adjective: "full"},
					34: {cost: 300, step: 1, adjective: "heavy"},
					39: {cost: 325, step: 1, adjective: "plentiful"},
					44: {cost: 350, step: 1, adjective: "sizable"},
					49: {cost: 375, step: 1, adjective: "generous"},
					54: {cost: 400, step: 1, adjective: "substantial"},
					59: {cost: 425, step: 1, adjective: "hefty"},
					64: {cost: 450, step: 1, adjective: "extensive"},
					70: {cost: 450, step: 1, adjective: "voluminous"},
					76: {cost: 475, step: 1, adjective: "gigantic"},
					82: {cost: 500, step: 1, adjective: "stupendous"},
					88: {cost: 525, step: 1, adjective: "massive"},
					94: {cost: 550, step: 1, adjective: "immense"},
					100: {cost: 575, step: 1, adjective: "enormous"}
				}
			},
			assFirmness: {
				min: 0, max: 100, cmMin: -1, cmMax: -1,
				leveling: {
					0: {adjective: "flabby"},
					5: {adjective: "flabby"},
					10: {adjective: "flabby"},
					15: {adjective: "flabby"},
					20: {adjective: "flabby"},
					26: {adjective: "flabby"},
					32: {adjective: "toned"},
					38: {adjective: "toned"},
					46: {adjective: "toned"},
					54: {adjective: "toned"},
					62: {adjective: "toned"},
					71: {adjective: "smooth and round"},
					80: {adjective: "smooth and round"},
					89: {adjective: "smooth and round"},
					98: {adjective: "smooth and round"},
					100: {adjective: "smooth and round"}
				}
			},
			waist: {
				min: 0, max: 100, cmMin: 40, cmMax: 130,
				leveling: {
					0: {cost: 3000, step: 1, adjective: "wasp"},
					5: {cost: 720, step: 1, adjective: "tiny"},
					10: {cost: 550, step: 1, adjective: "small"},
					17: {cost: 375, step: 1, adjective: "narrow"},
					25: {cost: 300, step: 1, adjective: "thin"},
					34: {cost: 240, step: 1, adjective: "slender"},
					44: {cost: 200, step: 1, adjective: "boyish"},
					56: {cost: 100, step: 1, adjective: "masculine"},
					66: {cost: 200, step: 1, adjective: "thickened"},
					75: {cost: 240, step: 1, adjective: "chubby"},
					83: {cost: 300, step: 1, adjective: "bulging"},
					90: {cost: 375, step: 1, adjective: "ample"},
					95: {cost: 550, step: 1, adjective: "hefty"},
					99: {cost: 720, step: 1, adjective: "substantial"},
					100: {cost: 3000, step: 1, adjective: "obese"}
				}
			},
			hips: {
				min: 0, max: 100, cmMin: 80, cmMax: 130,
				leveling: {
					0: {cost: 100, step: 1, adjective: "bony"},
					1: {cost: 100, step: 1, adjective: "boyish"},
					2: {cost: 100, step: 1, adjective: "narrow"},
					3: {cost: 150, step: 1, adjective: "lean"},
					5: {cost: 150, step: 1, adjective: "thin"},
					8: {cost: 150, step: 1, adjective: "slender"},
					11: {cost: 175, step: 1, adjective: "girlish"},
					15: {cost: 175, step: 1, adjective: "curved"},
					19: {cost: 200, step: 1, adjective: "ample"},
					24: {cost: 200, step: 1, adjective: "rounded"},
					29: {cost: 220, step: 1, adjective: "plump"},
					34: {cost: 300, step: 1, adjective: "shapely"},
					39: {cost: 325, step: 1, adjective: "womanly"},
					44: {cost: 350, step: 1, adjective: "broad"},
					49: {cost: 375, step: 1, adjective: "generous"},
					54: {cost: 400, step: 1, adjective: "substantial"},
					59: {cost: 425, step: 1, adjective: "matronly"},
					64: {cost: 450, step: 1, adjective: "extensive"},
					70: {cost: 450, step: 1, adjective: "voluminous"},
					76: {cost: 475, step: 1, adjective: "gigantic"},
					82: {cost: 500, step: 1, adjective: "stupendous"},
					88: {cost: 525, step: 1, adjective: "massive"},
					94: {cost: 550, step: 1, adjective: "immense"},
					100: {cost: 575, step: 1, adjective: "enormous"}
				}
			},
			penis: {
				min: 1, max: 100, cmMin: 1, cmMax: 36,
				leveling: {
					0: {cost: 1400, step: 1, adjective: "micro"},
					5: {cost: 700, step: 1, adjective: "tiny"},
					10: {cost: 550, step: 1, adjective: "shriveled"},
					17: {cost: 375, step: 1, adjective: "shrunken"},
					25: {cost: 300, step: 1, adjective: "diminutive"},
					34: {cost: 240, step: 1, adjective: "small"},
					44: {cost: 200, step: 1, adjective: "normal"},
					56: {cost: 100, step: 1, adjective: "swollen"},
					66: {cost: 200, step: 1, adjective: "thick"},
					75: {cost: 240, step: 1, adjective: "bulging"},
					83: {cost: 300, step: 1, adjective: "huge"},
					90: {cost: 375, step: 1, adjective: "gigantic"},
					95: {cost: 550, step: 1, adjective: "stupendous"},
					99: {cost: 700, step: 1, adjective: "massive"},
					100: {cost: 1400, step: 1, adjective: "enormous"}
				}
			},
			hair: {
				min: 0, max: 100, cmMin: 1, cmMax: 130,
				leveling: {
					0: {cost: 100, step: 1, adjective: ""},
					5: {cost: 100, step: 1, adjective: ""},
					10: {cost: 100, step: 1, adjective: ""},
					15: {cost: 125, step: 1, adjective: ""},
					20: {cost: 125, step: 1, adjective: ""},
					26: {cost: 200, step: 1, adjective: ""},
					32: {cost: 225, step: 1, adjective: ""},
					38: {cost: 250, step: 1, adjective: ""},
					46: {cost: 300, step: 1, adjective: ""},
					54: {cost: 350, step: 1, adjective: ""},
					62: {cost: 400, step: 1, adjective: ""},
					71: {cost: 450, step: 1, adjective: ""},
					80: {cost: 500, step: 1, adjective: ""},
					89: {cost: 550, step: 1, adjective: ""},
					98: {cost: 600, step: 1, adjective: ""},
					100: {cost: 1500, step: 1, adjective: ""}
				}
			},
			height: {
				min: 0, max: 100, cmMin: 145, cmMax: 200,
				leveling: {
					0: {cost: 3000, step: 1, adjective: ""},
					5: {cost: 720, step: 1, adjective: ""},
					10: {cost: 550, step: 1, adjective: ""},
					17: {cost: 375, step: 1, adjective: ""},
					25: {cost: 300, step: 1, adjective: ""},
					34: {cost: 240, step: 1, adjective: ""},
					44: {cost: 200, step: 1, adjective: ""},
					56: {cost: 100, step: 1, adjective: ""},
					66: {cost: 200, step: 1, adjective: ""},
					75: {cost: 240, step: 1, adjective: ""},
					83: {cost: 300, step: 1, adjective: ""},
					90: {cost: 375, step: 1, adjective: ""},
					95: {cost: 550, step: 1, adjective: ""},
					99: {cost: 720, step: 1, adjective: ""},
					100: {cost: 3000, step: 1, adjective: ""}
				}
			},
			balls: {
				min: 0, max: 100, cmMin: 1, cmMax: 20,
				leveling: {
					0: {cost: 3000, step: 1, adjective: "micro"},
					5: {cost: 720, step: 1, adjective: "tiny"},
					10: {cost: 550, step: 1, adjective: "shriveled"},
					17: {cost: 375, step: 1, adjective: "shrunken"},
					25: {cost: 300, step: 1, adjective: "diminutive"},
					34: {cost: 240, step: 1, adjective: "small"},
					44: {cost: 200, step: 1, adjective: "normal"},
					56: {cost: 100, step: 1, adjective: "swollen"},
					66: {cost: 200, step: 1, adjective: "thick"},
					75: {cost: 240, step: 1, adjective: "bulging"},
					83: {cost: 300, step: 1, adjective: "huge"},
					90: {cost: 375, step: 1, adjective: "gigantic"},
					95: {cost: 550, step: 1, adjective: "stupendous"},
					99: {cost: 720, step: 1, adjective: "massive"},
					100: {cost: 3000, step: 1, adjective: "enormous"}
				}
			}
		};
		export const skillConfig: Record<Skills.Any, Data.SkillStatConfig> = {
			handJobs: {
				min: 0, max: 100, altName: "Hand Jobs",
				leveling: {
					5: {cost: 100, step: 1, adjective: ""},
					10: {cost: 100, step: 1, adjective: ""},
					15: {cost: 125, step: 1, adjective: ""},
					20: {cost: 200, step: 1, adjective: ""},
					26: {cost: 200, step: 1, adjective: ""},
					32: {cost: 250, step: 1, adjective: ""},
					38: {cost: 250, step: 1, adjective: ""},
					46: {cost: 350, step: 1, adjective: ""},
					54: {cost: 450, step: 1, adjective: ""},
					62: {cost: 450, step: 1, adjective: ""},
					71: {cost: 500, step: 1, adjective: ""},
					80: {cost: 500, step: 1, adjective: ""},
					89: {cost: 600, step: 1, adjective: ""},
					98: {cost: 600, step: 1, adjective: ""},
					100: {cost: 1500, step: 1, adjective: ""}
				}
			},
			titFucking: {
				min: 0, max: 100, altName: "Tit Fucking",
				leveling: {
					5: {cost: 100, step: 1, adjective: ""},
					10: {cost: 100, step: 1, adjective: ""},
					15: {cost: 125, step: 1, adjective: ""},
					20: {cost: 200, step: 1, adjective: ""},
					26: {cost: 200, step: 1, adjective: ""},
					32: {cost: 250, step: 1, adjective: ""},
					38: {cost: 250, step: 1, adjective: ""},
					46: {cost: 350, step: 1, adjective: ""},
					54: {cost: 450, step: 1, adjective: ""},
					62: {cost: 450, step: 1, adjective: ""},
					71: {cost: 500, step: 1, adjective: ""},
					80: {cost: 500, step: 1, adjective: ""},
					89: {cost: 600, step: 1, adjective: ""},
					98: {cost: 600, step: 1, adjective: ""},
					100: {cost: 1500, step: 1, adjective: ""}
				}
			},
			blowJobs: {
				min: 0, max: 100, altName: "Blow Jobs",
				leveling: {
					5: {cost: 100, step: 1, adjective: "dick licker"},
					10: {cost: 100, step: 1, adjective: "dick licker"},
					15: {cost: 125, step: 1, adjective: "dick sucker"},
					20: {cost: 200, step: 1, adjective: "cock sucker"},
					26: {cost: 200, step: 1, adjective: "cock sucker"},
					32: {cost: 250, step: 1, adjective: "cock gobbler"},
					38: {cost: 250, step: 1, adjective: "cock gobbler"},
					46: {cost: 350, step: 1, adjective: "sperm swallower"},
					54: {cost: 450, step: 1, adjective: "sperm swallower"},
					62: {cost: 450, step: 1, adjective: "jizz drinker"},
					71: {cost: 500, step: 1, adjective: "jizz drinker"},
					80: {cost: 500, step: 1, adjective: "jizz drinker"},
					89: {cost: 600, step: 1, adjective: "cum gulper"},
					98: {cost: 600, step: 1, adjective: "cum gulper"},
					100: {cost: 1500, step: 1, adjective: "cock sucking queen"}
				}
			},
			assFucking: {
				min: 0, max: 100, altName: "Ass Fucking",
				leveling: {
					5: {cost: 100, step: 1, adjective: ""},
					10: {cost: 100, step: 1, adjective: ""},
					15: {cost: 125, step: 1, adjective: ""},
					20: {cost: 200, step: 1, adjective: ""},
					26: {cost: 200, step: 1, adjective: ""},
					32: {cost: 250, step: 1, adjective: ""},
					38: {cost: 250, step: 1, adjective: ""},
					46: {cost: 350, step: 1, adjective: ""},
					54: {cost: 450, step: 1, adjective: ""},
					62: {cost: 450, step: 1, adjective: ""},
					71: {cost: 500, step: 1, adjective: ""},
					80: {cost: 500, step: 1, adjective: ""},
					89: {cost: 600, step: 1, adjective: ""},
					98: {cost: 600, step: 1, adjective: ""},
					100: {cost: 1500, step: 1, adjective: ""}
				}
			},
			dancing: {
				min: 0, max: 100,
				leveling: {
					5: {cost: 50, step: 1, adjective: ""},
					10: {cost: 50, step: 1, adjective: ""},
					15: {cost: 62, step: 1, adjective: ""},
					20: {cost: 62, step: 1, adjective: ""},
					26: {cost: 100, step: 1, adjective: ""},
					32: {cost: 112, step: 1, adjective: ""},
					38: {cost: 125, step: 1, adjective: ""},
					46: {cost: 150, step: 1, adjective: ""},
					54: {cost: 175, step: 1, adjective: ""},
					62: {cost: 200, step: 1, adjective: ""},
					71: {cost: 225, step: 1, adjective: ""},
					80: {cost: 250, step: 1, adjective: ""},
					89: {cost: 275, step: 1, adjective: ""},
					98: {cost: 300, step: 1, adjective: ""},
					100: {cost: 750, step: 1, adjective: ""}
				}
			},
			singing: {
				min: 0, max: 100,
				leveling: {
					5: {cost: 50, step: 1, adjective: ""},
					10: {cost: 50, step: 1, adjective: ""},
					15: {cost: 62, step: 1, adjective: ""},
					20: {cost: 62, step: 1, adjective: ""},
					26: {cost: 100, step: 1, adjective: ""},
					32: {cost: 112, step: 1, adjective: ""},
					38: {cost: 125, step: 1, adjective: ""},
					46: {cost: 150, step: 1, adjective: ""},
					54: {cost: 175, step: 1, adjective: ""},
					62: {cost: 200, step: 1, adjective: ""},
					71: {cost: 225, step: 1, adjective: ""},
					80: {cost: 250, step: 1, adjective: ""},
					89: {cost: 275, step: 1, adjective: ""},
					98: {cost: 300, step: 1, adjective: ""},
					100: {cost: 750, step: 1, adjective: ""}
				}
			},
			seduction: {
				min: 0, max: 100,
				leveling: {
					5: {cost: 50, step: 1, adjective: ""},
					10: {cost: 50, step: 1, adjective: ""},
					15: {cost: 62, step: 1, adjective: ""},
					20: {cost: 62, step: 1, adjective: ""},
					26: {cost: 100, step: 1, adjective: ""},
					32: {cost: 112, step: 1, adjective: ""},
					38: {cost: 125, step: 1, adjective: ""},
					46: {cost: 150, step: 1, adjective: ""},
					54: {cost: 175, step: 1, adjective: ""},
					62: {cost: 200, step: 1, adjective: ""},
					71: {cost: 225, step: 1, adjective: ""},
					80: {cost: 250, step: 1, adjective: ""},
					89: {cost: 275, step: 1, adjective: ""},
					98: {cost: 300, step: 1, adjective: ""},
					100: {cost: 750, step: 1, adjective: ""}
				}
			},
			courtesan: {
				min: 0, max: 100,
				leveling: {
					5: {cost: 50, step: 1, adjective: ""},
					10: {cost: 50, step: 1, adjective: ""},
					15: {cost: 62, step: 1, adjective: ""},
					20: {cost: 62, step: 1, adjective: ""},
					26: {cost: 100, step: 1, adjective: ""},
					32: {cost: 112, step: 1, adjective: ""},
					38: {cost: 125, step: 1, adjective: ""},
					46: {cost: 150, step: 1, adjective: ""},
					54: {cost: 175, step: 1, adjective: ""},
					62: {cost: 200, step: 1, adjective: ""},
					71: {cost: 225, step: 1, adjective: ""},
					80: {cost: 250, step: 1, adjective: ""},
					89: {cost: 275, step: 1, adjective: ""},
					98: {cost: 300, step: 1, adjective: ""},
					100: {cost: 750, step: 1, adjective: ""}
				}
			},
			cooking: {
				min: 0, max: 100,
				leveling: {
					5: {cost: 50, step: 1, adjective: ""},
					10: {cost: 50, step: 1, adjective: ""},
					15: {cost: 62, step: 1, adjective: ""},
					20: {cost: 62, step: 1, adjective: ""},
					26: {cost: 100, step: 1, adjective: ""},
					32: {cost: 112, step: 1, adjective: ""},
					38: {cost: 125, step: 1, adjective: ""},
					46: {cost: 150, step: 1, adjective: ""},
					54: {cost: 175, step: 1, adjective: ""},
					62: {cost: 200, step: 1, adjective: ""},
					71: {cost: 225, step: 1, adjective: ""},
					80: {cost: 250, step: 1, adjective: ""},
					89: {cost: 275, step: 1, adjective: ""},
					98: {cost: 300, step: 1, adjective: ""},
					100: {cost: 750, step: 1, adjective: ""}
				}
			},
			cleaning: {
				min: 0, max: 100,
				leveling: {
					5: {cost: 50, step: 1, adjective: ""},
					10: {cost: 50, step: 1, adjective: ""},
					15: {cost: 62, step: 1, adjective: ""},
					20: {cost: 62, step: 1, adjective: ""},
					26: {cost: 100, step: 1, adjective: ""},
					32: {cost: 112, step: 1, adjective: ""},
					38: {cost: 125, step: 1, adjective: ""},
					46: {cost: 150, step: 1, adjective: ""},
					54: {cost: 175, step: 1, adjective: ""},
					62: {cost: 200, step: 1, adjective: ""},
					71: {cost: 225, step: 1, adjective: ""},
					80: {cost: 250, step: 1, adjective: ""},
					89: {cost: 275, step: 1, adjective: ""},
					98: {cost: 300, step: 1, adjective: ""},
					100: {cost: 750, step: 1, adjective: ""}
				}
			},
			serving: {
				min: 0, max: 100,
				leveling: {
					5: {cost: 50, step: 1, adjective: ""},
					10: {cost: 50, step: 1, adjective: ""},
					15: {cost: 62, step: 1, adjective: ""},
					20: {cost: 62, step: 1, adjective: ""},
					26: {cost: 100, step: 1, adjective: ""},
					32: {cost: 112, step: 1, adjective: ""},
					38: {cost: 125, step: 1, adjective: ""},
					46: {cost: 150, step: 1, adjective: ""},
					54: {cost: 175, step: 1, adjective: ""},
					62: {cost: 200, step: 1, adjective: ""},
					71: {cost: 225, step: 1, adjective: ""},
					80: {cost: 250, step: 1, adjective: ""},
					89: {cost: 275, step: 1, adjective: ""},
					98: {cost: 300, step: 1, adjective: ""},
					100: {cost: 750, step: 1, adjective: ""}
				}
			},
			swashbuckling: {
				min: 0, max: 100,
				leveling: {
					5: {cost: 50, step: 1, adjective: ""},
					10: {cost: 50, step: 1, adjective: ""},
					15: {cost: 62, step: 1, adjective: ""},
					20: {cost: 62, step: 1, adjective: ""},
					26: {cost: 100, step: 1, adjective: ""},
					32: {cost: 112, step: 1, adjective: ""},
					38: {cost: 125, step: 1, adjective: ""},
					46: {cost: 150, step: 1, adjective: ""},
					54: {cost: 175, step: 1, adjective: ""},
					62: {cost: 200, step: 1, adjective: ""},
					71: {cost: 225, step: 1, adjective: ""},
					80: {cost: 250, step: 1, adjective: ""},
					89: {cost: 275, step: 1, adjective: ""},
					98: {cost: 300, step: 1, adjective: ""},
					100: {cost: 750, step: 1, adjective: ""}
				}
			},
			sailing: {
				min: 0, max: 100,
				leveling: {
					5: {cost: 50, step: 1, adjective: ""},
					10: {cost: 50, step: 1, adjective: ""},
					15: {cost: 62, step: 1, adjective: ""},
					20: {cost: 62, step: 1, adjective: ""},
					26: {cost: 100, step: 1, adjective: ""},
					32: {cost: 112, step: 1, adjective: ""},
					38: {cost: 125, step: 1, adjective: ""},
					46: {cost: 150, step: 1, adjective: ""},
					54: {cost: 175, step: 1, adjective: ""},
					62: {cost: 200, step: 1, adjective: ""},
					71: {cost: 225, step: 1, adjective: ""},
					80: {cost: 250, step: 1, adjective: ""},
					89: {cost: 275, step: 1, adjective: ""},
					98: {cost: 300, step: 1, adjective: ""},
					100: {cost: 750, step: 1, adjective: ""}
				}
			},
			navigating: {
				min: 0, max: 100,
				leveling: {
					5: {cost: 50, step: 1, adjective: ""},
					10: {cost: 50, step: 1, adjective: ""},
					15: {cost: 62, step: 1, adjective: ""},
					20: {cost: 62, step: 1, adjective: ""},
					26: {cost: 100, step: 1, adjective: ""},
					32: {cost: 112, step: 1, adjective: ""},
					38: {cost: 125, step: 1, adjective: ""},
					46: {cost: 150, step: 1, adjective: ""},
					54: {cost: 175, step: 1, adjective: ""},
					62: {cost: 200, step: 1, adjective: ""},
					71: {cost: 225, step: 1, adjective: ""},
					80: {cost: 250, step: 1, adjective: ""},
					89: {cost: 275, step: 1, adjective: ""},
					98: {cost: 300, step: 1, adjective: ""},
					100: {cost: 750, step: 1, adjective: ""}
				}
			},
			styling: {
				min: 0, max: 100,
				leveling: {
					5: {cost: 50, step: 1, adjective: ""},
					10: {cost: 50, step: 1, adjective: ""},
					15: {cost: 62, step: 1, adjective: ""},
					20: {cost: 62, step: 1, adjective: ""},
					26: {cost: 100, step: 1, adjective: ""},
					32: {cost: 112, step: 1, adjective: ""},
					38: {cost: 125, step: 1, adjective: ""},
					46: {cost: 150, step: 1, adjective: ""},
					54: {cost: 175, step: 1, adjective: ""},
					62: {cost: 200, step: 1, adjective: ""},
					71: {cost: 225, step: 1, adjective: ""},
					80: {cost: 250, step: 1, adjective: ""},
					89: {cost: 275, step: 1, adjective: ""},
					98: {cost: 300, step: 1, adjective: ""},
					100: {cost: 750, step: 1, adjective: ""}
				}
			},
			boobJitsu: {
				min: 0, max: 100, altName: "Boob-Jitsu",
				leveling: {
					5: {cost: 50, step: 1, adjective: ""},
					10: {cost: 50, step: 1, adjective: ""},
					15: {cost: 62, step: 1, adjective: ""},
					20: {cost: 62, step: 1, adjective: ""},
					26: {cost: 100, step: 1, adjective: ""},
					32: {cost: 112, step: 1, adjective: ""},
					38: {cost: 125, step: 1, adjective: ""},
					46: {cost: 150, step: 1, adjective: ""},
					54: {cost: 175, step: 1, adjective: ""},
					62: {cost: 200, step: 1, adjective: ""},
					71: {cost: 225, step: 1, adjective: ""},
					80: {cost: 250, step: 1, adjective: ""},
					89: {cost: 275, step: 1, adjective: ""},
					98: {cost: 300, step: 1, adjective: ""},
					100: {cost: 750, step: 1, adjective: ""}
				}
			},
			assFu: {
				min: 0, max: 100, altName: "Ass-Fu",
				leveling: {
					5: {cost: 50, step: 1, adjective: ""},
					10: {cost: 50, step: 1, adjective: ""},
					15: {cost: 62, step: 1, adjective: ""},
					20: {cost: 62, step: 1, adjective: ""},
					26: {cost: 100, step: 1, adjective: ""},
					32: {cost: 112, step: 1, adjective: ""},
					38: {cost: 125, step: 1, adjective: ""},
					46: {cost: 150, step: 1, adjective: ""},
					54: {cost: 175, step: 1, adjective: ""},
					62: {cost: 200, step: 1, adjective: ""},
					71: {cost: 225, step: 1, adjective: ""},
					80: {cost: 250, step: 1, adjective: ""},
					89: {cost: 275, step: 1, adjective: ""},
					98: {cost: 300, step: 1, adjective: ""},
					100: {cost: 750, step: 1, adjective: ""}
				}
			},
		};
		export const coreConfig: Record<CoreStatStr, Data.CoreStatConfig> = {
			health: {
				min: 0, max: 100,
				leveling: {fixed: {cost: 100, step: 1}}
			},
			nutrition: {
				min: 0, max: 100,
				leveling: {fixed: {cost: 100, step: 10}}
			},
			willpower: {
				min: 0, max: 100,
				leveling: {fixed: {cost: 100, step: 1}}
			},
			perversion: {
				min: 0, max: 100,
				leveling: {
					5: {cost: 100, step: 1, adjective: ""},
					10: {cost: 100, step: 1, adjective: ""},
					15: {cost: 125, step: 1, adjective: ""},
					20: {cost: 125, step: 1, adjective: ""},
					26: {cost: 200, step: 1, adjective: ""},
					32: {cost: 225, step: 1, adjective: ""},
					38: {cost: 250, step: 1, adjective: ""},
					46: {cost: 300, step: 1, adjective: ""},
					54: {cost: 350, step: 1, adjective: ""},
					62: {cost: 400, step: 1, adjective: ""},
					71: {cost: 450, step: 1, adjective: ""},
					80: {cost: 500, step: 1, adjective: ""},
					89: {cost: 550, step: 1, adjective: ""},
					98: {cost: 600, step: 1, adjective: ""},
					100: {cost: 1500, step: 1, adjective: ""}
				}
			},
			femininity: {
				min: 0, max: 100,
				leveling: {
					5: {cost: 100, step: 1, adjective: ""},
					10: {cost: 100, step: 1, adjective: ""},
					15: {cost: 125, step: 1, adjective: ""},
					20: {cost: 125, step: 1, adjective: ""},
					26: {cost: 200, step: 1, adjective: ""},
					32: {cost: 225, step: 1, adjective: ""},
					38: {cost: 250, step: 1, adjective: ""},
					46: {cost: 300, step: 1, adjective: ""},
					54: {cost: 350, step: 1, adjective: ""},
					62: {cost: 400, step: 1, adjective: ""},
					71: {cost: 450, step: 1, adjective: ""},
					80: {cost: 500, step: 1, adjective: ""},
					89: {cost: 550, step: 1, adjective: ""},
					98: {cost: 600, step: 1, adjective: ""},
					100: {cost: 1500, step: 1, adjective: ""}
				}
			},
			fitness: {
				min: 0, max: 100,
				leveling: {
					0: {cost: 100, step: 1, adjective: "unhealthy"},
					5: {cost: 100, step: 1, adjective: "unhealthy"},
					10: {cost: 100, step: 1, adjective: "weak"},
					15: {cost: 125, step: 1, adjective: "flabby"},
					20: {cost: 125, step: 1, adjective: "lethargic"},
					26: {cost: 200, step: 1, adjective: "inactive"},
					32: {cost: 225, step: 1, adjective: "lazy"},
					38: {cost: 250, step: 1, adjective: "normal"},
					46: {cost: 300, step: 1, adjective: "active"},
					54: {cost: 350, step: 1, adjective: "trim"},
					62: {cost: 400, step: 1, adjective: "fit"},
					71: {cost: 450, step: 1, adjective: "athletic"},
					80: {cost: 500, step: 1, adjective: "toned"},
					89: {cost: 550, step: 1, adjective: "robust"},
					98: {cost: 600, step: 1, adjective: "energetic"},
					100: {cost: 1500, step: 1, adjective: "vigorous"}
				}
			},
			toxicity: {
				min: 0, max: 300,
				leveling: {fixed: {cost: 100, step: 1}}
			},
			hormones: {
				min: 0, max: 200,
				leveling: {
					0: {cost: 190, step: 1, adjective: "macho"},
					2: {cost: 180, step: 1, adjective: "manly"},
					3: {cost: 170, step: 1, adjective: "manly"},
					6: {cost: 160, step: 1, adjective: "manly"},
					10: {cost: 150, step: 1, adjective: "masculine"},
					15: {cost: 140, step: 1, adjective: "masculine"},
					22: {cost: 130, step: 1, adjective: "masculine"},
					30: {cost: 120, step: 1, adjective: "boyish"},
					38: {cost: 110, step: 1, adjective: "boyish"},
					47: {cost: 100, step: 1, adjective: "boyish"},
					56: {cost: 90, step: 1, adjective: "weak"},
					67: {cost: 80, step: 1, adjective: "weak"},
					78: {cost: 70, step: 1, adjective: "androgynous"},
					89: {cost: 60, step: 1, adjective: "androgynous"},
					100: {cost: 50, step: 1, adjective: "androgynous"},
					111: {cost: 50, step: 1, adjective: "androgynous"},
					122: {cost: 60, step: 1, adjective: "androgynous"},
					133: {cost: 70, step: 1, adjective: "androgynous"},
					144: {cost: 80, step: 1, adjective: "effeminate"},
					153: {cost: 90, step: 1, adjective: "effeminate"},
					162: {cost: 100, step: 1, adjective: "girlish"},
					170: {cost: 110, step: 1, adjective: "girlish"},
					178: {cost: 120, step: 1, adjective: "girlish"},
					185: {cost: 130, step: 1, adjective: "feminine"},
					190: {cost: 140, step: 1, adjective: "feminine"},
					194: {cost: 150, step: 1, adjective: "feminine"},
					197: {cost: 160, step: 1, adjective: "womanly"},
					198: {cost: 170, step: 1, adjective: "womanly"},
					199: {cost: 180, step: 1, adjective: "womanly"},
					200: {cost: 190, step: 1, adjective: "ladylike"}
				}
			},
			futa: {
				min: 0, max: 100,
				leveling: {
					0: {cost: 100, step: 1, adjective: "non-existent"},
					5: {cost: 100, step: 1, adjective: "shy"},
					10: {cost: 100, step: 1, adjective: "timid"},
					15: {cost: 125, step: 1, adjective: "timid"},
					20: {cost: 125, step: 1, adjective: "diffident"},
					30: {cost: 200, step: 1, adjective: "diffident"},
					32: {cost: 225, step: 1, adjective: "open"},
					38: {cost: 250, step: 1, adjective: "open"},
					46: {cost: 300, step: 1, adjective: "outright"},
					54: {cost: 350, step: 1, adjective: "outright"},
					62: {cost: 400, step: 1, adjective: "daring"},
					71: {cost: 450, step: 1, adjective: "daring"},
					80: {cost: 500, step: 1, adjective: "daring"},
					89: {cost: 550, step: 1, adjective: "perfect"},
					98: {cost: 600, step: 1, adjective: "perfect"},
					100: {cost: 1500, step: 1, adjective: "perfect"}
				}
			},
			energy: {
				min: 0, max: 10,
				leveling: {none: {cost: 0, step: 0}}
			}
		};
		export const makeupStyles: Data.MakeupStylesDesc[] = [ // RES1 = BASIC MAKEUP, RES2 = EXPENSIVE MAKEUP
			{name: "plain", difficulty: 0, resource1: 0, resource2: 0, style: 0, short: "plain faced"},
			{name: "clean", difficulty: 0, resource1: 0, resource2: 0, style: 5, short: "carefully cleaned and washed face"},
			{name: "minimal", difficulty: 20, resource1: 1, resource2: 0, style: 20, short: "minimal blush and lipstick"},
			{name: "basic", difficulty: 30, resource1: 1, resource2: 0, style: 25, short: "basic lipstick and eyeshadow"},
			{name: "youthful", difficulty: 40, resource1: 2, resource2: 0, style: 30, short: "bright and youthful looking"},
			{name: "cheap", difficulty: 50, resource1: 3, resource2: 0, style: 35, short: "cheap, slutty looking"},
			{name: "sultry", difficulty: 50, resource1: 1, resource2: 1, style: 40, short: "sultry and slightly exotic"},
			{name: "whorish", difficulty: 60, resource1: 2, resource2: 1, style: 45, short: "overdone and whorish"},
			{name: "gothic", difficulty: 70, resource1: 0, resource2: 1, style: 50, short: "gothic with smokey eyes and dark lips"},
			{name: "heavy", difficulty: 80, resource1: 1, resource2: 1, style: 55, short: "heavy and mysterious with dark features"},
			{name: "classic", difficulty: 90, resource1: 0, resource2: 2, style: 60, short: "full face, classical style"}
		];
		export const hairStyles: Data.HairStyleDesc[] = [ // RES1 = ACCESSORIES, RES2 = PRODUCT
			{name: "boy cut", difficulty: 20, resource1: 0, resource2: 0, min: 5, max: 10, style: 20, short: "a spunky boy cut"},
			{name: "loose cut", difficulty: 30, resource1: 0, resource2: 0, min: 10, max: 20, style: 25, short: "a loose and flippy style"},
			{name: "bob cut", difficulty: 40, resource1: 0, resource2: 1, min: 12, max: 18, style: 30, short: "a short bob style"},
			{name: "breezy style", difficulty: 40, resource1: 0, resource2: 0, min: 18, max: 50, style: 20, short: "a breezy style"},
			{name: "loose curls", difficulty: 50, resource1: 1, resource2: 1, min: 20, max: 50, style: 35, short: "a loose cut with flowing curls"},
			{name: "pig tails", difficulty: 50, resource1: 1, resource2: 0, min: 25, max: 50, style: 40, short: "cute pig tails"},
			{name: "pony tail", difficulty: 60, resource1: 1, resource2: 1, min: 32, max: 100, style: 45, short: "a long pony tail"},
			{name: "slick up-do", difficulty: 70, resource1: 2, resource2: 1, min: 5, max: 20, style: 50, short: "a slick up-do style"},
			{name: "braided", difficulty: 80, resource1: 0, resource2: 0, min: 40, max: 100, style: 35, short: "extravagantly long braids"},
			{name: "twin tails", difficulty: 90, resource1: 1, resource2: 1, min: 50, max: 80, style: 75, short: "extremely long twin tails"}
		];
		export const hairColors = {
			black: {h: 0, s: 0, l: 0},
			blond: {h: 60, s: 100, l: 80},
			brown: {h: 40, s: 0, l: 40},
			red: {h: 0, s: 100, l: 30},
			lavender: {h: 240, s: 67, l: 94}
		};
		export const bodyChanges: Partial<Record<BodyStatStr | CoreStatStr, {grow: string, shrink: string}>> = {
			willpower: {
				grow: "You feel @@.state-positive;&uArr;renewed resolve@@ to persevere. Your Willpower has increased.",
				shrink: "Your feel your @@.state-negative;&dArr;willpower dwindling@@ in the face of all this abuse. Will you make it out alive and sane?"
			},
			hormones: {
				grow: "Your body feels strange… as if you're becoming @@.state-feminity;&uArr;more feminine and girly@@. Maybe there's something in the food?",
				shrink: "Your body feels more normal… as if you're becoming more @@.state-masculinity;&dArr;more masculine and manly@@. Perhaps the drugs are wearing off?"
			},
			fitness: {
				grow: "You feel @@.state-positive;&uArr;stronger and more fit@@. Your overall fitness has increased.",
				shrink: "Your feel  @@.state-negative;&dArr;lazy and lethargic@@ from lack of proper exercise. Your fitness has decreased."
			},
			perversion: {
				grow: "All of the debauchery you have engaged in has made you @@.state-sexiness;&uArr;more perverted and debased@@. Is this a good thing?",
				shrink: "In a flash of insight, you seem to grasp upon some of your previous morals. You have become @@.state-masculinity;&dArr;less perverted@@. This is good, right?"
			},
			femininity: {
				grow: "The endless hours spent acting like a woman has subtly @@.state-feminity;&uArr;made you more feminine@@. You will act more like a woman now.",
				shrink: "In a flash of insight, you recall your previous persona and @@.state-masculinity;&dArr;become more masculine@@. You will act less like a woman now."
			},
			face: {
				grow: "Your @@.state-positive;face@@ feels warm and flush, @@.state-positive;&uArr;softer and more feminine@@.",
				shrink: "Your @@.state-negative;face@@ feels hot and sweaty, @@.state-negative;&dArr;harsher and more masculine@@."
			},
			lips: {
				grow: "Your @@.state-positive;lips@@ feel strange, like they've been @@.state-positive;&uArr;growing@@ slowly over night.",
				shrink: "Your @@.state-negative;lips@@ feel tight, like they're becoming @@.state-negative;&dArr;smaller and more withdrawn@@."
			},
			bust: {
				grow: "Your chest feels warm and tender, like your @@.state-positive;&uArr;breasts are growing@@.",
				shrink: "Your chest feels lighter, almost as if your @@.state-negative;&dArr;breasts are shrinking@@."
			},
			bustFirmness: {
				grow: "Your chest feels more resilient and round, like your @@.state-positive;&uArr;breasts are firming up@@.",
				shrink: "Your chest feels lighter and flatter, almost as if your @@.state-negative;&dArr;breasts are sagging@@."
			},
			lactation: {
				grow: "Your breasts feel warm and tough, like your @@.state-positive;&uArr;breasts are filling up@@.",
				shrink: "Your breasts feel lighter and shallower, almost as if your @@.state-negative;&dArr;breasts are empting@@."
			},
			ass: {
				grow: "Your @@.state-positive;&uArr;butt feels larger@@ and more prominent, maybe you should go on a diet?",
				shrink: "You notice that your @@.state-negative;&dArr;rump feels less prominent@@, as if it's been shrinking"
			},
			waist: {
				grow: "Your @@.state-negative;&dArr;waist feels thick and heavy@@, as if you've been putting on the pounds around your middle.",
				shrink: "Your @@.state-positive;&uArr;waist and stomach feel tighter@@, as if you are shedding mass around your middle"
			},
			hips: {
				grow: "Your @@.state-positive;hips@@ feel as if they've become @@.state-positive;&uArr;more broad and feminine@@ over night.",
				shrink: "Your @@.state-negative;hips@@ feel as if they've become @@.state-negative;&dArr;more narrow and masculine@@ over night."
			},
			penis: {
				grow: "It might be your imagination, but did your @@.state-positive;&uArr;cock seem to grow@@ recently?",
				shrink: "Something is going on here… your @@.state-negative;&dArr;penis has shrunk@@!"
			},
			hair: {
				grow: "You brush an errant strand of @@.state-positive;hair@@ out of your eyes. It's @@.state-positive;&uArr;getting longer@@ it seems.",
				shrink: "You brush your hand through your @@.state-negative;hair@@ and @@.state-negative;&dArr;several large clumps fall out@@. Are you going bald?"
			},
			height: {
				grow: "It seems almost impossible to believe at this age, but did you @@.state-positive;&uArr;get taller over night@@?",
				shrink: "Did the room around you grow, or did you somehow @@.state-negative;&dArr;get shorter over night@@?"
			},
			balls: {
				grow: "You feel a heavy weight between your legs. Is it possible that @@.state-positive;&uArr;your balls grew@@?",
				shrink: "Something feels strange down there… are @@.state-negative;&dArr;your balls smaller@@?"
			}
		};
		// The route the ship takes in the game.
		export const shipRoute = [
			// Isla Harbor Port
			{P: "IslaHarbor", left: 300, top: 60}, {P: "IslaHarbor", left: 300, top: 60}, {P: "IslaHarbor", left: 300, top: 60}, {P: "IslaHarbor", left: 300, top: 60},
			// At Sea
			{P: "AtSea", left: 348, top: 63}, {P: "AtSea", left: 394, top: 63}, {P: "AtSea", left: 441, top: 70}, {P: "AtSea", left: 488, top: 65},
			{P: "AtSea", left: 536, top: 60}, {P: "AtSea", left: 583, top: 60}, {P: "AtSea", left: 630, top: 63},
			// Golden Isle Port
			{P: "GoldenIsle", left: 630, top: 100}, {P: "GoldenIsle", left: 630, top: 100}, {P: "GoldenIsle", left: 630, top: 100}, {P: "GoldenIsle", left: 630, top: 100},
			// At Sea
			{P: "AtSea", left: 586, top: 105}, {P: "AtSea", left: 550, top: 113}, {P: "AtSea", left: 505, top: 123}, {P: "AtSea", left: 463, top: 133},
			{P: "AtSea", left: 420, top: 140}, {P: "AtSea", left: 380, top: 146}, {P: "AtSea", left: 338, top: 155}, {P: "AtSea", left: 290, top: 160},
			{P: "AtSea", left: 248, top: 168}, {P: "AtSea", left: 195, top: 190},
			// Abamond
			{P: "Abamond", left: 192, top: 238}, {P: "Abamond", left: 192, top: 238}, {P: "Abamond", left: 192, top: 238}, {P: "Abamond", left: 192, top: 238},
			// At Sea
			{P: "AtSea", left: 130, top: 235}, {P: "AtSea", left: 100, top: 270}, {P: "AtSea", left: 135, top: 315}, {P: "AtSea", left: 185, top: 315},
			{P: "AtSea", left: 235, top: 328}, {P: "AtSea", left: 280, top: 330}, {P: "AtSea", left: 320, top: 328}, {P: "AtSea", left: 370, top: 315},
			// Port Royale
			{P: "PortRoyale", left: 425, top: 300}, {P: "PortRoyale", left: 425, top: 300}, {P: "PortRoyale", left: 425, top: 300}, {P: "PortRoyale", left: 425, top: 300},
			// At Sea
			{P: "AtSea", left: 395, top: 285}, {P: "AtSea", left: 383, top: 255}, {P: "AtSea", left: 376, top: 225}, {P: "AtSea", left: 368, top: 190},
			{P: "AtSea", left: 365, top: 130}, {P: "AtSea", left: 345, top: 90}
		];
	}

	export const ratings = {
		lust: {
			0: "Content", 20: "Content", 40: "Satisfied", 60: "Horny",
			80: "Lustful", 99: "Aggressive", 100: "Overpowering"
		},
		mood: {
			0: "Angry", 20: "Angry", 40: "Annoyed", 60: "Satisfied",
			80: "Happy", 99: "Cheerful", 100: "Ecstatic!"
		},
		fetish: {
			0: "completely non-existent",
			10: "of minor notice",
			20: "definitely noticeable",
			30: "worthy of attention",
			40: "eye catching and interesting",
			50: "erotic and appealing",
			60: "highly sexualized",
			70: "sexually outrageous",
			80: "perverse and fetishized",
			90: "oozing sex and perversion",
			99: "bordering on depraved",
			100: "utterly depraved and perverse, the ultimate fuck toy"
		},
		beauty: {
			0: "masculine and ugly",
			10: "mannish and unappealing",
			20: "a perpetual wallflower",
			30: "slightly cute",
			40: "fairly attractive",
			50: "very cute and appealing",
			60: "pretty and sexy",
			70: "very sexy",
			80: "beautiful",
			90: "very beautiful",
			99: "exotic and alluring",
			100: "utterly bewitching"
		},
		balls: {
			0: "ADJECTIVE testes, so small they have shrunk into your abdomen",
			1: "ADJECTIVE testes, so small they have shrunk into your abdomen",
			5: "ADJECTIVE sissy balls",
			10: "ADJECTIVE pitiful testicles",
			17: "ADJECTIVE pitiful testicles",
			25: "ADJECTIVE balls, like a child",
			34: "ADJECTIVE balls, like a child",
			44: "ADJECTIVE sized testicles",
			56: "ADJECTIVE balls, they look painful",
			66: "ADJECTIVE balls, loaded with cum",
			75: "ADJECTIVE testicles, full of virility",
			83: "ADJECTIVE balls that would put a bull to shame",
			90: "balls so ADJECTIVE that it's difficult for you to move without swaying your hips",
			95: "ADJECTIVE balls full of cum, like those on a prize breeding bull",
			99: "ADJECTIVE balls that hang precipitously between your legs like over sized fruit",
			100: "ADJECTIVE balls that signify you are one of natures true freaks. They are so bloated with cum that even the slightest touch if painful"
		},
		penis: {
			0: "a ADJECTIVE penis, a perfect sissy clit",
			1: "a ADJECTIVE penis, a perfect sissy clit",
			5: "a ADJECTIVE penis, a perfect sissy clit",
			10: "a LENGTH ADJECTIVE dick or over sized sissy clit",
			17: "a LENGTH ADJECTIVE dick or over sized sissy clit",
			25: "a ADJECTIVE LENGTH dick",
			34: "a ADJECTIVE LENGTH dick",
			44: "a ADJECTIVE LENGTH dick",
			56: "a ADJECTIVE LENGTH tool",
			66: "a ADJECTIVE LENGTH cock",
			75: "a ADJECTIVE LENGTH dick",
			83: "a ADJECTIVE LENGTH cock, it dangles prominently between your legs",
			90: "a ADJECTIVE LENGTH cock, about the size of a small farm animal",
			95: "a ADJECTIVE LENGTH_C cock, it's constantly getting in the way and peeking out from under your skirts",
			99: "a ADJECTIVE LENGTH_C cock, so large it's inconvenient in day to day life and impossible to conceal",
			100: "a ADJECTIVE LENGTH_C tool, so big it's impossible to hide and serves as an advertisement for your perverse body"
		},
		waist: {
			0: "You have a LENGTH ADJECTIVE waist, freakishly small like a doll or small child",
			1: "You have a LENGTH ADJECTIVE waist, freakishly small like a doll or small child",
			5: "You have a LENGTH ADJECTIVE waist, freakishly small like a doll or small child",
			10: "You have a ADJECTIVE LENGTH waist, incredibly small for your size",
			17: "You have a ADJECTIVE LENGTH waist, incredibly small for your size",
			25: "You have a ADJECTIVE LENGTH waist",
			34: "You have a ADJECTIVE LENGTH waist",
			44: "You have a ADJECTIVE LENGTH waist",
			56: "You have a ADJECTIVE LENGTH waist",
			66: "You have a ADJECTIVE LENGTH waist, it looks like you've been putting on weight",
			75: "You have a ADJECTIVE LENGTH waist, you may need to go on a diet soon",
			83: "You have a ADJECTIVE LENGTH waist, it's floppy and protrudes in all the wrong places",
			90: "You have a ADJECTIVE LENGTH waist, there's no doubt about it, since you're not pregnant, you're fat",
			95: "You have a ADJECTIVE LENGTH waist, your fat belly rolls and jiggles when you walk",
			99: "You have a ADJECTIVE LENGTH waist, it looks like a sack of jello and wobbles as you walk",
			100: "You have a ADJECTIVE LENGTH waist, your corpulence is magnificent to behold"
		},
		hips: {
			0: "You have ADJECTIVE LENGTH hips that make you look emaciated.",
			1: "You have ADJECTIVE LENGTH hips that look more fitting on a child or teenager.",
			2: "You have ADJECTIVE LENGTH hips, too small for your own health.",
			3: "You have ADJECTIVE LENGTH hips. Maybe you should eat more?",
			5: "You have ADJECTIVE LENGTH hips.",
			8: "You have ADJECTIVE LENGTH hips.",
			11: "You have ADJECTIVE LENGTH hips, they have a slight curve which is almost attractive.",
			15: "You have ADJECTIVE LENGTH hips, they give your derriere a slight emphasis.",
			19: "You have ADJECTIVE LENGTH hips, they roll very slightly when you walk.",
			24: "You have ADJECTIVE LENGTH hips, they are slightly attractive to look at.",
			29: "You have ADJECTIVE LENGTH hips, they flare to the sides attractively.",
			34: "You have ADJECTIVE LENGTH hips, they flare to the sides attractively.",
			39: "You have ADJECTIVE LENGTH hips, they give you a feminine figure.",
			44: "You have ADJECTIVE LENGTH hips, perfect for grabbing a hold of when being fucked.",
			49: "You have ADJECTIVE LENGTH hips, perfect for grabbing a hold of when being fucked.",
			54: "You have ADJECTIVE LENGTH hips, perfect for grabbing a hold of when being fucked.",
			59: "You have ADJECTIVE LENGTH hips. They roll and undulate when you walk, shaking your NOUN_Ass.",
			64: "You have ADJECTIVE LENGTH hips. They roll and undulate when you walk, shaking your NOUN_Ass.",
			70: "You have ADJECTIVE LENGTH hips. They roll and undulate when you walk, shaking your NOUN_Ass.",
			76: "You have ADJECTIVE LENGTH hips. They sway back and forth, advertising your pASS NOUN_Ass for a good fuck.",
			82: "You have ADJECTIVE LENGTH hips. They sway back and forth, advertising your pASS NOUN_Ass for a good fuck.",
			88: "You have ADJECTIVE LENGTH hips. They sway back and forth, advertising your pASS NOUN_Ass for a good fuck.",
			94: "You have ADJECTIVE LENGTH hips. They sway back and forth constantly drawing attention to your pASS NOUN_Ass and practically begging for a good butt fuck.",
			100: "You have ADJECTIVE LENGTH hips. They sway back and forth constantly drawing attention to your pASS NOUN_Ass and practically begging for a good butt fuck."
		},
		ass: {
			0: "Your ADJECTIVE NOUN looks emaciated and unhealthy.",
			1: "You have a ADJECTIVE NOUN, it looks painful to sit on.",
			2: "You have a ADJECTIVE NOUN, it looks painful to sit on.",
			3: "Your ADJECTIVE NOUN looks almost hollow and unattractive.",
			5: "Your ADJECTIVE NOUN looks almost hollow and unattractive.",
			8: "You have a ADJECTIVE sized NOUN, neither too big or too small.",
			11: "You have a ADJECTIVE NOUN, it has a slight swell but nothing special.",
			15: "You have a pert ADJECTIVE NOUN that doesn't move in the slightest.",
			19: "You have an ADJECTIVE NOUN that does a good job of filling out a pair of panties.",
			24: "You have a ADJECTIVE NOUN that stretches sexily against your clothes.",
			29: "You have a ADJECTIVE NOUN that stretches sexily against your clothes.",
			34: "You have a ADJECTIVE NOUN, it's attractive enough to draw stares from men.",
			39: "You have a ADJECTIVE NOUN, it's attractive enough to draw stares from men.",
			44: "You have a ADJECTIVE NOUN that strains your bottoms and shakes when men slap it.",
			49: "You have a ADJECTIVE NOUN that strains your bottoms and shakes when men slap it.",
			54: "You have a ADJECTIVE NOUN that shakes and draws attention when you walk.",
			59: "You have a ADJECTIVE NOUN that shakes and draws attention when you walk.",
			64: "You have an ADJECTIVE NOUN that sways and jiggles sexily when you walk.",
			70: "You have a ADJECTIVE NOUN that gyrates back and forth when you strut your stuff.",
			76: "Your ADJECTIVE NOUN sensuously wobbles and jiggles at every minor movement, entrancing watchers.",
			82: "Your ADJECTIVE NOUN sensuously wobbles and jiggles at every minor movement, entrancing watchers.",
			88: "Your ADJECTIVE NOUN shakes and wobbles at the most minor movement of your nHIPS. Clothing can barely contain it and men lust after it.",
			94: "Your ADJECTIVE NOUN shakes and wobbles at the most minor movement of your nHIPS. Clothing can barely contain it and men lust after it.",
			100: "Your ADJECTIVE NOUN jiggles almost uncontrollably at the most minor prompting. A small slap sends off an 'butt quake' of jiggly NOUN flesh and men dream of fucking it."
		},
		bust: {
			0: "Your NOUN is ADJECTIVE and unappealing.",
			1: "Your NOUN is ADJECTIVE and unappealing, it would barely fit an pCUP bra.",
			2: "You have ADJECTIVE ADJECTIVE_BustFirmness NOUN that would fill out a pCUP bra.",
			3: "You have ADJECTIVE ADJECTIVE_BustFirmness NOUN that would fill out a pCUP bra.",
			5: "You have ADJECTIVE ADJECTIVE_BustFirmness NOUN that would fill out a pCUP bra, they sit high and firm.",
			8: "You have ADJECTIVE ADJECTIVE_BustFirmness NOUN that would nicely fill out a pCUP bra.",
			11: "You have ADJECTIVE ADJECTIVE_BustFirmness pCUP sized NOUN, they slightly wobble and jiggle when you walk.",
			15: "You have ADJECTIVE ADJECTIVE_BustFirmness pCUP sized NOUN, they are fleshy, softy, jiggly and eye catching.",
			19: "You have ADJECTIVE ADJECTIVE_BustFirmness pCUP sized NOUN, they enticingly fill out your tops and jiggle sexily when you walk.",
			24: "You have ADJECTIVE ADJECTIVE_BustFirmness pCUP sized NOUN, they enticingly fill out your tops and shimmy and quake when you walk.",
			29: "You have ADJECTIVE ADJECTIVE_BustFirmness pCUP sized NOUN, your overwhelming cleavage attracts lustful stares from men and they shake and wobble when you strut your stuff.",
			34: "You have ADJECTIVE ADJECTIVE_BustFirmness pCUP sized NOUN, your overwhelming cleavage attracts lustful stares from men and they jiggle and bounce when you strut your stuff.",
			39: "You have ADJECTIVE ADJECTIVE_BustFirmness pCUP sized NOUN, they wobble when you walk and your expansive cleavage attracts lustful stares from men.",
			44: "You have ADJECTIVE ADJECTIVE_BustFirmness pCUP sized NOUN, they wobble when you walk and your expansive cleavage attracts lustful stares from men.",
			49: "You have ADJECTIVE ADJECTIVE_BustFirmness pCUP sized NOUN, they wobble when you walk and your expansive cleavage attracts lustful stares from men.",
			54: "You have ADJECTIVE ADJECTIVE_BustFirmness pCUP sized NOUN, they jiggle and shake constantly trying to escape your top.",
			59: "You have ADJECTIVE ADJECTIVE_BustFirmness pCUP sized NOUN, they jiggle and shake constantly trying to escape your top.",
			64: "You have ADJECTIVE ADJECTIVE_BustFirmness pCUP sized NOUN, they spill out of your clothes and entice men to grope and grab them.",
			70: "You have ADJECTIVE ADJECTIVE_BustFirmness pCUP sized NOUN, they spill out of your clothes and entice men to grope and grab them.",
			76: "You have ADJECTIVE ADJECTIVE_BustFirmness pCUP sized NOUN on your chest, they make every outfit you wear look obscene and men salivate at the thought of putting their cocks in between them.",
			82: "You have ADJECTIVE ADJECTIVE_BustFirmness pCUP sized NOUN on your chest, they make every outfit you wear look obscene and men salivate at the thought of putting their cocks in between them.",
			88: "You have ADJECTIVE ADJECTIVE_BustFirmness pCUP sized NOUN on your chest, you mostly have given up trying to cover them <<if vBUST_FIRMNESS < 50>>because they'll wobble and shake free of anything at the slightest provocation<</if>>.",
			94: "Your ADJECTIVE ADJECTIVE_BustFirmness pCUP sized NOUN make it known to everyone that you're a total fuck slut ready for sex. <<if vBUST_FIRMNESS < 50>>They shake and jiggle at the slightest motion and spill out of your top every couple of minutes.<</if>>",
			100: "Your ADJECTIVE ADJECTIVE_BustFirmness pCUP sized NOUN make it known to everyone that you're a total fuck slut ready for sex. <<if vBUST_FIRMNESS < 50>>They shake and jiggle at the slightest motion and spill out of your top every couple of minutes.<</if>> When you eat, you place them on the table to avoid back strain and you have to sleep on your side or you'll suffocate."
		},
		bustFirmness: {
			0: "Your NOUN_Bust sag down as two <<if vBUST < 35>>empty<<else>>heavy<</if>> sacks.",
			10: "Your NOUN_Bust sag down as two <<if vBUST < 35>>empty<<else>>heavy<</if>> sacks.",
			15: "Your <<if vBUST > 45>>heavy<</if>> NOUN_Bust sag towards your navel.",
			20: "Your NOUN_Bust are extremely soft and a bit saggy.",
			26: "Your ADJECTIVE_Bust breasts are very soft to touch and pleasurable to squeeze.",
			/* 32 : "Your mellow pBUST breast ", */
			38: "Your NOUN_Bust are of pleasant shape, well defined by their weight and size.",
			46: "Your NOUN_Bust maintain almost ideal tear-drop shape.",
			54: "Your NOUN_Bust sit firm on your chest.",
			62: "Your NOUN_Bust are very firm, they can be squeezed and still feel full.",
			71: "Your NOUN_Bust are unnaturally firm, do not jiggle but move a little as a whole.",
			80: "Your ADJECTIVE_Bust NOUN_Bust are nearly <<if vBUST < 50>>hemi<</if>>spherical in shape.",
			89: "Your ADJECTIVE_Bust NOUN_Bust are completely <<if vBUST < 50>>hemi<</if>>spherical in shape.",
			98: "Your NOUN_Bust are so firm that their <<if vBUST < 50>>hemi<</if>>spherical shape can be deformed only by squeezing them as hard as you can.",
			100: "Your ADJECTIVE_Bust NOUN_Bust are unnaturally rigid, they always maintain their ideal shape. It is impossible to squeeze them even by a tiny bit.",
		},
		lactation: {
			0: "",
			1: "A few drops of milk can be squeezed out of your nipples.",
			3: "Your NOUN_Bust can produce a weak stream of milk if squeezed hard enough.",
			5: "Your puffy nipples leak a drop or two of milk sometimes, creating occasional wet spots on your dress.",
			10: "Your nipples can easily sprinkle milk when squeezed.",
			20: "You breast steadily produce milk that fills up your NOUN_Bust.",
			30: "Your NOUN_Bust usually itches because it is full of milk.",
			40: "Your NOUN_Bust is well accustomed to producing milk, steadily producing more than a few mouthfuls of it.",
			50: "Your NOUN_Bust can produce a cup of milk per day.",
			60: "Your NOUN_Bust are very productive, you could easily feed a baby.",
			70: "You spend some few hours daily milking your ADJECTIVE_Bust NOUN_Bust, producing a few cups of milk.",
			80: "You may benefit from a milkmaid services to milk your super productive ADJECTIVE_Bust NOUN_Bust.",
			90: "You must be made of a milky substance somehow, because no matter how much milk you drain out of your ADJECTIVE_Bust NOUN_Bust, they never feel empty.",
			100: "Never ending streams from your ADJECTIVE_Bust milky NOUN_Bust provides enough milk to feed the whole ship crew.",
		},
		lips: {
			0: "ADJECTIVE NOUN like an old person or a zombie.",
			5: "ADJECTIVE NOUN like an old person or a zombie.",
			10: "ADJECTIVE NOUN that look unappealing and harsh.",
			15: "ADJECTIVE NOUN that look dry and parched.",
			20: "ADJECTIVE, masculine NOUN that are rather unappealing.",
			26: "ADJECTIVE, slightly masculine NOUN that are slightly chapped.",
			32: "ADJECTIVE and moist, slightly feminine NOUN.",
			38: "ADJECTIVE, almost girlish NOUN.",
			46: "ADJECTIVE, girly NOUN with a cute pout.",
			54: "ADJECTIVE, womanly NOUN with an enticing smile.",
			62: "ADJECTIVE, kissable NOUN in a classic bow shape.",
			71: "ADJECTIVE NOUN like those on an air-headed bimbo.",
			80: "ADJECTIVE NOUN, almost comical in appearance. They look good wrapped around a cock.",
			89: "ADJECTIVE NOUN that are so fat your mouth is in a perpetual 'o' shape, just waiting for a dick.",
			98: "ADJECTIVE NOUN, they are always partway open and wet with your saliva. They make your face look like a   <<if vFACE < 50>>sissy <</if>>fuckhole.",
			100: "ADJECTIVE NOUN, so enormous that they look like a pair of pillows made for sucking cock. They cause you to speak with a pronounced lisp and they're always so wet with your saliva that you often find yourself constantly wiping your own drool from your pBUST tits."
		},
		style: {
			0: "repulsive, like a homeless person",
			10: "repulsive, like a homeless person",
			20: "frumpy and unattractive",
			30: "plain and uninspired",
			40: "barely noticeable with minor sex appeal",
			50: "passably fashionable, attractive enough",
			60: "equal to a courtesan or trophy wife",
			70: "on point and attractive, stylish and sexually appealing",
			80: "sexually provocative and beautiful",
			90: "eye catching, alluring and sexually intriguing",
			99: "provocative and bewitching, sexually stimulating",
			100: "the height of sexual allure and beauty"
		},
		clothing: {
			0: "barely better than rags.",
			10: "barely better than rags.",
			20: "horrible looking",
			30: "completely out of fashion",
			40: "unattractive and ordinary",
			50: "reasonably attractive and stylish",
			60: "stylish and cute",
			70: "stylish and sexy",
			80: "eye catching and flirty",
			90: "seductive and appealing",
			99: "emphasizing your sexual appeal",
			100: "overflowing with sex appeal"
		},
		face: {
			0: "ADJECTIVE pHORMONES looking face",
			4: "ADJECTIVE pHORMONES looking face",
			8: "ADJECTIVE pHORMONES looking face",
			13: "ADJECTIVE pHORMONES looking face",
			18: "ADJECTIVE pHORMONES looking face",
			24: "ADJECTIVE pHORMONES looking face",
			29: "ADJECTIVE pHORMONES looking face",
			34: "ADJECTIVE pHORMONES looking face",
			39: "ADJECTIVE pHORMONES looking face",
			44: "ADJECTIVE pHORMONES looking face",
			49: "ADJECTIVE pHORMONES looking face",
			54: "ADJECTIVE pHORMONES looking face",
			59: "ADJECTIVE pHORMONES looking face",
			63: "ADJECTIVE pHORMONES looking face",
			67: "ADJECTIVE pHORMONES looking face",
			71: "ADJECTIVE pHORMONES looking face",
			75: "ADJECTIVE pHORMONES looking face",
			79: "ADJECTIVE pHORMONES looking face",
			83: "ADJECTIVE pHORMONES looking face",
			87: "ADJECTIVE pHORMONES looking face",
			91: "ADJECTIVE pHORMONES looking face",
			95: "ADJECTIVE pHORMONES looking face",
			99: "ADJECTIVE pHORMONES looking face",
			100: "ADJECTIVE pHORMONES looking face"
		},
		fitness: {
			0: "extremely ADJECTIVE, almost sickly and ill.",
			5: "extremely ADJECTIVE, almost sickly and ill.",
			10: "extremely ADJECTIVE, almost sickly and ill.",
			15: "extremely ADJECTIVE, almost sickly and ill.",
			20: "ADJECTIVE, with weak muscles and a poor constitution.",
			26: "ADJECTIVE, with weak muscles and a poor constitution.",
			32: "relatively ADJECTIVE.",
			38: "ADJECTIVE level of fitness, capable of handling a moderate amount of physical activity.",
			46: "ADJECTIVE, with some muscle definition and a fair amount of endurance.",
			54: "ADJECTIVE, with some muscle definition and a fair amount of endurance.",
			62: "ADJECTIVE, with a fit body and a constitution of an intermediate athlete.",
			71: "ADJECTIVE, with well defined muscles and stamina to match.",
			80: "ADJECTIVE, with a fit and sexy body.",
			89: "ADJECTIVE, with a body close to physical perfection.",
			98: "ADJECTIVE, a virtual dynamo with a knockout body and constitution to match.",
			100: "ADJECTIVE, with an iron constitution and powerful pHORMONES muscles, the apex of physical perfection."
		}
	};

	export const naming: NamingConfig = {
		bodyConfig: {
			face: {
				adjective: {rating: ["body/face"]},
				noun: "face"
			},
			hair: {
				adjective: {rating: ["body/hair"]},
				noun: "hair"
			},
			lips: {
				adjective: {rating: ["body/lips"]},
				noun: {
					leveling: {
						0: "lips",
						100: "cock pillows"
					}
				}
			},
			bust: {
				adjective: {
					applicableLevel: [{min: 0, max: 100}, {min: 3, max: 100}],
					rating: ["body/bust", "body/bustFirmness"],
				},
				noun: {
					index: ["body/bust", "body/bustFirmness"],
					leveling: {
						0: "chest",
						3: "tits",
						11: "hooters",
						15: "jugs",
						19: "funbags",
						24: "tits",
						29: "jugs",
						34: "boobies",
						39: "knockers",
						44: "jugs",
						49: "hooters",
						54: "tits",
						64: "hooters",
						70: "funbags",
						76: "tits",
						88: "fuck bags",
						94: "tits",
						100: {
							0: "udders",
							10: "udders",
							20: "torpedoes",
							40: "tits",
							60: "melons",
							80: "globes",
							100: "spheres"
						}
					}
				}
			},
			ass: {
				adjective: {
					rating: ["body/ass", "body/assFirmness"],
					index: ["body/ass", "core/fitness"],
					applicableLevel: [{min: 0, max: 100}, {min: 10, max: 100}],
				},
				noun: {
					index: ["body/ass", "core/fitness"],
					leveling: {
						0: "ass",
						20: "bum",
						30: "bottom",
						40: "tail",
						50: "posterior",
						60: "backside",
						70: "rump",
						80: "keister",
						90: "derrière",
						100: "derrière",
					}
				}
			},
			hips: {
				adjective: {rating: ["body/hips"]},
				noun: "hips"
			},
			waist: {
				adjective: {rating: ["body/waist"]},
				noun: "waist"
			},
			penis: {
				adjective: {rating: ["body/penis"]},
				noun: {
					leveling: {
						0: "sissy clit",
						10: "over sized sissy clit",
						25: "dick",
						45: "cock",
						66: "dong",
						70: "schlong",
						100: "tool"
					}
				},
			},
			balls: {
				adjective: {rating: ["body/balls"]},
				noun: {
					leveling: {
						0: "testes",
						5: "balls",
						100: "balls"
					}
				},
			}
		}
	};

	export namespace Fashion {
		export enum Style {
			SexyDancer = "Sexy Dancer",
			GothicLolita = "Gothic Lolita",
			SissyLolita = "Sissy Lolita",
			DaddyGirl = "Daddy's Girl",
			Bimbo = "Bimbo",
			PirateSlut = "Pirate Slut",
			PetGirl = "Pet Girl",
			Bdsm = "BDSM",
			NaughtyNun = "Naughty Nun",
			HighClassWhore = "High Class Whore",
			SluttyLady = "Slutty Lady",
			Ordinary = "Ordinary"
		}
		export type StyleId = keyof typeof Style;
	}

	export const game = {
		version: {
			majour: 0,
			minor: 2,
			patch: 0,
			special: "",
			/** Version of the save data format, integer */
			save: 1
		}
	};
}
