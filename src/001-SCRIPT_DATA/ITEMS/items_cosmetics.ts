namespace App.Data {
	Object.append(App.Data.cosmetics, {
		"hair accessories": {
			name: "hair accessories",
			shortDesc: "hair accessories",
			longDesc: "A collection of cheap brushes, combs, ribbons and other accessories for styling hair. Usually comes in packages of 5.",
			type: CosmeticsType.HairTool,
			charges: 5,
			skillBonus: {styling: [5, 0, 0]},
			value: 20
		},
		"hair products": {
			name: "hair products",
			shortDesc: "haircare supplies",
			longDesc: "Various substances for styling and maintaining hair. Usually comes in packages of 5.",
			type: CosmeticsType.HairTreatment,
			charges: 5,
			skillBonus: {styling: [10, 0, 0]},
			value: 30
		},
		"basic makeup": {
			name: "basic makeup",
			shortDesc: "basic makeup",
			longDesc: "A collection of cheap makeup. Basic pigments and foundation with lipstick. Usually comes in packages of 5.",
			type: CosmeticsType.BasicMakeup,
			charges: 5,
			skillBonus: {styling: [10, 0, 0]},
			value: 20
		},
		"expensive makeup": {
			name: "expensive makeup",
			shortDesc: "expensive makeup",
			longDesc: "A collection of nice makeup. It contains attractive pigments, eyeshadow and lipstick. Usually comes in packages of 5.",
			type: CosmeticsType.ExpensiveMakeup,
			charges: 5,
			skillBonus: {styling: [20, 0, 0]},
			value: 40
		}
	});
}
