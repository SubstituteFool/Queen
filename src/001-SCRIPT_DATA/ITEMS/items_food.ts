namespace App.Data {
	Object.append(App.Data.food, {
		/** ALCOHOL */
		grog: {
			name: "grog",
			shortDesc: "Low Quality Grog",
			longDesc: "It's the typical alcoholic drink that pirates and mariners drink on long trips. It'll keep you hydrated and won't make you too sick.",
			message: "You tip back the crude swill. It has an unpleasant taste.",
			type: ItemTypeConsumable.Food,
			effects: ["LIGHT_ALCOHOL"]
		},
		beer: {
			name: "beer",
			shortDesc: "Common Beer",
			longDesc: "The typical beverage of choice for most men and some women. It's refreshingly cool and with a malty aftertaste.",
			message: "You tip back the beer and have a drink. It's refreshing.",
			type: ItemTypeConsumable.Food,
			effects: ["LIGHT_ALCOHOL"]
		},
		"smugglers ale": {
			name: "smugglers ale",
			shortDesc: "A pint of Smuggler's Ale",
			longDesc: "Only brewed and sold on Golden Isle, this dark ale has a tangy, almost fruit after taste.",
			message: "You tip back the ale and have a drink. It's refreshing.",
			type: ItemTypeConsumable.Food,
			effects: ["LIGHT_ALCOHOL", "ENERGY_COMMON"]
		},
		"cheap wine": {
			name: "cheap wine",
			shortDesc: "A bottle of cheap wine",
			longDesc: "Not the best quality wine, but palatable and in abundant supply.",
			message: "The wine has a deep red color and an almost sweet after taste.",
			type: ItemTypeConsumable.Food,
			effects: ["LIGHT_ALCOHOL"]
		},
		"expensive wine": {
			name: "expensive wine",
			shortDesc: "A bottle of Lady Oliver's Reserve (1423)",
			longDesc: "This rather expensive beverage became well known several years ago both due to the quantity it was made in as well as for it's popularity with women. It's rare to see a bottle these days, and even rarer to not see it in some Noblewoman's cup.",
			message: "The wine is sweeter than treacle with a strong after taste reminiscent of cherries. It has more kick than you would imagine for such a sweet drink.",
			type: ItemTypeConsumable.Food,
			effects: ["LIGHT_ALCOHOL", "FEMININITY_XP_RARE", "PERVERSION_XP_RARE", "BREAK_WILL_XP_COMMON"]
		},
		rum: {
			name: "rum",
			shortDesc: "A bottle of Captain Morganna's Spiced Rum",
			longDesc: "This cheap rum is mass produced in these islands by a purported 'Captain Morganna'. You've never heard of such a woman except in connection with her ubiquitous liquor.",
			message: "You tip back the bottle and have a drink. It burns going down and fills your stomach with fire.",
			type: ItemTypeConsumable.Food,
			effects: ["HARD_ALCOHOL"]
		},
		"ol musky": {
			name: "ol musky",
			shortDesc: "A bottle of Ol' Musky (unknown vintage)",
			longDesc: "A world renown whisky, although for what reasons you can't tell. One thing's for sure, a bottle or two will definitely loosen your tongue.",
			message: "You tip back the bottle and have a drink. It burns going down and fills your stomach with fire and your mind with delusions of grandeur",
			type: ItemTypeConsumable.Food,
			effects: ["HARD_ALCOHOL", "SEDUCTION_XP_COMMON"]
		},
		"fortified wine": {
			name: "fortified wine",
			shortDesc: "@@.item-quest;a dusty bottle of fortified wine@@",
			longDesc: "This dusty old bottle has an almost illegible label, but you can make out that it appears to be extremely fortified (highly alcoholic) wine",
			message: "You tip back the bottle and have a drink. It burns going down and fills your stomach with fire. Holy shit that's strong!",
			type: ItemTypeConsumable.Food,
			inMarket: false,
			quest: true,
			effects: ["HARD_ALCOHOL"],
			priceCoefficient: 6
		},
		cider: {
			name: "cider",
			shortDesc: "Cider",
			longDesc: "A low-potency cider fermented from pressed apples.",
			message: "The cider goes down with a pleasantly sour taste and leaves you with a mild warming sensation.",
			type: ItemTypeConsumable.Food,
			effects: ["LIGHT_ALCOHOL"]
		},
		// PIRATEY??? DRINKS - Courtesy of anonymous
		arrack: {
			name: "arrack",
			shortDesc: "Arrack",
			longDesc: "A milky-colored coconut-derived beverage cut with cream and sugarcane, with moderate alcoholic qualities.",
			message: "The creamy-white beverage goes down smoothly with a milky, sweet flavor and a little bit of kick.",
			type: ItemTypeConsumable.Food,
			effects: ["LIGHT_ALCOHOL"]
		},
		flip: {
			name: "flip",
			shortDesc: "Flip",
			longDesc: "An egg-based drink beaten with sugar, mixed with rum and spiced with nutmeg, heated with a hot iron and served in a small glass.",
			message: "Smooth, creamy and sweet with a generous amount of froth, you savor the drink and feel pleasantly warmed by it.",
			type: ItemTypeConsumable.Food,
			effects: ["LIGHT_ALCOHOL"]
		},
		bumbo: {
			name: "bumbo",
			shortDesc: "Bumbo",
			longDesc: "Grog's better cousin, bumbo is a spicier, higher-quality mixture of rum, lime, brown sugar, cinnamon and nutmeg.",
			message: "You down the dark liquor and feel its smooth-yet-firey kick immediately. The flavor of sweet spice lingers nicely on your tongue.",
			type: ItemTypeConsumable.Food,
			effects: ["HARD_ALCOHOL"]
		},
		"cordial waters": {
			name: "cordial waters",
			shortDesc: "Cordial Waters",
			longDesc: "A distilled drink said to fortify one's spirit, ward off disease and settle the stomach. It's rumored to be an aphrodisiac.",
			message: "You drink the spicy beverage and feel a gentle embering in your chest. You feel a little more vigorous (and a little looser).",
			type: ItemTypeConsumable.Food,
			effects: ["LIGHT_ALCOHOL", "ENERGY_UNCOMMON"]
		},
		orgeat: {
			name: "orgeat",
			shortDesc: "Orgeat",
			longDesc: "A distilled drink similar to rum, but made from almonds, sugar and orange blossom.",
			message: "You toss back the firey spirit, tasting its nutty, woody flavor. It lights up well going down.",
			type: ItemTypeConsumable.Food,
			effects: ["HARD_ALCOHOL"]
		},
		"buttered toddy": {
			name: "buttered toddy",
			shortDesc: "Buttered Toddy",
			longDesc: "A hot drink of rum, generous amounts of butter, boiling water, honey and lemon juice.",
			message: "The tasty, soothing dessert drink fills you up with warmth and a delicious, contented feeling.",
			type: ItemTypeConsumable.Food,
			effects: ["LIGHT_ALCOHOL", "NUTRITION_COMMON", "NUTRITION_XP_COMMON"]
		},
		"whip syllabub": {
			name: "whip syllabub",
			shortDesc: "Whip Syllabub",
			longDesc: "Derived from fortified white wine, this sugary dessert drink adds a lemony twist and heavy cream.",
			message: "You enjoy a frothy serving of heavily-sweetened wine, cream and lemon in a tasty gulp, leaving hints of citrus and nutmeg.",
			type: ItemTypeConsumable.Food,
			effects: ["LIGHT_ALCOHOL", "NUTRITION_COMMON", "NUTRITION_XP_COMMON"]
		},
		"birch wine": {
			name: "birch wine",
			shortDesc: "Birch Wine",
			longDesc: "A honey-colored liqueur wine fermented from birch sap, sweetened and flavorfully spiced with cinnamon and finely chopped raisin.",
			message: "Your tongue is pleased by the syrupy overtones of this delicious spiced wine, which leaves you with a toasty warmth.",
			type: ItemTypeConsumable.Food,
			effects: ["LIGHT_ALCOHOL"]
		},
		/** NORMAL FOOD **/
		"meat and beans": {
			name: "meat and beans",
			shortDesc: "A piece of meat with some beans in a bowl",
			longDesc: "It actually smells really nice and probably fills you up quite well.",
			message: "The meat is overcooked and the beans are hard but it tastes really good compared to what you have been eating so far.",
			type: ItemTypeConsumable.Food,
			effects: ["NUTRITION_RARE", "NUTRITION_XP_RARE"]
		},
		"mystery stew": {
			name: "mystery stew",
			shortDesc: "A deep brown stew of unidentifiable origin",
			longDesc: "This stew is so dark as to practically be mud. It has bits of unidentifiable meat and vegetables (you think?) floating in it.",
			message: "The stew tastes surprisingly better than you thought and certainly better than it looks.",
			type: ItemTypeConsumable.Food,
			effects: ["NUTRITION_UNCOMMON", "NUTRITION_XP_UNCOMMON"]
		},
		"bread crust": {
			name: "bread crust",
			shortDesc: "A stale crust of bread",
			longDesc: "This piece of bread has seen better days… about 5 days ago. It's still edible however.",
			message: "You bite into the bread. It's as hard as a rock, but eventually you manage to chew and swallow it.",
			type: ItemTypeConsumable.Food,
			effects: ["SNACK"]
		},
		/** WHOLESOME FOOD */
		"bread and cheese": {
			name: "bread and cheese",
			shortDesc: "A serving of bread with cheese",
			longDesc: "Standard and relatively wholesome food.",
			message: "The bread has a very yeasty taste and the cheese a sharp bite.",
			type: ItemTypeConsumable.Food,
			effects: ["LIGHT_WHOLESOME_MEAL"]
		},
		"meat pie": {
			name: "meat pie",
			shortDesc: "A beef and cheese meat pie",
			longDesc: "Popular hand-held food commonly sold at pubs or on the side of the street. The meat is of dubious origin, but delicious nonetheless..",
			message: "The meat pie is warm and fatty on you platte while the crust is slightly hard. An errant trail of gravy drips down your lip and you wipe it away.",
			type: ItemTypeConsumable.Food,
			effects: ["LIGHT_WHOLESOME_MEAL"]
		},
		"roast fish": {
			name: "roast fish",
			shortDesc: "A roasted fish",
			longDesc: "A local type of fish, roasted on a stick.",
			message: "You take a bite of the fish and chew. It's bland, but wholesome.",
			type: ItemTypeConsumable.Food,
			effects: ["WHOLESOME_MEAL"]
		},
		"apple tart": {
			name: "apple tart",
			shortDesc: "A fresh apple tart",
			longDesc: "A pastry made of baked apples and dusted with powdered sugar",
			message: "The sweetness of the apples overpowers the sugary outside of this pastry. It's delicious and fills you with energy.",
			type: ItemTypeConsumable.Food,
			effects: ["WHOLESOME_MEAL", "ENERGY_COMMON"]
		},
		hardtack: {
			name: "hardtack",
			shortDesc: "A piece of hardtack",
			longDesc: "A well preserved biscuit. Plain, but nutritious and filling.",
			message: "The dryness of the hardtack makes it difficult to consume without first soaking it in water. While boring to eat, ultimately you feel yourself refreshed.",
			type: ItemTypeConsumable.Food,
			effects: ["WHOLESOME_MEAL", "ENERGY_COMMON"]
		},
		/** MAGICAL FOOD */
		"slave gruel": {
			name: "slave gruel",
			shortDesc: "Slave gruel mixed with semen",
			longDesc: "\
		This watery looking gruel smells and tastes disgusting. The crew has 'donated' some extra nutrition to it.\
		",
			message: "\
		You wrinkle your nose and drink down the disgusting drool. You gag occasionally as thick clumps of rancid \
		cum slide down your throat.\
		",
			type: ItemTypeConsumable.Food,
			effects: [
				"UNWHOLESOME_MEAL",
				"SLAVE_GRUEL"
			]
		},
		"red plum": {
			name: "red plum",
			shortDesc: "A dazzlingly bright red plum",
			longDesc: "This unusual plum is bright red and cool to the touch. It's so ripe that it practically oozes sweetness.",
			message: "You bite into the plum and instantly you are overcome with a wave of euphoria. You momentarily lose yourself as the sweet juices drip down your chin.",
			type: ItemTypeConsumable.Food,
			effects: ["PURGE_RARE", "HEAL_UNCOMMON", "NUTRITION_COMMON", "NUTRITION_XP_COMMON"]
		},
		"pirates plunder": {
			name: "pirates plunder",
			shortDesc: "A flask of 'Pirates Plunder'",
			longDesc: "No one knows the origin of this legendary drink, but Pirates up and down the Lost Coast swear by it's effects. A single flask is said to turn the most pathetic land lubber into a half way decent pirate. Could that possibly be true?",
			message: "You tip back the flask and drink deeply of the 'Pirates Plunder'. The taste is like nothing you've had before, dark, stormy and violent.",
			type: ItemTypeConsumable.Food,
			effects: [
				"NUTRITION_COMMON", "NUTRITION_XP_COMMON", "TOXICITY_UNCOMMON",
				"SWASHBUCKLING_XP_RARE", "NAVIGATING_XP_RARE", "SAILING_XP_RARE"
			]
		},
		ambrosia: {
			name: "ambrosia",
			shortDesc: "A dark green bottle of ambrosia",
			longDesc: "Ambrosia is known far and wide as the 'Food of the Gods'. Legends tell that it is brewed by Sea Nymphs in their under water caves, but how that is possible you have no idea.",
			message: "You uncork the bottle imbibe the potent brew. Words fail to describe how overwhelmingly good it tastes. You feel yourself filled with vigour and strength!",
			type: ItemTypeConsumable.Food,
			effects: ["NUTRITION_LEGENDARY", "NUTRITION_XP_LEGENDARY", "PURGE_LEGENDARY", "HEAL_LEGENDARY", "BOLSTER_WILL_XP_LEGENDARY"]
		},
		/** TF FOOD */
		starfruit: {
			name: "starfruit",
			shortDesc: "Strange starfruit",
			longDesc: "\
		This slightly pink starfruit is pulpy and contains many black seeds. It has a fragrant and intoxicating smell.\
		",
			message: "\
		You take a bite out of the starfruit and savor it's sweet, but citrus taste. You feel your body filling with energy.\
		",
			type: ItemTypeConsumable.Food,
			charges: 1,
			effects: [
				"UNWHOLESOME_MEAL",
				"NUTRITION_UNCOMMON",
				"NUTRITION_XP_UNCOMMON",
				"ENERGY_COMMON",
				"FEMALE_HORMONE_XP_COMMON"
			]
		},
		"milkdew melon": {
			name: "milkdew melon",
			shortDesc: "Milkdew Melon",
			longDesc: "\
		This rare melon has a milky white interior. It's has a sweet, but creamy taste.\
		",
			message: "\
		You crack the melon open and eat the contents. It tastes delicious and thick white juice dribbles down your chin.\
		",
			type: ItemTypeConsumable.Food,
			effects: [
				"UNWHOLESOME_MEAL",
				"NUTRITION_COMMON",
				"NUTRITION_XP_COMMON",
				"FEMALE_HORMONE_XP_COMMON",
				"BUST_XP_UNCOMMON",
				"PERVERSION_XP_COMMON"
			]
		},
		"peaches and cream": {
			name: "peaches and cream",
			shortDesc: "Peaches and Cream",
			longDesc: "\
		This rare desert is made from the thick juice of the Milkdew Melon that has been combined with preserved \
		peaches of unknown origin.\
		",
			message: "\
		The taste of this desert is sinful on your tongue - it's unlike anything you've eaten before. The sweetness, \
		the creamy texture, the cloying aroma… all together they overwhelm your senses and bring a flush to your body.\
		",
			type: ItemTypeConsumable.Food,
			effects: [
				"NUTRITION_RARE",
				"NUTRITION_XP_RARE",
				"FEMALE_HORMONE_XP_RARE",
				"BUST_XP_RARE",
				"PERVERSION_XP_RARE",
				"PURGE_UNCOMMON"
			]
		},
		"honey mead": {
			name: "honey mead",
			shortDesc: "Bzzangr Honey Mead",
			longDesc: "\
		A common drink along the Lingering Coast, made from the honey of giant Bzzangr insects.\
		",
			message: "\
		The mead has a delicious taste that tingles your tongue and lips.\
		",
			type: ItemTypeConsumable.Food,
			effects: [
				"UNWHOLESOME_MEAL",
				"NUTRITION_COMMON",
				"NUTRITION_XP_COMMON",
				"LIPS_XP_UNCOMMON"
			]
		},
		"butter gourd": {
			name: "butter gourd",
			shortDesc: "An oddly shaped gourd vegetable",
			longDesc: "\
		The flesh of this vegetable can easily be eaten raw and it has a pleasant, almost buttery flavor and texture.\
		",
			message: "\
		You break open the gourd and dig into the fleshy interior. It's surprisingly smooth and melts in your mouth like \
		butter or pure fat.\
		",
			type: ItemTypeConsumable.Food,
			effects: [
				"UNWHOLESOME_MEAL",
				"NUTRITION_COMMON",
				"NUTRITION_XP_COMMON",
				"ASS_XP_UNCOMMON",
				"HIPS_XP_UNCOMMON"
			]
		},
		"butter gourd pie": {
			name: "butter gourd pie",
			shortDesc: "A small butter gourd pie",
			longDesc: "\
		Best served warm, but still tasty cold - this delicious pastry is filled with spiced butter gourd and \
		glazed with honey\
		",
			message: "\
		You bite into the pie, suppressing an involuntary moan as the delicious filling slides across your tongue.\
		",
			type: ItemTypeConsumable.Food,
			effects: [
				"UNWHOLESOME_MEAL",
				"NUTRITION_RARE",
				"NUTRITION_XP_RARE",
				"ASS_XP_RARE",
				"HIPS_XP_RARE",
				"LIPS_XP_RARE"
			]
		},
		"jojo nut pie": {
			name: "jojo nut pie",
			shortDesc: "Buttered Jojo nut and pecan pie",
			longDesc: "\
		A relatively common island delicacy. The JoJo nuts that make up the crust of this pie are usually toxic, \
		but a long blanching process renders them harmless and quite tasty - almost like doughy bread.\
		",
			message: "\
		You bite into the pie and chew vigorously. The candied pecans contrast nicely with the odd dough like texture \
		of the Jojo nut crust.\
		",
			type: ItemTypeConsumable.Food,
			effects: [
				"UNWHOLESOME_MEAL",
				"NUTRITION_RARE",
				"NUTRITION_XP_RARE",
				"FACE_XP_RARE"
			]
		},
		"pink peach": {
			name: "pink peach",
			shortDesc: "A brightly colored pink peach",
			longDesc: "\
		This brightly colored fruit has a sweet and musky scent. It's bulbous exterior is dimpled with purple specks.\
		",
			message: "\
		You bite into the peach and savor the sweetness of the fruit. You feel almost as if it has a calming, or curative \
		effect on you.\
		",
			type: ItemTypeConsumable.Food,
			effects: [
				"NUTRITION_COMMON",
				"NUTRITION_XP_COMMON",
				"PURGE_UNCOMMON",
				"FEMALE_HORMONE_XP_COMMON"
			]
		},
		"mighty banana": {
			name: "mighty banana",
			shortDesc: "A big red banana that is shaped like a certain organ",
			longDesc: "\
		This banana was imported from a mystified island where the population is said to have dicks as long as their legs.\
		",
			message: "\
		You take a bite from the peeled banana and realise that it has a salty aftertaste. At the same time you feel \
		really manly and empowered\
		",
			type: ItemTypeConsumable.Food,
			effects: [
				"UNWHOLESOME_MEAL",
				"NUTRITION_COMMON",
				"NUTRITION_XP_COMMON",
				"MALE_HORMONE_XP_UNCOMMON",
				"PENIS_XP_UNCOMMON",
				"BALLS_XP_UNCOMMON"
			]
		},
		"purple mushrooms": {
			name: "purple mushroom",
			shortDesc: "A handful of rare purple mushrooms",
			longDesc: "\
		These mushrooms range in shapes from small and skinny to rather large specimens. Their caps are bright purple \
		and spotted with darker purple blotches. These are safe to eat, right?\
		",
			message: "\
		You ingest the mushrooms. They don't have much of a taste, but they definitely seem to have some 'kick' to them. \
		You hope this wasn't a foolish idea…\
		",
			type: ItemTypeConsumable.Food,
			effects: [
				"NUTRITION_COMMON",
				"NUTRITION_XP_COMMON",
				"TOXICITY_UNCOMMON",
				"MALE_HORMONE_XP_RARE",
				"PENIS_XP_RARE",
				"BALLS_XP_RARE"
			]
		},
		"purple sucker": {
			name: "erotic sucker",
			shortDesc: "An erotic purple sucker",
			longDesc: "\
		This large sucker is shaped like a realistic penis, complete with veins and a flared head. The tag \
		advertises the flavor as 'grape and semen'\
		",
			message: "Suck… suck… slurp… gasp. Mmm. Fruity, but slightly salty.",
			type: ItemTypeConsumable.Food,
			effects: [
				"UNWHOLESOME_MEAL",
				"NUTRITION_COMMON",
				"NUTRITION_XP_COMMON",
				"LIPS_XP_UNCOMMON",
				"PERVERSION_XP_COMMON",
				"PENIS_XP_COMMON"
			]
		},
		"purple tentacle": {
			name: "purple tentacle",
			shortDesc: "A slimy purple tentacle",
			longDesc: "\
	This slimy tentacle comes from a rare form of ocean creature called a Kraken. It's deep purple skin is mottled \
	with splotches of green and the underside is lined with suckers.\
	",
			message: "The tentacle is rubbery, faintly oily and tastes disturbingly unidentifiable. You feel like you've had better ideas…",
			type: ItemTypeConsumable.Food,
			charges: 1,
			effects: [
				"UNWHOLESOME_MEAL",
				"NUTRITION_COMMON",
				"NUTRITION_XP_COMMON",
				"PERVERSION_XP_RARE"
			]
		},
		"giant island pear": {
			name: "giant island pear",
			shortDesc: "A giant island pear",
			longDesc: "\
	These prized fruits are a rare treat as they can only be found on trees growing on otherwise desolate atolls. They \
	are said to increase vigor and strength. \
	",
			message: "\
	Despite being a fruit, the island pear has more of a savory than sweet taste. The flesh is soft and melts in your \
	mouth like the fat off a well cooked side of meat. While the fruit is not sweet, it's certainly delicious.",
			type: ItemTypeConsumable.Food,
			charges: 1,
			effects: [
				"UNWHOLESOME_MEAL",
				"NUTRITION_RARE",
				"NUTRITION_XP_RARE",
				"WAIST_REVERT_RARE",
				"FITNESS_XP_COMMON"
			]
		}
	});
}
