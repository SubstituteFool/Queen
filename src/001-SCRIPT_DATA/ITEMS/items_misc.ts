// Non usable items, not quest items, can have multiple's of.
namespace App.Data {
	Object.append(App.Data.misc, {
		torch: {
			name: "torch",
			shortDesc: "a wooden torch",
			longDesc: "A wooden torch. It burns quickly, so carry more than one.",
			type: ItemTypeConsumable.MiscConsumable,
			value: 20
		},

		shovel: {
			name: "shovel",
			shortDesc: "a sturdy shovel",
			longDesc: "A sturdy shovel, useful for digging up buried treasure. A true pirate never leaves port without one.",
			type: ItemTypeConsumable.MiscConsumable,
			value: 100
		},

		junk: {
			name: "junk",
			shortDesc: "a random collection of junk",
			longDesc: "Literally junk. Why are you carrying this around?",
			type: ItemTypeConsumable.MiscConsumable,
			value: 5
		}
	});
}
