// Relic parts - found in dungeons

// Common Parts
namespace App.Data {
	Object.append(miscLoot, {
		"broken rune": {
			name: "broken rune",
			shortDesc: "<span class='item-common'>a broken rune</span>",
			longDesc: "A small, flat oval piece of stone with part of a rune etched into it. It's broken now, but \
	perhaps there may still be some magic left in it.",
			type: ItemTypeConsumable.MiscLoot,
			value: 20,
			inMarket: false,
		},

		// Uncommon Parts
		"old arrowhead": {
			name: "old arrowhead",
			shortDesc: "<span class='item-uncommon'>an old arrowhead</span>",
			longDesc: "This arrowhead is crafted from an unusual silvery metal. The most amazing thing about it is \
	that despite being neglected for so long, the edge and point are still sharp.",
			type: ItemTypeConsumable.MiscLoot,
			value: 40,
			inMarket: false,
		},

		// Rare Parts
		"glowing crystal": {
			name: "glowing crystal",
			shortDesc: "<span class='item-rare'>a faintly glowing crystal</span>",
			longDesc: "This crystal gives off a faint, but cool white light. Upon closer inspection it has tiny intricate \
	glyphs carved onto its many face. Evidently, this is not a natural phenomina.",
			type: ItemTypeConsumable.MiscLoot,
			value: 80,
			inMarket: false,
		},

		// Legendary Parts
		"stone tablet": {
			name: "stone tablet",
			shortDesc: "<span class='item-legendary'>an ancient stone tablet</span>",
			longDesc: "This ancient stone tablet is inscribed with incomprehensible glyphs and figures in a \
	language that must be long dead. Is it a diary? An alchemical formula? A pie recipe? You have \
	no idea!",
			type: ItemTypeConsumable.MiscLoot,
			value: 160,
			inMarket: false,
		}
	});
}
