namespace App.Data {
	Object.append(jobs, {
		DANCE_AMATEUR: {
			id: "DANCE_AMATEUR",
			title: "Amateur Hour",
			giver: "DANCEHALL_JOBS",
			pay: 25,
			rating: 2,
			phases: [0, 1, 2, 3],
			days: 0,
			cost: [
				{type: "time", value: 1},
				{type: Stat.Core, name: CoreStat.Energy, value: 2},
			],
			requirements: [
				{type: Stat.Skill, name: Skills.Charisma.Dancing, value: 30, condition: "gte"},
				{type: Stat.Core, name: CoreStat.Perversion, value: 30, condition: "gte"}
			],
			intro: "\
    A basic routine, requiring minimal skills. You won't get a lot of tips, but you'll get valuable experience. \
    ",
			start: "\
    The music begins and that's your cue to slip out of the velvet curtains and on to the stage.\
    ",
			scenes: [
				{
					id: "Intro Scene",
					checks: [
						{tag: "beautyCheck", type: "meta", name: "beauty", difficulty: 120, value: 10},
						{tag: "styleCheck", type: "meta", name: "danceStyle", difficulty: 120, value: 10},
						{
							tag: "APPEARANCE_CHECK", type: "func", value: 10,
							func: (_p, _s, c) => (c["BEAUTY_CHECK"].result + c["STYLE_CHECK"].result) / 2,
						}
					],
					post: [
						{type: "money", value: 10, factor: "BEAUTY_CHECK"},
						{type: "money", value: 10, factor: "STYLE_CHECK"},
					],
					text: [
						{
							APPEARANCE_CHECK: 20,
							text: "\
                    The immediate reaction from the crowd to your appearance @@is tepid@@. Maybe you should \
                    work on your beauty or fashion?\
                    "
						},
						{
							APPEARANCE_CHECK: 40,
							text: "\
                    The crowd @@appears disinterested@@ in your upcoming performance. Maybe you should \
                    work on your beauty or fashion?\
                    "
						},
						{
							APPEARANCE_CHECK: 60,
							text: "\
                    The crowd @@appears mildly interested@@ in your upcoming performance. Maybe you should \
                    work on your beauty or fashion?\
                    "
						},
						{
							APPEARANCE_CHECK: 80,
							text: "\
                    Your arrival is greeted with the sound of several @@catcalls and cheers@@. It's a good reaction but \
                    you feel that you could do better.\
                    "
						},
						{
							APPEARANCE_CHECK: 500,
							text: "\
                    The @@crowd goes wild@@ as you appear. You're greeted with a thunderous applause and wolf whistling.\
                    "
						}
					],
				},
				{
					id: "Dance Scene 1",
					checks: [{tag: "DANCE_CHECK_1", type: Stat.Skill, name: Skills.Charisma.Dancing, difficulty: 50, value: 10}],
					post: [
						{type: "statXp", name: CoreStat.Fitness, value: 10, factor: "DANCE_CHECK_1"},
						{type: "money", value: 10, factor: "DANCE_CHECK_1"}
					],
					text: [
						"In the middle of the stage is a large revolving pole that is used to perform acrobatic stunts. You \
            saunter towards it, doing your best to exaggerate the gait of your nHIPS and accentuate your \
            nASS. Once there, you lean over and place your hands on the pole, smile at the crowd and attempt \
            a simple move.\
            ",
						{
							DANCE_CHECK_1: 33,
							text: "\
                    With a twist of your hips, you lay one leg across the pole and attempt to dive into a simple \
                    spin. At first it goes well enough, but on the second revolution you lose your grip and \
                    @@tumble uncerimonially to the floor@@.\
                    "
						},
						{
							DANCE_CHECK_1: 66,
							text: "\
                    With a twist of your hips, you lay one leg across the pole and attempt to dive into a simple \
                    spin. You do your best to arch your back and display your nBUST to the crowd, hoping to \
                    gain some favor. Your efforts are rewarded with a @@smattering of applause@@.\
                    "
						},
						{
							DANCE_CHECK_1: 500,
							text: "\
                    With a twist of your hips, you lay one leg across the pole and attempt to dive into a simple \
                    spin. You climb as far as you can up on the pole, wrapping your legs around it and then \
                    leaning backwards, your nBUST on display as you rotate in front of the crowd. Your skill \
                    earns you a @@healthy round of applause and cheers@@ from the crowd.\
                    "
						},
						"The tempo of the music picks up and you strut towards the front of the stage, dropping to your knees \
            in front of the crowd and running your hands up your body to your hair.\
            "
					]
				},
				{
					id: "Stripping Scene 1",
					checks: [{tag: "STRIPPING_CHECK_1", type: Stat.Skill, name: Skills.Charisma.Seduction, difficulty: 50, value: 10}],
					post: [{type: "money", value: 10, factor: "STRIPPING_CHECK_1"}],
					text: [
						"You start the sway side to side, grinding yourself against the stage on your knees while your hands \
            roam up and down your body. \
            <<if setup.player.IsEquipped(['Costume', 'Dress', 'Shirt'], true)>>\
            With deft moves, you undo the top of your pEQUIP(Costume|Dress|Shirt) \
            and expose your pEQUIP(Bra|$naked flesh). <</if>>\
            <<if setup.player.IsEquipped('Bra', true)>>\
            You grope your pBUST chest through your pEQUIP(Bra)<<else>>\
            You grope your pBUST chest<</if>>, roughly shaking and fondling it for \
            the audience.\
            ",
						{
							STRIPPING_CHECK_1: 33,
							text: "\
                    They seem @@mostly unimpressed@@ by your actions.\
                    "
						},
						{
							STRIPPING_CHECK_1: 66,
							text: "\
                    You get a @@few cheers and a bit of applause@@ for your display.\
                    "
						},
						{
							STRIPPING_CHECK_1: 500,
							text: "\
                    The @@crowd shouts out for more@@ and encourages you to go further!\
                    "
						},
					],
				},
				{
					id: "Dance Scene 2",
					checks: [{tag: "DANCE_CHECK_2", type: Stat.Skill, name: Skills.Charisma.Dancing, difficulty: 50, value: 10}],
					post: [
						{type: "statXp", name: CoreStat.Fitness, value: 10, factor: "DANCE_CHECK_2"},
						{type: "money", value: 10, factor: "DANCE_CHECK_2"}
					],
					text: [
						"You stand up and pivot, facing your rear to the crowd and swaying your pASS butt at them. \
            <<if setup.player.IsEquipped(['Costume', 'Dress', 'Shirt'])>>\
            With a wiggle and a bump, you remove your pEQUIP(Costume|Dress|Shirt) and drop it to the stage. \
            <</if>>\
            <<if setup.player.IsEquipped('Pants')>>\
            You then bend over, swaying your ass from side to side as you wiggle out of your pEQUIP(Pants).\
            <</if>>\
            You slowly spin around to face the crowd, clad only in your pEQUIP(Bra|$bare chest), pEQUIP(Panty|$naked ass) \
            and a (fake) smile plastered on your face.  You time your movements to the music, undulating around \
            the stage in a series of exaggeratedly flirty movements, slapping your nASS and fake moaning as you grope \
            yourself.",
						{
							DANCE_CHECK_2: 33,
							text: "\
                    Unfortunately your performance is a little @@stilted and awkward@@ and the crowd doesn't seem \
                    to enjoy it very much.\
                    "
						},
						{
							DANCE_CHECK_2: 66,
							text: "\
                    You get a @@few scattered cheers and some whistles@@ at your display. Not bad.\
                    "
						},
						{
							DANCE_CHECK_2: 500,
							text: "\
                    The @@crowd shouts out for more@@ and encourages you to go further!\
                    "
						}
					],
				},
				{
					id: "Stripping Scene 2",
					checks: [{tag: "STRIPPING_CHECK_2", type: Stat.Skill, name: Skills.Charisma.Seduction, difficulty: 50, value: 10}],
					post: [{type: "money", value: 10, factor: "STRIPPING_CHECK_2"}],
					text: [
						"You waltz across the stage, your pEQUIP(Shoes|$bare feet) \
            <<if setup.player.IsEquipped('Shoes',true)>>clicking<<else>>slapping<</if>> \
            on the wooden surface. \
            <<if setup.player.IsEquipped('Bra',true)>>\
            With practiced ease you remove your pEQUIP(Bra), using your arm to cover your \
            nBUST and fling it over your shoulder. You dance that way for a minute, trying to tease \
            the crowd with a hint of your nipples. After a while, you remove your arm and thrust your \
            hands under your breasts and give them a firm shake. \
            <<else>>\
            You place your hands under your breasts and give them a firm shake at the crowd while \
            smiling broadly. <</if>>\
            Slowly you rub and tweak your nipples, your nLIPS making an exaggerated 'o' to delight them.\
            <<if setup.player.IsEquipped('Panty',true)>>\
            You bend over in front of your crowd, your hands going to the sides of your \
            pEQUIP(Panty). With exaggerated movements you slowly wiggle them down your legs until they \
            reach the floor and you casually step out of them, exposing your nPENIS to the crowd. \
            <<else>>\
            You do another exaggerated movement, wiggling your hips and flopping your nPENIS around \
            at the crowd. <</if>>\
            <<if setup.player.getStat('body', 'penis') <= 10>>\
            There is a smattering of laughter at the size of your member and a few shouts of 'Sissy!' \
            out in the crowd. It's humilating but you smile through it. <</if>>\
            With a final pirouette, you expose your nASS to the crowd and spread your cheeks at them\
            <<if setup.player.isEquipped('ass', true)>> exposing your pEQUIP(ass), much to their delight. \
            <<else>>.<</if>>\
            Just as the music begins to finish up, you give yourself one last double handed slap on the \
            cheeks, making an loud (and you hope sexy) shout.\
            ",
						{
							STRIPPING_CHECK_2: 33,
							text: "\
                    Sadly, there isn't much applause and the crowd @@mostly ignores you@@ while seeming to \
                    be more interested in their drinks than your performance. What did you do wrong?\
                    "
						},
						{
							STRIPPING_CHECK_2: 66,
							text: "\
                    There is a @@round of applause and a few cheers@@ from the crowd and some extra tips \
                    fly through the air as you finish your performance.\
                    "
						},
						{
							STRIPPING_CHECK_2: 500,
							text: "\
                    There is a @@large amount of cheering and laughing@@ from the crowd at the end of your \
                    performance, some of them even going so far as to yell out for an encore! A large amount \
                    of tips shower down on the stage.\
                    "
						},
					],
				},
				{
					id: "Final Fixup",
					checks: [
						{
							tag: "PERFORMANCE_BONUS", type: "func",
							func: (_p, _s, c) => {
								return (c["DANCE_CHECK_1"].mod +
									c["DANCE_CHECK_2"].mod +
									c["STRIPPING_CHECK_1"].mod +
									c["STRIPPING_CHECK_2"].mod) / 4;
							},
							value: 1
						}
					],
					post: [
						{type: "corruptWillpower", name: "low", value: -20},
						{type: "statXp", name: CoreStat.Perversion, value: 50, factor: "PERFORMANCE_BONUS"},
						{type: "npcStat", name: NpcStat.Mood, value: 5, factor: "PERFORMANCE_BONUS"},
					],
				}

			],
			end: "\
    With your performance over, you take one last bow, your nBUST dangling for the crowd and blow them a kiss. \
    You scoop up the money that was thrown on the stage and exit behind the curtains, but not before \
    noticing that NPC_NAME was watching you dance… JOB_RESULTS\
    ",
			jobResults: [
				{
					PERFORMANCE_BONUS: 0.33,
					text: "they @@didn't look very impressed@@."
				},
				{
					PERFORMANCE_BONUS: 0.66,
					text: "they @@looked slightly impressed@@."
				},
				{
					PERFORMANCE_BONUS: 1,
					text: "they @@seemed to enjoy the show@@."
				},
				{
					PERFORMANCE_BONUS: 100,
					text: "they @@looked rather pleased with your performance@@."
				}
			]
		},

		DANCE_PROFESSIONAL: {
			id: "DANCE_PROFESSIONAL",
			title: "Strutting Your Stuff",
			giver: "DANCEHALL_JOBS",
			pay: 50,
			rating: 3,
			phases: [0, 1, 2, 3],
			days: 0,
			cost: [
				{type: "time", value: 1},
				{type: Stat.Core, name: CoreStat.Energy, value: 2},
			],
			requirements: [
				{type: Stat.Skill, name: Skills.Charisma.Dancing, value: 50, condition: "gte"},
				{type: Stat.Core, name: CoreStat.Perversion, value: 30, condition: "gte"}
			],
			intro: "\
    An advanced routine, typical of an experienced dancer. Higher difficulty but more tips. \
    ",
			start: "",
			scenes: [
				{
					id: "Intro Scene",
					checks: [
						{tag: "BEAUTY_CHECK", type: "meta", name: "beauty", difficulty: 120, value: 10},
						{tag: "STYLE_CHECK", type: "meta", name: "danceStyle", difficulty: 120, value: 10},
						{
							tag: "APPEARANCE_CHECK", type: "func", value: 10,
							func: (_p, _s, c) => Math.mean(c["BEAUTY_CHECK"].result, c["STYLE_CHECK"].result)
						}
					],
					post: [
						{type: "money", value: 10, factor: "BEAUTY_CHECK"},
						{type: "money", value: 10, factor: "STYLE_CHECK"}
					],
					text: [
						{
							APPEARANCE_CHECK: 20,
							text: "\
                     The immediate reaction from the crowd to your appearance @@is tepid@@. Maybe you should \
                     work on your beauty or fashion?\
                     "
						},
						{
							APPEARANCE_CHECK: 40,
							text: "\
                     The crowd @@appears disinterested@@ in your upcoming performance. Maybe you should \
                     work on your beauty or fashion?\
                     "
						},
						{
							APPEARANCE_CHECK: 60,
							text: "\
                     The crowd @@appears mildly interested@@ in your upcoming performance. Maybe you should \
                     work on your beauty or fashion?\
                     "
						},
						{
							APPEARANCE_CHECK: 80,
							text: "\
                     Your arrival is greeted with the sound of several @@catcalls and cheers@@. It's a good reaction but \
                     you feel that you could do better.\
                     "
						},
						{
							APPEARANCE_CHECK: 500,
							text: "\
                     The @@crowd goes wild@@ as you appear. You're greeted with a thunderous applause and wolf whistling.\
                     "
						}
					],
				},
				{
					id: "Dance Scene 1",
					checks: [{tag: "DANCE_CHECK_1", type: Stat.Skill, name: Skills.Charisma.Dancing, difficulty: 70, value: 20}],
					post: [
						{type: "statXp", name: CoreStat.Fitness, value: 10, factor: "DANCE_CHECK_1"},
						{type: "money", value: 20, factor: "DANCE_CHECK_1"}
					],
					text: [
						"In the middle of the stage is a large revolving pole that is used to perform acrobatic stunts. You \
             saunter towards it, doing your best to exaggerate the gait of your nHIPS and accentuate your \
             nASS. Once there, you lean over and place your hands on the pole, smile at the crowd and attempt \
             a simple move.\
             ",
						{
							DANCE_CHECK_1: 33,
							text: "\
                     With a twist of your hips, you lay one leg across the pole and attempt to dive into a simple \
                     spin. At first it goes well enough, but on the second revolution you lose your grip and \
                     @@tumble uncerimonially to the floor@@.\
                     "
						},
						{
							DANCE_CHECK_1: 66,
							text: "\
                     With a twist of your hips, you lay one leg across the pole and attempt to dive into a simple \
                     spin. You do your best to arch your back and display your nBUST to the crowd, hoping to \
                     gain some favor. Your efforts are rewarded with a @@smattering of applause@@.\
                     "
						},
						{
							DANCE_CHECK_1: 500,
							text: "\
                     With a twist of your hips, you lay one leg across the pole and attempt to dive into a simple \
                     spin. You climb as far as you can up on the pole, wrapping your legs around it and then \
                     leaning backwards, your nBUST on display as you rotate in front of the crowd. Your skill \
                     earns you a @@healthy round of applause and cheers@@ from the crowd.\
                     "
						},
						"The tempo of the music picks up and you strut towards the front of the stage, dropping to your knees \
             in front of the crowd and running your hands up your body to your hair.\
             "
					]
				},
				{
					id: "Stripping Scene 1",
					checks: [{tag: "STRIPPING_CHECK_1", type: Stat.Skill, name: Skills.Charisma.Seduction, difficulty: 70, value: 20}],
					post: [{type: "money", value: 20, factor: "STRIPPING_CHECK_1"}],
					text: [
						"You start the sway side to side, grinding yourself against the stage on your knees while your hands \
             roam up and down your body. \
             <<if setup.player.IsEquipped(['Costume', 'Dress', 'Shirt'], true)>>\
             With deft moves, you undo the top of your pEQUIP(Costume|Dress|Shirt) \
             and expose your pEQUIP(Bra|$naked flesh). <</if>>\
             <<if setup.player.IsEquipped('Bra', true)>>\
             You grope your pBUST chest through your pEQUIP(Bra)<<else>>\
             You grope your pBUST chest<</if>>, roughly shaking and fondling it for \
             the audience.\
             ",
						{
							STRIPPING_CHECK_1: 33,
							text: "\
                     They seem @@mostly unimpressed@@ by your actions.\
                     "
						},
						{
							STRIPPING_CHECK_1: 66,
							text: "\
                     You get a @@few cheers and a bit of applause@@ for your display.\
                     "
						},
						{
							STRIPPING_CHECK_1: 500,
							text: "\
                     The @@crowd shouts out for more@@ and encourages you to go further!\
                     "
						},
					],
				},
				{
					id: "Dance Scene 2",
					checks: [{tag: "DANCE_CHECK_2", type: Stat.Skill, name: Skills.Charisma.Dancing, difficulty: 70, value: 20}],
					post: [
						{type: "statXp", name: CoreStat.Fitness, value: 10, factor: "DANCE_CHECK_2"},
						{type: "money", value: 20, factor: "DANCE_CHECK_2"}
					],
					text: [
						"You stand up and pivot, facing your rear to the crowd and swaying your pASS butt at them. \
             <<if setup.player.IsEquipped(['Costume', 'Dress', 'Shirt'])>>\
             With a wiggle and a bump, you remove your pEQUIP(Costume|Dress|Shirt) and drop it to the stage. \
             <</if>>\
             <<if setup.player.IsEquipped('Pants')>>\
             You then bend over, swaying your ass from side to side as you wiggle out of your pEQUIP(Pants).\
             <</if>>\
             You slowly spin around to face the crowd, clad only in your pEQUIP(Bra|$bare chest), pEQUIP(Panty|$naked ass) \
             and a (fake) smile plastered on your face.  You time your movements to the music, undulating around \
             the stage in a series of exaggeratedly flirty movements, slapping your nASS and fake moaning as you grope \
             yourself.\
             ",
						{
							DANCE_CHECK_2: 33,
							text: "\
                     Unfortunately your performance is a little @@stilted and awkward@@ and the crowd doesn't seem \
                     to enjoy it very much.\
                     "
						},
						{
							DANCE_CHECK_2: 66,
							text: "\
                     You get a @@few scattered cheers and some whistles@@ at your display. Not bad.\
                     "
						},
						{
							DANCE_CHECK_2: 500,
							text: "\
                     The @@crowd shouts out for more@@ and encourages you to go further!\
                     "
						}
					],
				},
				{
					id: "Stripping Scene 2",
					checks: [{tag: "STRIPPING_CHECK_2", type: Stat.Skill, name: Skills.Charisma.Seduction, difficulty: 70, value: 20}],
					post: [{type: "money", value: 20, factor: "STRIPPING_CHECK_2"}],
					text: [
						"You waltz across the stage, your pEQUIP(Shoes|$bare feet) \
             <<if setup.player.IsEquipped('Shoes',true)>>clicking<<else>>slapping<</if>> \
             on the wooden surface. \
             <<if setup.player.IsEquipped('Bra',true)>>\
             With practiced ease you remove your pEQUIP(Bra), using your arm to cover your \
             nBUST and fling it over your shoulder. You dance that way for a minute, trying to tease \
             the crowd with a hint of your nipples. After a while, you remove your arm and thrust your \
             hands under your breasts and give them a firm shake. \
             <<else>>\
             You place your hands under your breasts and give them a firm shake at the crowd while \
             smiling broadly. <</if>>\
             Slowly you rub and tweak your nipples, your nLIPS making an exaggerated 'o' to delight them.\
             <<if setup.player.IsEquipped('Panty',true)>>\
             You bend over in front of your crowd, your hands going to the sides of your \
             pEQUIP(Panty). With exaggerated movements you slowly wiggle them down your legs until they \
             reach the floor and you casually step out of them, exposing your nPENIS to the crowd. \
             <<else>>\
             You do another exaggerated movement, wiggling your hips and flopping your nPENIS around \
             at the crowd. <</if>>\
             <<if setup.player.getStat('body, 'penis') <= 10>>\
             There is a smattering of laughter at the size of your member and a few shouts of 'Sissy!' \
             out in the crowd. It's humilating but you smile through it. <</if>>\
             With a final pirouette, you expose your nASS to the crowd and spread your cheeks at them\
             <<if setup.player.isEquipped('ass', true)>> exposing your pEQUIP(ass), much to their delight. \
             <<else>>.<</if>>\
             Just as the music begins to finish up, you give yourself one last double handed slap on the \
             cheeks, making an loud (and you hope sexy) shout.\
             ",
						{
							STRIPPING_CHECK_2: 33,
							text: "\
                     Sadly, there isn't much applause and the crowd @@mostly ignores you@@ while seeming to \
                     be more interested in their drinks than your performance. What did you do wrong?\
                     "
						},
						{
							STRIPPING_CHECK_2: 66,
							text: "\
                     There is a @@round of applause and a few cheers@@ from the crowd and some extra tips \
                     fly through the air towards the stage.\
                     "
						},
						{
							STRIPPING_CHECK_2: 500,
							text: "\
                     There is a @@large amount of cheering and laughing@@ from the crowd. A large amount \
                     of tips shower down on the stage and a few audience members shout for an encore!\
                     "
						},
					],
				},
				{
					id: "Optional Dildo Scene",
					triggers: [
						{type: "hasItem", name: "quest/14 inch purple dildo"},
						{type: Stat.Core, name: CoreStat.Perversion, value: 50, condition: "gte"}
					],
					checks: [{tag: "BLOWJOB_CHECK", type: Stat.Skill, name: Skills.Sexual.BlowJobs, difficulty: 100, value: 50}],
					post: [
						{type: "statXp", name: CoreStat.Perversion, value: 50},
						{type: "money", value: 50, factor: "BLOWJOB_CHECK"}
					],
					text: [
						"Something about the thrill of performing naked on stage for a bunch of horny men resonates deeply \
             within your corrupted mind. Before the performance you had placed the @@.item-quest;14 inch purple dildo@@ \
             that you found in GF_NAME's wardrobe nearby and now you know just how to use it…\n\n\
             A small cheer goes up from the crowd as you produce the gigantic rubber phallus and start rubbing it up \
             and down your body. You place it between your nBUST in a simulated titfuck while you \
             slather the tip with your tongue and mouth. Such a lewd act of behavior gets you a giant cheer and spurred \
             on you decide to take your act a bit further. You move to the side so that the audience can see your \
             profile and arch your head back, then you slowly start to swallow the massive dildo.\
             ",
						{
							BLOWJOB_CHECK: 33,
							text: "\
                     You do your best to get the monster down your throat, but after a minute of struggling in vain \
                     you are forced to give up. The crowd @@seems somewhat disappointed@@ but at least you tried."
						},
						{
							BLOWJOB_CHECK: 66,
							text: "\
                    You do your best to get the monster down your throat, and after a minute of struggling you feel \
                    it's simulated plastic balls touching your chin. The crowd @@applauds your efforts@@ and marvels \
                    that they can see the distended bulge in your neck as you've swallowed the giant cock to the hilt!"
						},
						{
							BLOWJOB_CHECK: 500,
							text: "\
                    You swallow the simulated dong with an ease that amazes the crowd. With a slight hum through your nose \
                    you begin to rapidly work the dildo in and out of your throat, your neck distending with every thrust \
                    and causing the @@crowd to go wild with excitement@@!"
						},
						"Finally, you pull the dildo out of your mouth with a large 'plop' sound and smile, trails of drools \
             dripping from your chin and running in between your breasts."
					]
				},
				{
					id: "Final Fixup",
					checks: [
						{
							tag: "PERFORMANCE_BONUS", type: "func",
							func: (_p, _s, c) => {
								return (c["DANCE_CHECK_1"].mod +
									c["DANCE_CHECK_2"].mod +
									c["STRIPPING_CHECK_1"].mod +
									c["STRIPPING_CHECK_2"].mod) / 4;
							},
							value: 1
						}
					],
					post: [
						{type: "corruptWillpower", name: "low", value: -20},
						{type: "statXp", name: CoreStat.Perversion, value: 50, factor: "PERFORMANCE_BONUS"},
						{type: "npcStat", name: NpcStat.Mood, value: 5, factor: "PERFORMANCE_BONUS"},
					],
				}

			],
			end: "\
     With your performance over, you take one last bow, your nBUST dangling for the crowd and blow them a kiss. \
     You scoop up the money that was thrown on the stage and exit behind the curtains, but not before \
     noticing that NPC_NAME was watching you dance… JOB_RESULTS\
     ",
			jobResults: [
				{
					PERFORMANCE_BONUS: 0.33,
					text: "they @@didn't look very impressed@@."
				},
				{
					PERFORMANCE_BONUS: 0.66,
					text: "they @@looked slightly impressed@@."
				},
				{
					PERFORMANCE_BONUS: 1,
					text: "they @@seemed to enjoy the show@@."
				},
				{
					PERFORMANCE_BONUS: 100,
					text: "they @@looked rather pleased with your performance@@."
				}
			]
		},

		DANCE_EXPERT: {
			id: "DANCE_EXPERT",
			title: "Bringing Down the House",
			giver: "DANCEHALL_JOBS",
			pay: 75,
			rating: 5,
			phases: [0, 1, 2, 3],
			days: 0,
			cost: [
				{type: "time", value: 1},
				{type: Stat.Core, name: CoreStat.Energy, value: 2},
			],
			requirements: [
				{type: Stat.Skill, name: Skills.Charisma.Dancing, value: 70, condition: "gte"},
				{type: Stat.Core, name: CoreStat.Perversion, value: 30, condition: "gte"}
			],
			intro: "\
    An expert level routine. Very challenging, but incredibly lucrative for a skilled dancer. \
    ",
			start: "",
			scenes: [
				{
					id: "Intro Scene",
					checks: [
						{tag: "beautyCheck", type: "meta", name: "beauty", difficulty: 120, value: 10},
						{tag: "styleCheck", type: "meta", name: "danceStyle", difficulty: 120, value: 10},
						{
							tag: "APPEARANCE_CHECK", type: "func", value: 10,
							func: (_p, _s, c) => (c["BEAUTY_CHECK"].result + c["STYLE_CHECK"].result) / 2
						}
					],
					post: [
						{type: "money", value: 10, factor: "BEAUTY_CHECK"},
						{type: "money", value: 10, factor: "STYLE_CHECK"}
					],
					text: [
						{
							APPEARANCE_CHECK: 20,
							text: "\
                     The immediate reaction from the crowd to your appearance @@is tepid@@. Maybe you should \
                     work on your beauty or fashion?\
                     "
						},
						{
							APPEARANCE_CHECK: 40,
							text: "\
                     The crowd @@appears disinterested@@ in your upcoming performance. Maybe you should \
                     work on your beauty or fashion?\
                     "
						},
						{
							APPEARANCE_CHECK: 60,
							text: "\
                     The crowd @@appears mildly interested@@ in your upcoming performance. Maybe you should \
                     work on your beauty or fashion?\
                     "
						},
						{
							APPEARANCE_CHECK: 80,
							text: "\
                     Your arrival is greeted with the sound of several @@catcalls and cheers@@. It's a good reaction but \
                     you feel that you could do better.\
                     "
						},
						{
							APPEARANCE_CHECK: 500,
							text: "\
                     The @@crowd goes wild@@ as you appear. You're greeted with a thunderous applause and wolf whistling.\
                     "
						}
					],
				},
				{
					id: "Dance Scene 1",
					checks: [{tag: "DANCE_CHECK_1", type: Stat.Skill, name: Skills.Charisma.Dancing, difficulty: 100, value: 30}],
					post: [
						{type: "statXp", name: CoreStat.Fitness, value: 10, factor: "DANCE_CHECK_1"},
						{type: "money", value: 30, factor: "DANCE_CHECK_1"}
					],
					text: [
						"In the middle of the stage is a large revolving pole that is used to perform acrobatic stunts. You \
             saunter towards it, doing your best to exaggerate the gait of your nHIPS and accentuate your \
             nASS. Once there, you lean over and place your hands on the pole, smile at the crowd and attempt \
             a simple move.\
             ",
						{
							DANCE_CHECK_1: 33,
							text: "\
                     With a twist of your hips, you lay one leg across the pole and attempt to dive into a simple \
                     spin. At first it goes well enough, but on the second revolution you lose your grip and \
                     @@tumble uncerimonially to the floor@@.\
                     "
						},
						{
							DANCE_CHECK_1: 66,
							text: "\
                     With a twist of your hips, you lay one leg across the pole and attempt to dive into a simple \
                     spin. You do your best to arch your back and display your nBUST to the crowd, hoping to \
                     gain some favor. Your efforts are rewarded with a @@smattering of applause@@.\
                     "
						},
						{
							DANCE_CHECK_1: 500,
							text: "\
                     With a twist of your hips, you lay one leg across the pole and attempt to dive into a simple \
                     spin. You climb as far as you can up on the pole, wrapping your legs around it and then \
                     leaning backwards, your nBUST on display as you rotate in front of the crowd. Your skill \
                     earns you a @@healthy round of applause and cheers@@ from the crowd.\
                     "
						},
						"The tempo of the music picks up and you strut towards the front of the stage, dropping to your knees \
             in front of the crowd and running your hands up your body to your hair.\
             "
					]
				},
				{
					id: "Stripping Scene 1",
					checks: [{tag: "STRIPPING_CHECK_1", type: Stat.Skill, name: Skills.Charisma.Seduction, difficulty: 100, value: 30}],
					post: [{type: "money", value: 30, factor: "STRIPPING_CHECK_1"}],
					text: [
						"You start the sway side to side, grinding yourself against the stage on your knees while your hands \
             roam up and down your body. \
             <<if setup.player.IsEquipped(['Costume', 'Dress', 'Shirt'], true)>>\
             With deft moves, you undo the top of your pEQUIP(Costume|Dress|Shirt) \
             and expose your pEQUIP(Bra|$naked flesh). <</if>>\
             <<if setup.player.IsEquipped('Bra', true)>>\
             You grope your pBUST chest through your pEQUIP(Bra)<<else>>\
             You grope your pBUST chest<</if>>, roughly shaking and fondling it for \
             the audience.\
             ",
						{
							STRIPPING_CHECK_1: 33,
							text: "\
                     They seem @@mostly unimpressed@@ by your actions.\
                     "
						},
						{
							STRIPPING_CHECK_1: 66,
							text: "\
                     You get a @@few cheers and a bit of applause@@ for your display.\
                     "
						},
						{
							STRIPPING_CHECK_1: 500,
							text: "\
                     The @@crowd shouts out for more@@ and encourages you to go further!\
                     "
						},
					],
				},
				{
					id: "Dance Scene 2",
					checks: [{tag: "DANCE_CHECK_2", type: Stat.Skill, name: Skills.Charisma.Dancing, difficulty: 100, value: 30}],
					post: [
						{type: "statXp", name: CoreStat.Fitness, value: 10, factor: "DANCE_CHECK_2"},
						{type: "money", value: 30, factor: "DANCE_CHECK_2"}
					],
					text: [
						"You stand up and pivot, facing your rear to the crowd and swaying your pASS butt at them. \
             <<if setup.player.IsEquipped(['Costume', 'Dress', 'Shirt'])>>\
             With a wiggle and a bump, you remove your pEQUIP(Costume|Dress|Shirt) and drop it to the stage. \
             <</if>>\
             <<if setup.player.IsEquipped('Pants')>>\
             You then bend over, swaying your ass from side to side as you wiggle out of your pEQUIP(Pants).\
             <</if>>\
             You slowly spin around to face the crowd, clad only in your pEQUIP(Bra|$bare chest), pEQUIP(Panty|$naked ass) \
             and a (fake) smile plastered on your face.  You time your movements to the music, undulating around \
             the stage in a series of exaggeratedly flirty movements, slapping your nASS and fake moaning as you grope \
             yourself.\
             ",
						{
							DANCE_CHECK_2: 33,
							text: "\
                     Unfortunately your performance is a little @@stilted and awkward@@ and the crowd doesn't seem \
                     to enjoy it very much.\
                     "
						},
						{
							DANCE_CHECK_2: 66,
							text: "\
                     You get a @@few scattered cheers and some whistles@@ at your display. Not bad.\
                     "
						},
						{
							DANCE_CHECK_2: 500,
							text: "\
                     The @@crowd shouts out for more@@ and encourages you to go further!\
                     "
						}
					],
				},
				{
					id: "Stripping Scene 2",
					checks: [{tag: "STRIPPING_CHECK_2", type: Stat.Skill, name: Skills.Charisma.Seduction, difficulty: 100, value: 30}],
					post: [{type: "money", value: 30, factor: "STRIPPING_CHECK_2"}],
					text: [
						"You waltz across the stage, your pEQUIP(Shoes|$bare feet) \
             <<if setup.player.IsEquipped('Shoes',true)>>clicking<<else>>slapping<</if>> \
             on the wooden surface. \
             <<if setup.player.IsEquipped('Bra',true)>>\
             With practiced ease you remove your pEQUIP(Bra), using your arm to cover your \
             nBUST and fling it over your shoulder. You dance that way for a minute, trying to tease \
             the crowd with a hint of your nipples. After a while, you remove your arm and thrust your \
             hands under your breasts and give them a firm shake. \
             <<else>>\
             You place your hands under your breasts and give them a firm shake at the crowd while \
             smiling broadly. <</if>>\
             Slowly you rub and tweak your nipples, your nLIPS making an exaggerated 'o' to delight them.\
             <<if setup.player.IsEquipped('Panty',true)>>\
             You bend over infront of your crowd, your hands going to the sides of your \
             pEQUIP(Panty). With exaggerated movements you slowly wiggle them down your legs until they \
             reach the floor and you casually step out of them, exposing your nPENIS to the crowd. \
             <<else>>\
             You do another exaggerated movement, wiggling your hips and flopping your nPENIS around \
             at the crowd. <</if>>\
             <<if setup.player.getStat('body, 'penis') <= 10>>\
             There is a smattering of laughter at the size of your member and a few shouts of 'Sissy!' \
             out in the crowd. It's humilating but you smile through it. <</if>>\
             With a final pirouette, you expose your nASS to the crowd and spread your cheeks at them\
             <<if setup.player.isEquipped('ass', true)>> exposing your pEQUIP(ass), much to their delight. \
             <<else>>.<</if>>\
             Just as the music begins to finish up, you give yourself one last double handed slap on the \
             cheeks, making an loud (and you hope sexy) shout.\
             ",
						{
							STRIPPING_CHECK_2: 33,
							text: "\
                    Sadly, there isn't much applause and the crowd @@mostly ignores you@@ while seeming to \
                    be more interested in their drinks than your performance. What did you do wrong?\
                    "
						},
						{
							STRIPPING_CHECK_2: 66,
							text: "\
                    There is a @@round of applause and a few cheers@@ from the crowd and some extra tips \
                    fly through the air towards the stage.\
                    "
						},
						{
							STRIPPING_CHECK_2: 500,
							text: "\
                    There is a @@large amount of cheering and laughing@@ from the crowd. A large amount \
                    of tips shower down on the stage and a few audience members shout for an encore!\
                    "
						},
					],
				},
				{
					id: "Optional Dildo Scene",
					triggers: [
						{type: "hasItem", name: "quest/14 inch purple dildo"},
						{type: Stat.Core, name: CoreStat.Perversion, condition: "gte", value: 70}
					],
					checks: [{tag: "ASSFUCK_CHECK", type: Stat.Skill, name: Skills.Sexual.AssFucking, difficulty: 100, value: 100}],
					post: [
						{type: "statXp", name: CoreStat.Perversion, value: 50},
						{type: "money", value: 100, factor: "ASSFUCK_CHECK"}
					],
					text: [
						"Before the performance you had arrainged a special treat with the help of one of the other dancers. \
            You nod subtly to the side of the stage and a young girl with giant tits steps up brandishing the \
            @@.item-quest;14 inch purple dildo@@ that once belonged to your girlfriend GF_NAME. The crowd \
            gasps in shock as they anticipate what they are about to witness, their surprise bringing an \
            amused smile to your face.\n\n\
            You kneel in front of the dancer as she holds the dildo out in front of her like it's her own cock. \
            With practiced movements you fellate the giant purple monster, coating it with your saliva. Once \
            satisified, you stand up and lean over, bracing yourself against the stripper pole. The dancer takes \
            this movement as it's intended and places the head of the giant phallus against your sphincter.\n\n\
            The crowd collectively holds it's breath as you pause for a moment, and then with force you start \
            pushing back against the fake cock.\
            ",
						{
							ASSFUCK_CHECK: 33,
							text: "\
                    You do your best to get the monster up your ass, but only manage to swallow about half of it. \
                    The dancer you enlisted gives you a few solid thrusts, each one eliciting a gasp and a small \
                    scream from you. It's not quite what you intended and the crowd @@doesn't look amused@@."
						},
						{
							ASSFUCK_CHECK: 66,
							text: "\
                   You struggle valiantly against the giant dong, wiggling your ass from side to side as you slowly \
                   swallow it up your backside. Within a minute the majority of it's 14 inches is buried deeply in \
                   your bowels and you relax, giving out a small sigh. It doesn't last long before the dancer behind \
                   you starts to move the dildo in and out of your rectum, each thrust causing you to elicit a small \
                   gasp and moan. The crowd is @@eating this up@@ and cheering while you get fucked on stage."
						},
						{
							ASSFUCK_CHECK: 500,
							text: "\
                   Like the whore you are, you take the giant dildo up your backside with one long fluid movement. \
                   You turn your head and smile at the girl holding the root of the monster and mouth the words \
                   'do it hard' to her. She nods back to you and immediately starts reaming your butthole with the \
                   fake cock. The feeling is intense and you give yourself over to your natural instincts, gasping and \
                   moaning as the beast fucks you hard. The crowd is eating the entire act up, particularly the fact \
                   that you're obviously enjoying it. Within minutes you're thrashing around as the cock mercilessly \
                   ravages your backside, culiminating in a massive sissygasm that sends cum flying across the stage and \
                   eliciting a @@massive cheer from the crowd@@!"
						},
						"Finally, your accomplice pulls the dildo out of your ass with a large 'plop' and you turn around to face the \
            crowd with a smile."
					]
				},
				{
					id: "Final Fixup",
					checks: [
						{
							tag: "PERFORMANCE_BONUS", type: "func",
							func: (_p, _s, c) => Math.mean(c["DANCE_CHECK_1"].mod, c["DANCE_CHECK_2"].mod,
								c["STRIPPING_CHECK_1"].mod, c["STRIPPING_CHECK_2"].mod),
							value: 1
						}
					],
					post: [
						{type: "corruptWillpower", name: "low", value: -20},
						{type: "statXp", name: CoreStat.Perversion, value: 50, factor: "PERFORMANCE_BONUS"},
						{type: "npcStat", name: NpcStat.Mood, value: 5, factor: "PERFORMANCE_BONUS"},
					],
				}

			],
			end: "\
     With your performance over, you take one last bow, your nBUST dangling for the crowd and blow them a kiss. \
     You scoop up the money that was thrown on the stage and exit behind the curtains, but not before \
     noticing that NPC_NAME was watching you dance… JOB_RESULTS\
     ",
			jobResults: [
				{
					PERFORMANCE_BONUS: 0.33,
					text: "they @@didn't look very impressed@@."
				},
				{
					PERFORMANCE_BONUS: 0.66,
					text: "they @@looked slightly impressed@@."
				},
				{
					PERFORMANCE_BONUS: 1,
					text: "they @@seemed to enjoy the show@@."
				},
				{
					PERFORMANCE_BONUS: 100,
					text: "they @@looked rather pleased with your performance@@."
				}
			]
		}
	});
}
