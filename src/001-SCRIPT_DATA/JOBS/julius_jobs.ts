namespace App.Data {
	Object.append(jobs, {
		QUAR01: {
			id: "QUAR01", title: "Helping down in the hold", giver: "Quartermaster", pay: 10,
			rating: 1, // of 5
			phases: [0, 1],
			days: 1,
			cost: [
				{type: "time", value: 2},
				{type: Stat.Core, name: CoreStat.Energy, value: 2}
			],
			intro:
				"NPC_NAME says s(If you're looking for a way to earn some extra coin, I usually need help cleaning up and taking stock down \
        here in the hold every morning. It's backbreaking work and pays like shit, but maybe it's better than sucking dick all day?)",
			start: "",
			scenes: [
				{
					id: "SCENE01",
					checks: [{tag: "A", type: Stat.Skill, name: Skills.Domestic.Cleaning, difficulty: 50, value: 10}],
					post: [
						{type: "money", value: 1, factor: "A"},
						{type: "statXp", name: CoreStat.Fitness, value: 50},
						{type: "npcStat", name: NpcStat.Mood, value: 10}
					],
					text: [
						"The cargo hold is vast, dark and dirty. It's a huge amount of tiring work, but thankfully you're so far out of the \
                way that most of the crew leaves you alone to get on with it. That's not to say that this lack of distraction makes \
                this an easy good, not at all. In fact, you find the work rather difficult and exhausting. The boxes are heavy and \
                dust is covering everything. Occasionally you are surprised by the presence of a small rodent or spider - stow aways \
                on the ship, much like you.\n\n",
						{
							A: 25, text:
								"The work is back breaking and grueling, but you manage to work for a couple of hours and get the majority of \
                    the task done. While keeping any semblance of order down here in the hold is probably impossible, for today at \
                    least @@you've done your part@@."
						},
						{
							A: 50, text:
								"The work is back breaking and grueling, but you manage to work for a couple of hours and get the majority of \
                    the task done. At the end of the shift, while not perfect, the area is @@much improved@@."
						},
						{
							A: 500, text:
								"The work is back breaking and grueling, but you manage to work for a couple of hours and get the majority of \
                    the task done. Taking a break to wipe the sweat from your eyes, you look upon your handy work with a @@sense of \
                    satisfaction@@."
						}
					]
				},
				{
					id: "SCENE02",
					triggers: [{type: "random100", value: 33, condition: "lte"}],
					post: [{type: Items.Category.Drugs, name: "medicinal herbs", value: 1}],
					text:
						"While cleaning, out of the corner of your eye you spot a small pouch of herbs. These must belong to NPC_NAME, \
                or he must have lost them. With a quick glance to make sure no one is looking, you snatch them up.",
				}
			],
			end: "NPC_NAME comes up to you and says, s(JOB_RESULTS) He then hands you some coins.",
			jobResults: [
				{A: 25, text: "Well, I guess that's @@better than nothing@@."},
				{A: 50, text: "Hmm… @@not too bad@@. You certainly know your way around a broom."},
				{A: 500, text: "That's a @@pretty good job@@ PLAYER_NAME. Come back again sometime for more work."}
			]
		},

		QUAR02: {
			id: "QUAR02", title: "Human Experimentation", giver: "Quartermaster", pay: 20,
			rating: 3, // of 5
			phases: [0, 1, 2],
			days: 3,
			cost: [
				{type: "time", value: 2},
				{type: Stat.Core, name: CoreStat.Energy, value: 3}
			],
			intro:
				"NPC_NAME says s(You know, I've got this new special 'substance' that I've been working on. The effects are currently \
        unknown. How about it? Are you willing to be my human guinea pig? It shouldn't be dangerous… or at least fatal… \
        hopefully. I can pay you a bit of coin for your time and who knows… I might discover something useful?)",
			start:
				"NPC_NAME takes you to a dark corner of the cargo hold, it's a small area set off to the side with ramshackle partitions \
        made of scavenged wood and cloth. He motions for you to take a seat on a long table, which you do and then he proceeds to \
        tinker with some alchemical equipment.\n\n\
        NPC_NAME says s(Now… I can't honestly say what will happen PLAYER_NAME, but it shouldn't be fatal.)\n\n\
        He coughs for a moment and mutters under his breath, s(Probably…)\n\n\
        Before you have time to reconsider if this is a wise idea or not, NPC_NAME approaches you with a vial filled to the brim \
        with a strange bubbling liquid. He holds it up to your nLIPS and says, s(Come now, PLAYER_NAME, just the tip here…)\n\n\
        It's not the first time you've heard that on this ship.\n\n\
        Hesitantly you swallow the liquid. It burns your throat as it goes down. Minutes pass by and you don't seem to notice any \
        immediate effects. Then, suddenly, your vision goes blurry. You try to inform NPC_NAME of this, but your words come out \
        sodden and slurry. The room is spinning and everything becomes incredibly colourful and loud.",
			scenes: [
				{
					id: "SCENE01",
					checks: [{tag: "A", type: Stat.Skill, name: Skills.Sexual.AssFucking, difficulty: 30, value: 10}],
					post: [
						{type: "statXp", name: CoreStat.Nutrition, value: -50},
						{type: Stat.Core, name: CoreStat.Toxicity, value: 20},
						{type: Stat.Core, name: CoreStat.Toxicity, value: 100, opt: "random"},
						{type: "statXp", name: CoreStat.Hormones, value: 100, opt: "random"},
						{type: "statXp", name: CoreStat.Hormones, value: 100},
						{type: "statXp", name: CoreStat.Willpower, value: -50, opt: "random"},
						{type: "bodyXp", name: BodyPart.Ass, value: 50},
						{type: "bodyXp", name: BodyPart.Ass, value: 100, opt: "random"},
						{type: "bodyXp", name: BodyPart.Hips, value: 50},
						{type: "bodyXp", name: BodyPart.Hips, value: 100, opt: "random"},
						{type: "npcStat", name: NpcStat.Mood, value: 10},
						{type: "npcStat", name: NpcStat.Lust, value: -10},
						{type: "counter", name: "CARGO_XP", value: 1},
						{type: "money", value: 10, factor: "A"}
					],
					text: [
						"NPC_NAME says, s(Ah, I see it's taken effect. Now to wait and see what happens…)\n\n\
            He looks at you for a moment and then seems to have an idea. He quickly comes over to you and rolls you over on \
            your stomach, your legs danging over the table and revealing your nASS. The last thing you hear before you lose \
            consciousness is the phrase, s(Well, at least this gives me something to do while I wait.)\
            \n...\n....\n.....\n......\n.......\n........\n",
						{A: 50, text: "You wake up hours later, still groggy and with a suspiciously slippery and @@sore ass@@."},
						{A: 500, text: "You wake up hours later, still groggy and with what you suspect is a @@thoroughly fucked ass@@"},
						"\n\nYou start collecting your thing (how did you clothes end up on the floor?) and you notice NPC_NAME is sitting at \
            a desk going over some papers. He turns around to face you…"
					]
				},
				{
					id: "SCENE02",
					triggers: [
						{type: "counter", name: "CARGO_XP", value: 10, condition: "gte", opt: "random"},
						{type: "flagSet", name: "CARGO_DISCOVER1", value: false}
					],
					post: [
						{type: "money", value: 50},
						{type: "money", value: 50, opt: "random"},
						{type: "jobFlag", name: "CARGO_DISCOVER1", value: 1},
						{type: "jobFlag", name: "CARGO_XP", value: 0},
						{type: "npcStat", name: NpcStat.Mood, value: 10},
						{type: "store", name: "cargo", value: "khafkir", opt: "unlock"}
					],
					text:
						"NPC_NAME says s(Ah, PLAYER_NAME, you're up! @@.state-positive;Well, I have good news@@ - it seems that the test results \
                are in and I've been able to re-create an old brew from these notes the Captain found. Thanks for your help… here \
                have some extra coin and if you're interested, @@.attention;a new drink should be available in my shop@@… it's a \
                liquor called \"Khafkir\" and it supposedly has some quite strong healing properties.)",
				},
				{
					id: "SCENE03",
					triggers: [
						{type: "counter", name: "CARGO_XP", value: 10, condition: "gte", opt: "random"},
						{type: "flagSet", name: "CARGO_DISCOVER1", value: true},
						{type: "flagSet", name: "CARGO_DISCOVER2", value: false}
					],
					post: [
						{type: "money", value: 50},
						{type: "money", value: 50, opt: "random"},
						{type: "jobFlag", name: "CARGO_DISCOVER2", value: 1},
						{type: "jobFlag", name: "CARGO_XP", value: 0},
						{type: "npcStat", name: NpcStat.Mood, value: 10},
						{type: "store", name: "cargo", value: "siren elixir", opt: "unlock"}
					],
					text:
						"NPC_NAME says s(Ah, PLAYER_NAME, you're up! @@.state-positive;Well, I have good news@@ - it seems that the test results \
                are in and I've been able to re-create an old brew from these notes the Captain found. Thanks for your help… here \
                have some extra coin and if you're interested, @@.attention;a new potion should be available in my shop@@… it's \
                an elixir called \"Siren's Tears\" and it supposedly has the power to instill tranquility in a person. Amazing!)",
				}
			],
			end:
				"NPC_NAME tosses a small bag of coins at you and then waves you on his way, continuing to furiously study at his notes.",
		}
	});
}
