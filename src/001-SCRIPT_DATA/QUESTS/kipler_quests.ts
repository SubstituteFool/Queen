namespace App.Data {
	Object.append(quests, {
		Q002: {
			id: "Q002", title: "Tools of the Trade - Part 1",
			giver: "FirstMate",
			checks: [{type: "npcStat", name: NpcStat.Mood, option: "Crew", value: 41, condition: "gte"}],
			reward: [
				{type: Items.Category.Cosmetics, name: "hair accessories", value: 40},
				{type: Items.Category.Cosmetics, name: "hair products", value: 40},
				{type: Items.Category.Cosmetics, name: "basic makeup", value: 60},
				{type: "slot"},
				{type: "itemChoice", name: "reel/rareWhore", value: 1},
				{type: "itemChoice", name: "reel/commonWildcard", value: 1}
			],
			intro: "\
        NPC_NAME says s(The crew on the Salty Mermaid are a ravenously horny lot, but even they have standards and frankly, \
        you don't really meet them. If you ever want to earn enough money to buy your freedom, then you'll need to focus on \
        improving your appearance. It's pretty simple really, the higher your @@.state-feminity;Beauty@@ and @@color:cyan;Style@@ \
        are, the more money you will earn. Even a sissy like you can at least wear sexy clothes and put on make-up to be more \
        attractive. If you go and improve the @@color:lime;Mood@@ of the crew, I'll give you some items that can help you earn \
        money.)",
			middle: "\
        NPC_NAME says s(Hey PLAYER_NAME… what are you doing wasting your time here when you should be on deck making the crew \
            happy? Come back to me when you've improved their @@color:lime;Mood@@ and then we'll talk.)",
			finish: "\
        NPC_NAME says s(I've heard that you've been out on deck whoring up a storm. Good, that's what you're supposed to do. You'll \
        find that the crew is more generous with their coin if you both look good and perform well, so here, have these, they'll help.)",
			journalEntry: "\
        You've been tasked by NPC_NAME to go forth and improve the @@color:lime;Mood@@ of the crew… obviously by letting them fuck \
        you. If you manage to complete this task, then NPC_NAME has promised to give you some items that will enhance your \
        @@.state-feminity;Beauty@@ and @@color:cyan;Style@@, making it easier for you to earn more coin.",
			journalComplete: "\
        It was difficult, but after hours spent humiliating and degrading yourself as the personal fuck-toy for the crew of the \
        @@.npc;Salty Mermaid@@, you managed to make them happy enough for NPC_NAME give you his reward. It was an ominous \
        first step into your life as a sissy prostitute."
		},

		Q003: {
			id: "Q003", title: "Tools of the Trade - Part 2",
			giver: "FirstMate",
			pre: [{type: "questFlag", name: "Q002", value: "COMPLETED", condition: "eq"}],
			checks: [{type: "npcStat", name: NpcStat.Mood, option: "Cook", value: 81, condition: "gte"}],
			reward: [
				{type: Items.Category.Food, name: "milkdew melon", value: 3},
				{type: Items.Category.Food, name: "butter gourd", value: 3},
				{type: "itemChoice", name: "clothes/silk bra", value: 1},
				{type: "itemChoice", name: "clothes/frilly bloomers", value: 1}
			],
			intro: "\
        NPC_NAME says s(Not bad slut, you did pretty good keeping the crew entertained but if you want to survive on this \
        ship then you'll need to also get the favor of the officers. Take @@.npc;Cookie@@ for instance. That fat tub \
        of lard controls the galley on this tub and if you get on his bad side, then you'll be eating gruel and cum for the \
        rest of your life. Go and get on his @@color:cyan;good side@@ and then come back.)",
			middle: "\
        NPC_NAME says s(Hey PLAYER_NAME… what are you doing wasting your time here when you should be getting on \
        @@.npc;Cookie's@@ good side? Come back to me when you've improved his @@color:magenta;Mood@@ and then we'll talk.)",
			finish: "\
        NPC_NAME says s(I've heard that @@.npc;Cookie's@@ taken a liking to you. That's good. You've probably noticed that \
        the more people like you, the more services they are willing to offer. That holds true for everyone really, if you think \
        about it. Anyway, now that he's willing to occasionally feed you better grub and not just his filthy cock, you'll have a \
        better chance of lasting on this ship. Just be careful, some food has… certain side effects. Why don't you experiment with \
        these and see?)",
			journalEntry: "\
        You've been tasked by NPC_NAME get on @@.npc;Cookie's@@ good side. You'll need to make sure he's \
        @@color:magenta;really cheerful@@.",
			journalComplete: "\
        It was difficult, but after days spent granting \"favors\" for Cookie in the galley, he's become quite fond of you. In the \
        end it paid off because not only did NPC_NAME give you a small reward, but now @@.npc;Cookie@@ will occasionally sell \
        you higher quality food."
		},

		Q004: {
			id: "Q004", title: "Tools of the Trade - Part 3",
			giver: "FirstMate",
			pre: [{type: "questFlag", name: "Q003", value: "COMPLETED"}],
			checks: [{type: "npcStat", name: NpcStat.Mood, option: "Quartermaster", value: 81, condition: "gte"}],
			reward: [
				{type: Items.Category.Drugs, name: "face cream", value: 3},
				{type: Items.Category.Drugs, name: "fairy dust", value: 3},
				{type: "itemChoice", name: "clothes/silk stockings", value: 1},
				{type: "itemChoice", name: "clothes/frilly dress", value: 1}
			],
			intro: "\
        NPC_NAME says s(By now you should know the score. I won't waste any time explaining it to you, go and get \
        @@.npc;Julius@@ on your side. He's a strange one to be sure. More interested in his experiments than anything \
        else… although, I wouldn't trust your asshole around him if you're passed out. You know what I mean.)",
			middle: "\
        NPC_NAME says s(Hey PLAYER_NAME… what are you doing wasting your time here when you should be getting on \
        @@color:magenta;Julius's@@ good side? Come back to me when you've improved his @@color:magenta;Mood@@ and then we'll talk.)",
			finish: "\
        NPC_NAME says s(I've heard that @@.npc;Julius's@@ taken a liking to you. Just like @@.npc;Cookie@@, getting on \
        his good side will make it so that he offers you more drugs for sale and at cheaper prices as well. You'll need those drugs \
        if you want to be able to optimize your earning potential. Here, have these samples and you'll see what I mean. Just be \
        careful, using too many drugs will increase your @@color:yellow;Toxicity@@, basically they're poison. Just as you'd expect, \
        if you're poisoned, not only will you not heal effectively, but you'll eventually take damage over time.)",
			journalEntry: "\
        You've been tasked by NPC_NAME get on @@.npc;Julius's@@ good side. You'll need to make sure he's \
        @@color:magenta;really cheerful@@.",
			journalComplete: "\
        It was difficult, but after days spent granting \"favors\" for Julius in the cargo hold, he's become quite fond of you. \
        In the end it paid off because not only did NPC_NAME give you a small reward, but now @@.npc;Julius@@ will occasionally \
        sell you higher quality drugs."
		},

		KIPLER_SPAR_QUEST: {
			id: "KIPLER_SPAR_QUEST",
			title: "Student of the Blade",
			giver: "FirstMate",
			pre: [{type: "questFlag", name: "EE_KiplerSpar_COUNT", value: 1}],
			checks: [{type: "questFlag", name: "KIPLER_DEFEATED_SUB_QUEST", value: "COMPLETED", altTitle: "Defeat Kipler in a Duel"}],
			reward: [{type: Items.Category.Weapon, name: "kiplers kutter", value: 1}],
			intro: "\
    NPC_NAME eyes you as you approach him. s(You were watching me spar on the deck? Did you like what you saw?), \
    he asks. You try not to roll your eyes at his lame pickup line and instead ask him if what you heard was true.\n\n\
    sp(Will you really give your sword to anyone who can beat you in a duel?)\n\n\
    NPC_NAME snorts.\
    s(Aye, tis true. But so far that's never happened… and it likely never will.)\n\n\
    You wonder if that's true or not.\
        ",
			middle: "\
    s(Still practicing are you, PLAYER_NAME?), says NPC_NAME.\n\n\
    You nod your head sheepishly, avoiding the knowing look on NPC_NAME's face. You both know that \
    you'll have to endure many, many more of his special \"lessons\" before you even come close to \
    defeating him.\
        ",
			finish: "\
    NPC_NAME grimaces and then lets out a defeated sigh.\n\n\
    s(I can't… I can't believe it.. beaten, and by a…) NPC_NAME looks off into the distance, it's \
    obvious that he's having trouble dealing with the fact that he was beaten by a sissy whore on his \
    own ship. The rammifications of this to his reputation will be large, but that's not your concern.\n\n\
    s(Here… take it), he says. s(Take it and get lost…)\n\n\
    Well, you had no idea he'd be such a spoiled loser, but at least you got a shiny new sword out of it.\
        ",
			journalEntry: "\
    If you can best NPC_NAME in a duel, he'll be honor bound to give you his rare magical sword.\
        ",
			journalComplete: "\
    You somehow managed to defeat NPC_NAME in a straight up duel and as a result he granted you \
    his magical sword. He was quite despondant over the fact that he was beaten by a mere whore and \
    it's certain that the crew won't let him live this down.\
        "
		}
	});
}
