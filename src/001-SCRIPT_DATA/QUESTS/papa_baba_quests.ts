namespace App.Data {
	Object.append(quests, {
		BOBOLA_SAP_2: {
			id: "BOBOLA_SAP_2", title: "Just What the Doctor Ordered",
			giver: "Papa Baba",
			pre: [{type: "questFlag", name: "BOBOLA_SAP_1", value: "ACTIVE", condition: "eq"}],
			checks: [
				{type: "questFlag", name: "BOBOLA_SAP_1", value: "ACTIVE", altTitle: "Be looking for bobola sap"},
				{type: "item", name: "food/smugglers ale", value: 6, condition: "gte"}
			],
			reward: [{type: "npcStat", name: NpcStat.Mood, value: 10}],
			intro: "\
        s(So ya be wanting da sap o' da Bobola tree?) Papa Baba says as he laughs to himself. \
        s(Surely ya must be some kind o' crazy slut, but neermind dat.)\n\n\
        He thinks for a moment, scratching his natty hair underneath his tattered hat.\n\n\
        s(Tell ya wot, gimme some o' dat fine smugglers beer and I be tellin ya wot you want ta knew!) he says.\n\n\
        You stare at him incredulously for a moment. If all this madman wants is alcohol, then isn't that \
        easy enough to get?\
        ",
			middle: "\
        NPC_NAME says, s(Ehhh… PLAYER_NAME. I be gettin thirsty o'er here! Gimme dat beer and make it quick!)\
        ",
			finish:
				"\
        NPC_NAME eyes light up as he sees you return with the bottles of ale. You can't say you blame him as \
        Petey's brew is renown in the isles for it's taste and invigourating qualities. He quickly snatches them \
        out of your hands, pops one open, downing it in a long fluid motion.\n\n\
        s(Aaaaah!!! Dat be hittin da spot!) he exclaims as he wipes his mouth. s(Normally this be da part where I be \
        makin ewe suck mah dick, but since you be visitin da Bobola, that'd be just mean.) he says, his expression \
        uncharacteristically somber. He reaches for a shelf in the shack and pulls down a wooden bucket, thrusting it \
        into your hands.\n\n\
        s(Take dis, try ta get some o dat dere sap innit. Ewe figgur out howe!)\n\n\
        You're starting to wonder if you've bitten off more than you can chew with this quest. In any regards, NPC_NAME \
        helpfully draws you a map in the dirt of the location of the Bobola tree. You can find it now from the \
        @@.location-name;Swamp River@@.\
        ",
			journalEntry: "\
        NPC_NAME will tell you where to find the rumoured Bobola tree, but first he wants you to bring him a brace of \
        six Smuggler's Ales.\
    ",
			journalComplete: "\
        You brought the ales to NPC_NAME and he drew you a map to the Bobola tree and handed you a bucket for collecting \
        the sap. Something about this seems wrong to you, but you should be able to find the tree now somewhere from the \
        @@.location-name;Swamp River@@.\
    "
		},

		// Part of Bertie's Queen's Favor Part 2
		BERTIE_QUEEN_PT2_INFO_PAPA_BABA: {
			id: "BERTIE_QUEEN_PT2_INFO_PAPA_BABA",
			title: "Chasing the Thieves",
			giver: "Papa Baba",
			pre: [
				{type: "questFlag", name: "BERTIE_QUEEN_PT2_DELIVERY_B", value: "COMPLETED", condition: "eq"},
				{type: "questFlag", name: "BERTIE_QUEEN_PT2_INFO", value: "ACTIVE", condition: "eq"},
			],
			post: [
				{type: "questFlag", name: "BERTIE_QUEEN_PT2_INFO", value: "COMPLETED"},
				{type: "questFlag", name: "BERTIE_QUEEN_PT2_INFO_PAOLA", value: undefined},
			],
			intro: "",
			middle: "",
			finish: "\
    You approach NPC_NAME, the mysterious jungle medicine man. He greets you somberly and and gestures for \
    you to take a seat. s(Wot ye be needin' girlie? Sumthin Baba ken do for ye?)\n\
    You clear your throat and lick your lips, then reach into your garments and produce the drawing that \
    <span class='npc'>Jonah Blythe</span> gave you and politely ask her if she recognizes it.\n\n\
    NPC_NAME's mouth twists in a grimace. He hands the picture back and sits down across from you on a simple \
    stool. s(Ye, I know et. I know et well.) he says. s(Ets a devil the white man worship. Dey be callin' et \
    'Mahomet') he stops and makes a sign over his heart, possibly some kind of protective charm.\n\n\
    Who is that? What does it mean? You have a dozen questions pop into your mind. However, all you can get \
    out of NPC_NAME is a single statement - s(I dun noe much, but dis - dat devil fills a man with madness \
    and lust and demands a price in blood. Unfaithful. Dat's what eet likes - unfaithful, disloyal, liars.)\n\n\
    You ponder this information for a moment, it's not much, but you at least have a name and some indication \
    of who might be behind it. It's probably enough to return to <span class='npc'>Bertie</span> with.\
     ",
			journalEntry: "HIDDEN",
			journalComplete: "\
     You asked NPC_NAME if he knew anything about the mysterious symbol. He told you a couple of key \
     pieces of information. Namely the symbol belongs to a cult that worships some type of demonic \
     figure named 'Mahomet' and that you should look for his followers amongst the 'unfaithful'.\
     ",
		}
	});
}
