namespace App.Data {
	stores.emily = {
		name: "Emily Ligget", open: [0, 1, 2, 3], restock: 7,
		inventory: [
			{category: Items.Rarity.Common, type: Items.Category.Food, qty: 4, max: 4, price: 1.0, mood: 0, lock: 0, tag: "bread and cheese"},
			{category: Items.Rarity.Common, type: Items.Category.Food, qty: 4, max: 4, price: 1.0, mood: 0, lock: 0, tag: "meat pie"},
			{category: Items.Rarity.Common, type: Items.Category.Food, qty: 2, max: 2, price: 1.0, mood: 0, lock: 0, tag: "butter gourd"},
			{category: Items.Rarity.Common, type: Items.Category.Food, qty: 2, max: 2, price: 1.0, mood: 0, lock: 0, tag: "starfruit"},
			{category: Items.Rarity.Common, type: Items.Category.Food, qty: 2, max: 2, price: 1.0, mood: 0, lock: 0, tag: "pink peach"},
			{category: Items.Rarity.Common, type: Items.Category.Food, qty: 2, max: 1, price: 1.0, mood: 0, lock: 0, tag: "milkdew melon"},
			{category: Items.Rarity.Rare, type: Items.Category.Food, qty: 1, max: 1, price: 1.0, mood: 0, lock: 0, tag: "butter gourd pie"},
			{category: Items.Rarity.Rare, type: Items.Category.Food, qty: 1, max: 1, price: 1.0, mood: 0, lock: 0, tag: "peaches and cream"}
		]
	};
}
