namespace App.Data {
	stores.fineas = {
		name: "Fineas Quincy", open: [0, 1, 2, 3], restock: 7,
		inventory: [
			{category: Items.Rarity.Common, type: Items.Category.Drugs, qty: 2, max: 2, price: 1.0, mood: 0, lock: 0, tag: "apBlackberry"},
			{category: Items.Rarity.Common, type: Items.Category.Drugs, qty: 2, max: 2, price: 1.0, mood: 0, lock: 0, tag: "apEscubac"},
			{category: Items.Rarity.Common, type: Items.Category.Drugs, qty: 2, max: 2, price: 1.0, mood: 0, lock: 0, tag: "apColdsfoot"},
			{category: Items.Rarity.Common, type: Items.Category.Drugs, qty: 2, max: 2, price: 1.0, mood: 0, lock: 0, tag: "apLongjack"},
			{category: Items.Rarity.Common, type: Items.Category.Drugs, qty: 2, max: 2, price: 1.0, mood: 0, lock: 0, tag: "apDamiana"},
			{category: Items.Rarity.Common, type: Items.Category.Drugs, qty: 2, max: 2, price: 1.0, mood: 0, lock: 0, tag: "apChrysanthemum"},
			{category: Items.Rarity.Rare, type: Items.Category.Drugs, qty: 1, max: 1, price: 1.0, mood: 0, lock: 0, tag: "apStarwort"},
			{category: Items.Rarity.Rare, type: Items.Category.Drugs, qty: 1, max: 1, price: 1.0, mood: 0, lock: 0, tag: "apFenugreek"},
			{category: Items.Rarity.Rare, type: Items.Category.Drugs, qty: 1, max: 1, price: 1.0, mood: 0, lock: 0, tag: "apSaffron"},
			{category: Items.Rarity.Rare, type: Items.Category.Drugs, qty: 1, max: 1, price: 1.0, mood: 0, lock: 0, tag: "apFireweed"}
		]
	};
}
