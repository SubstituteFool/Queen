namespace App.Data {
	stores.islastore = {
		name: "Isla Harbor General Store", open: [0, 1, 2, 3], restock: 7,
		inventory: [
			{category: Items.Rarity.Common, type: Items.Category.Cosmetics, qty: 5, max: 5, price: 1.0, mood: 0, lock: 0, tag: "basic makeup"},
			{category: Items.Rarity.Common, type: Items.Category.Cosmetics, qty: 5, max: 5, price: 1.0, mood: 0, lock: 0, tag: "hair products"},
			{category: Items.Rarity.Common, type: Items.Category.MiscConsumable, qty: 20, max: 20, price: 1.0, mood: 0, lock: 0, tag: "torch"},
			{category: Items.Rarity.Common, type: Items.Category.MiscConsumable, qty: 10, max: 10, price: 1.0, mood: 0, lock: 0, tag: "shovel"},
			{category: Items.Rarity.Common, type: Items.Category.Food, qty: 3, max: 3, price: 1.0, mood: 0, lock: 0, tag: "fortified wine"},
			{category: Items.Rarity.Common, type: Items.Category.Clothes, qty: 1, max: 1, price: 1.0, mood: 0, lock: 0, tag: "wig"},
			{category: Items.Rarity.Common, type: Items.Category.Clothes, qty: 1, max: 1, price: 1.0, mood: 0, lock: 0, tag: "sundress"},
			{category: Items.Rarity.Common, type: Items.Category.Clothes, qty: 1, max: 1, price: 1.0, mood: 0, lock: 0, tag: "striped bra"},
			{category: Items.Rarity.Common, type: Items.Category.Clothes, qty: 1, max: 1, price: 1.0, mood: 0, lock: 0, tag: "striped panties"},
			{category: Items.Rarity.Common, type: Items.Category.Clothes, qty: 1, max: 1, price: 1.0, mood: 0, lock: 0, tag: "striped stockings"},
			// RARE ITEMS                                                                                1.0
			{category: Items.Rarity.Rare, type: Items.Category.Clothes, qty: 1, max: 1, price: 1.0, mood: 0, lock: 0, tag: "frilly bloomers"},
			{category: Items.Rarity.Rare, type: Items.Category.Clothes, qty: 1, max: 1, price: 1.0, mood: 0, lock: 0, tag: "frilly dress"},
			{category: Items.Rarity.Rare, type: Items.Category.Clothes, qty: 1, max: 1, price: 1.0, mood: 0, lock: 0, tag: "pink mary janes"},
			{category: Items.Rarity.Rare, type: Items.Category.Clothes, qty: 1, max: 1, price: 1.0, mood: 0, lock: 0, tag: "pink platform mary janes"},
			{category: Items.Rarity.Rare, type: Items.Category.Clothes, qty: 1, max: 1, price: 1.0, mood: 0, lock: 0, tag: "pink head bow"},
			{category: Items.Rarity.Rare, type: Items.Category.Drugs, qty: 1, max: 1, price: 1.0, mood: 0, lock: 0, tag: "red hair dye"}
		]
	};
}
