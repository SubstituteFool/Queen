namespace App.Data {
	stores.lustyLass = {
		name: "The Lusty Lass", open: [0, 1, 2, 3, 4], restock: 7,
		// Simone's mood starts at 40.
		inventory: [
			// Food
			{category: Items.Rarity.Common, type: Items.Category.Food, qty: 8, max: 8, price: 1.0, mood: 0, lock: 0, tag: "beer"},
			{category: Items.Rarity.Common, type: Items.Category.Food, qty: 8, max: 8, price: 1.0, mood: 0, lock: 0, tag: "cheap wine"},
			{category: Items.Rarity.Common, type: Items.Category.Food, qty: 4, max: 4, price: 1.0, mood: 0, lock: 0, tag: "bread and cheese"},
			{category: Items.Rarity.Common, type: Items.Category.Food, qty: 4, max: 4, price: 1.0, mood: 0, lock: 0, tag: "roast fish"},
			{category: Items.Rarity.Common, type: Items.Category.Food, qty: 2, max: 2, price: 1.0, mood: 0, lock: 0, tag: "rum"},
			// Clothes COMMON/UNCOMMON -mood 40+
			{category: Items.Rarity.Common, type: Items.Category.Clothes, qty: 1, max: 1, price: 1.0, mood: 0, lock: 0, tag: "silk stockings"},
			{category: Items.Rarity.Common, type: Items.Category.Clothes, qty: 1, max: 1, price: 1.0, mood: 0, lock: 0, tag: "tall heels"},
			// Clothes UNCOMMON - Mood 50+
			{category: Items.Rarity.Common, type: Items.Category.Clothes, qty: 1, max: 1, price: 1.0, mood: 50, lock: 0, tag: "cute pink bra"},
			{category: Items.Rarity.Common, type: Items.Category.Clothes, qty: 1, max: 1, price: 1.0, mood: 60, lock: 0, tag: "cute pink panties"},
			// Clothes RARE - Mood 80+
			{category: Items.Rarity.Common, type: Items.Category.Clothes, qty: 1, max: 1, price: 1.0, mood: 80, lock: 0, tag: "pigtail wig"},
			// RARE ITEMS
			{category: Items.Rarity.Rare, type: Items.Category.Drugs, qty: 1, max: 1, price: 1.0, mood: 0, lock: 0, tag: "khafkir"},
			{category: Items.Rarity.Rare, type: Items.Category.Drugs, qty: 1, max: 1, price: 1.0, mood: 0, lock: 0, tag: "pixie dust"},
			{category: Items.Rarity.Rare, type: Items.Category.Drugs, qty: 1, max: 1, price: 1.0, mood: 0, lock: 0, tag: "blond hair dye"},
			{category: Items.Rarity.Rare, type: Items.Category.Food, qty: 1, max: 1, price: 1.0, mood: 0, lock: 0, tag: "butter gourd"},
			// RARE CLOTHES - Mood 90+
			{category: Items.Rarity.Common, type: Items.Category.Clothes, qty: 1, max: 1, price: 1.0, mood: 90, lock: 0, tag: "pink tartan"},
			{category: Items.Rarity.Common, type: Items.Category.Clothes, qty: 1, max: 1, price: 1.0, mood: 90, lock: 0, tag: "pink t-shirt"},
			// COMMON WHORE SPECIALTY REELS
			{category: Items.Rarity.Common, type: Items.Category.Reel, qty: 1, max: 1, price: 1.0, mood: 80, lock: 1, tag: "commonAnalAngel"},
			{category: Items.Rarity.Common, type: Items.Category.Reel, qty: 1, max: 1, price: 1.0, mood: 80, lock: 1, tag: "commonBoobjitsuMatriarch"},
			{category: Items.Rarity.Common, type: Items.Category.Reel, qty: 1, max: 1, price: 1.0, mood: 80, lock: 1, tag: "commonHandmaiden"},
			{category: Items.Rarity.Common, type: Items.Category.Reel, qty: 1, max: 1, price: 1.0, mood: 80, lock: 1, tag: "commonBreathMint"}
		]
	};
}
