namespace App.Data {
	stores.emi = {
		name: "Mamazon Village Trader",
		open: [0, 1, 2],
		restock: 7,
		maxRares: 4,
		inventory: [
			{category: Items.Rarity.Common, type: Items.Category.Food, tag: "milkdew melon", qty: 5, max: 5, price: 1.0, mood: 0, lock: 0},
			{category: Items.Rarity.Common, type: Items.Category.Drugs, tag: "apBaneberry", qty: 1, max: 2, price: 1.0, mood: 0, lock: 0},
			{category: Items.Rarity.Common, type: Items.Category.Drugs, tag: "apThorn", qty: 1, max: 1, price: 1.0, mood: 0, lock: 0},
			{category: Items.Rarity.Common, type: Items.Category.Drugs, tag: "apFenugreek", qty: 1, max: 1, price: 1.0, mood: 0, lock: 0},
			{category: Items.Rarity.Common, type: Items.Category.Drugs, tag: "apDamiana", qty: 1, max: 1, price: 1.0, mood: 0, lock: 0},
			{category: Items.Rarity.Common, type: Items.Category.Drugs, tag: "apStarwort", qty: 1, max: 1, price: 1.0, mood: 0, lock: 0},
		]
	};
}
