// Special store
namespace App.Data {
	export const relicStore = [
		// Common Face, Lips, Hair
		{
			category: "DRUGS", type: "potion", tag: "relic enhancer a", cost: [
				{
					type: "drugs/fairy dust",
					amount: 1
				},
				{
					type: "MISCLOOT/broken rune",
					amount: 3
				}
			]
		},
		{
			category: "DRUGS",
			type: "potion",
			tag: "relic enhancer b", // Bust, Perversion, Femininity, Seduction
			cost: [
				{
					type: "food/milkdew melon",
					amount: 1
				},
				{
					type: "miscLoot/broken rune",
					amount: 8
				}
			]
		},
		{
			category: "DRUGS",
			type: "potion",
			tag: "relic enhancer c", // Ass, Hips, Perversion, Seduction
			cost: [
				{
					type: "food/butter gourd",
					amount: 1
				},
				{
					type: "miscLoot/broken rune",
					amount: 8
				}
			]
		},
		{
			category: "DRUGS",
			type: "potion",
			tag: "relic enhancer d", // Fitness, Waist, Dancing
			cost: [
				{
					type: "food/cheap wine",
					amount: 1
				},
				{
					type: "miscLoot/broken rune",
					amount: 3
				}
			]
		},
		{
			category: "DRUGS",
			type: "potion",
			tag: "relic enhancer e", // Swashbuckling, Navigating, Sailing
			cost: [
				{
					type: "food/rum",
					amount: 1
				},
				{
					type: "miscLoot/broken rune",
					amount: 4
				},
				{
					type: "miscLoot/old arrowhead",
					amount: 2
				}
			]
		},
		{
			category: "DRUGS",
			type: "potion",
			tag: "relic enhancer f", // Seduction, Singing, Dancing, Styling
			cost: [
				{
					type: "food/orgeat",
					amount: 1
				},
				{
					type: "miscLoot/broken rune",
					amount: 4
				},
				{
					type: "miscLoot/old arrowhead",
					amount: 2
				}
			]
		},
		{
			category: "DRUGS",
			type: "potion",
			tag: "relic enhancer g", // Seduction, Singing, Dancing, Styling
			cost: [
				{
					type: "food/bumbo",
					amount: 1
				},
				{
					type: "miscLoot/broken rune",
					amount: 4
				},
				{
					type: "miscLoot/old arrowhead",
					amount: 2
				}
			]
		},
		{
			category: "DRUGS",
			type: "potion",
			tag: "relic enhancer h", // Penis, Balls, Perversion
			cost: [
				{
					type: "food/mighty banana",
					amount: 2
				},
				{
					type: "miscLoot/broken rune",
					amount: 4
				},
				{
					type: "miscLoot/old arrowhead",
					amount: 2
				},
				{
					type: "miscLoot/glowing crystal",
					amount: 1
				}
			]
		},
		{
			category: "DRUGS",
			type: "potion",
			tag: "relic enhancer i", // Fitness, Waist Up
			cost: [
				{
					type: "drugs/khafkir",
					amount: 2
				},
				{
					type: "miscLoot/broken rune",
					amount: 4
				},
				{
					type: "miscLoot/old arrowhead",
					amount: 2
				},
				{
					type: "miscLoot/glowing crystal",
					amount: 1
				}
			]
		},
		{
			category: "DRUGS",
			type: "potion",
			tag: "relic enhancer j", // TitFucking, AssFucking, BlowJobs, HandJobs
			cost: [
				{
					type: "drugs/siren elixir",
					amount: 1
				},
				{
					type: "miscLoot/broken rune",
					amount: 6
				},
				{
					type: "miscLoot/old arrowhead",
					amount: 4
				},
				{
					type: "miscLoot/glowing crystal",
					amount: 2
				},
				{
					type: "miscLoot/stone tablet",
					amount: 1
				}
			]
		},
	];
}
