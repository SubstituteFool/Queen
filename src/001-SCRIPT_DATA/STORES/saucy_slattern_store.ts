namespace App.Data {
	stores.saucySlattern = {
		name: "The Saucy Slattern", open: [0, 1, 2, 3, 4], restock: 7,
		inventory: [
			// Food
			{category: Items.Rarity.Common, type: Items.Category.Food, qty: 6, max: 6, price: 1.0, mood: 0, lock: 0, tag: "beer"},
			{category: Items.Rarity.Common, type: Items.Category.Food, qty: 6, max: 6, price: 1.0, mood: 0, lock: 0, tag: "cider"},
			{category: Items.Rarity.Common, type: Items.Category.Food, qty: 4, max: 4, price: 1.0, mood: 0, lock: 0, tag: "meat pie"},
			{category: Items.Rarity.Common, type: Items.Category.Food, qty: 4, max: 4, price: 1.0, mood: 0, lock: 0, tag: "roast fish"},
			{category: Items.Rarity.Common, type: Items.Category.Food, qty: 2, max: 2, price: 1.0, mood: 0, lock: 0, tag: "bumbo"},
			// Clothes COMMON/UNCOMMON - Mood 40+
			{category: Items.Rarity.Common, type: Items.Category.Clothes, qty: 1, max: 1, price: 1.0, mood: 0, lock: 0, tag: "fishnet tights"},
			{category: Items.Rarity.Common, type: Items.Category.Clothes, qty: 1, max: 1, price: 1.0, mood: 0, lock: 0, tag: "go-go boots"},
			// Clothes UNCOMMON - Mood 50+
			{category: Items.Rarity.Common, type: Items.Category.Clothes, qty: 1, max: 1, price: 1.0, mood: 50, lock: 0, tag: "gothic black bra"},
			{category: Items.Rarity.Common, type: Items.Category.Clothes, qty: 1, max: 1, price: 1.0, mood: 60, lock: 0, tag: "black panties"},
			// Clothes RARE - Mood 80+
			{category: Items.Rarity.Common, type: Items.Category.Clothes, qty: 1, max: 1, price: 1.0, mood: 80, lock: 0, tag: "cupless corset"},
			// RARE ITEMS
			{category: Items.Rarity.Rare, type: Items.Category.Drugs, qty: 1, max: 1, price: 1.0, mood: 0, lock: 0, tag: "lip balm"},
			{category: Items.Rarity.Rare, type: Items.Category.Drugs, qty: 1, max: 1, price: 1.0, mood: 0, lock: 0, tag: "pixie dust"},
			{category: Items.Rarity.Rare, type: Items.Category.Drugs, qty: 1, max: 1, price: 1.0, mood: 0, lock: 0, tag: "hair tonic"},
			{category: Items.Rarity.Rare, type: Items.Category.Food, qty: 1, max: 1, price: 1.0, mood: 0, lock: 0, tag: "milkdew melon"},
			// RARE CLOTHES - Mood 90+
			{category: Items.Rarity.Common, type: Items.Category.Clothes, qty: 1, max: 1, price: 1.0, mood: 90, lock: 0, tag: "sexy micro dress"},
			{category: Items.Rarity.Common, type: Items.Category.Clothes, qty: 1, max: 1, price: 1.0, mood: 90, lock: 0, tag: "exotic wig"},
			// COMMON WHORE SPECIALTY REELS
			{category: Items.Rarity.Common, type: Items.Category.Reel, qty: 1, max: 1, price: 1.0, mood: 80, lock: 1, tag: "commonAnalAngel"},
			{category: Items.Rarity.Common, type: Items.Category.Reel, qty: 1, max: 1, price: 1.0, mood: 80, lock: 1, tag: "commonBoobjitsuMatriarch"},
			{category: Items.Rarity.Common, type: Items.Category.Reel, qty: 1, max: 1, price: 1.0, mood: 80, lock: 1, tag: "commonHandmaiden"},
			{category: Items.Rarity.Common, type: Items.Category.Reel, qty: 1, max: 1, price: 1.0, mood: 80, lock: 1, tag: "commonBreathMint"}
		]
	};
}
