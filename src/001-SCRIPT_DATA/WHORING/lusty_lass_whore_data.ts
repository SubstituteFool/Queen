namespace App.Data {
	whoring.LustyLass = {
		// Shows in the UI as location.
		desc: "The Lusty Lass",
		// Sex act weighted table.
		wants: {hand: 1, ass: 4, bj: 4, tits: 3},
		// Minimum payout rank.
		minPay: 2,
		// Maximum payout rank.
		maxPay: 3,
		// Bonus payout, weighted table
		bonus: {
			body: {bust: 4, ass: 3, face: 2, lips: 2},
			style: {Bimbo: 2, PirateSlut: 1, SexyDancer: 3, SluttyLady: 2},
			stat: {[CoreStat.Perversion]: 3, [CoreStat.Femininity]: 2, beauty: 3}
		},
		names: Data.names.Male,
		title: null,
		rares: [],
		npcTag: "LustyLassCustomers"
	};
}
