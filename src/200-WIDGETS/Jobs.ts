Macro.add("JobsWidget", {
	handler() {
		const pl = setup.player;
		const je = setup.jobEngine;

		this.output.append(App.UI.Widgets.rJobList(pl, setup.world.npc(this.args[1]),
			je.getAvailableLocationJobs(pl, this.args[0]), je.getUnavailableLocationJobs(pl, this.args[0])));
	}
});
