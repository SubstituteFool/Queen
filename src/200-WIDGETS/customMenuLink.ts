Macro.add("customMenuLink", {
	handler() {
		const link = App.UI.appendNewElement('a', this.output, this.args[0]);
		if (!_.isUndefined(this.args[1])) {
			link.dataset.menuaction = this.args[1] as string;
		}
		link.dataset.target = _.isUndefined(this.args[2]) ? this.args[0] as string : this.args[2] as string;
		link.onclick = (ev) => {
			const tgt = ev.target as HTMLElement;
			const menuAction = tgt.dataset.menuaction;
			if (menuAction) {
				State.variables.menuAction = menuAction;
			}
			if (!tags().includes('custom-menu')) {
				State.variables.gameBookmark = passage();
			}
			const targetPassage = tgt.dataset.target;
			if (targetPassage) {
				Engine.play(targetPassage);
			}
		};
	}
});
