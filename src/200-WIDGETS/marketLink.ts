Macro.add("marketLink", {
	handler() {
		const se = App.StoreEngine;
		const npc = setup.world.npc(this.args[0] as string);
		if (se.hasStore(npc) && se.isOpen(npc) && setup.world.phase < App.Phase.LateNight) {
			if (setup.player.coreStats.energy < 1) {
				App.UI.appendFormattedText(this.output, {text: 'Shop - Too tired.', style: 'state-disabled'});
			} else {
				const link = App.UI.passageLink("Shop", "Shop", () => {
					const st = se.openStore(setup.player, npc);
					st.generateMarket();
					variables().menuAction = npc;
					setup.player.adjustStat(App.CoreStat.Energy, -1);
					setup.world.nextPhase(1);
					if (!tags().includes("custom-menu")) {
						variables().gameBookmark = passage();
					}
				});
				this.output.append(link);
				App.UI.appendFormattedText(this.output,
					' - [', {text: "Time 1", style: 'item-time'}, ' ', {text: "Energy 1", style: 'item-energy'}, ']'
				);
			}
		} else {
			App.UI.appendNewElement('span', this.output, 'Shop - closed', ['state-disabled']);
		}
	}
});
