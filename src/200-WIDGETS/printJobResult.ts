Macro.add("printJobResult", {
	handler() {
		$(this.output).wiki(App.UI.pJobResults(setup.player, temporary()['MPC'] as App.Entity.NPC, this.args[0] as App.Job))
	}
});
