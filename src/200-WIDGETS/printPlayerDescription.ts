Macro.add("printPlayerDescription", {
	skipArgs: true,
	handler() {
		const mirrorContainer = document.createElement('div');
		mirrorContainer.id = 'mirrorContainer';
		if (settings.displayAvatar) {
			const elementId = 'avatarUI';
			App.UI.appendNewElement('div', this.output).id = elementId;
			setup.avatar.drawCanvas(elementId, 800, 360);
		}

		const textContainer = settings.displayAvatar ? App.UI.appendNewElement('div', mirrorContainer) : mirrorContainer;
		const p = App.PR;
		const pl = setup.player;

		$(textContainer).wiki(`<p>You are ${p.pHeight(pl)} and ${p.pFitness(pl)}</p>\
		<p>You have a ${p.pFace(pl)} ${p.pHair(pl)} ${p.pEyes(pl)} You have ${p.pLips(pl)}</p>\
		<p>${p.pBust(pl)} ${pl.getStat("body", "bust") > 7 ? p.pBustFirmness(pl) : ""} ${pl.getStat("body", "lactation") > 0 ? p.pLactation(pl) : ""}</p>\
		<p>${p.pAss(pl)} ${p.pHips(pl)} ${p.pWaist(pl)} and ${p.pFigure(pl)}.</p>\
		<p>You have ${p.pPenis(pl)} and ${p.pBalls(pl)} between your legs. ${pl.isFuta ? p.pFutaStatus(pl) : ""}</p>\

		<p>You consider your natural beauty to be ${p.pBeauty(pl)}.\nThe fetish appeal of your body is ${p.pFetish(pl)}.</p>`);

		this.output.append(mirrorContainer);
	}
});
