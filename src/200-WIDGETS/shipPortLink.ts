Macro.add("shipPortLink", {
	handler() {
		const port = setup.player.getShipLocation();
		if (port.passage !== "") {
			const controlQuest = App.Quest.byTag("BOARDINGPASS");
			const res = new DocumentFragment();
			const jRes = $(res);
			if (controlQuest.isActive(setup.player)) {
				jRes.wiki(App.UI.pTravelLinkStrip([[port.title, null]], "Disembark"));
				App.UI.appendNewElement('span', res, "(LOCKED)", ['state-negative']);
			} else if (!controlQuest.isCompleted(setup.player)) {
				App.UI.appendNewElement('span', res, "Disembark", ['action-travel']);
				res.append(": ");
				const link = App.UI.appendNewElement('a', res, port.title, ['link-internal']);
				link.onclick = () => {
					App.Quest.setFlag(setup.player, "PRE_BOARDINGPASS", "COMPLETED");
					App.UI.replace('#BoardingPass', "As you approach the gangplank to leave the @@.npc;Salty Mermaid@@, two burly armed Pirates bar your way. Even with the magical geas on you, it appears it won't be possible for you to leave the ship without permission.")
				};
				App.UI.appendNewElement('div', res).id = 'BoardingPass';
			} else {
				jRes.wiki(App.UI.pTravelLinkStrip([[port.title, port.passage]], "Disembark"));
			}
			this.output.append(res);
		}
	}
});
