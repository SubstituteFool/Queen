Macro.add("tarot", {
	handler() {
		const pl = this.args[0] as App.Entity.Player;
		const card = App.JobEngine.getJobFlag<string>(pl, "TAROT_CARD");
		if (card && App.Data.tarot.hasOwnProperty(card)) {
			const dict = App.Data.tarot[card];
			$(this.output).wiki(dict.msg);
			pl.applyEffects(dict.effects);
			App.JobEngine.setJobFlag(pl, "TAROT_CARD", undefined);
		}
	}
});

Macro.add("readTarot", {
	handler() {
		const pl = this.args[0] as App.Entity.Player;
		const card = Object.keys(App.Data.tarot).randomElement();
		console.log(card);
		this.output.append(App.UI.makeElement('div', undefined, ['tcard', ...App.Data.tarot[card].css.split(' ')]));
		$(this.output).wiki(App.Data.tarot[card].chat);
		App.JobEngine.setJobFlag(pl, "TAROT_CARD", card);
	}
});
