namespace App.UI.Passages {
	export class Jobs extends App.UI.DOMPassage {
		constructor() {
			super("Jobs");
		}

		render(): DocumentFragment { // eslint-disable-line class-methods-use-this
			const res = document.createDocumentFragment();
			const player = setup.player;

			const npcName = State.variables.menuAction;
			if (typeof npcName !== "string") {
				throw "Expect $menuAction to be an NPC name";
			}

			if (settings.displayNPC) {
				const npcDiv = appendNewElement("div", res, undefined, ["npc-portrait-small"]);
				$(npcDiv).wiki(`<<DrawPortraitNPC "${npcName}">>`); // TODO replace with JS call
			}

			res.append(App.UI.Widgets.rJobList(player, Entity.World.instance.npc(npcName),
				setup.jobEngine.getAvailableJobs(player, npcName),
				setup.jobEngine.getUnavailableJobs(player, npcName)));

			appendNewElement("br", res);
			appendNewElement("br", res);
			appendNewElement("span", res, "Interact: ", ["action-interact"]);
			res.append(UI.passageLink("Exit", State.variables.gameBookmark));

			return res
		}
	}
}

new App.UI.Passages.Jobs();
