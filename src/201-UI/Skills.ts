namespace App.UI.Passages {
	class SkillsPassage extends App.UI.DOMPassage {
		constructor() {
			super("Skills");
		}

		// eslint-disable-next-line class-methods-use-this
		override render(): DocumentFragment {
			const res = new DocumentFragment();
			$(res).wiki(pInteractLinkStrip()); // for the "Exit" link
			const skillsTable = appendNewElement('table', res, undefined, ['score-table']);
			const body = skillsTable.appendNewElement('tbody');
			SkillsPassage._section(body, "Piracy Skills", Object.values(Skills.Piracy));
			SkillsPassage._section(body, "Charisma Skills", Object.values(Skills.Charisma),	[Skills.Charisma.Courtesan]);
			SkillsPassage._section(body, "Domestic Skills", Object.values(Skills.Domestic));
			SkillsPassage._section(body, "Sexual Skills", Object.values(Skills.Sexual), [Skills.Sexual.AssFu, Skills.Sexual.BoobJitsu]);

			appendFormattedText(res, {style: 'item-category-general', text: "Slot Reel Management"});
			const reelsInventory = appendNewElement('div', res, undefined, ['SlotInventoryUI']);
			reelsInventory.id = 'SlotInventoryUI';
			SlotEngine.instance.drawSlotInventory(setup.player);
			return res;
		}

		private static _section(body: HTMLTableSectionElement, caption: string, commonSkills: Skills.Any[], hiddenSkills: Skills.Any[] = []) {
			const captionRow = body.appendNewElement('tr');
			const captionCell = captionRow.appendNewElement('td');
			captionCell.appendNewElement('span', caption, ['item-category-general']);
			captionCell.colSpan = 6;
			captionCell.style.borderBottom = "1px solid";

			let counter = 0;
			let row: HTMLTableRowElement; // eslint-disable-line @typescript-eslint/init-declarations
			const skills = commonSkills.concat(hiddenSkills);
			for (const s of skills) {
				if (hiddenSkills.includes(s) && setup.world.pc.getStat(Stat.Skill, s) < 1) {
					continue;
				}

				if (counter % 3 === 0) {
					row = body.appendNewElement('tr');
				}
				++counter;
				row!.appendNewElement('td', `${PR.pSkillName(s)}:`);
				const sk = row!.appendNewElement('td', undefined, ['fixed-font']);
				$(sk).wiki(PR.pSkillMeter(s, setup.world.pc));
			}
		}
	}

	new SkillsPassage();
}
