import {DrawingExports} from "../draw/draw";
import {SideValue} from "../parts/part";
import {Clothing, ClothingPart, ClothingPartConstructor} from "./clothing";


export class CuirassPart extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


export class CuirassBreastPart extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


export class GreavePart extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


export class VambracePart extends ClothingPart {
	constructor(...data: object[]);

	armCoverage: number;

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


/**
 * Base Clothing classes
 */
export class Armor extends Clothing {
	constructor(...data: object[]);
	stroke(): string;
	fill(): string;
}

export class Cuirass extends Armor {
	constructor(...data: object[]);

	cleavageOpeness: number;
	cleavageCoverage: number;
	sideLoose: number;
	waistCoverage: number;
	curveCleavageX: number;
	curveCleavageY: number;
	legCoverage: number; //used only by DressBreastPart but important

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

export class Greaves extends Armor {
	constructor(...data: object[]);

	legCoverage: number;

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

export class Vambraces extends Armor {
	constructor(...data: object[]);

	armCoverage: number;

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}
