//import {setStrokeAndFill} from "../util/draw";
import {DrawPoint} from "drawpoint/dist-esm";
import {DrawingExports} from "../draw/draw";
import {SideValue} from "../parts/part";
import {Clothing, ClothingPart, ClothingPartConstructor} from "./clothing";


//basically calcDressBase but only goes to waist
export function calcTee(ex: DrawingExports): DrawPoint[];


export class TeePart extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

//basically Tee+SuperPanties
export class LeotardPart extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


export class HalterTopBreastPart extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


export class TubeTopBreastPart extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


export class TopChestPart extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

export class TopGroinPart extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

export function calcTopBody(ex: DrawingExports): {
	armpit: DrawPoint,
	lat: DrawPoint,
	waist: DrawPoint,
	hip: DrawPoint,
	out: DrawPoint,
	bottom: DrawPoint
};

export class BikiniTopBreastPart extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

/**
 * Base Clothing classes
 */

export class Top extends Clothing {
	constructor(...data: object[]);

	thickness: number;

	stroke(): string;
	fill(): string;
}


export class Tee extends Top {
	constructor(...data: object[]);
	cleavageOpeness: number;
	cleavageCoverage: number;
	sideLoose: number;
	waistCoverage: number;
	curveCleavageX: number;
	curveCleavageY: number;
	legCoverage: number; //used only by DressBreastPart but important

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

export class Leotard extends Top {
	constructor(...data: object[]);

	cleavageOpeness: number;
	cleavageCoverage: number;
	waistCoverage: number;
	curveCleavageX: number;
	curveCleavageY: number;
	armCoverage: number;
	sideLoose: number;
	genCoverage: number;
	curveBotX: number;
	curveBotY: number;
	legCoverage: number; //used only by DressBreastPart but important

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

export class HalterTop extends Top {
	constructor(...data: object[]);

	cleavageCoverage: number;
	outerNeckCoverage: number;
	innerNeckCoverage: number;
	curveCleavageX: number;
	curveCleavageY: number;
	waistCoverage: number;
	sideLoose: number;

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}


export class TubeTop extends Top {
	constructor(...data: object[]);

	chestCoverage: number;
	waistCoverage: number;
	sideLoose: number;

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}


export class TubeTopSleeves extends Top {
	constructor(...data: object[]);
	chestCoverage: number;
	waistCoverage: number;
	sideLoose: number;

	shoulderCoverage: number;
	armCoverage: number;
	armLoose: number;

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

export class BikiniTop extends Top {
	constructor(...data: object[]);

	radius: number;
	outerNeckCoverage: number;
	innerNeckCoverage: number;
	curveCleavageX: number;
	curveCleavageY: number;

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

export class Swimsuit extends Top {
	constructor(...data: object[]);
	cleavageCoverage: number;
	outerNeckCoverage: number
	innerNeckCoverage: number;
	curveCleavageX: number;
	curveCleavageY: number;
	waistCoverage: number;
	sideLoose: number;
	genCoverage: number;
	curveBotX: number;
	curveBotY: number;

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}
