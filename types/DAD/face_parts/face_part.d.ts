import {Player} from "../player/player";
import {DrawingExports} from "../draw/draw";
import {BasePart} from "../parts/part";

/**
 * Base class for parts that are on the face (head)
 */
export class FacePart extends BasePart{
	constructor(...data: object[]);

	stroke(ctx?: CanvasRenderingContext2D, ex?: DrawingExports): string;
	fill(ctx?: CanvasRenderingContext2D, ex?: DrawingExports): string;

    // how thick the stroke line should be
	getLineWidth(avatar: Player): number;
}
