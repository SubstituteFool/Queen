import {FacePart} from "./face_part";
import {DrawPoint} from "drawpoint/dist-esm";
import {DrawingExports} from "../draw/draw";
import {Player} from "../player/player";

declare class Pupil extends FacePart {
	constructor(...data: object[]);
}

export class PupilHuman extends Pupil {
	constructor(...data: object[]);

	stroke(): string;
	clipStroke(ex: DrawingExports): DrawPoint[];
	getLineWidth(avatar: Player): number;
	calcDrawPoints(ex: DrawingExports, mods: object): DrawPoint[];
}
