import {ShadingPart} from "../draw/shading_part";
import {BodyPart} from "./part";
import {DrawPoint} from "drawpoint/dist-esm";
import {DrawingExports} from "../draw/draw";

declare class LeftArmShading extends ShadingPart {
	constructor(...data: object[]);

	fill(): string;

	calcDrawPoints(ex: DrawingExports): DrawPoint[];
}


declare class LeftArmUnderShading extends ShadingPart {
	constructor(...data: object[]);

	calcDrawPoints(ex: DrawingExports): DrawPoint[];
}


declare class RightArmShading extends ShadingPart {
	constructor(...data: object[]);

	fill(): string;
	calcDrawPoints(ex: DrawingExports): DrawPoint[];
}


declare class RightArmUnderShading extends ShadingPart {
	constructor(...data: object[]);

	calcDrawPoints(ex: DrawingExports): DrawPoint[];
}


declare class Arm extends BodyPart {
	constructor(...data: object[]);

	clipStroke(): void;
	clipFill(): void;
}

export class ArmHuman extends Arm {
	constructor(...data: object[]);

	calcDrawPoints(ex: object, mods: object, calculate: boolean): DrawPoint[];
}
