import {DrawPoint} from "drawpoint/dist-esm";
import {DrawingExports} from "../draw/draw";
import {ShadingPart} from "../draw/shading_part";
import {Player} from "../player/player";
import {BodyPart} from "./part";

declare class Head extends BodyPart {
	constructor(...data: object[]);
}


declare class BrowShading extends ShadingPart {
	constructor(...data: object[]);

	calcDrawPoints(ex: DrawingExports): DrawPoint[];
}


declare class HeadHumanShading extends ShadingPart {
	constructor(...data: object[]);

	calcDrawPoints(ex: DrawingExports): DrawPoint[];
}


// head will export skull, ear.top, ear.bot, jaw, chin.out, chin.bot
export class HeadHuman extends Head {
	constructor(...data: object[]);

	getLineWidth(avatar: Player): number;
	calcDrawPoints(ex: DrawingExports, mods: object, calculate: boolean): DrawPoint[];
}
